﻿using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using MovieModel.Lists;
using System.ComponentModel;

namespace KnkScrapers.Interfaces
{
    interface EnricherItf
    {
        KnkChangesItf Results { get; }
        BackgroundWorker Worker { get; }

        Roots Roots { get; }
        Files Files { get; }
        Folders Folders { get; }
        Genres Genres { get; }
        Companies Companies { get; }
        Countries Countries { get; }
        Languages Languages { get; }
        Persons Persons { get; }

        bool InitializedLibs { get; }

        bool IsScanning { get; }
        bool IsSaving { get; }
        bool IsBusy { get; }

        void BestOnes(BackgroundWorker aWorker);
        void RatedOnes(BackgroundWorker aWorker);
        void ScrapFiles(BackgroundWorker aWorker);
        void ScanForNewFiles(BackgroundWorker aWorker);
        void ArrangeFiles(BackgroundWorker aWorker);

        void ScanFolders();

        void ReportProgress();
        void SaveChanges();

        void ResetLibraries();
        void ResetMissing();
        void ResetOldfashion();
        void ClearResults();

        KnkChangesItf InitMessages();

        void FillFileProperties(Folder aFolder, string aFile);
        bool IsExtension(string aFile);
        void FillFileEspecificProperties(File aFile);

        void CheckFile(File aFile);
    }
}
