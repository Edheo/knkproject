﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Interfaces;
using KnkScrapers.Classes;
using MovieModel.Entities;
using MovieModel.Enums;
using MovieModel.Lists;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkScrapers.BooksScraper
{
    public class BookEnricher : EnrichCollections
    {

        internal Books Books;

        public BookEnricher(KnkConnectionItf aCon):base(aCon, MediaTypeEnum.Books)
        {
        }

        public override void ResetLibraries()
        {
            Books = new Books(Connection) { Changes = Results };
            base.ResetLibraries();
        }

        public override void ScrapFiles(BackgroundWorker worker)
        {
            if (!IsBusy)
            {
                base.ScrapFiles(worker);
                IsScanning = true;
                ScrapFiles();
                IsScanning = false;
                SaveChanges();
            }
        }

        public override void BestOnes(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                base.BestOnes(aWorker);
            }
        }

        public override void RatedOnes(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                base.RatedOnes(aWorker);
            }
        }

        public override bool IsExtension(string aFile)
        {
            bool ret = false;
            string lVal = System.IO.Path.GetExtension(aFile).ToLower();
            switch (lVal)
            {
                case ".epub":
                    ret = true;
                    break;
                default:
                    break;
            }
            return ret;
        }

        void ScrapFiles()
        {
            Results.AddMessage("Scraping", "Process Started");
            int i = 0;
            foreach (var lFil in FilesMissingMedia.Items)
            {
                if (ScrapFile(lFil))
                {
                    i++;
                }
                if (i > 5) break;
            }

        }

        private bool ScrapFile(File aFile)
        {
            var lOrg = FindBook(aFile);
            if (lOrg != null)
                EnrichFile(lOrg, aFile);
            return lOrg != null;
        }

        private Book FindBook(File aFile)
        {
            if(string.IsNullOrEmpty( aFile.TitleSearch))
            {
                aFile.TitleSearch = aFile.Extender.TitleFromFilename();
                aFile.Update("Extract Title Search");
            }
            return Books.FindBook(null, aFile.TitleSearch, null, null);
        }

        bool EnrichFile(Book aBookDst, File aFomFile)
        {
            //var lDst = aBookDst;
            //if (lDst != null)
            //{
            //    FillFile(lDst, aFomFile);                                       //  File from library
            //    aFomFile.Scraped = 1;
            //    aFomFile.Update("Book found");
            //    aBookDst.ScrapedDate = null;
            //    return EnrichMovie(aMovieDst, aMovieId).Result;
            //}
            //else
            //{
            //    aFomFile.Scraped++;
            //    aFomFile.Update("No Tmdb movie found!!");
            //    return null;
            //}
            return false;
        }

        MediaFile FillFile(Book aBook, File aFile)
        {

            var lFile = (from f in Files.Items where f.ToString() == aFile.ToString() select f).FirstOrDefault();
            var lFound = (from fil in aBook.Files().Items
                          where fil.ToString().ToLower().Equals(aFile.ToString().ToLower())
                          select fil
                ).FirstOrDefault();
            var lReturn = lFound;
            if (lFound == null)
            {
                lReturn = aBook.Files().Create();
                lReturn.IdBook = aBook;
            }
            lReturn.IdFile = lFile;
            lReturn.Update("File assigned to Book");
            return lReturn;
        }

    }
}
