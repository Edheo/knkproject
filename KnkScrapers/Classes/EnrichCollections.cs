﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Interfaces;
using KnkScrapers.Interfaces;
using KnkScrapers.Utilities;
using MovieModel.Entities;
using MovieModel.Enums;
using MovieModel.Lists;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkScrapers.Classes
{
    public class EnrichCollections : EnricherItf
    {
        BackgroundWorker worker;
        KnkChangesItf results = new KnkChanges();

        public Roots Roots { get; }
        public Files Files { get; }
        public Folders Folders { get; }
        public Genres Genres { get; }
        public Companies Companies { get; }
        public Countries Countries { get; }
        public Languages Languages { get; }
        public Persons Persons { get; }

        internal FilesMissingMedia FilesMissingMedia;

        public bool IsScanning { get; internal set; }
        public bool IsSaving { get; internal set; }
        public bool IsBusy { get { return IsSaving || IsScanning; } }

        public BackgroundWorker Worker => worker;
        public KnkChangesItf Results => results;
        internal readonly KnkConnectionItf Connection;


        public EnrichCollections(KnkConnectionItf aCon, MediaTypeEnum aType)
        {
            Connection = aCon;
            Folders = new Folders(aCon) { Changes = Results };
            Roots = new Roots(aType);
            Files = new Files(aCon) { Changes = Results };
            Files.Criteria = new KnkCriteria<File, File>(Files);
            KnkCoreUtils.CreateInParameter(Roots, Files.Criteria, "IdRoot");
            Genres = new Genres(aCon);
            Companies = new Companies(aCon);
            Countries = new Countries(aCon);
            Languages = new Languages(aCon);
            Persons = new Persons(aCon);

            FilesMissingMedia = new FilesMissingMedia(Connection, aType);

            ResetLibraries();

        }

        public bool InitializedLibs { get; set; }

        public void ScanForNewFiles(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                worker = aWorker;
                ResetLibraries();
                ResetMissing();
                ResetOldfashion();
                ClearResults();
                IsScanning = true;
                Results.AddMessage("Scanning", "Process Started");
                ReportProgress();
                ScanFolders();
                IsScanning = false;
                SaveChanges();
            }
        }

        public virtual void ScrapFiles(BackgroundWorker aworker)
        {
            if (!IsBusy)
            {
                worker = aworker;
                ResetLibraries();
                ResetMissing();
                ResetOldfashion();
                ClearResults();
            }
        }

        public virtual void BestOnes(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                worker = aWorker;
                ResetLibraries();
                ResetMissing();
                ResetOldfashion();
                ClearResults();
            }
        }

        public virtual void RatedOnes(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                worker = aWorker;
                ResetLibraries();
                ResetMissing();
                ResetOldfashion();
                ClearResults();
            }
        }

        public virtual void ArrangeFiles(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                worker = aWorker;
                ResetLibraries();
                ResetMissing();
                ResetOldfashion();
                ClearResults();
            }
        }

        public virtual void ImportVideoStation(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                worker = aWorker;
                ResetLibraries();
                ResetMissing();
                ResetOldfashion();
                ClearResults();
            }
        }

        public void ReportProgress()
        {
            if (worker != null && worker.IsBusy) worker?.ReportProgress(0);
        }

        public virtual void SaveChanges()
        {
            if (!IsBusy)
            {
                IsSaving = true;

                Results.AddMessage("Saving Changes", "Folders");
                ReportProgress();
                Folders.SaveChanges();

                Results.AddMessage("Saving Changes", "Files");
                ReportProgress();
                Files.SaveChanges();

                IsSaving = false;
            }
        }

        public virtual void ResetLibraries()
        {
            Files.Refresh();
            Folders.Refresh();
            Genres.Refresh();
            Companies.Refresh();
            Countries.Refresh();
            Languages.Refresh();
            Persons.Refresh();
            InitMessages();
            InitializedLibs = true;
        }

        public virtual void ResetMissing()
        {
            FilesMissingMedia.Refresh();
        }

        public virtual void ResetOldfashion()
        {
        }

        public virtual KnkChangesItf InitMessages()
        {
            var itms = FilesMissingMedia.Items;
            Results.AddMessages((from itm in FilesMissingMedia.Items select new KnkChangeDescriptor(itm) as KnkChangeDescriptorItf).ToList());
            return Results;
        }

        public virtual void ClearResults()
        {
            Results.Clear();
        }

        public void ScanFolders()
        {
            foreach (var lFol in Roots.Items)
            {
                Results.AddMessage("Scanning Folders", $"Root {lFol.ToString()}");
                ReportProgress();
                ScanAFolder(lFol.Path, null);
            }
            Results.AddMessage("Scanning", "Process Finissed");
            ReportProgress();
        }

        private void ScanAFolder(string aFolder, Folder aParentFolder)
        {
            bool lAvailable;
            var lFol = Folders.Items.FirstOrDefault(e => e.Path.ToLower().Equals(aFolder.ToLower()));
            if (lFol != null)
            {
                if (aParentFolder == null)
                {
                    var FoldersToScan = (from fol in Folders.Items
                                         where fol.IdRoot.Value == lFol.IdPath.Value
                                         orderby fol.IdPath descending
                                         select fol).ToList();

                    foreach (var lFld in FoldersToScan)
                    {
                        if (!CheckFolder(lFld.Path))
                        {
                            CheckFiles(lFld);
                            lFld.Delete("Missing folder");
                            ReportProgress();
                        }
                    }
                }
                CheckFiles(lFol);
            }
            if (CheckFolder(aFolder, out lAvailable))
            {
                if (lAvailable)
                {
                    if (lFol == null && aParentFolder != null)
                    {
                        lFol = Folders.Create(aParentFolder,aFolder);
                        lFol.Update("Folder added to Library");
                        ReportProgress();
                    }
                    ScanFolderFiles(lFol);
                    var lDirs = System.IO.Directory.GetDirectories(aFolder);
                    {
                        foreach (var lFolder in lDirs)
                        {
                            ScanAFolder(lFolder + "/", lFol);
                        }
                    }
                }
                else
                {
                    if (lFol != null)
                    {
                        lFol.Delete("Folder not available");
                        ReportProgress();
                    }
                }
            }
            else
            {
                if (lFol != null)
                {
                    lFol.Delete("Folder check Fails");
                    ReportProgress();
                }
            }
        }

        bool CheckFolder(string aFolder)
        {
            bool lAvailable;
            return (KnkScrapersUtils.DirectoryExists(aFolder, out lAvailable));
        }

        bool CheckFolder(string aFolder, out bool aAvailable)
        {
            return (KnkScrapersUtils.DirectoryExists(aFolder, out aAvailable));
        }

        public void CheckFiles(Folder aFolder)
        {
            if (aFolder.IdRoot.Equals(aFolder))
            {
                var lFilestodel = (from fil in Files.Items
                                   where fil.IdRoot != null && fil.IdRoot.Equals(aFolder)
                                   select fil);
                if (aFolder.IdRoot.Reference.IsTemp)
                {
                    foreach (var lFile in lFilestodel)
                    {
                        if (!System.IO.File.Exists(lFile.ToString()))
                        {
                            lFile.Delete("Missing File");
                            ReportProgress();
                        }
                    }
                }
            }
            var lFiles = (from fil in Files.Items
                          where fil.IdPath.Equals(aFolder)
                          select fil);
            //int i = 1;
            foreach (var lFile in lFiles)
            {
                CheckFile(lFile);
            }

            Folders.SaveChanges();
            Files.SaveChanges();
        }

        public virtual void ScanFolderFiles(Folder aFolder)
        {
            //var lFiles = Files.Items.OrderByDescending(e => e.IdFile.GetInnerValue());
            foreach (var lFile in System.IO.Directory.GetFiles(aFolder.Path))
            {
                FillFileProperties(aFolder, lFile);
            }
        }

        public virtual void FillFileProperties(Folder aFolder, string aFile)
        {
            string lFileName = System.IO.Path.GetFileName(aFile);
            var lFil = Files.Items.Where(e => e.Filename.ToLower().Equals(lFileName.ToLower()) && e.IdPath.Equals(aFolder)).FirstOrDefault();
            if (lFil == null && IsExtension(lFileName))
            {
                lFil = Files.Create(aFolder, aFile);
                FillFileEspecificProperties(lFil);
                lFil.Update("New File Scanned");
                ReportProgress();
            }
        }
        
        public virtual bool IsExtension(string aFile)
        {
            return false;
        }

        public virtual void FillFileEspecificProperties(File aFile)
        {

        }

        public virtual void CheckFile(File aFile)
        {
            var lFile = aFile;
            if (!System.IO.File.Exists(lFile.ToString()))
            {
                lFile.Delete("Missing File");
                ReportProgress();
            }
            else
            {
                System.IO.FileInfo lInfo = new System.IO.FileInfo(lFile.ToString());
                int lSeconds = (int)Math.Abs((lInfo.LastWriteTime - lFile.Filedate).TotalSeconds);
                var lBui = new StringBuilder();
                if (aFile.IdFile == null) aFile.SaveChanges();
                var lDelimiter = ", ";
                if (lSeconds != 0)
                {
                    lBui.Append("File Date");
                    lFile.Filedate = lInfo.LastWriteTime;
                }
                if ((lFile.Filesize ?? 0) == 0)
                {
                    if (lBui.Length > 0) lBui.Append(lDelimiter);
                    lBui.Append("File Size");
                    lFile.Filesize = (int)(lInfo.Length / 1024);
                }
                if ((string.IsNullOrEmpty(lFile.Elink) || string.IsNullOrEmpty(lFile.TorrentLink)) && lFile.Extender.Exists() && !lFile.IsNew())
                {
                    if (lBui.Length > 0) lBui.Append(lDelimiter);
                    lBui.Append("Elink");
                    lFile.LinksBuild();
                }
                if (lBui.Length > 0)
                {
                    lBui.Append(" Changed");
                    lFile.Update(lBui.ToString());
                    ReportProgress();
                }
            }
        }
    }
}