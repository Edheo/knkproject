﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkScrapers.Tmdb
{
    public class TmdbToken
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("expires_at")]
        public string Expires { get; set; }

        [JsonProperty("request_token")]
        public string Token { get; set; }
    }
}
