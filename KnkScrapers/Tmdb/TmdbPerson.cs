﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace KnkScrapers.Tmdb
{
    public partial class TmdbPerson
    {
        [JsonProperty("birthday")]
        public string Birthday { get; set; }

        [JsonProperty("known_for_department")]
        public string KnownForDepartment { get; set; }

        [JsonProperty("deathday")]
        public string Deathday { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("also_known_as")]
        public string[] AlsoKnownAs { get; set; }

        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("biography")]
        public string Biography { get; set; }

        [JsonProperty("popularity")]
        public double Popularity { get; set; }

        [JsonProperty("place_of_birth")]
        public string PlaceOfBirth { get; set; }

        [JsonProperty("profile_path")]
        public string ProfilePath { get; set; }

        [JsonProperty("adult")]
        public bool Adult { get; set; }

        [JsonProperty("imdb_id")]
        public string ImdbId { get; set; }

        [JsonProperty("homepage")]
        public object Homepage { get; set; }

        private TmdbPersonExternals _external;
        public TmdbPersonExternals External()
        {
            if (_external == null) _external = MoviesScraper.ScrapperClient.GetPersonExternal(this.Id);
            return _external;
        }

        private TmdbPersonImages _images;
        public TmdbPersonImages Images()
        {
            if (_images == null) _images = MoviesScraper.ScrapperClient.GetPersonImages(this.Id);
            return _images;
        }
    }
}
