﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace KnkScrapers.Tmdb
{
    class TmdbConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };

        //public static readonly JsonSerializerSettings SettingsUtc = new JsonSerializerSettings
        //{
        //    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        //    //DateParseHandling = DateParseHandling.DateTime,
        //    //DateFormatString = "'yyyy'-'MM'-'dd' 'HH':'mm':'ss' UTC", 
        //    //Converters = {
        //    //    new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
        //    //},
        //};

        public static TmdbType FromJsonToClass<TmdbType>(string json)
            where TmdbType : class
        {
            //if (utcdates) SettingsUtc.DateFormatString = "'yyyy'-'MM'-'dd' 'HH':'mm':'ss' UTC";
            if (!string.IsNullOrEmpty(json))
                return JsonConvert.DeserializeObject<TmdbType>(json, Settings);
            else
                return null;
        }

    }
}
