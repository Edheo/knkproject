﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace KnkScrapers.Tmdb
{

    public partial class TmdbMoviesPage
    {
        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("results")]
        public TmdbMovie[] Movies { get; set; }

        [JsonProperty("total_results")]
        public long TotalResults { get; set; }

        [JsonProperty("total_pages")]
        public long TotalPages { get; set; }
    }
}
