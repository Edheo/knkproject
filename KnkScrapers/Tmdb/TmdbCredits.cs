﻿using Newtonsoft.Json;

namespace KnkScrapers.Tmdb
{
    public partial class TmdbCredits
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("cast")]
        public TmdbCast[] Cast { get; set; }

        [JsonProperty("crew")]
        public TmdbCrew[] Crew { get; set; }
    }


    public partial class TmdbCredit
    {
        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("credit_id")]
        public string CreditId { get; set; }

        //[JsonProperty("order")]
        //public long Order { get; set; }

        [JsonProperty("profile_path")]
        public string ProfilePath { get; set; }

        [JsonProperty("department")]
        public virtual string Department { get; set; }

        [JsonProperty("job")]
        public virtual string Job { get; set; }

    }

    public partial class TmdbCast : TmdbCredit
    {
        [JsonProperty("cast_id")]
        public long CastId { get; set; }

        [JsonProperty("character")]
        public string Character { get; set; }

        [JsonProperty("order")]
        public long Order { get; set; }

        public override string Job { get => "Actor"; set => base.Job = value; }

        public override string Department { get => "Casting"; set => base.Department = value; }
    }

    public partial class TmdbCrew : TmdbCredit
    {
    }

}
