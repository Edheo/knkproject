﻿using Newtonsoft.Json;

namespace KnkScrapers.Tmdb
{
    public partial class TmdbMovieImages
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("backdrops")]
        public TmdbBackdrop[] Backdrops { get; set; }

        [JsonProperty("posters")]
        public TmdbBackdrop[] Posters { get; set; }
    }

    public partial class TmdbBackdrop
    {
        [JsonProperty("aspect_ratio")]
        public double AspectRatio { get; set; }

        [JsonProperty("file_path")]
        public string FilePath { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("iso_639_1")]
        public string Iso639_1 { get; set; }

        [JsonProperty("vote_average")]
        public long VoteAverage { get; set; }

        [JsonProperty("vote_count")]
        public long VoteCount { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }
    }
}
