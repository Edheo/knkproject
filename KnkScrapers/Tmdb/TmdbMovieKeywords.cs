﻿using Newtonsoft.Json;

namespace KnkScrapers.Tmdb
{
    public partial class TmdbMovieKeywords
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("keywords")]
        public TmdbMovieKeyword[] Keywords { get; set; }
    }

    public partial class TmdbMovieKeyword
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
