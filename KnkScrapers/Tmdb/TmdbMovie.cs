﻿using System;
using Newtonsoft.Json;

namespace KnkScrapers.Tmdb
{
    public partial class TmdbMovie
    {
        [JsonProperty("adult")]
        public bool Adult { get; set; }

        [JsonProperty("backdrop_path")]
        public string BackdropPath { get; set; }

        [JsonProperty("belongs_to_collection")]
        public TmdbBelongsToCollection BelongsToCollection { get; set; }

        [JsonProperty("budget")]
        public long Budget { get; set; }

        [JsonProperty("genres")]
        public TmdbGenre[] Genres { get; set; }

        [JsonProperty("homepage")]
        public string Homepage { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("imdb_id")]
        public string ImdbId { get; set; }

        [JsonProperty("original_language")]
        public string OriginalLanguage { get; set; }

        [JsonProperty("original_title")]
        public string OriginalTitle { get; set; }

        [JsonProperty("overview")]
        public string Overview { get; set; }

        [JsonProperty("popularity")]
        public decimal Popularity { get; set; }

        [JsonProperty("poster_path")]
        public string PosterPath { get; set; }

        [JsonProperty("production_companies")]
        public TmdbProductionCompany[] ProductionCompanies { get; set; }

        [JsonProperty("production_countries")]
        public TmdbProductionCountry[] ProductionCountries { get; set; }

        [JsonProperty("release_date")]
        public DateTime? ReleaseDate { get; set; }

        [JsonProperty("revenue")]
        public long Revenue { get; set; }

        [JsonProperty("runtime")]
        public long? Runtime { get; set; }

        [JsonProperty("spoken_languages")]
        public TmdbSpokenLanguage[] SpokenLanguages { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("tagline")]
        public string Tagline { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("video")]
        public bool Video { get; set; }

        [JsonProperty("vote_average")]
        public decimal VoteAverage { get; set; }

        [JsonProperty("vote_count")]
        public int VoteCount { get; set; }

        private TmdbCredits _credits;
        public TmdbCredits Credits()
        {
            if(_credits==null) _credits = MoviesScraper.ScrapperClient.GetCreditsData(this.Id);
            return _credits;
        }

        private TmdbMovieImages _images;
        public TmdbMovieImages Images()
        {
            if (_images == null) _images = MoviesScraper.ScrapperClient.GetMovieImages(this.Id);
            return _images;
        }

        private TmdbMovieVideos _videos;
        public TmdbMovieVideos Videos()
        {
            if (_videos == null) _videos = MoviesScraper.ScrapperClient.GetMovieVideos(this.Id);
            return _videos;
        }

        private TmdbMovieKeywords _keywords;
        public TmdbMovieKeywords Keywords()
        {
            if (_keywords == null || _keywords.Keywords == null) _keywords = MoviesScraper.ScrapperClient.GetMovieKeywords(this.Id);
            return _keywords;
        }
    }

    public partial class TmdbBelongsToCollection
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("poster_path")]
        public string PosterPath { get; set; }

        [JsonProperty("backdrop_path")]
        public string BackdropPath { get; set; }

        private TmdbCollection _col;

        public TmdbPart[] Parts()
        {
            if (_col == null) _col = MoviesScraper.ScrapperClient.GetCollectionData(this.Id);
            return _col?.Parts;
        }
    }

    public partial class TmdbGenre
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class TmdbProductionCompany
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("logo_path")]
        public string LogoPath { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("origin_country")]
        public string OriginCountry { get; set; }

        private TmdbCompany _company;

        public TmdbCompany Company()
        {
            if (_company == null) _company = MoviesScraper.ScrapperClient.GetCompanyData(this.Id);
            return _company;
        }
    }

    public partial class TmdbProductionCountry
    {
        [JsonProperty("iso_3166_1")]
        public string Iso3166_1 { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class TmdbSpokenLanguage
    {
        [JsonProperty("iso_639_1")]
        public string Iso639_1 { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

}
