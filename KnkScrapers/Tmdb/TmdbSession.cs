﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkScrapers.Tmdb
{
    public class TmdbSession
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("expires_at")]
        public string Expires { get; set; }

        [JsonProperty("guest_session_id")]
        public string SessionId { get; set; }
    }
}
