﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace KnkScrapers.Tmdb
{
    public partial class TmdbPersonImages
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("profiles")]
        public TmdbProfile[] Profiles { get; set; }
    }

    public partial class TmdbProfile
    {
        [JsonProperty("aspect_ratio")]
        public double AspectRatio { get; set; }

        [JsonProperty("file_path")]
        public string FilePath { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("iso_639_1")]
        public string Iso639_1 { get; set; }

        [JsonProperty("vote_average")]
        public double VoteAverage { get; set; }

        [JsonProperty("vote_count")]
        public long VoteCount { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }
    }
}
