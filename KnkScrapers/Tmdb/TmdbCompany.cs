﻿using Newtonsoft.Json;
using System;

namespace KnkScrapers.Tmdb
{

    public partial class TmdbCompany
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("headquarters")]
        public string Headquarters { get; set; }

        [JsonProperty("homepage")]
        public Uri Homepage { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("logo_path")]
        public string LogoPath { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("origin_country")]
        public string OriginCountry { get; set; }

        [JsonProperty("parent_company")]
        public TmdbCompany ParentCompany { get; set; }
    }
}
