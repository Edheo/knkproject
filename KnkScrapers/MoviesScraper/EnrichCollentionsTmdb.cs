﻿using KnkScrapers.Utilities;
using MovieModel.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KnkScrapers.Classes
{
    public partial class EnrichCollections
    {
        public List<Movie> FindMovie(MovieModel.Entities.File aFile, bool aCompleteList)
        {
            KnkSolutionMoviesUtils.EnrichTitleYear(aFile);
            var lTitleNoYear = aFile.TitleSearch;
            var lYears = aFile.YearSearch;

            List<Titleyear> lItems = new List<Titleyear>();
            if (lYears.Split(';').Length > 0)
            {
                foreach (string lYear in lYears.Split(';'))
                    lItems.Add(new Titleyear() { Title = lTitleNoYear, Year = KnkSolutionMoviesUtils.ToNullableInt32(lYear) });
            }
            else
            {
                lItems.Add(new Titleyear() { Title = lTitleNoYear, Year = null });
            }
            return FindMovie(lItems, aCompleteList);
        }

        public List<Movie> FindMovie(string aTitle, int? aYear, bool aCompleteList)
        {
            var lLst = new List<Titleyear>();
            lLst.Add(new Titleyear() { Title = aTitle, Year = aYear });
            return FindMovie(lLst, aCompleteList);
        }

        public List<Movie> FindMovie(List<Titleyear> aItems, bool aCompleteList)
        {
            List<Movie> lLst = new List<Movie>();
            try
            {
                foreach (var lItm in aItems)
                {
                    Results.AddMessage("Finding", $"{lItm.Title} {lItm.Year}");
                    _Worker?.ReportProgress(0);
                    var lTaskSearch = Task.Factory.StartNew(() => Client.SearchMovie(lItm));
                    lTaskSearch.Wait();
                    var movies = lTaskSearch.Result.Result;
                    var lAux = (from mov in movies.Results orderby KnkScrapersUtils.LevenshteinDistance(lItm.Title, mov.OriginalTitle) select mov);
                    foreach (Movie lMovie in lAux)
                    {
                        Results.AddMessage("Found", $"{lItm.Title} {lItm.Year}");
                        _Worker?.ReportProgress(0);
                        var lTask1 = Task.Factory.StartNew(() => Client.GetMovieData(lMovie.Id));
                        lTask1.Wait();
                        lLst.Add(lTask1.Result.Result.Movie);
                        if (!aCompleteList) break;
                    }
                }

                if (lLst.Count == 0)
                {
                    foreach (var lItm in aItems)
                    {
                        var lTaskSearch = Task.Factory.StartNew(() => Client.SearchMovie(lItm));
                        lTaskSearch.Wait();
                        var movies = lTaskSearch.Result.Result;
                        var lAux = (from mov in movies.Results orderby KnkScrapersUtils.LevenshteinDistance(lItm.Title, mov.Title) select mov);
                        foreach (Movie lMovie in lAux)
                        {
                            var lTaskGetData = Task.Factory.StartNew(() => Client.GetMovieData(lMovie.Id));
                            lTaskGetData.Wait();
                            lLst.Add(lTaskGetData.Result.Result.Movie);
                            if (!aCompleteList) break;
                        }
                    }
                }
            }
            catch (Exception lExc)
            {
                this.Results.AddMessage("Tmdb Scraper", "FindMovies", lExc.Message);
                _Worker?.ReportProgress(0);
            }
            return lLst;
        }

        public async Task<TmdbMovie> FindMovie(int aIdMovie, MovieModel.Entities.File aFile, BackgroundWorker aWorker)
        {
            TmdbMovie lRet = null;
            try
            {
                //var lTask1 = Task.Factory.StartNew(() => Client.GetMovieData(aIdMovie));
                //lTask1.Wait();
                //lRet = lTask1.Result.Result;
                lRet = await Client.GetMovieData(aIdMovie);
                var lMovie = aFile.MovieFiles().Items.FirstOrDefault().IdMovie.Reference;
                if(lMovie!=null && !lMovie.IsNew())
                {
                    lMovie = (from lMov in Movies.Items where lMov.IdMovie.Value == lMovie.IdMovie.Value select lMov).FirstOrDefault();
                }

                if (lMovie == null)
                {
                    lMovie = Movies.FindMovie(lRet.Movie.Id, lRet.Movie.Title, lRet.Movie.OriginalTitle, lRet.Movie?.ReleaseDate?.Year);
                }
                EnrichFile(lRet.Movie.Id, lMovie, aFile);
                SaveChanges(aWorker);
            }
            catch (Exception lExc)
            {
                this.Results.AddMessage("Tmdb Scraper", "FindMovies", lExc.Message);
                _Worker?.ReportProgress(0);
            }
            return lRet;
        }

        public async Task<List<Movie>> GetRelatedMoviesTask(MovieModel.Entities.Movie aMovie)
        {
            return await Task.FromResult(GetRelatedMovies(aMovie));
        }

        public List<Movie> GetRelatedMovies(MovieModel.Entities.Movie aMovie)
        {
            var lLst = new List<Movie>();
            if (aMovie.TmdbId > 0)
            {
                var lSimilar = Client.GetRelatedMovies(aMovie.TmdbId ?? 0).Result.Results;

                foreach (var lMov in lSimilar)
                {
                    lLst.Add(lMov);
                }

                lSimilar = Client.GetRelatedMovies(aMovie.TmdbId ?? 0, 2).Result.Results;
                foreach (var lMov in lSimilar)
                {
                    lLst.Add(lMov);
                }
                var lGenres = aMovie.Genres().Items.Select(g => g.Genre.GenreName.ToLower());
                var lDirectors = aMovie.Casting().Items.Where(c => c.IdCastingType.Reference.Department == "Directing" && c.IdPerson.Reference.TmdbId != null).Select(c => c.IdPerson.Reference);
                foreach(var lDir in lDirectors)
                {
                    var lMovs = Client.GetPersonMovies(int.Parse(lDir.TmdbId)).Result;
                    foreach(var lCastMov in lMovs)
                    {
                        var lMovie = Client.SearchMovie(lCastMov.Id).Result;
                        if (lMovie.Genres.Any(g => lGenres.Contains(g.Name.ToLower())))
                            lLst.Add(lMovie);
                    }
                }

                if (aMovie != null && lLst.Count() > 0)
                {
                    foreach (var lItem in lLst)
                    {
                        if (lItem.VoteCount > 50 && lItem.VoteAverage > 6)
                        {
                            var lMov = EnrichMovie(new TmdbMovie(lItem), aMovie.Files().Count() > 0).Result;
                            if (lMov!=null && (lMov.IsNew() || !aMovie.Extender.AlreadyExistsRelated(aMovie, lMov)))
                            {
                                var lMor = aMovie.Relateds().Create();
                                lMor.IdMovie = aMovie.IdSet == null ? aMovie : null;
                                lMor.IdSet = aMovie.IdSet?.Reference;
                                lMor.IdMovieRelated = lMov.IdSet == null ? lMov : null;
                                lMor.IdSetRelated = lMov.IdSet?.Reference;
                                lMor.Update("Scrap Relates Movie");
                            }
                        }
                    }
                    aMovie.Update("Related Movies");
                }
            }
            return lLst;
        }

        public async Task<List<TmdbMovie>> GetBestMovies()
        {
            var lLst = new List<TmdbMovie>();
            int lPage = 1;
            var lBest = await Client.GetTopRated();
            while (lLst.Count < 5)
            {
                Results.AddMessage("Best Movies", $"Page {lPage}/30");
                _Worker?.ReportProgress(0);
                foreach (var lMov in lBest.Results)
                {
                    var lMovie = Movies.FindMovie(lMov.Id, lMov.Title, lMov.OriginalTitle, lMov?.ReleaseDate?.Year);
                    if (lMovie.IsNew() && (lMovie.TmdbId??0)!=0)
                    {
                        Results.AddMessage("Best Movies", $"Found {lMov.Title}");
                        _Worker?.ReportProgress(0);
                        lLst.Add(await Client.GetMovieData(lMov.Id));
                        if (lLst.Count >= 5) break;
                    }
                }
                if (lLst.Count < 5 && lPage < 30)
                    lBest = await Client.GetTopRated(++lPage);
                if (lBest.Results.Count() == 0 || lPage >= 30)
                    break;
            }
            Results.AddMessage("Bringing Movies", $"Found {lLst.Count}/5 Movies");
            _Worker?.ReportProgress(0);
            return lLst;
        }

        public struct Titleyear
        {
            public string Title;
            public int? Year;
        }

    }
}
