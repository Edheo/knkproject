﻿using KnkCore.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkScrapers.MoviesScraper
{
    public static class ParametersTmdb
    {
        static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        static ConfigurationSectionGroup applicationSettingSectionGroup = config.SectionGroups["applicationSettings"];
        static ClientSettingsSection settings = applicationSettingSectionGroup.Sections["Movies.Properties.Settings"] as ClientSettingsSection;

        static string _apikey;
        static string _lang;

        public static string Pwd = "knk*7123";

        public static string TmdbApikey
        {
            get
            {
                if (string.IsNullOrEmpty(_apikey)) _apikey = GetSetting("TmdbApikey");
                return _apikey;
            }
        }

        public static string Language
        {
            get
            {
                if (string.IsNullOrEmpty(_lang)) _lang = GetSetting("Language");
                return _lang;
            }
        }


        private static string SetSetting(string aSetting, string aValue)
        {
            SettingElement element = settings.Settings.Get(aSetting);
            if (element != null)
            {
                settings.Settings.Remove(element);
                element.Value.ValueXml.InnerXml = aValue;
                settings.Settings.Add(element);
            }
            else
            {
                element = new SettingElement(aSetting, SettingsSerializeAs.String);
                element.Value = new SettingValueElement();
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                element.Value.ValueXml = doc.CreateElement("value");

                element.Value.ValueXml.InnerXml = aValue;
                settings.Settings.Add(element);
            }
            config.Save();

            ConfigurationManager.RefreshSection("Movies.Properties.Settings");
            return aValue;
        }

        private static string GetSetting(string aSetting)
        {
            SettingElement element = settings.Settings.Get(aSetting);

            if (element == null)
            {
                return SetSetting(aSetting, string.Empty);
            }
            return element.Value.ValueXml.InnerXml;
        }
    }
}
