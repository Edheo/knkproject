﻿using KnkCore;
using KnkInterfaces.Interfaces;
using KnkScrapers.Classes;
using MovieModel.Criterias;
using MovieModel.Entities;
using MovieModel.Enums;
using MovieModel.Lists;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;

namespace KnkScrapers.MoviesScraper
{
    public partial class MovieEnricher: EnrichCollections
    {
        internal MoviesOldfashioned MoviesOldfashion;
        internal Movies MoviesRepo;
        internal MovieSets MovieSetsRepo;

        internal CastingTypes CastingTypes;

        public MovieEnricher(KnkConnectionItf aCon):base(aCon, MediaTypeEnum.Movies)
        {
        }

        public override void ResetLibraries()
        {
            MoviesOldfashion = new MoviesOldfashioned(Connection) { Changes = Results };
            MovieSetsRepo = new MovieSets(Connection) { Changes = Results };
            MoviesRepo = new Movies(Connection) { Changes = Results };
            CastingTypes = new CastingTypes(Connection);

            ResetMissing();
            ResetOldfashion();
            ClearResults();

            base.ResetLibraries();
        }

        public override void ResetOldfashion()
        {
            base.ResetOldfashion();
        }

        public override KnkChangesItf InitMessages()
        {
            base.InitMessages();
            Results.AddMessages((from itm in MoviesOldfashion.Items select new KnkChangeDescriptor(itm) as KnkChangeDescriptorItf).ToList());
            return Results;
        }

        public override void ScrapFiles(BackgroundWorker worker)
        {
            if (!IsBusy)
            {
                base.ScrapFiles(worker);
                IsScanning = true;
                ScrapFiles();
                IsScanning = false;
                SaveChanges();
            }
        }

        public override void ImportVideoStation(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                base.ImportVideoStation(aWorker);
                IsScanning = true;
                ImportVideoStation();
                IsScanning = false;
                SaveChanges();
            }
        }

        public override void BestOnes(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                base.BestOnes(aWorker);
                IsScanning = true;
                BestMovies();
                IsScanning = false;
                SaveChanges();
            }
        }

        public override void RatedOnes(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                base.RatedOnes(aWorker);
                IsScanning = true;
                Results.AddMessage("Rated Movies", "Process Started");
                var lCriteria = new MoviesCriteria();
                lCriteria.TmdbId = 0;
                lCriteria.UserRatingFrom = 1;
                var lMovies = lCriteria.RefreshList();
                lMovies.Changes = Results;
                int i = 0;
                foreach (var mov in lMovies.Items)
                {
                    Results.AddMessage("Rating Movies", $"Rating Movie {++i}/{lMovies.Count()}: {mov.ToString()}");
                    ReportProgress();
                    UploadRating(mov.TmdbId ?? 0, mov.UserRating);
                }
                IsScanning = false;
                SaveChanges();
            }
        }

        public override void ArrangeFiles(BackgroundWorker aWorker)
        {
            if (!IsBusy)
            {
                base.ArrangeFiles(aWorker);
                var myroot = Roots.Items.Where(r => r.IsPermanent).FirstOrDefault();
                IsScanning = true;
                Results.AddMessage("Arranging Files", "Process Started");
                ReportProgress();
                var lCriteria = new MoviesCriteria();
                var lMoviesBase = lCriteria.RefreshList();
                lMoviesBase.Changes = Results;

                lCriteria.HasFiles = true;
                var lMovies = (from mv in lMoviesBase.Items
                           where mv.Extender.ShouldRenameFile() || mv.Extender.ShouldBeMoved()
                               select mv).ToList();
                int i = 0;
                int ordinal = 0;
                Results.AddMessage("Arranging Files", $"Processing {lMovies.Count} Movies");
                ReportProgress();
                foreach (var mov in lMovies)
                {
                    ordinal = 1;
                    Results.AddMessage("Arranging Files", $"Processing Movie {i}/{lMovies.Count}");
                    foreach (var filmov in mov.Files().Items.OrderByDescending(f=>f.CreationDate))
                    {
                        var fil = Files.Items.Where(f => f.IdFile.Value == filmov.IdFile.Value).FirstOrDefault();
                        var daddy = myroot;
                        var folder = fil.IdPath.Reference;
                        if (mov.Extender.ShouldBeMoved(fil) && mov.SuggestedPath.StartsWith(myroot.ToString()))
                        {
                            //fix path
                            var fol = mov.SuggestedPath.Remove(0, myroot.ToString().Length).Split('/').Reverse().Skip(1).Reverse().ToList();
                            var acum = myroot.ToString();
                            foreach(var name in fol)
                            {
                                acum = $"{acum}{name}/";
                                var aux = Folders.Items.Where(f => f.IdParentPath?.Value == daddy.IdPath.Value);
                                folder = Folders.Items.Where(f => f.IdParentPath?.Value == daddy.IdPath.Value && f.Path.ToLower() == acum.ToLower()).FirstOrDefault();
                                if(folder==null)
                                {
                                    folder = Folders.Create(daddy, acum);
                                    folder.Update("Folder Added to Library");
                                    folder.SaveChanges();
                                }
                                else if (folder.Path != acum)
                                {
                                    folder.Path = acum;
                                    folder.Update("Folder Name Updated");
                                    folder.SaveChanges();
                                }
                                daddy = folder;
                            }
                        }

                        if (fil != null)
                        {
                            //fix filename
                            var proposal = mov.Extender.FilenameProposal(fil, ordinal);
                            var proposalfilename = System.IO.Path.Combine(folder.Path, proposal);
                            if (!string.IsNullOrEmpty(proposal) && proposal.Length>12 && fil.ToString()!=proposalfilename)
                            {
                                Results.AddMessage("Renaming", $"{fil.Filename} to {proposal}");
                                if (fil.Extender.Exists())
                                {
                                    folder.CreateIfNotExists();
                                    System.IO.File.Move(fil.ToString(), proposalfilename);
                                    fil.IdPath.Reference.DeleteIfHasNoFiles();
                                }
                                fil.IdPath = folder;
                                fil.Filename = proposal;
                                fil.Update("File renamed");
                                fil.SaveChanges();
                                ReportProgress();
                            }
                        }
                        ordinal++;
                    }
                    i++;
                }
                IsScanning = false;
                SaveChanges();
            }
        }

        void ScrapFiles()
        {
            Results.AddMessage("Scraping", "Process Started");
            int i = 0;
            foreach (var lFil in FilesMissingMedia.Items)
            {
                if (lFil.Extender.Exists())
                {
                    if (ScrapFile(lFil))
                    {
                        i++;
                    }
                    if (i > 20) break;
                }
            }

            RefreshOldfashion(i);
        }


        void ImportVideoStation()
        {
            Results.AddMessage("Importing Folders", "Process Started");

            var newfolders = Connection.GetData("Select * from vieSynMissingFolders Order by len(Path)");
            foreach(var itm in newfolders)
            {
                var dad = Folders.Items.Where(f => f.Path == itm.ParentPath).FirstOrDefault();
                var folder = Folders.Create(dad, itm.Path);
                folder.Update("Folder Added to Library");
                folder.SaveChanges();
                ReportProgress();
            }

            Results.AddMessage("Importing Files", "Process Started");
            var newfiles = Connection.GetData("Select * from vieSynMissingFiles Order by SynologyId");
            File file = null;
            foreach (var itm in newfiles)
            {
                if (itm.SynologyId is null)
                {
                    file = Files.Items.Where(f => f.IdFile == itm.IdFile).FirstOrDefault();
                    file.Delete("File Removed from Synology");
                }
                else
                {
                    var dad = Folders.Items.Where(f => f.IdPath == itm.IdPath).FirstOrDefault();
                    if (itm.IdFile is null)
                    {
                        file = Files.Create(dad, itm.Filename, itm.SynologyId);
                        file.Update("New Synology File Scanned");
                    }
                    else
                    {
                        file = Files.Items.Where(f => f.IdFile == itm.IdFile).FirstOrDefault();
                        file.Filename = itm.Filename;
                        file.SynologyId = itm.SynologyId;
                        file.IdPath = dad;
                        file.Update("Updated Synology File");
                    }
                    CheckFile(file);
                }
                file.SaveChanges();
                ReportProgress();
            }
            Files.SaveChanges();
            int i = 0;
            Results.AddMessage("Importing Movies", "Process Started");
            var newmovies = Connection.GetData("select * from vieSynMissingMovieFiles Order by SynofileId");
            foreach (var itm in newmovies)
            {
                file = Files.Items.Where(f => f.IdFile == itm.IdFile).FirstOrDefault();
                var mov = MoviesRepo.FindMovie(Convert.ToInt32(itm.TmdbId));
                EnrichMovie(mov, file, mov.TmdbId, 0);
                mov.SaveChanges();
                i++;
                ReportProgress();
            }
            Results.AddMessage("Syncing Views", "Process Started");
            Connection.GetData("Exec Sp_syncsynology");
            RefreshOldfashion(i);
            ReportProgress();
        }

        void RefreshOldfashion(int current)
        {
            SaveChanges();
            Results.AddMessage("Refreshing Oldfashioned", "Process Started");
            foreach (var mov in MoviesOldfashion.Items)
            {
                EnrichMovie(mov, null);
                ReportProgress();
                current++;
            }
        }
        void BestMovies()
        {
            Results.AddMessage("Best Movies", "Process Started");
            ReportProgress();
            int i = 0;
            var lBest = GetBestMovies();
            foreach (var mov in lBest)
            {
                EnrichMovie(mov.Id, null);
                i++;
                if (i > 5) break;
            }
        }

        void ManageFiles()
        {
            Results.AddMessage("Managing", "Managing Files");
            ReportProgress();
            int i = 0;
            foreach (var lFil in Files.Items)
            {
                if(!lFil.IsNew())
                {

                }
            }
        }

        private bool ScrapFile(File aFile)
        {
            var lOrg = FindMovies(aFile, false).FirstOrDefault();
            if (lOrg > 0)
                EnrichFile(lOrg, MoviesRepo.FindMovie(lOrg), aFile);
            return lOrg > 0;
        }

        void EnrichFile(int aMovieId, Movie aMovieDst, File aFomFile)
        {
            var lDst = aMovieDst;
            if (lDst != null)
            {
                var medfile = FillFile(lDst, aFomFile);
                aFomFile.Scraped = 1;
                aFomFile.Update("Movie found in Tmdb");
                if (!aMovieDst.IsNew())
                {
                    medfile.SaveChanges();
                    aFomFile.SaveChanges();
                }
            }
            else
            {
                aFomFile.Scraped++;
                aFomFile.Update("No Tmdb movie found!!");
            }
        }

        internal Movie EnrichMovie(int tmdbId, File aFile, int aCalllevel = 0, bool aCreateIfNotExists = true)
        {
            return EnrichMovie(MoviesRepo.FindMovie(tmdbId, aCreateIfNotExists), aFile, tmdbId, aCalllevel);
        }

        public Movie EnrichMovie(Movie aMovieDst, File aFile)
        {
            int lMovId = aMovieDst.TmdbId ?? 0;
            if (lMovId == 0)
                lMovId = Client().SearchMovieAsync(aMovieDst.Title, year: aMovieDst.Year??0).Result.Results.FirstOrDefault()?.Id??0;
            return EnrichMovie(aMovieDst, aFile, lMovId, 0);
        }

        public Movie EnrichMovie(Movie aMovieDst, File aFile, int aTmdbId, int aCalllevel)
        {
            var lDst = aMovieDst;
            if (lDst != null && lDst.IsScrappable(aCalllevel))
            {
                var methods = TMDbLib.Objects.Movies.MovieMethods.Credits
                    | TMDbLib.Objects.Movies.MovieMethods.Images
                    | TMDbLib.Objects.Movies.MovieMethods.Keywords
                    | TMDbLib.Objects.Movies.MovieMethods.Videos
                    | TMDbLib.Objects.Movies.MovieMethods.ReleaseDates;
                var lOrg = Client().GetMovieAsync(aTmdbId,methods).Result;
                if (lOrg != null && lOrg.ReleaseDates.Results.Count > 0 && lOrg.ReleaseDate > lOrg.ReleaseDates.Results[0].ReleaseDates[0].ReleaseDate)
                {
                    lOrg.ReleaseDate = lOrg.ReleaseDates.Results[0].ReleaseDates[0].ReleaseDate;
                }
                if (lOrg == null)
                {
                    if(lDst.FilesNumber==0)
                    {
                        lDst.Delete("Unable to Re-Scrap Movie, Movie Lost");
                        lDst.SaveChanges();
                    }
                    return lDst;
                }
                else if (lOrg.ReleaseDate > DateTime.Now)
                {
                    lDst.Delete("Movies from future shouldn't be scrapped");
                    lDst.SaveChanges();
                    return lDst;
                }
                else if (lOrg.ReleaseDate == null)
                {
                    lDst.Delete("No release date, Movie removed");
                    lDst.SaveChanges();
                    return lDst;
                }
                if (lDst.IsNew())
                    lDst.Update("Movie Scraped From Tmdb");
                else
                    lDst.Update("Movie Re-Scraped From Tmdb");
                ReportProgress();
                lDst.Title = lOrg.Title;                                        //	string					Title
                lDst.OriginalTitle = lOrg.OriginalTitle;                        //	string					OriginalTitle
                lDst.TagLine = lOrg.Tagline;                                    //	string					TagLine
                //	string					Poster                              Will be imported in Images
                //	string					Backdrop                            Will be imported in Images
                lDst.AdultContent = lOrg.Adult;                                 //	bool					Adult
                //	Collection				BelongsTo                           Will not be imported
                lDst.Budget = lOrg.Budget;                                      //	int						Budget
                lDst.HomePage = lOrg.Homepage;                                  //	string					HomePage
                lDst.ImdbId = lOrg.ImdbId;                                        //	string					Imdb
                lDst.TmdbId = lOrg.Id;                                          //	int 					Tmdb.Id
                lDst.ReleaseDate = lOrg.ReleaseDate;                            //	DateTime?				ReleaseDate
                lDst.Year = lOrg.ReleaseDate?.Year;                             //  Will be deleted in a future
                lDst.Revenue = lOrg.Revenue;                                    //	Int64					Revenue
                if (lDst.Files().Items.Count > 0)
                    lDst.Duration = lDst.Files().Items.FirstOrDefault().IdFile.Reference.Duration; //	int?					Runtime
                else
                    lDst.Duration = TimeSpan.FromSeconds((double)(lOrg.Runtime ?? 0) * 60); //	int?					Runtime
                //	AlternativeTitles		AlternativeTitles                   Will not be imported
                //	Keywords				Keywords
                //	Releases				Releases
                //	Translations			Translations
                lDst.Popularity = Convert.ToDecimal(lOrg.Popularity);                              //	decimal					Popularity
                lDst.Rating = Convert.ToDecimal(lOrg.VoteAverage);                                 //	decimal					VoteAverage
                lDst.Votes = lOrg.VoteCount;                                    //	int						VoteCount
                //	string					Status
                //	ExternalIds				External
                lDst.ScrapedDate = DateTime.Now;
                FillMovieSet(lDst, lOrg.BelongsToCollection?.Name);                                //	MediaCredits			Credits
                FillCasting(lDst, lOrg.Credits);                                //	MediaCredits			Credits
                FillMediaLinks(lDst, lOrg);                                     //	Images					Images
                if (lDst.IsNew()) FillUser(lDst, aMovieDst.Connection().CurrentUser);                                           //  It belongs to the user
                FillSummaries(lDst, lOrg.Overview);                             //	string					Overview
                FillTags(lDst, lOrg);                             //	string					Overview
                ReportProgress();
                FillGenres(lDst, lOrg.Genres);                         //	IEnumerable<Genre>		Genres
                FillCompanies(lDst, lOrg.ProductionCompanies);                   //	IEnumerable<Company>	Companies
                FillCountries(lDst, lOrg.ProductionCountries);                   //	IEnumerable<Country>	Countries
                FillLanguages(lDst, lOrg.SpokenLanguages);                   //	IEnumerable<Language>	Languages
                if (aFile != null) EnrichFile(lOrg.Id, lDst, aFile);
                lDst.SaveChanges();
                if (aMovieDst.UserRating > 0) UploadRating(lOrg.Id, aMovieDst.UserRating);
                if (lOrg.BelongsToCollection != null && (aMovieDst.FilesNumber > 0 || aFile != null))
                {
                    lDst.Update($"Checking collection {lOrg.BelongsToCollection.Name}");
                    ReportProgress();
                    foreach (var lPrt in Client().GetCollectionAsync(lOrg.BelongsToCollection.Id).Result.Parts)
                    {
                        var lResult = EnrichMovie(lPrt.Id, null, aCalllevel);
                    }
                    lDst.Update($"Collection {lOrg.BelongsToCollection.Name} Checked");
                    ReportProgress();
                }
                if (lDst.IdSet != null)
                {
                    var lRel = lDst.Relateds().Create();
                    lRel.IdSet = lDst.IdSet;
                    lRel.IdSetRelated = lDst.IdSet;
                    lRel.Update("Set is Auto-Related");
                }
                GetRelatedMovies(lDst, aCalllevel);
                lDst.SaveChanges();
            }
            else if (aFile != null && lDst?.TmdbId != null)
            {
                EnrichFile(lDst.TmdbId.Value, lDst, aFile);
            }
            ReportProgress();
            return lDst;
        }

        void FillCasting(Movie aMovie, TMDbLib.Objects.Movies.Credits aCredits)
        {
            aMovie.Casting().DeleteAll("Scrap replaces old Casting");
            foreach(var lItm in aCredits.Cast)
            {
                CheckMovieCasting(lItm, aMovie);
            }
            foreach (var lItm in aCredits.Crew)
            {
                CheckMovieCasting(lItm, aMovie);
            }
        }

        CastingType CheckCastingType(string aType, string aDepartment)
        {
            var lFound = CastingTypes.Items.Where(g => g.Type.ToLower().Equals(aType.ToLower())).FirstOrDefault();
            var lReturn = lFound;
            if (lFound == null)
            {
                lReturn = CastingTypes.Create();
                lReturn.Type = aType;
            }
            if(string.IsNullOrEmpty(lReturn.Department))
            {
                lReturn.Department = aDepartment;
                lReturn.Update("Scrap updates Casting Types");
            }
            return lReturn;
        }

        void CheckMovieCasting(TMDbLib.Objects.Movies.Cast aItem, Movie aMovie)
        {
            var lType = CheckCastingType("Actor", "Casting");
            if (lType != null)
            {
                var lCast = aMovie.Casting().Items.ToList();
                var lFound = lCast.Where(g => g.IdCastingType.Reference.Type.Equals(lType.Type) && g.IdPerson?.Reference.TmdbId == aItem.Id.ToString()).FirstOrDefault();
                var lReturn = lFound;
                if (lFound == null)
                {
                    lReturn = aMovie.Casting().Create();
                    lReturn.IdMovie = aMovie;
                }
                lReturn.IdCastingType = lType;
                lReturn.Ordinal = lCast.Where(g => g.IdCastingType.Reference.Type.Equals(lType.Type) && !g.Deleted).Count() + 1;
                lReturn.IdPerson = CheckPerson(aItem.Id, aItem.Name, lType);
                lReturn.Role = aItem.Character;
                lReturn.Update("Scraper checked Movie Casting");
            }
        }

        void CheckMovieCasting(TMDbLib.Objects.General.Crew aItem, Movie aMovie)
        {
            var lType = CheckCastingType(aItem.Job, aItem.Department);
            if (lType != null)
            {
                var lCast = aMovie.Casting().Items.ToList();
                var lFound = lCast.Where(g => g.IdCastingType.Reference.Type.Equals(lType.Type) && g.IdPerson?.Reference?.Name?.ToLower() == aItem.Name.ToLower()).FirstOrDefault();
                var lReturn = lFound;
                if (lFound == null)
                {
                    lReturn = aMovie.Casting().Create();
                    lReturn.IdMovie = aMovie;
                }
                lReturn.IdCastingType = lType;
                lReturn.Ordinal = lCast.Where(g => g.IdCastingType.Reference.Type.Equals(lType.Type) && !g.Deleted).Count() + 1;
                lReturn.IdPerson = CheckPerson(aItem.Id, aItem.Name, lType);
                lReturn.Role = aItem.Job;
                lReturn.Update("Scraper checked Movie Casting");
            }
        }

        Person CheckPerson(int id, string name, CastingType aType)
        {
            var lChk = Persons.Items.Where(g => g.TmdbId == id.ToString()).ToList().FirstOrDefault();
            if (lChk == null)
            {
                lChk = Persons.Create();
            }
            lChk.Name = name;
            lChk.TmdbId = id.ToString();
            if (aType.ImportCrew && id > 0 && (lChk.ScrapedDate == null || lChk.ScrapedDate < DateTime.Now.AddYears(-1)))
            {
                TMDbLib.Objects.People.Person Person = Client().GetPersonAsync(id,TMDbLib.Objects.People.PersonMethods.Images | TMDbLib.Objects.People.PersonMethods.ExternalIds).Result;
                lChk.BirthDay = Person.Birthday?.ToString();
                lChk.DeathDay = Person.Deathday?.ToString();
                lChk.BirthPlace = Person.PlaceOfBirth;
                lChk.ImdbId = Person.ExternalIds.ImdbId;
                //lChk.TvdbId = Person.External().Tvdb?.ToString();
                lChk.ScrapedDate = DateTime.Now;
                FillBiography(lChk, Person.Biography);
                FillNames(lChk, Person);
                lChk.Pictures().DeleteAll("Scrap replaces old images");
                if (Person.Images.Profiles != null)
                {
                    foreach (var lImg in Person.Images.Profiles)
                        CheckMediaLink(lImg.FilePath, null, lChk, LinkTypeEnum.Poster);
                }
            }
            lChk.Update("Scraper checked Casting");
            return lChk;
        }

        void FillMediaLinks(Movie aMovie, TMDbLib.Objects.Movies.Movie aOrg)
        {
            aMovie.Pictures().DeleteAll("Scrap replaces old images");
            if(!string.IsNullOrEmpty(aOrg.PosterPath))
                CheckMediaLink(aOrg.PosterPath, aMovie, null, LinkTypeEnum.Poster); //Posters

            foreach (var lItem in aOrg.Images.Posters.ToList())
            {
                CheckMediaLink(lItem.FilePath, aMovie, null, LinkTypeEnum.Poster); //Posters
            }
            foreach (var lItem in aOrg.Images.Backdrops.ToList())
            {
                CheckMediaLink(lItem.FilePath, aMovie, null, LinkTypeEnum.Fanart); //Fanarts
            }
            foreach (var lItem in aOrg.Videos.Results)
            {
                CheckMediaLink(lItem.Key, aMovie, null, LinkTypeEnum.Videos); //Videos
            }
        }

        void FillMediaFile(Movie aMovie, File aFile)
        {
            var lReturn = aMovie.Files().Items.FirstOrDefault(f => f.IdFile.Reference.Filename.ToLower().Equals(aFile.Filename.ToLower()));
            if (lReturn == null)
            {
                lReturn = aMovie.Files().Create();
                lReturn.IdMovie = aMovie;
                lReturn.IdFile = aFile;
                lReturn.Update("Scrapert Linked File & Movie");
            }
        }

        MediaLink CheckMediaLink(string aUrl, Movie aMovie, Person aPerson, LinkTypeEnum aidType)
        {
            string lValue = aUrl.Replace("/", "");
            string lSite = "http://image.tmdb.org/t/p/original/{0}";
            string lSiteThumbnail = "http://image.tmdb.org/t/p/w500/{0}";
            if(aidType.Equals(LinkTypeEnum.Videos))
            {
                lSite = "https://www.youtube.com/watch?v={0}";
                lSiteThumbnail = "http://img.youtube.com/vi/{0}/default.jpg";
            }

            MediaLink lFound = null;
            if (aMovie != null)
            {
                lFound = aMovie.Pictures().Items.Where(g => g.ToString().ToLower().Equals(lValue.ToLower())).FirstOrDefault();
            }
            if (aPerson != null)
            {
                //lSite = "http://thetvdb.com/banners/actors/{0}";
                //lSiteThumbnail = "http://image.tmdb.org/t/p/w500/{0}";
                lFound = aPerson.Pictures().Items.Where(g => g.ToString().ToLower().Equals(lValue.ToLower())).FirstOrDefault();
                if (lFound == null)
                {
                    lFound = aPerson.Pictures().Items.Where(g => g.ToString().ToLower().Equals(lValue.ToLower())).FirstOrDefault();
                }
            }
            var lReturn = lFound;
            if (lFound == null)
            {
                if (aMovie != null)
                    lReturn = aMovie.Pictures().Create();
                else if(aPerson != null)
                    lReturn = aPerson.Pictures().Create();

                lReturn.IdMovie = aMovie;
                lReturn.IdPerson = aPerson;
            }
            lReturn.IdType = aidType;
            lReturn.Site = lSite;
            lReturn.SiteThumbnail = lSiteThumbnail;
            lReturn.Value = lValue;
            lReturn.Update("Scraper checked Link");
            return lReturn;
        }

        MediaUser FillUser(Movie aMovie, KnkItemItf aUser)
        {
            User lUsr = aUser as User;
            var lUsers = aMovie.Users();
            var lFound = (from fil in lUsers.Items
                          where fil.IdUser.Value.Equals(lUsr.IdUser.Value)
                          select fil).FirstOrDefault();
            var lReturn = lFound;
            if (lFound == null)
            {
                lReturn = lUsers.Create();
                lReturn.IdMovie = aMovie;
            }
            lReturn.IdUser = lUsr;
            lReturn.Update("Movie added to User");
            return lReturn;
        }

        void FillCountries(Movie aMovie, List<TMDbLib.Objects.Movies.ProductionCountry> aColection)
        {
            aMovie.Countries().DeleteAll("Scrap replaces old countries");
            foreach (var lItem in aColection)
            {
                CheckMovieCountry(lItem, aMovie);
            }
        }

        void FillCompanies(Movie aMovie, List<TMDbLib.Objects.Movies.ProductionCompany> aColection)
        {
            aMovie.Companies().DeleteAll("Scrap replaces old companies");
            foreach (var lItem in aColection)
            {
                CheckMovieCompany(lItem, aMovie);
            }
        }

        void FillGenres(Movie aMovie, List<TMDbLib.Objects.General.Genre> aColection)
        {
            aMovie.Genres().DeleteAll("Scrap replaces old Genres");
            var lMin = new TimeSpan(0, 0, 0);
            var lCorto = new TimeSpan(0, 35, 0);
            if (aMovie.Duration != null && aMovie.Duration > lMin && aMovie.Duration < lCorto)
            {
                CheckMovieGenre("Corto", aMovie);
            }
            foreach (var lItem in aColection)
            {
                CheckMovieGenre(lItem.Name, aMovie);
            }
        }

        void FillLanguages(Movie aMovie, List<TMDbLib.Objects.Movies.SpokenLanguage> aColection)
        {
            aMovie.Languages().DeleteAll("Scrap replaces old Languages");
            foreach (var lItem in aColection)
            {
                CheckMovieLanguage(lItem, aMovie);
            }
        }

        MovieLanguage CheckMovieLanguage(TMDbLib.Objects.Movies.SpokenLanguage aItem, Movie aMovie)
        {
            var lFound = aMovie.Languages().Items.Where(g => g.ToString().ToLower().Equals(aItem.Iso_639_1.ToLower())).FirstOrDefault();
            var lReturn = lFound;
            if (lFound == null)
            {
                lReturn = aMovie.Languages().Create();
                lReturn.IdMovie = aMovie;
            }
            lReturn.IdLanguage = CheckLanguage(aItem);
            lReturn.Update("Scraper checked Language");
            return lReturn;
        }

        Language CheckLanguage(TMDbLib.Objects.Movies.SpokenLanguage aItem)
        {
            var lChk = Languages.Items.Where(g => g.ToString().ToLower().Equals(aItem.Iso_639_1.ToLower())).FirstOrDefault();
            if (lChk == null)
            {
                lChk = Languages.Create();
            }
            lChk.Code = aItem.Iso_639_1;
            lChk.Name = aItem.Name;
            lChk.Update("Scraper checked Language");
            return lChk;
        }

        MovieCountry CheckMovieCountry(TMDbLib.Objects.Movies.ProductionCountry aItem, Movie aMovie)
        {
            var lFound = aMovie.Countries().Items.Where(g => g.ToString().ToLower().Equals(aItem.Name.ToLower())).FirstOrDefault();
            var lReturn = lFound;
            if (lFound == null)
            {
                lReturn = aMovie.Countries().Create();
                lReturn.IdMovie = aMovie;
            }
            lReturn.IdCountry = CheckCountry(aItem);
            lReturn.Update("Scraper checked Country");
            return lReturn;
        }

        Country CheckCountry(TMDbLib.Objects.Movies.ProductionCountry aItem)
        {
            var lChk = Countries.Items.Where(g => g.ToString().ToLower().Equals(aItem.Name.ToLower())).FirstOrDefault();
            if (lChk == null)
            {
                lChk = Countries.Create();
            }
            lChk.CountryName = aItem.Name;
            lChk.Update("Scraper checked Country");
            return lChk;
        }

        Genre CheckGenre(string aGenre)
        {
            var lChk = Genres.Items.Where(g => g.GenreName.ToLower().Equals(aGenre.ToLower())).FirstOrDefault();
            if (lChk == null)
            {
                lChk = Genres.Create();
            }
            lChk.GenreName = aGenre;
            lChk.Update("Scraper checked Genre");
            return lChk;
        }

        MovieGenre CheckMovieGenre(string aGenre, Movie aMovie)
        {
            var lFound = aMovie.Genres().Items.Where(g => g.ToString().ToLower().Equals(aGenre.ToLower())).FirstOrDefault();
            var lReturn = lFound;
            if (lFound == null)
            {
                lReturn = aMovie.Genres().Create();
                lReturn.IdMovie = aMovie;
            }
            lReturn.IdGenre = CheckGenre(aGenre);
            lReturn.Update("Scraper checked Genre");
            return lReturn;
        }

        Company CheckCompany(int id)
        {
            var comp = Client().GetCompanyAsync(id).Result;
            return CheckCompany(comp);
        }
        Company CheckCompany(TMDbLib.Objects.Search.SearchCompany aItem)
        {
            return CheckCompany(aItem.Id);
        }
        Company CheckCompany(TMDbLib.Objects.Companies.Company aItem)
        {
            if (aItem?.Name != null)
            {
                var lChk = Companies.Items.Where(g => g.CompanyName.ToLower().Equals(aItem.Name.ToLower())).FirstOrDefault();
                if (lChk == null)
                {
                    lChk = Companies.Create();
                }
                lChk.CompanyName = aItem.Name;
                lChk.Description = aItem.Description;
                lChk.HeadQuarters = aItem.Headquarters;
                lChk.HomePage = aItem.Homepage?.ToString();
                lChk.Logo = aItem.LogoPath;
                if (aItem.ParentCompany != null)
                    lChk.IdParentCompany = CheckCompany(aItem.ParentCompany);
                lChk.Update("Scraper checked Company");
                return lChk;
            }
            else
                return null;
        }

        MovieCompany CheckMovieCompany(TMDbLib.Objects.Movies.ProductionCompany aItem, Movie aMovie)
        {
            var lFound = aMovie.Companies().Items.Where(g => (g.ToString()??string.Empty).ToLower().Equals(aItem.Name.ToLower())).FirstOrDefault();
            var lReturn = lFound;
            var com = CheckCompany(aItem.Id);
            if (com != null)
            {
                if (lFound == null)
                {
                    lReturn = aMovie.Companies().Create();
                    lReturn.IdMovie = aMovie;
                }
                lReturn.IdCompany = CheckCompany(aItem.Id);
                lReturn.Update("Scraper checked Company");
            }
            return lReturn;
        }

        MediaFile FillFile(Movie aMovie, File aFile)
        {
            File lFile = null;
            MediaFile lFound = null;
            if (aFile.IdFile != null)
            {
                lFile = Files.Items.FirstOrDefault(f => f.IdFile.Value.HasValue && f.IdFile.Value.Value == aFile.IdFile.Value.Value);
            }
            else
            {
                lFile = (from f in Files.Items where f.ToString() == aFile.ToString() select f).FirstOrDefault();
            }

            if(lFile?.IdFile?.Value.Value!=null)
            {
                lFound = aMovie.Files().Items.FirstOrDefault(f => f.IdFile.Value.Value == lFile.IdFile.Value.Value);
            }
            var lReturn = lFound;
            if(lFound == null)
            {
                lReturn = aMovie.Files().Create();
                lReturn.IdMovie = aMovie;
                foreach (var lMed in lFile.MovieFiles().Items)
                {
                    lMed.Delete("Movie Replaced");
                }
            }
            aMovie.Duration = aFile.Duration;
            aMovie.Update("Seconds Updated");
            lReturn.IdFile = lFile;
            lReturn.Update("File assigned to Movie");
            lReturn.SaveChanges();
            return lReturn;
        }

        void FillBiography(Person aPerson, string aBiography)
        {
            var lBio = aPerson.Biography();
            lBio.DeleteAll("Scrap replaces old Biography");
            string[] lines = aBiography?.Split(new string[] { ". " }, StringSplitOptions.None);
            if (lines != null)
            {
                int lOrdinal = 1;
                foreach (var lLine in lines)
                {
                    var lLin = lBio.Create();
                    lLin.IdPerson = aPerson;
                    lLin.Ordinal = lOrdinal;
                    lLin.Text = lLine + ".";
                    lLin.Update("Scraper added Biography");
                }
            }
        }

        void FillNames(Person aPerson, TMDbLib.Objects.People.Person aPersonRemote)
        {
            var lNam = aPerson.Names();
            lNam.DeleteAll("Scraper replaces old Names");
            foreach (var lNamPer in aPersonRemote.AlsoKnownAs)
            {
                var Found = lNam.Items.FirstOrDefault(n => n.Name.ToLower().Equals(lNamPer.ToLower()));
                if (Found == null) Found = lNam.Create();
                Found.IdPerson = aPerson;
                Found.Name = lNamPer;
                Found.Update("Scraper added Name");
            }
        }

        void FillMovieSet(Movie aMovie, string aSet)
        {
            if(!string.IsNullOrEmpty(aSet))
            {
                aMovie.IdSet = CheckMovieSet(aSet);
            }
        }

        MovieSet CheckMovieSet(string aMovieSet)
        {
            MovieSet lReturn = null;
            if(!string.IsNullOrEmpty(aMovieSet))
            {
                aMovieSet = aMovieSet.Replace(" - La Colección", "");
                aMovieSet = aMovieSet.Replace(" - La Coleccion", "");
                aMovieSet = aMovieSet.Replace(" - Colección", "");
                aMovieSet = aMovieSet.Replace(" - Coleccion", "");
                aMovieSet = aMovieSet.Replace("Colección: ", "");
                aMovieSet = aMovieSet.Replace("Coleccion: ", "");
                aMovieSet = aMovieSet.Replace(" Collection", "");
                aMovieSet = aMovieSet.Replace(" Colección", "");
                aMovieSet = aMovieSet.Replace(" colección", "");
                var lFound = MovieSetsRepo.Items.Where(g => g.Name.ToLower().Equals(aMovieSet.ToLower())).FirstOrDefault();
                lReturn = lFound;
                if (lFound == null)
                {
                    lReturn = MovieSetsRepo.Create();
                    lReturn.Name = aMovieSet;
                    lReturn.Update("New Saga");
                }
            }
            return lReturn;
        }

        void FillSummaries(Movie aMovie, string aOverviews)
        {
            var lSum = aMovie.Summary();
            lSum.DeleteAll("Scraper replaces old Summaries");
            string[] lines = aOverviews?.Split(new string[] { ". " }, StringSplitOptions.None);
            if (lines != null)
            {
                int lOrdinal = 1;
                foreach (var lLine in lines)
                {
                    var lLin = lSum.Create();
                    lLin.IdMovie = aMovie;
                    lLin.Ordinal = lOrdinal;
                    lLin.SummaryItem = lLine + ".";
                    lLin.Update("Scraper added Sumary");
                }
            }
        }

        void FillTags(Movie aMovie, TMDbLib.Objects.Movies.Movie tmdbMovie)
        {
            var lTags = aMovie.Tags();
            lTags.DeleteAll("Scraper replaces old Tags");
            var keywords = tmdbMovie.Keywords;
            if (keywords != null && keywords.Keywords != null)
            {
                foreach (var lTag in keywords.Keywords)
                {
                    var lLin = lTags.Create();
                    lLin.IdMovie = aMovie;
                    lLin.Tag = lTag.Name;
                    lLin.Update("Scraper added Tag");
                }
            }
        }

        private void FillFileEspecificProperties(File aFile)
        {
            MediaInfoDotNet.MediaFile lMedia = new MediaInfoDotNet.MediaFile(aFile.ToString());
            var lStream = lMedia.Video?.FirstOrDefault();
            if (lStream != null) aFile.Duration = new TimeSpan(0, 0, 0, 0, lStream.Duration);
            aFile.AudioTracks = lMedia.Audio.Count;
            aFile.Subtitles = lMedia.Text.Count;
        }

        public override void SaveChanges()
        {
            base.SaveChanges();
            if (!IsBusy)
            {
                IsSaving = true;

                Results.AddMessage("Saving Changes", "Movies");
                ReportProgress();
                MoviesRepo.SaveChanges();

                Results.AddMessage("Saving Changes", "Process Finished");
                ReportProgress();

                IsSaving = false;
            }
        }

        public override bool IsExtension(string aFile)
        {
            bool ret = false;
            string lVal = System.IO.Path.GetExtension(aFile).ToLower();
            switch (lVal)
            {
                case ".avi":
                case ".mkv":
                case ".mpg":
                case ".mp4":
                    ret = true;
                    break;
                default:
                    break;
            }
            return ret;
        }

        public override void CheckFile(File aFile)
        {
            var lFile = aFile;
            base.CheckFile(lFile);
            if (!lFile.Deleted && (lFile.Duration == null || lFile.AudioTracks == null || lFile.Subtitles == null))
            {
                FillFileEspecificProperties(lFile);
                lFile.Update("Video Properties Changed");
            }

        }

        public bool UploadRating(int aId, decimal? aRating)
        {
            Client().SetSessionInformation(UserSessionId(), TMDbLib.Objects.Authentication.SessionType.UserSession);
            Client().AccountChangeFavoriteStatusAsync(TMDbLib.Objects.General.MediaType.Movie, aId, Convert.ToDouble(aRating) >= 8);
            return Client().MovieSetRatingAsync(aId, Convert.ToDouble(aRating)).Result;
        }

    }
}
