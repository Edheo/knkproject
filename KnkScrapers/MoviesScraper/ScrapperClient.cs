﻿using KnkScrapers.Tmdb;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace KnkScrapers.MoviesScraper
{
    public class ScrapperClient
    {
        string Language = ParametersTmdb.Language;
        private static DateTime? _LastRequest;

        private static List<Tmdb.TmdbMovie> _scraped = new List<Tmdb.TmdbMovie>();

        static string TmdbApi = "https://api.themoviedb.org/3/";
        static int CurrentRemaining = 40;

        internal ScrapperClient()
        {
        }

        private static string TmdbApiKey=> $"api_key={ParametersTmdb.TmdbApikey}";
        private static string TmdbApiKeyLang => $"{TmdbApiKey}&language={ParametersTmdb.Language}";

        private static string TmdbApiMovieById(long movie_id) => $"{TmdbApi}movie/{movie_id}?{TmdbApiKeyLang}";
        private static string TmdbApiMovieCredits(long movie_id) => $"{TmdbApi}movie/{movie_id}/credits?{TmdbApiKeyLang}";
        private static string TmdbApiMovieImages(long movie_id) => $"{TmdbApi}movie/{movie_id}/images?{TmdbApiKeyLang}";
        private static string TmdbApiMovieVideos(long movie_id) => $"{TmdbApi}movie/{movie_id}/videos?{TmdbApiKeyLang}";
        private static string TmdbApiMovieKeywords(long movie_id) => $"{TmdbApi}movie/{movie_id}/keywords?{TmdbApiKeyLang}";
        private static string TmdbApiMovieSimilar(long movie_id, int page = 1) => $"{TmdbApi}movie/{movie_id}/similar?{TmdbApiKeyLang}&page={page}";

        private static string TmdbApiCollectionById(long collection_id) => $"{TmdbApi}collection/{collection_id}?{TmdbApiKeyLang}";

        private static string TmdbApiMovieSetRating(long movie_id, string session_id) => $"{TmdbApi}movie/{movie_id}/rating?{TmdbApiKey}&session_id={session_id}";

        private static string TmdbApiMovieSearchTitle(string title, int page=1) => $"{TmdbApi}search/movie?{TmdbApiKeyLang}&query={WebUtility.UrlEncode(title)}&page={page}";
        private static string TmdbApiMovieSearchTitleYear(string title, int year, int page=1) => $"{TmdbApi}search/movie?{TmdbApiKeyLang}&query={WebUtility.UrlEncode(title)}&year={year}&page={page}";
        private static string TmdbApiMoviesTopRated(int page = 1) => $"{TmdbApi}search/movie/top_rated?{TmdbApiKeyLang}&page={page}";

        private static string TmdbApiPersonById(long person_id) => $"{TmdbApi}person/{person_id}?{TmdbApiKeyLang}";
        private static string TmdbApiPersonExternals(long person_id) => $"{TmdbApi}person/{person_id}/external_ids?{TmdbApiKeyLang}";
        private static string TmdbApiPersonImages(long person_id) => $"{TmdbApi}person/{person_id}/images?{TmdbApiKeyLang}";

        private static string TmdbApiCompanyById(long company_id) => $"{TmdbApi}company/{company_id}?{TmdbApiKeyLang}";

        private static string TmdbApiCreateToken() => $"{TmdbApi}authentication/token/new?{TmdbApiKey}";
        private static string TmdbApiCreateSession() => $"{TmdbApi}/authentication/session/new?{TmdbApiKey}";

        static string GetTmdbData(string call)
        {
            if(CurrentRemaining<10) WaitTime();
            var client = new RestClient(call);
            var request = new RestRequest(Method.GET);
            request.AddParameter("undefined", "{}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var pre = CurrentRemaining;
            CurrentRemaining = int.Parse(response.Headers.Where(h => h.Name == "X-RateLimit-Remaining").FirstOrDefault().Value.ToString());
            if (response.StatusCode!=HttpStatusCode.NotFound)
                return response.Content;
            else
                return null;
        }

        static string PostTmdbData(string call, string body)
        {
            if (CurrentRemaining < 10) WaitTime();
            var client = new RestClient(call);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json;charset=utf-8");
            request.AddParameter("application/json;charset=utf-8", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                response = response;
            CurrentRemaining = int.Parse(response.Headers.Where(h => h.Name == "X-RateLimit-Remaining").FirstOrDefault().Value.ToString());
            return response.Content;
        }

        internal static TmdbMovie GetMovieData(int movie_id)
        {
            var mov = _scraped.Where(m => m?.Id == movie_id).FirstOrDefault();
            if (mov == null)
            {
                mov = TmdbConverter.FromJsonToClass<TmdbMovie>(GetTmdbData(TmdbApiMovieById(movie_id)));
                _scraped.Add(mov);
            }
            return mov;
        }

        internal static TmdbCredits GetCreditsData(int movie_id)
        {
            var cre = TmdbConverter.FromJsonToClass<TmdbCredits>(GetTmdbData(TmdbApiMovieCredits(movie_id)));
            return cre;
        }

        internal static TmdbMovieImages GetMovieImages(int movie_id)
        {
            var cre = TmdbConverter.FromJsonToClass<TmdbMovieImages>(GetTmdbData(TmdbApiMovieImages(movie_id)));
            return cre;
        }

        internal static TmdbMovieVideos GetMovieVideos(int movie_id)
        {
            var cre = TmdbConverter.FromJsonToClass<TmdbMovieVideos>(GetTmdbData(TmdbApiMovieVideos(movie_id)));
            return cre;
        }

        internal static TmdbMovieKeywords GetMovieKeywords(int movie_id)
        {
            var cre = TmdbConverter.FromJsonToClass<TmdbMovieKeywords>(GetTmdbData(TmdbApiMovieKeywords(movie_id)));
            return cre;
        }

        internal static TmdbCollection GetCollectionData(int collection_id)
        {
            var cre = TmdbConverter.FromJsonToClass<TmdbCollection>(GetTmdbData(TmdbApiCollectionById(collection_id)));
            return cre;
        }

        public async Task<TmdbSession> AuthenticationCreateGuestSessionAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            RestRequest request = _client.Create("authentication/guest_session/new");
            //{
            //    DateFormat = "yyyy-MM-dd HH:mm:ss UTC"
            //};

            RestResponse<TmdbSession> response = await request.ExecuteGet<TmdbSession>(cancellationToken).ConfigureAwait(false);

            return response;
        }

        public async Task<RestResponse<T>> ExecuteGet<T>(CancellationToken cancellationToken)
        {
            HttpResponseMessage resp = await SendInternal(HttpMethod.Get, cancellationToken).ConfigureAwait(false);

            return new RestResponse<T>(resp, _client);
        }

        public static bool UploadRating(int movie_id, decimal? aRating)
        {
            var lRet = false;
            //if (aRating.HasValue && aRating.Value > 0)
            //{
            //    NumberFormatInfo nfi = new NumberFormatInfo();
            //    nfi.NumberDecimalSeparator = ".";
            //    nfi.NumberDecimalDigits = 1;
            //    var value = $" {{ \"value\": {aRating.Value.ToString("0.0", nfi)} }}";
            //    var call = TmdbApiMovieSetRating(movie_id, CreateSession().SessionId);
            //    var ret = PostTmdbData(call, value);
            //}
            //lRet = true;
            return lRet;
        }


        //public async Task<List<PersonCredit>> GetPersonMovies(int aIdPerson)
        //{
        //    List<PersonCredit> medias = null;
        //    try
        //    {
        //        using (var lClient = Client)
        //        {
        //            WaitTime();
        //            var med = await lClient.People.GetCreditsAsync(aIdPerson, Language,DataInfoType.Movie, CancellationToken.None);
        //            medias = med.ToList();
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return medias;
        //}

        public static TmdbMovie[] SearchMovie(string aTitle, int aYear)
        {
            var pag = TmdbConverter.FromJsonToClass<TmdbMoviesPage>(GetTmdbData(TmdbApiMovieSearchTitleYear(aTitle, aYear)));
            return pag?.Movies;
        }

        public static TmdbMovie[] SearchMovie(MovieEnricher.Titleyear aMovie)
        {
            return SearchMovie(aMovie.Title, aMovie.Year ?? 0);
        }

        public static TmdbMovie[] GetRelatedMovies(int aIdMovie, int aPage=1)
        {
            var pag = TmdbConverter.FromJsonToClass<TmdbMoviesPage>(GetTmdbData(TmdbApiMovieSimilar(aIdMovie, aPage)));
            return pag.Movies;
        }

        public static TmdbMovie[] GetTopRated(int aPage = 1)
        {
            var pag = TmdbConverter.FromJsonToClass<TmdbMoviesPage>(GetTmdbData(TmdbApiMoviesTopRated(aPage)));
            return pag.Movies;
        }

        internal static TmdbPerson GetPersonData(int person_id)
        {
            var per = TmdbConverter.FromJsonToClass<TmdbPerson>(GetTmdbData(TmdbApiPersonById(person_id)));
            return per;
        }

        internal static TmdbPersonExternals GetPersonExternal(int person_id)
        {
            var ext = TmdbConverter.FromJsonToClass<TmdbPersonExternals>(GetTmdbData(TmdbApiPersonExternals(person_id)));
            return ext;
        }

        internal static TmdbPersonImages GetPersonImages(int person_id)
        {
            var img = TmdbConverter.FromJsonToClass<TmdbPersonImages>(GetTmdbData(TmdbApiPersonImages(person_id)));
            return img;
        }

        private static void WaitTime()
        {
            if (_LastRequest == null)
            {
                _LastRequest = DateTime.Now;
                return;
            }
            else
            {
                Thread.Sleep(100);
                while ((DateTime.Now - (DateTime)_LastRequest).TotalSeconds < 1)
                {
                    Thread.Sleep(100);
                }
                _LastRequest = DateTime.Now;
                return;
            }
        }

        internal static TmdbCompany GetCompanyData(int company_id)
        {
            var mov = TmdbConverter.FromJsonToClass<TmdbCompany>(GetTmdbData(TmdbApiCompanyById(company_id)));
            return mov;
        }

        private static TmdbToken CreateToken()
        {
            var tok = TmdbConverter.FromJsonToClass<TmdbToken>(GetTmdbData(TmdbApiCreateToken()));
            return tok;
        }

        private static TmdbSession CreateSession()
        {
            var token = CreateToken();
            var data = $"{{ \"request_token\": \"{token.Token}\" }}";
            var ret = TmdbConverter.FromJsonToClass<TmdbSession>(PostTmdbData(TmdbApiCreateSession(), data));
            return ret;
        }
    }
}
