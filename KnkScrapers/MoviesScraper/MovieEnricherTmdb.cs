﻿using KnkScrapers.Utilities;
using MovieModel.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace KnkScrapers.MoviesScraper
{
    public partial class MovieEnricher
    {
        TMDbLib.Client.TMDbClient _client;
        Random random = new Random();
        string _usersessionid;

        private TMDbLib.Client.TMDbClient Client()
        {
            if (_client == null)
            {
                _client = new TMDbLib.Client.TMDbClient(ParametersTmdb.TmdbApikey);
                _client.DefaultLanguage = ParametersTmdb.Language;
            }
            return _client;
        }

        internal string UserSessionId()
        {
            if (string.IsNullOrEmpty(_usersessionid))
            {
                var session = Client().AuthenticationGetUserSessionAsync("Edheo", ParametersTmdb.Pwd).Result;
                _usersessionid = session.SessionId;
            }
            return _usersessionid;
        }

        private string GetRandomHexNumber(int digits)
        {
            byte[] buffer = new byte[digits / 2];
            random.NextBytes(buffer);
            string result = String.Concat(buffer.Select(x => x.ToString("X2")).ToArray());
            if (digits % 2 == 0)
                return result;
            return result + random.Next(16).ToString("X");
        }

        internal List<int> FindMovies(MovieModel.Entities.File aFile, bool aSingleResult)
        {
            KnkSolutionMoviesUtils.EnrichTitleYear(aFile);
            var lTitleNoYear = aFile.TitleSearch;
            var lYears = aFile.YearSearch;

            List<Titleyear> lItems = new List<Titleyear>();
            if (lYears.Split(';').Length > 0)
            {
                foreach (string lYear in lYears.Split(';'))
                    lItems.Add(new Titleyear() { Title = lTitleNoYear, Year = KnkSolutionMoviesUtils.ToNullableInt32(lYear) });
            }
            else
            {
                lItems.Add(new Titleyear() { Title = lTitleNoYear, Year = null });
            }
            return FindMovies(lItems, aSingleResult);
        }

        public int FindMovie(string aTitle, int? aYear)
        {
            var lLst = new List<Titleyear>();
            lLst.Add(new Titleyear() { Title = aTitle, Year = aYear });
            return FindMovies(lLst).FirstOrDefault();
        }

        internal List<int> FindMovies(List<Titleyear> aItems, bool aSingleResult=true)
        {
            var lLst = new List<int>();
            try
            {
                foreach (var lItm in aItems)
                {
                    Results.AddMessage("Finding", $"{lItm.Title} {lItm.Year}");
                    ReportProgress();
                    var movies = Client().SearchMovieAsync(lItm.Title, year: lItm.Year ?? 0).Result.Results;
                    var lAux = (from mov in movies orderby KnkScrapersUtils.LevenshteinDistance(lItm.Title, mov.Title) select mov);
                    foreach (var lMovie in lAux)
                    {
                        Results.AddMessage("Found", $"{lItm.Title} {lItm.Year}");
                        ReportProgress();
                        lLst.Add(lMovie.Id);
                        if (aSingleResult) break;
                    }
                }

                if (lLst.Count == 0)
                {
                    foreach (var lItm in aItems)
                    {
                        var movies = Client().SearchMovieAsync(lItm.Title, year: lItm.Year ?? 0).Result.Results;
                        var lAux = (from mov in movies orderby KnkScrapersUtils.LevenshteinDistance(lItm.Title, mov.Title) select mov);
                        foreach (var lMovie in lAux)
                        {
                            Results.AddMessage("Found", $"{lItm.Title} {lItm.Year}");
                            ReportProgress();
                            lLst.Add(lMovie.Id);
                            if (aSingleResult) break;
                        }
                    }
                }
            }
            catch (Exception lExc)
            {
                this.Results.AddMessage("Tmdb Scraper", "FindMovies", lExc.Message);
                ReportProgress();
            }
            return lLst;
        }

        public void FindEnrichMovie(int movie_id, MovieModel.Entities.File aFile, BackgroundWorker aWorker)
        {
            try
            {
                var lMovie = aFile.MovieFiles().Items.FirstOrDefault().IdMovie.Reference;
                if(lMovie!=null && !lMovie.IsNew())
                {
                    lMovie = MoviesRepo.FindMovie(lMovie.TmdbId.Value);
                }

                if (lMovie == null)
                {
                    lMovie = MoviesRepo.FindMovie(movie_id);
                }
                EnrichFile(movie_id, lMovie, aFile);
                SaveChanges();
            }
            catch (Exception lExc)
            {
                this.Results.AddMessage("Tmdb Scraper", "FindMovies", lExc.Message);
                ReportProgress();
            }
            return;// lRet;
        }

        public List<TMDbLib.Objects.Search.SearchMovie> GetRelatedMovies(MovieModel.Entities.Movie aMovie, int aCalllevel)
        {
            var lLst = new List<TMDbLib.Objects.Search.SearchMovie>();
            if (aMovie.TmdbId > 0 && aCalllevel<2)
            {
                var lSimilar = Client().GetMovieSimilarAsync(aMovie.TmdbId ?? 0).Result.Results;

                foreach (var lMov in lSimilar.Where(src => src.VoteCount > 50 && src.VoteAverage > 6))
                {
                    lLst.Add(lMov);
                }

                var aux = Client().GetMovieSimilarAsync(aMovie.TmdbId ?? 0, 1);
                var aux2 = aux.Result;
                lSimilar = aux2.Results;
                foreach (var lMov in lSimilar.Where(src => src.VoteCount > 50 && src.VoteAverage > 6))
                {
                    lLst.Add(lMov);
                }
                //var lGenres = aMovie.Genres().Items.Select(g => g.Genre.GenreName.ToLower());
                //var lDirectors = aMovie.Casting().Items.Where(c => c.IdCastingType.Reference.Department == "Directing" && c.IdPerson.Reference.TmdbId != null).Select(c => c.IdPerson.Reference);
                //foreach(var lDir in lDirectors)
                //{
                //    var lMovs = ScrapperClient.GetPersonMovies(int.Parse(lDir.TmdbId)).Result;
                //    foreach(var lCastMov in lMovs)
                //    {
                //        var lMovie = Client.SearchMovie(lCastMov.Id).Result;
                //        if (lMovie.Genres.Any(g => lGenres.Contains(g.Name.ToLower())))
                //            lLst.Add(lMovie);
                //    }
                //}

                if (lLst.Count() > 0)
                {
                    aMovie.Update("Searching Relations");
                    ReportProgress();
                    foreach (var lItem in lLst.Where(src=>src.VoteCount>50 && src.VoteAverage>6))
                    {
                        var lMov = EnrichMovie(lItem.Id, null, aCalllevel + 1, aMovie.Files().Count() > 0);
                        if (lMov!=null && (lMov.IsNew() || !aMovie.Extender.AlreadyExistsRelated(aMovie, lMov)))
                        {
                            var lMor = aMovie.Relateds().Create();
                            lMor.IdMovie = aMovie.IdSet == null ? aMovie : null;
                            lMor.IdSet = aMovie.IdSet?.Reference;
                            lMor.IdMovieRelated = lMov.IdSet == null ? lMov : null;
                            lMor.IdSetRelated = lMov.IdSet?.Reference;
                            lMor.Update("Scrap Relates Movie");
                        }
                    }
                    aMovie.Update("Related Movies");
                }
            }
            return lLst;
        }

        public List<TMDbLib.Objects.Movies.Movie> GetBestMovies()
        {
            var lLst = new List<TMDbLib.Objects.Movies.Movie>();
            int lPage = 0;
            var lBest = Client().GetMovieTopRatedListAsync(ParametersTmdb.Language).Result.Results;
            while (lLst.Count < 5)
            {
                Results.AddMessage("Best Movies", $"Page {lPage}/30");
                ReportProgress();
                foreach (var lMov in lBest)
                {
                    var lMovie = MoviesRepo.FindMovie(lMov.Id);
                    if (lMovie.IsNew() && (lMovie.TmdbId??0)!=0)
                    {
                        Results.AddMessage("Best Movies", $"Found {lMov.Title}");
                        ReportProgress();
                        lLst.Add(Client().GetMovieAsync(lMov.Id).Result);
                        if (lLst.Count >= 5) break;
                    }
                }
                if (lLst.Count < 5 && lPage < 30)
                    lBest = Client().GetMovieTopRatedListAsync(ParametersTmdb.Language, lPage++).Result.Results;
                if (lBest.Count() == 0 || lPage >= 30)
                    break;
            }
            Results.AddMessage("Bringing Movies", $"Found {lLst.Count}/5 Movies");
            ReportProgress();
            return lLst;
        }

        public struct Titleyear
        {
            public string Title;
            public int? Year;
        }

    }
}
