﻿using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using KnkInterfaces.PropertyAtributes;
using KnkInterfaces.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace KnkCore.Utilities
{
    public static class KnkCoreUtils
    {
        public static KnkConnectionItf GlobalConn;

        private static Rijndael Crypto(string aFile)
        {
            Rijndael lCrypto = Rijndael.Create();
            string lKey = "Knky16";
            while (lKey.Length < 16)
                lKey += lKey;
            lKey = lKey.Substring(1, 16);

            lCrypto.IV = ASCIIEncoding.ASCII.GetBytes(lKey);
            lCrypto.Key = ASCIIEncoding.ASCII.GetBytes(GetProcessorId());
            return lCrypto;
        }

        internal static CryptoStream ToCryptoStream(string aFile)
        {
            Stream lFileStream = new FileStream(aFile, FileMode.OpenOrCreate, FileAccess.Write);
            return new CryptoStream(lFileStream, Crypto(aFile).CreateEncryptor(), CryptoStreamMode.Write);
        }

        internal static CryptoStream FromCryptoStream(string aFile)
        {
            Stream lFileStream = new FileStream(aFile, FileMode.Open, FileAccess.Read);
            return new CryptoStream(lFileStream, Crypto(aFile).CreateDecryptor(), CryptoStreamMode.Read);
        }

        static string GetProcessorId()
        {
            //string lReturn = string.Empty;
            //ManagementClass managClass = new ManagementClass("win32_processor");
            //ManagementObjectCollection managCollec = managClass.GetInstances();

            //foreach (ManagementObject managObj in managCollec)
            //{
            //    lReturn = managObj.Properties["processorID"].Value.ToString();
            //    break;
            //}
            //return lReturn;
            return "BFEBFBFF000506E3";
        }

        public static IList<DataRow> ToList(DataTable table)
        {
            IList<DataRow> result = new List<DataRow>();

            foreach (DataRow row in table.Rows)
            {
                result.Add(row);
            }

            return result;
        }

        public static T DataColumnToValue<T>(object aValue)
        {
            if (Convert.IsDBNull(aValue))
                aValue = null;
            return (T)aValue;
        }

        public static string AppFileName()
        {
            string lCodeBase = Assembly.GetEntryAssembly().CodeBase;
            string[] lSplit = lCodeBase.Split('.');
            Array.Resize(ref lSplit, lSplit.Length - 1);
            return new Uri(string.Join(".", lSplit)).LocalPath;
        }

        internal static string ConfigFilename(Assembly aLibrary, string aExt)
        {
            var lAsm = new Uri(aLibrary.CodeBase).LocalPath;
            var lPath = Path.GetDirectoryName(lAsm);
            string lFile = $"{Path.GetFileNameWithoutExtension(lAsm)}.{aExt.ToLower()}";
            return Path.Combine(lPath, lFile);
        }

        internal static string CurrentDirectory()
        {
            string lCodeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder lUri = new UriBuilder(lCodeBase);
            string lPath = Uri.UnescapeDataString(lUri.Path);
            return Path.GetDirectoryName(lPath);
        }

        static string AppName()
        {
            return AppName(Assembly.GetEntryAssembly().CodeBase);
        }

        public static string AppDataFolder(string aFolder)
        {
            string lPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppName());
            if(!string.IsNullOrEmpty(aFolder))
                lPath= lPath = Path.Combine(lPath, aFolder);
            if (!Directory.Exists(lPath))
                Directory.CreateDirectory(lPath);
            return lPath;
        }

        public static string AppDataFolder()
        {
            return AppDataFolder(null);
        }

        static string AppName(string aFile)
        {
            string[] lNames = Path.GetFileName(aFile).Split('.');
            Array.Resize(ref lNames, lNames.Length - 1);
            return string.Join(".", lNames);
        }

        public static void CreateInParameter<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, KnkCriteriaItf aCriteria, string aField, string aDistinctField = "")
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            if (string.IsNullOrEmpty(aList.Criteria.Alias))
                aList.Criteria.Alias = $"SubQuery{aCriteria.GetParameters().Count.ToString().PadLeft(3, '0')}";
            aCriteria.AddParameter(typeof(string), aField, OperatorsEnu.In, aList.GetCommandListIds(aDistinctField));
        }

        //public static void CreateInParameter<Tdad, Tlst>(List<KnkEntityIdentifierItf> aList, KnkCriteriaItf<Tdad, Tdad> aCriteria, string aField)
        //    where Tdad : KnkItemItf, new()
        //    where Tlst : KnkItemItf, new()
        //{
        //    var lStr = KnkInterfacesUtils.ConcatStrings((from a in aList select a.Value.ToString()).ToList());
        //    aCriteria.AddParameter(typeof(string), aField, OperatorsEnu.In, lStr);
        //}

        public static void CreateNotInParameter<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, KnkCriteriaItf aCriteria, string aField, string aDistinctField = "")
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            if (string.IsNullOrEmpty(aList.Criteria.Alias))
                aList.Criteria.Alias = $"Sub{aCriteria.GetParameters().Count.ToString().PadLeft(3, '0')}";
            aCriteria.AddParameter(typeof(string), aField, OperatorsEnu.NotIn, aList.GetCommandListIds(aDistinctField));
        }

        //public static void CreateNotInParameter<Tdad, Tlst>(List<KnkEntityIdentifierItf> aList, KnkCriteriaItf<Tdad, Tdad> aCriteria, string aField)
        //    where Tdad : KnkItemItf, new()
        //    where Tlst : KnkItemItf, new()
        //{
        //    var lStr = KnkInterfacesUtils.ConcatStrings((from a in aList select a.Value.ToString()).ToList());
        //    aCriteria.AddParameter(typeof(string), aField, OperatorsEnu.NotIn, lStr);
        //}

        public static KnkCriteria<Tdad, Tlst> BuildLikeCriteria<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, string aField, string aValue, string aTable, string aFieldId)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return BuildCriteria<Tdad, Tlst>(aList, aField, OperatorsEnu.Like, aValue, aTable, aFieldId);
        }

        public static KnkCriteria<Tdad, Tlst> BuildCriteria<Tdad, Tlst>(KnkListItf<Tdad,Tlst> aList, string aField, OperatorsEnu aOperator, object aValue, string aTable, string aFieldId)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            KnkCriteria<Tdad, Tlst> lCri = new KnkCriteria<Tdad, Tlst>(aList, new KnkTableEntityRelation<Tdad>(aTable, aFieldId));
            lCri.AddParameter(typeof(string), aField, aOperator, aValue);
            return lCri;
        }

        public static KnkCriteria<Tdad, Tlst> BuildEqualCriteria<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, Type aType, string aField, object aValue)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            var tmp = new Tlst();
            return BuildCriteria<Tdad, Tlst>(aList, aField, OperatorsEnu.Equal, aValue, tmp.SourceEntity().SourceTable, tmp.PrimaryKey());
        }

        public static KnkCriteria<Tdad, Tlst> BuildEqualCriteria<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, Type aType, string aField, object aValue, string aTable, string aFieldId)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return BuildCriteria<Tdad, Tlst>(aList, aField, OperatorsEnu.Equal, aValue, aTable, aFieldId);
        }

        public static KnkCriteria<Tdad, Tlst> BuildGreatherThanCriteria<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, string aField, object aValue)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            KnkCriteria<Tdad, Tlst> lCri = new KnkCriteria<Tdad, Tlst>(aList);
            lCri.AddParameter(typeof(int), aField, OperatorsEnu.GreatThan, aValue);
            return lCri;
        }

        public static KnkCriteriaItf<Tdad, Tlst> BuildEqualCriteria<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, Type aType, string aField, object aValue)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            aCriteria.AddParameter(aType, aField, OperatorsEnu.Equal, aValue);
            return aCriteria;
        }

        public static KnkCriteriaItf<Tdad, Tlst> BuildIsNullCriteria<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, string aField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            aCriteria.AddParameter(typeof(int), aField, OperatorsEnu.IsNull);
            return aCriteria;
        }

        public static KnkCriteriaItf<Tdad, Tlst> BuildIsNotNullCriteria<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, string aField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            aCriteria.AddParameter(typeof(int), aField, OperatorsEnu.IsNotNull);
            return aCriteria;
        }

        public static KnkCriteriaItf<Tdad, Tlst> BuildGreatherThanCriteria<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, string aField, object aValue)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            aCriteria.AddParameter(typeof(int), aField, OperatorsEnu.GreatThan, aValue);
            return aCriteria;
        }

        public static KnkCriteria<Tdad, Tlst> BuildRelationCriteria<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, Tdad aItem, string aTableView, string aField = null)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            var lEnt = new Tlst().SourceEntity();
            if (string.IsNullOrEmpty(aField)) aField = aItem.PrimaryKey();
            KnkCriteria<Tdad, Tlst> lCri = new KnkCriteria<Tdad, Tlst>(aList, new KnkTableEntityRelation<Tdad>(aTableView, lEnt.TableBase));
            lCri.AddParameter(typeof(string), aField, OperatorsEnu.Equal, aItem.PropertyGet(aItem.PrimaryKey()));
            return lCri;
        }

        public static Titm CopyRecord<Titm>(KnkListItf aOwner, DataRow aRow)
            where Titm : KnkItemItf, new()
        {
            Titm lNewItem = new Titm();
            lNewItem.SetParent(aOwner);
            return CopyRecord<Titm>(lNewItem, aRow);
        }

        public static Titm CopyRecord<Titm>(Titm aItem, DataRow aRow)
            where Titm : KnkItemItf, new()
        {
            //Get Properties
            var lJoined = from prp in aItem.GetType().GetProperties()
                          join fld in aRow.Table.Columns.Cast<DataColumn>()
                          on prp.Name.ToLower() equals fld.ColumnName.ToLower()
                          where prp.CanWrite
                          select prp;

            foreach (PropertyInfo lPrp in lJoined)
            {
                //If property is generic list, continue
                if (lPrp.PropertyType.IsGenericType && lPrp.PropertyType.GetGenericTypeDefinition() == typeof(List<>)) continue;
                //Check for dbnull, return null if true, or convert to correct type
                dynamic lValue = Convert.IsDBNull(aRow[lPrp.Name]) ? null : ChangeType<Titm>(aRow[lPrp.Name], lPrp);
                lPrp.SetValue(aItem, lValue);
            }
            return aItem;
        }

        private static object ChangeType<Titm>(object aValue, PropertyInfo aPrp)
        {
            var lType = GetPropertyType(aPrp);
            var lIsReference = lType.FullName.Contains("KnkEntityReference");
            if (lIsReference)
                return Activator.CreateInstance(aPrp.PropertyType, (int)aValue);
            if (lType == typeof(KnkEntityIdentifier))
                return new KnkEntityIdentifier((int)aValue);
            else if (lType == typeof(KnkEntityIdentifierItf))
                return new KnkEntityIdentifier((int)aValue);
            else if (lType.IsEnum)
                return Enum.Parse(lType, aValue.ToString());
            else
            {
                return Convert.ChangeType(aValue, lType);
            }
        }

        public static PropertyInfo GetPrimaryKey<T>(T item)
            where T : KnkItemItf
        {
            return (from prp in KnkInterfacesUtils.GetProperties(item) where Attribute.IsDefined(prp, typeof(AtributePrimaryKey)) select prp).FirstOrDefault();
        }

        public static Type GetPropertyType(PropertyInfo aProperty)
        {
            return Nullable.GetUnderlyingType(aProperty.PropertyType) ?? aProperty.PropertyType;
        }

        public static int? ObjectToKnkInt(object aVal)
        {
            if (aVal == null)
                return null;

            KnkEntityIdentifier lVal = aVal as KnkEntityIdentifier;
            if (lVal != null) return lVal.Value;
            try
            {
                int lInt = (int)aVal;
                return lInt;
            }
            catch
            {
                return null;
            }
        }

        public static int? ObjectToKnkInt<Tref>(object aVal)
        where Tref : KnkItemItf, new()
        {
            return ObjectToKnkInt(aVal);
        }

        public static string CleanFileName(string aFileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(aFileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        public static string XmlSerializeObject<T>(T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static T XmlDeserializeFromString<T>(string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        private static object XmlDeserializeFromString(string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }

        public static string GetNameWithoutGenericArity(Type t)
        {
            string name = t.Name;
            int index = name.IndexOf('`');
            return index == -1 ? name : name.Substring(0, index);
        }
    }
}
