﻿using KnkInterfaces.Interfaces;
using System.Collections.Generic;

namespace KnkCore
{
    public class KnkChanges : KnkChangesItf
    {
        List<KnkChangeDescriptorItf> _messages = new List<KnkChangeDescriptorItf>();

        public KnkChangeDescriptorItf AddMessage(KnkItemItf aItem)
        {
            return AddMessage(new KnkChangeDescriptor(aItem) as KnkChangeDescriptorItf);
        }

        public KnkChangeDescriptorItf AddMessage(KnkChangeDescriptorItf aMessage)
        {
            Messages.Add(aMessage);
            return aMessage;
        }

        public void AddMessages(List<KnkChangeDescriptorItf> aMessages)
        {
            Messages.AddRange(aMessages);
        }

        public KnkChangeDescriptorItf AddMessage(string aAction, string aText)
        {
            return AddMessage(aAction, aText, string.Empty);
        }

        public KnkChangeDescriptorItf AddMessage(string aAction, string aText, string aMessage)
        {
            return AddMessage(new KnkChangeDescriptor(aAction, aText, aMessage));
        }


        public void Clear()
        {
            _messages.Clear();
        }

        public List<KnkChangeDescriptorItf> Messages
        {
            get
            {
                return _messages;
            }
        }

        public KnkChangeDescriptorItf UpdateMessage(KnkItemItf aItem, string aMessage)
        {
            KnkItemItf lItm = aItem;
            var lFound = Messages?.Find(m => m.Item == lItm);
            if (Messages != null)
            {
                if (lFound == null)
                {
                    lFound = AddMessage(aItem);
                }
                lFound.UpdateMessage(aItem.Status.ToString(), aMessage);
            }
            return lFound;
        }

    }
}
