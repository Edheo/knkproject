﻿using KnkInterfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Classes;

namespace KnkCore
{
    [Serializable]
    public class KnkList<Tdad, Tlst> : KnkListItf<Tdad, Tlst>
        where Tdad : KnkItemItf, new()
        where Tlst : KnkItemItf, new()
    {
        public KnkConnectionItf Connection { get; set; }
        KnkCriteriaItf<Tdad, Tlst> _Criteria;
        Tdad _Parent;
        ConcurrentList<Tlst> _List = null;
        List<Tlst> _Deleted = null;
        KnkChangesItf _Changes = new KnkChanges();

        public KnkList(KnkConnectionItf aConnection)
        {
            Connection = aConnection;
        }

        public KnkList(Tdad aParent)
        {
            _Parent = aParent;
            Connection = aParent?.Connection();
        }

        public KnkList(KnkConnectionItf aConnection, List<Tlst> aList) : this(aConnection)
        {
            Items = new ConcurrentList<Tlst>(aList);
        }

        public KnkList(KnkConnectionItf aConnection, KnkCriteriaItf<Tdad, Tlst> aCriteria) : this(aConnection)
        {
            _Criteria = aCriteria;
        }

        public int Count()
        {
            return Items.Count;
        }

        public ConcurrentList<Tlst> Items
        {
            get
            {
                if(_List==null)
                {
                    _List = new ConcurrentList<Tlst>(Connection.GetList(Criteria));
                }
                return _List;
            }
            set
            {
                _List = value;
            }
        }

        public List<Tlst> DeletedItems
        {
            get
            {
                if (_Deleted == null)
                {
                    var lTyp = new Tlst();
                    KnkTableEntityItf lTbl = new KnkTableEntity(lTyp.SourceEntity().TableBase, lTyp.SourceEntity().TableBase);
                    var lCri = new KnkCriteria<Tdad, Tlst>(this, lTbl);
                    lCri.AddParameter(typeof(bool), "Deleted", OperatorsEnu.Distinct, 0);
                    _Deleted = Connection.GetList(lCri);
                }
                return _Deleted;
            }
        }

        public List<KnkSortAttribute> Sorting { get; set; }

        public string InfoValue { get; set; }

        public KnkChangesItf Changes
        {
            get
            {
                return _Changes;
            }

            set
            {
                _Changes = value;
            }
        }

        public virtual List<Tlst> Datasource(int? page = null, int? elements = null)
        {
            List<Tlst> lRet = new List<Tlst>();
            if (page == null || elements == null)
            {
                IOrderedEnumerable<Tlst> lOrdered = null;
                if (Sorting!=null)
                {
                    foreach (var sortitm in Sorting)
                    {
                        if (!sortitm.Ascendent)
                        {
                            if (lOrdered == null)
                                lOrdered = Items.OrderByDescending(itm => itm.PropertyGet(sortitm.SortProperty));
                            else
                                lOrdered = lOrdered.ThenByDescending(itm => itm.PropertyGet(sortitm.SortProperty));
                        }
                        else
                        {
                            if (lOrdered == null)
                                lOrdered = Items.OrderBy(itm => itm.PropertyGet(sortitm.SortProperty));
                            else
                                lOrdered = lOrdered.ThenBy(itm => itm.PropertyGet(sortitm.SortProperty));
                        }
                    }
                }
                if (lOrdered != null)
                    lRet = lOrdered.ToList();
                else
                    lRet = Items.ToList();
            }
            else
            {
                int start = elements.Value * (page.Value - 1);
                int end = elements.Value * page.Value;

                lRet = (from m in Datasource() select m).Take(end).Reverse().Take(elements.Value).Reverse().ToList();
            }
            return lRet;
        }

        public KnkSortAttribute SortAttribute
        {
            get
            {
                return Sorting?.FirstOrDefault();
            }
            set
            {
                if(value!=null)
                {
                    if (Sorting == null) Sorting = new List<KnkSortAttribute>();
                    if (Sorting.Count == 0) Sorting.Add(value);
                    else Sorting[0] = value;
                }
            }
        }

        public KnkCriteriaItf<Tdad, Tlst> Criteria
        {
            get
            {
                if (_Criteria == null)
                {
                    KnkItemItf lDad = new Tdad();
                    lDad.SetParent(this);
                    _Criteria = new KnkCriteria<Tdad, Tlst>(this);
                }
                return _Criteria;
            }
            set
            {
                _Criteria = value;
                if (_Criteria != null) _Criteria.Parent = this;
            }
        }

        List<KnkItemItf> KnkListItf.Items => this.Items.Cast<KnkItemItf>().ToList();

        public List<KnkEntityIdentifierItf> GetListIds(List<Tlst> aItems)
        {
            var lLst = aItems.Select(itm => itm.PropertyGet(itm.PrimaryKey()) as KnkEntityIdentifierItf);
            return lLst.ToList();
        }

        public void Add(Tlst aItem, string aMessage)
        {
            aItem.Update(aMessage);
            this.Items.Add(aItem);
        }

        public virtual bool SaveChanges()
        {
            bool lRet = SaveChanges(ItemsChanged());
            if (lRet) Refresh();
            return lRet;
        }

        public bool SaveChanges(KnkItemItf aItem)
        {
            var lChanges = new List<Tlst>();
            lChanges.Add((Tlst)aItem);
            return SaveChanges(lChanges);
        }

        public bool SaveChanges(List<Tlst> aList)
        {
            var lChanges = (from itm in aList where itm.Status!=UpdateStatusEnu.NoChanges orderby itm.ModifiedDate select itm).ToList();
            Connection.SaveData(lChanges);
            Refresh();
            return true;
        }


        public List<Tlst> ItemsChanged()
        {
            return ItemsChanged(Items);
        }

        public List<Tlst> ItemsChanged(ConcurrentList<Tlst> aList)
        {
            return (from itm in aList where itm.Status != UpdateStatusEnu.NoChanges orderby itm.ModifiedDate select itm).ToList();
        }

        public void Refresh()
        {
            _List = null;
            _Deleted = null;
        }

        public Tlst Create(bool aAddToList = true)
        {
            Tlst lItem = new Tlst();
            lItem.SetParent(this);
            if (aAddToList)
            {
                Items.Add(lItem);
            }
            return lItem;
        }

        public void DeleteAll(string aMessage)
        {
            foreach(var lItm in Items)
            {
                lItm.Delete(aMessage);
            }
        }

        public Tdad GetParent()
        {
            return _Parent;
        }

        public object GetCommandListIds(string aDistinctField)
        {
            return Connection.GetCommandListIds(Criteria, aDistinctField);
        }
    }

    public class KnkList<Tlst> : KnkList<Tlst, Tlst>
        where Tlst : KnkItemItf, new()
    {
        public KnkList(KnkConnectionItf aConnection) : base(aConnection)
        {
        }

        public KnkList(KnkConnectionItf aConnection, List<Tlst> aList) : base(aConnection, aList)
        {
        }

        public KnkList(KnkConnectionItf aConnection, KnkCriteriaItf<Tlst, Tlst> aCriteria) : base(aConnection, aCriteria)
        {
        }

        public KnkList(Tlst aParent):base(aParent)
        {
        }
    }

}
