﻿using KnkInterfaces.Interfaces;
using System;

namespace KnkCore
{
    public class KnkEntityReference<Tref> : KnkEntityIdentifier, KnkEntityReferenceItf<Tref>
        where Tref : class, KnkItemItf, new()
    {
        private Func<int?, Tref> Load { get; set; }

        public KnkEntityReference(int? aValue)
        : this(aValue, (new Tref()).Connection().GetItem<Tref>)
        {
        }

        public KnkEntityReference(Tref aItem)
        : this(aItem, (new Tref()).Connection().GetItem<Tref>)
        {
        }

        public KnkEntityReference(int? aValue, Func<int?, Tref> aLoad) 
        : base(aValue)
        {
            Load = aLoad;
        }

        public KnkEntityReference(Tref aItem, Func<int?, Tref> aLoad) 
        : base()
        {
            Load = aLoad;
            Reference = aItem;
        }

        #region member variables

        public override int? Value
        {
            get
            {
                if (base.Reference != null && !base.Reference.IsNew()) base.Value = base.Reference.PrimaryKeyValue().Value;
                return base.Value;
            }

            set
            {
                Release();
                base.Value = value;
            }
        }

        public new Tref Reference
        {
            get
            {
                if (Value != null && base.Reference == null && Load != null) base.Reference = Load(Value);
                return (Tref)base.Reference;
            }

            private set
            {
                Release();
                base.Reference = value;
                base.Value = this.Value;
            }
        }
        #endregion

        private void Release()
        {
            base.Value = null;
            base.Reference = default(Tref);
        }

        public override string ToString()
        {
            return Reference?.ToString();
        }

        public static implicit operator KnkEntityReference<Tref>(int value)
        {
            return new KnkEntityReference<Tref>(value);
        }

        public static implicit operator KnkEntityReference<Tref>(int? value)
        {
            return new KnkEntityReference<Tref>(value);
        }

        public static implicit operator int(KnkEntityReference<Tref> value)
        {
            return (int?)value?.Value ?? 0;
        }

        public static implicit operator int?(KnkEntityReference<Tref> value)
        {
            return value?.Value;
        }

        public static implicit operator KnkEntityReference<Tref>(Tref value)
        {
            return new KnkEntityReference<Tref>(value);
        }

        public static implicit operator Tref(KnkEntityReference<Tref> value)
        {
            return value.Reference;
        }

        public override bool Equals(object obj)
        {
            var valueobj = obj as Tref;
            if (valueobj != null)
            {
                var value = valueobj.PrimaryKeyValue();
                if (value?.Value == null && Reference != null)
                    return Reference?.Equals(valueobj) ?? false;
                else
                    return Value.Equals(value?.Value);
            }
            else
                return Value?.Equals(obj) ?? false;
        }

        public override int GetHashCode()
        {
            if (base.Reference == null)
                return Value.GetHashCode();
            else
                return Reference.GetHashCode();
        }

    }
}
