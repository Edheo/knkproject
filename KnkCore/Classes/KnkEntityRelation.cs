﻿using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;

namespace KnkCore
{
    public class KnkEntityRelation<Tdad, Titm> : KnkList<Tdad, Titm>, KnkEntityRelationItf<Tdad, Titm>
        where Tdad : KnkItemItf, new()
        where Titm : KnkItemItf, new()
    {
        public KnkEntityRelation(Tdad aItem)
        :this(aItem, (new Titm()).SourceEntity().SourceTable)
        {
        }

        public KnkEntityRelation(Tdad aItem, string aRelatedView, string aField = null)
        : base(aItem.Connection())
        {
            var lCri = KnkCore.Utilities.KnkCoreUtils.BuildRelationCriteria(this, aItem, aRelatedView, aField);
            this.Criteria = lCri;
        }
    }
}
