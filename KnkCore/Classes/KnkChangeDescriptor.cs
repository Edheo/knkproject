﻿using KnkInterfaces.Interfaces;
using System;
using System.ComponentModel;

namespace KnkCore
{
    public class KnkChangeDescriptor : KnkChangeDescriptorItf
    {
        public KnkChangeDescriptor(KnkItemItf aItm)
        {
            Item = aItm;
            IdValue = Item.PropertyGet(aItm.PrimaryKey()) as KnkEntityIdentifier;
            CreationDate = aItm.CreationDate;
            DeletedDate = aItm.DeletedDate;
            ModifiedDate = aItm.ModifiedDate;
            Action = aItm.Status.ToString();
            Text = aItm.ToString();
            Message = aItm.UpdateMessage();
        }

        internal KnkChangeDescriptor(string aAction, string aText, string aMessage)
        {
            CreationDate = DateTime.Now;
            Action = aAction;
            Message = aMessage;
            Text = aText;
        }

        [Browsable(false)]
        public KnkItemItf Item { get; }
        [Browsable(false)]
        public KnkEntityIdentifierItf IdValue { get; }
        public string Object
        {
            get
            {
                return Item?.GetType().Name;
            }
        }

        public DateTime? CreationDate { get; }
        public DateTime? ModifiedDate { get; private set; }

        [Browsable(false)]
        public DateTime? DeletedDate { get; }

        public string Action { get; private set; }
        public string Text { get; private set; }
        public string Message { get; private set; }

        public void UpdateMessage(string aAction, string aMessage)
        {
            Action = aAction;
            UpdateMessage(aMessage);
        }

        public void UpdateMessage(string aMessage)
        {
            Message = aMessage;
            ModifiedDate = DateTime.Now;
        }
    }
}
