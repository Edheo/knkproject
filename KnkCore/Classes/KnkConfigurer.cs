﻿using KnkInterfaces.Interfaces;
using System;
using System.Linq;
using KnkInterfaces.Enumerations;
using System.Data;
using System.IO;
using KnkCore.Utilities;
using KnkInterfaces.Utilities;
using System.Collections.Generic;
using System.Configuration;

namespace KnkCore
{
    [Serializable]
    public class KnkConfigurer : KnkConfigurationItf
    {

        public KnkConfigurer()
        {
            ReadConfig();
        }

        public ConnectionTypeEnu ConnectionType { get; set; } = ConnectionTypeEnu.SqlServer;

        public string Database { get; set; }

        public virtual string Name { get; set; }

        public string Password { get; set; }

        public string ServerPath { get; set; }

        public string User { get; set; }

        public string MediaFolder { get; set; } = "Media";

        public bool IsConfigured()
        {
            return !string.IsNullOrEmpty(ServerPath);
        }

        public KnkDataItf CreateConnection(string aConnectionName)
        {
            KnkDataItf _Conection;

            switch (this.ConnectionType)
            {
                case ConnectionTypeEnu.SqlServer:
                    var connectionString = ConfigurationManager.ConnectionStrings[aConnectionName].ConnectionString;
                    if(!string.IsNullOrEmpty(connectionString))
                        _Conection = new KnkDataSqlServer.Connection.KnkSqlConnection(connectionString);
                    else
                        _Conection = new KnkDataSqlServer.Connection.KnkSqlConnection(this);
                    break;
                default:
                    throw new Exception($"Connection type {ConnectionType} not implemented");
            }
            return _Conection;
        }

        private void ReadConfig()
        {
            DataSet lDts = new DataSet();
            var lFile = KnkCoreUtils.ConfigFilename(this.GetType().Assembly, "knk");
            if (File.Exists(lFile))
            {
                Stream lStream = KnkCoreUtils.FromCryptoStream(lFile);
                try
                {
                    lDts.ReadXml(lStream);
                    if (lDts.Tables.Count > 0)
                    {
                        LoadConfigurationValuesToDatamodelers(lDts.Tables[0]);
                    }
                }
                finally
                {
                    lStream.Close();
                    lStream = null;
                }
            }
        }

        public void WriteConfig()
        {
            DataSet lDts = new DataSet();
            DataTable lTbl = KnkInterfacesUtils.CreateDataTable<KnkConfigurer>(new List<KnkConfigurer> { this });
            lDts.Tables.Add(lTbl);
            var lFile = KnkCoreUtils.ConfigFilename(this.GetType().Assembly, "knk");
            if (File.Exists(lFile)) File.Delete(lFile);
            using (var lCryp = KnkCoreUtils.ToCryptoStream(lFile))
            {
                lDts.WriteXml(lCryp, XmlWriteMode.WriteSchema);
            }
            ReadConfig();
        }



        private void LoadConfigurationValuesToDatamodelers(DataTable aTable)
        {
            var lRow = KnkCoreUtils.ToList(aTable).FirstOrDefault();
            Name = KnkCoreUtils.DataColumnToValue<string>(lRow["Name"]);
            ConnectionType = (ConnectionTypeEnu)KnkCoreUtils.DataColumnToValue<int>(lRow["ConnectionType"]);
            ServerPath = KnkCoreUtils.DataColumnToValue<string>(lRow["ServerPath"]);
            Database = KnkCoreUtils.DataColumnToValue<string>(lRow["Database"]);
            User = KnkCoreUtils.DataColumnToValue<string>(lRow["User"]);
            Password = KnkCoreUtils.DataColumnToValue<string>(lRow["Password"]);
        }
    }
}
