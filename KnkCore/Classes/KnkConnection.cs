﻿using KnkCore.Utilities;
using KnkInterfaces.Interfaces;
using KnkInterfaces.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;

namespace KnkCore
{
    public class KnkConnection : KnkConnectionItf
    {
        KnkItemItf _CurrentUser;
        ConcurrentDictionary<string, KnkConfigurer> _Configurers = new ConcurrentDictionary<string, KnkConfigurer>();

        public KnkConnection(bool aTest)
        {
            if (!aTest && KnkCoreUtils.GlobalConn?.CurrentUser != null)
                this.Login(KnkCoreUtils.GlobalConn.CurrentUser);
        }

        public KnkConnection() : this(false)
        {
        }

        KnkDataItf GetConnection(Type aType)
        {
            
            return GetConfigurer(aType).CreateConnection("MoviesDb");
        }

        KnkDataItf GetConnectionDefault()
        {
            return GetConfigurerDefault().CreateConnection("MoviesDb");
        }

        public string GetMediaFolder(Type aType)
        {
            return KnkCoreUtils.AppDataFolder(GetConfigurer(aType).MediaFolder);
        }

        public bool CheckConfiguration()
        {
            foreach(var lCon in LoadConfigurers())
            {
                if (!lCon.IsConfigured())
                    return false;
            }
            return true;
        }

        public List<KnkConfigurer> LoadConfigurers()
        {
            var lTypesList = new List<Type>();
            var lDir = new DirectoryInfo(KnkCoreUtils.CurrentDirectory());

            foreach (var lFil in lDir.GetFiles("*.dll"))
            {
                try
                {
                    var lAsm = Assembly.LoadFile(lFil.FullName);
                    lTypesList.AddRange(lAsm.GetTypes().ToList().FindAll(t => t.GetInterfaces().Contains(typeof(KnkDataModelerItf))));
                }
                catch
                {

                }
            }
            var Configurers = lTypesList.ConvertAll(t => Activator.CreateInstance(t) as KnkConfigurer);

            return Configurers;
        }

        private KnkConfigurer GetConfigurerDefault() => _Configurers.FirstOrDefault().Value;

        private KnkConfigurer GetConfigurer(Type aType)
        {
            var lAsm = aType.Assembly.CodeBase;
            KnkConfigurer lRet = null;
            if (!_Configurers.Keys.Contains(lAsm))
            {
                var lType = aType.Assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(KnkDataModelerItf))).FirstOrDefault();
                lRet = Activator.CreateInstance(lType) as KnkConfigurer;
                _Configurers.TryAdd(lAsm, lRet);
            }
            else
                lRet = _Configurers[lAsm];
            return lRet;
        }

        public T GetItem<T>(int? aEntityId) where T : KnkItemItf, new()
        {
            return (T)GetItem(typeof(T), aEntityId);
        }

        public KnkItemItf GetItem(Type atype, int? aEntityId)
        {
            dynamic lItm = Activator.CreateInstance(atype);
            lItm.PropertySet(lItm.PrimaryKey(), aEntityId);
            return ReadItem(lItm);
        }


        public T ReadItem<T>(T aItm) where T : KnkItemItf, new()
        {
            KnkCriteria<T, T> lCri = KnkCoreUtils.BuildEqualCriteria<T, T>(aItm.GetParent<T, T>(), aItm.PropertyInfo(aItm.PrimaryKey()).PropertyType, aItm.PrimaryKey(), aItm.PropertyGet(aItm.PrimaryKey()));
            var lItm = GetList<T, T>(lCri).FirstOrDefault();
            return lItm;
        }

        private List<Tlst> ListFromDataTable<Tdad, Tlst>(KnkListItf<Tdad, Tlst> aList, DataTable aTable)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return KnkCoreUtils.ToList(aTable).Select(row => KnkCoreUtils.CopyRecord<Tlst>(aList, row)).ToList();
        }

        //public List<KnkEntityIdentifierItf> GetListIds<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
        //    where Tdad : KnkItemItf, new()
        //    where Tlst : KnkItemItf, new()
        //{
        //    var lLst = new List<KnkEntityIdentifierItf>();
        //    using (var lDat = GetConnection(typeof(Tdad)).GetListIds<Tdad, Tlst>(aCriteria))
        //    {
        //        foreach (DataRow lRow in lDat.Rows)
        //        {
        //            KnkEntityIdentifierItf lValue = new KnkEntityIdentifier((int)lRow[0]);
        //            lLst.Add(lValue);
        //        }
        //    }
        //    if(lLst.Count.Equals(0))
        //        lLst.Add(new KnkEntityIdentifier(-1));
        //    return lLst;
        //}

        public List<Tlst> GetList<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            List<Tlst> lLst;
            using (var lDat = GetConnection(typeof(Tlst)).GetData(aCriteria))
            {
                lLst = ListFromDataTable(aCriteria.Parent, lDat);
            };
            return lLst;
        }

        public IEnumerable<dynamic> GetData(string aQuery)
        {
            return GetConnectionDefault().GetData(aQuery);
        }

        public void SaveData<T>(List<T> aItems) where T : KnkItemItf, new()
        {
            var lDat = GetConnection(typeof(T));
            var lMessage = string.Empty;
            foreach (var lItm in aItems)
            {
                if (lItm.Status != KnkInterfaces.Enumerations.UpdateStatusEnu.NoChanges)
                {
                    var lRelations = KnkInterfacesUtils.GetProperties(lItm);
                    foreach (var lProperty in lRelations)
                    {
                        var lResult = (lProperty.GetValue(lItm) as KnkEntityIdentifierItf)?.Reference;
                        if (lResult != null)
                            if(lResult.IsNew())
                                lResult.SaveChanges();
                        lResult = null;
                    }

                    var lType = lItm.Status;
                    try
                    {
                        lMessage = lDat.SaveData(lItm);
                        (lItm as KnkItem).Status = KnkInterfaces.Enumerations.UpdateStatusEnu.NoChanges;
                    }
                    catch (Exception lExc)
                    {
                        lMessage = lExc.Message;
                    }
                    lItm.GetParent()?.Changes.UpdateMessage(lItm, lMessage);
                    if (lItm.Status == KnkInterfaces.Enumerations.UpdateStatusEnu.NoChanges)
                    {
                        foreach (var lProperty in lRelations)
                        {
                            var lResult = (lProperty.GetValue(lItm) as KnkEntityIdentifierItf)?.Reference;
                            if (lResult != null)
                                if (lResult.IsChanged() && !lResult.IsNew() && lResult.Status != KnkInterfaces.Enumerations.UpdateStatusEnu.NoChanges)
                                    lResult.SaveChanges();
                            lResult = null;
                        }
                        //If Saved check if it has chilrens
                        var lMethods = KnkInterfacesUtils.GetMethodsRelations(lItm);
                        foreach (var lMethod in lMethods)
                        {
                            var lResult = lMethod.Invoke(lItm, null) as KnkListItf;
                            if (lResult != null)
                            {
                                lResult.SaveChanges();
                            }
                            lResult = null;
                        }
                    }
                }
            }
        }

        public KnkItemItf CurrentUser
        {
            get
            {
                return _CurrentUser;
            }

            set
            {
                Login(value);
            }
        }

        public KnkItemItf Login(KnkItemItf aUser)
        {
            _CurrentUser = aUser;
            if (CurrentUser != null) KnkCoreUtils.GlobalConn = this;
            return CurrentUser;
        }

        public object GetCommandListIds<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, string aDistinctField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return GetConnection(typeof(Tdad)).GetCommandListIds<Tdad, Tlst>(aCriteria, aDistinctField);
        }

    }
}
