﻿using KnkInterfaces.Interfaces;
using KnkInterfaces.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KnkDataSqlServer.Utilities
{
    static class KnkSqlServer
    {
        internal static SqlConnection ConnectionBuilder(string aConnectionString)
        {
            //SqlConnectionStringBuilder lConBui = new SqlConnectionStringBuilder();
            //lConBui.DataSource = aServer;
            //lConBui.InitialCatalog = aDatabase;
            //if (string.IsNullOrEmpty(aUser))
            //    lConBui.IntegratedSecurity = true;
            //else
            //{
            //    lConBui.UserID = aUser;
            //    lConBui.Password = aPassword;
            //}
            return new SqlConnection(aConnectionString);
        }
        internal static SqlConnection ConnectionBuilder(string aServer, string aDatabase, string aUser, string aPassword)
        {
            SqlConnectionStringBuilder lConBui = new SqlConnectionStringBuilder();
            lConBui.DataSource = aServer;
            lConBui.InitialCatalog = aDatabase;
            if (string.IsNullOrEmpty(aUser))
                lConBui.IntegratedSecurity = true;
            else
            {
                lConBui.UserID = aUser;
                lConBui.Password = aPassword;
            }
            return new SqlConnection(lConBui.ConnectionString);
        }

        internal static string GetDynamicSelect<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return GetDynamicSelect(aCriteria, false, string.Empty);
        }

        internal static string OrderBySentence<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            string lSort = null;
            if (aCriteria.TopElements != null && aCriteria.Parent.Sorting!=null && aCriteria.Parent.Sorting.Count()>0)
            {
                foreach (var sortitm in aCriteria.Parent.Sorting)
                {
                    if (lSort == null)
                    {
                        lSort = $"Order By [{sortitm.SortProperty}]";
                        if (!sortitm.Ascendent) lSort = $"{lSort} desc";
                    }
                    else
                    {
                        lSort = $"{lSort}, [{sortitm.SortProperty}]";
                        if (!sortitm.Ascendent) lSort = $"{lSort} desc";
                    }
                }
            }
            return lSort;
        }

        internal static string GetDynamicSelect<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, bool aDistinct, string aDistinctField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            string lTable = aCriteria.EntityTable().SourceTable;
            string lTop = aCriteria.TopElements != null ? $" Top {aCriteria.TopElements}" : null;

            var lSel = string.Empty;
            if (aDistinct)
            {
                string lKey = aDistinctField;
                if (string.IsNullOrEmpty(aDistinctField))
                    lKey = aCriteria.EntityRelation() != null ? aCriteria.EntityRelation().RelatedKey : (new Tdad()).PrimaryKey();
                lSel = $"Select Distinct [{lKey}] From [{lTable}]";
            }
            else
                lSel = GetSimpleTableSelect(lTable, lTop, false);
            if (aCriteria != null) lSel += GetWhereFromParameters(aCriteria);
            return lSel;
        }

        internal static string GetSimpleTableSelect(string aTable, string aTop = null)
        {
            return GetSimpleTableSelect(aTable, aTop, false);
        }

        internal static string GetSimpleTableSelect(string aTable, bool aNullResult)
        {
            return GetSimpleTableSelect(aTable, null, aNullResult);
        }

        internal static string GetSimpleTableSelect(string aTable, string aTop, bool aNullResult)
        {
            var lwhere = aNullResult ? " Where 1=0" : null;
            return $"Select {aTop} * From [{aTable}] {lwhere}";
        }

        internal static string GetWhereFromParameters<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            string lWhere = KnkInterfacesUtils.JoinParameters(aCriteria.GetParameters());
            return lWhere.Length > 0 ? " Where " + lWhere : string.Empty;
        }

        internal static string GetDynamicInsert<T>(SqlConnection aCon, T aItem)
            where T : KnkItemItf
        {
            return GetDynamicInsert(aItem, GetData(aCon, GetSimpleTableSelect(aItem.SourceEntity().TableBase,true)).Columns);
        }

        private static string GetDynamicInsert<T>(T aItem, DataColumnCollection aCols )
            where T : KnkItemItf
        {
            bool lAuto = aItem.PrimaryKeyAutoGenerated();
            string lPk = aItem.PrimaryKey();
            string lInsertTable = $"[{aItem.SourceEntity().TableBase}]";
            var lProperties = from prp in KnkInterfacesUtils.GetProperties<KnkItemItf>(aItem)
                              join fld in aCols.Cast<DataColumn>()
                              on prp.Name.ToLower() equals fld.ColumnName.ToLower()
                              where ((lAuto && prp.Name != lPk) || (!lAuto && prp.Name == lPk))
                                && aItem.PropertyGet(prp.Name)!=null
                                && !KnkInterfacesUtils.ModifiedFields().Contains(prp.Name.ToLower())
                                && !KnkInterfacesUtils.DeletedFields().Contains(prp.Name.ToLower())
                              select new { Property = $"[{prp.Name}]", Value = $"@{prp.Name} " };

            string lInsertFields = lProperties.Aggregate((i, j) => new { Property = $"{i.Property}, {j.Property}", Value=string.Empty }).Property;
            string lInsertValues = lProperties.Aggregate((i, j) => new { Property = string.Empty, Value = $"{i.Value}, {j.Value}" }).Value;

            string lOutput = string.Empty;
            if (lAuto && aItem.InsertOutput)
            {
                lOutput = $"OUTPUT INSERTED.[{aItem.PrimaryKey()}]";
            }
            string lResultInsert = $"Insert Into {lInsertTable} ({lInsertFields}) {lOutput} VALUES ({lInsertValues})";
            string lSelectWhere = string.Empty;
            if (lAuto)
            {
                lSelectWhere = $"{lResultInsert}";
            }
            else
            {
                lSelectWhere = $"{lResultInsert} Select @{lPk}";
            }
            return lSelectWhere;
        }

        internal static string GetDynamicUpdate<T>(SqlConnection aCon, T aItem)
            where T : KnkItemItf
        {
            return GetDynamicUpdate(aItem, GetData(aCon, GetSimpleTableSelect(aItem.SourceEntity().TableBase, true)).Columns);
        }

        private static string GetDynamicUpdate<T>(T aItem, DataColumnCollection aCols)
            where T : KnkItemItf
        {
            string lPk = aItem.PrimaryKey();
            string lUpdateTable = $"[{aItem.SourceEntity().TableBase}]";
            var lProperties = from prp in KnkInterfacesUtils.GetProperties<KnkItemItf>(aItem)
                              join fld in aCols.Cast<DataColumn>()
                              on prp.Name.ToLower() equals fld.ColumnName.ToLower()
                              where (prp.Name != lPk)
                                && !KnkInterfacesUtils.CreatedFields().Contains(prp.Name.ToLower())
                                && !KnkInterfacesUtils.DeletedFields().Contains(prp.Name.ToLower())
                              select $"[{prp.Name}] = @{prp.Name} ";

            string lUpdateFields = KnkInterfacesUtils.ConcatStrings(lProperties.ToList());
            string lWhereValues = $"[{lPk}] = @{lPk} ";

            return $"Update {lUpdateTable} Set {lUpdateFields} Where {lWhereValues}";
        }

        internal static string GetDynamicDelete<T>(SqlConnection aCon, T aItem)
            where T : KnkItemItf
        {
            return GetDynamicDelete(aItem, GetData(aCon, GetSimpleTableSelect(aItem.SourceEntity().TableBase, true)).Columns);
        }

        internal static string GetDynamicDelete<T>(T aItem, DataColumnCollection aCols)
            where T : KnkItemItf
        {
            string lPk = aItem.PrimaryKey();
            string lDeleteTable = $"[{aItem.SourceEntity().TableBase}]";
            var lProperties = from prp in KnkInterfacesUtils.GetProperties<KnkItemItf>(aItem)
                              join fld in aCols.Cast<DataColumn>()
                              on prp.Name.ToLower() equals fld.ColumnName.ToLower()
                              where (KnkInterfacesUtils.DeletedFields().Contains(prp.Name.ToLower()))
                              select $"[{prp.Name}] = @{prp.Name} ";
            string lWhereValues = $"[{lPk}] = @{lPk} ";

            if (lProperties.Count() > 0)
            {
                string lUpdateFields = KnkInterfacesUtils.ConcatStrings(lProperties.ToList());
                return $"Update {lDeleteTable} Set {lUpdateFields} Where {lWhereValues}";
            }
            else
                return $"Delete {lDeleteTable} Where {lWhereValues}";
        }

        internal static DataTable GetData(SqlConnection aCon, string aCommand)
        {
            return GetData(GetCommand(aCon, aCommand));
        }

        internal static DataTable GetData(SqlCommand aCommand)
        {
            DataTable lTbl = null;
            using (DataSet lDts = new DataSet())
            {
                new SqlDataAdapter(aCommand).Fill(lDts);
                lTbl = lDts.Tables.Cast<DataTable>().FirstOrDefault();
            }
            return lTbl;
        }

        internal static DataTable GetData<Tdad, Tlst>(SqlConnection aCon, KnkCriteriaItf<Tdad, Tlst> aCriteria)
                    where Tdad : KnkItemItf, new()
                    where Tlst : KnkItemItf, new()
        {
            return GetData(GetCommand(aCon, aCriteria));
        }

        public static SqlCommand GetCommand(SqlConnection aCon, string aCommand)
        {
            return new SqlCommand(aCommand, aCon);
        }

        internal static SqlCommand GetCommandListIds<Tdad, Tlst>(SqlConnection aCon, KnkCriteriaItf<Tdad, Tlst> aCriteria, string aDistinctField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return GetCommand(aCon, aCriteria, true, aDistinctField);
        }

        //internal static DataTable GetListIds<Tdad, Tlst>(SqlConnection aCon, KnkCriteriaItf<Tdad, Tlst> aCriteria)
        //    where Tdad : KnkItemItf, new()
        //    where Tlst : KnkItemItf, new()
        //{
        //    DataTable lTbl = null;
        //    using (DataSet lDts = new DataSet())
        //    {
        //        var lCmd = GetCommandListIds(aCon, aCriteria);
        //        new SqlDataAdapter(lCmd).Fill(lDts);
        //        lTbl = lDts.Tables[0];
        //    }
        //    return lTbl;
        //}

        internal static SqlCommand GetCommand<Tdad, Tlst>(SqlConnection aConnection, KnkCriteriaItf<Tdad, Tlst> aCriteria)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            return GetCommand(aConnection, aCriteria, false, string.Empty);
        }

        internal static SqlCommand GetCommand<Tdad, Tlst>(SqlConnection aConnection, KnkCriteriaItf<Tdad, Tlst> aCriteria, bool aDistinct, string aDistinctField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new()
        {
            string lCommand = KnkSqlServer.GetDynamicSelect(aCriteria, aDistinct, aDistinctField);

            var parameters = new List<SqlParameter>();
            if (aCriteria != null)
            {
                var lInParameters = (from par in aCriteria.GetParameters() where par.Operator == KnkInterfaces.Enumerations.OperatorsEnu.In || par.Operator == KnkInterfaces.Enumerations.OperatorsEnu.NotIn select par);
                foreach (KnkParameterItf lParameter in lInParameters)
                {
                    var cmd = lParameter.Value as SqlCommand;
                    if (cmd != null)
                    {
                        lCommand = lCommand.Replace($"@List[{aCriteria.Alias}{lParameter.ParameterName}]", cmd.CommandText);
                        foreach (SqlParameter par in cmd.Parameters)
                        {
                            parameters.Add(par);
                        }
                    }
                    else if (!string.IsNullOrEmpty(lParameter.Value))
                    {
                        lCommand = lCommand.Replace($"@List[{aCriteria.Alias}{lParameter.ParameterName}]", lParameter.Value);
                    }
                    else
                        lCommand = lCommand.Replace($"@List[{aCriteria.Alias}{lParameter.ParameterName}]", "null");
                }
            }

            lCommand = $"{lCommand} {OrderBySentence(aCriteria)}";

            var lRet = KnkSqlServer.GetCommand(aConnection, lCommand);
            foreach(var par in parameters)
            {
                lRet.Parameters.AddWithValue(par.ParameterName, par.Value);
            }
            var lExpectedParams = Regex.Matches(lCommand, @"\@\w+ ").Cast<Match>().Select(m => m.Value).ToList();
            lExpectedParams = (from par in lExpectedParams select par.Replace("@", "").TrimEnd().TrimStart()).ToList();

            if (aCriteria != null)
            {
                var lRestParameters = (from par in aCriteria.GetParameters() where !(par.Operator == KnkInterfaces.Enumerations.OperatorsEnu.In || par.Operator == KnkInterfaces.Enumerations.OperatorsEnu.NotIn) select par);

                foreach (string lParameter in lExpectedParams)
                {
                    KnkParameterItf lPar = (from par in lRestParameters where par.ParameterName.ToLower().Equals(lParameter.ToLower()) select par).FirstOrDefault();
                    if (lPar != null)
                    {
                        if(!lRet.Parameters.Contains(lParameter) && lPar.Value!=null)
                            lRet.Parameters.AddWithValue(lPar.ParameterName, lPar.Value);
                        else
                            lRet.Parameters.AddWithValue(lPar.ParameterName, lPar.Value);
                    }
                    else
                    {
                        lPar= (from par in lRestParameters where par.InnerParammerters.Where(inp=>inp.ParameterName.Equals(lParameter.ToLower())).Any() select par).FirstOrDefault();
                        if (lPar != null && lPar.InnerParammerters.Count > 0)
                        {
                            foreach (KnkParameterItf lSubParameter in lPar.InnerParammerters)
                            {
                                if (!lRet.Parameters.Contains(lSubParameter.ParameterName) && lSubParameter.Value!=null)
                                    lRet.Parameters.AddWithValue(lSubParameter.ParameterName, lSubParameter.Value);
                                else
                                    lRet.Parameters.AddWithValue(lSubParameter.ParameterName, lSubParameter.Value);
                            }
                        }
                    }
                }
            }

            var lChkBase = (from par in lRet.Parameters.Cast<SqlParameter>() select par.ParameterName).ToList();
            var lChk = (from par in lExpectedParams where !lChkBase.Contains(par) select par);
            if(lChk.Count()>0)
            {
                var lCua = lChk.Count();
            }
            return lRet;
        }


        internal static SqlCommand GetCommand<T>(SqlConnection aConnection, T aItem, string aCommand)
            where T : KnkItemItf
        {
            string lCommand = aCommand;

            var lExpectedParams = Regex.Matches(lCommand, @"\@\w+ ").Cast<Match>().Select(m => m.Value).ToList();
            lExpectedParams = (from par in lExpectedParams select par.Replace("@", "").TrimEnd().TrimStart()).ToList();

            var lRet = KnkSqlServer.GetCommand(aConnection, lCommand);
            foreach(var lPrp in KnkInterfacesUtils.GetProperties(aItem))
            {
                if(lCommand.Contains($"@{lPrp.Name} "))
                {
                    var lValue = aItem.PropertyGet(lPrp.Name);
                    var lKid = lValue as KnkEntityIdentifierItf;
                    if (lKid != null)
                    {
                        int? lInt = lKid.Value;
                        if (lInt!=null)
                            lRet.Parameters.AddWithValue(lPrp.Name, lInt);
                        else
                            lRet.Parameters.AddWithValue(lPrp.Name, DBNull.Value);
                    }
                    else
                        lRet.Parameters.AddWithValue(lPrp.Name, lValue ?? DBNull.Value);

                }

            }

            return lRet;
        }
    }
}
