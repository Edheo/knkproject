﻿namespace MovieControls.Usercontrols
{
    partial class MovieControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPlay = new System.Windows.Forms.Button();
            this.tabCrews = new MetroFramework.Controls.MetroTabControl();
            this.floCrew = new System.Windows.Forms.FlowLayoutPanel();
            this.floArt = new System.Windows.Forms.FlowLayoutPanel();
            this.floVideos = new System.Windows.Forms.FlowLayoutPanel();
            this.btnRelated = new System.Windows.Forms.Button();
            this.btnDelUser = new System.Windows.Forms.Button();
            this.btnDelFile = new System.Windows.Forms.Button();
            this.grdFiles = new MovieControls.Usercontrols.FilesControl();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.btnMovFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Transparent;
            this.btnPlay.BackgroundImage = global::MovieControls.Properties.Resources.YouTube_2_128;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlay.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Location = new System.Drawing.Point(646, 0);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Padding = new System.Windows.Forms.Padding(5);
            this.btnPlay.Size = new System.Drawing.Size(32, 29);
            this.btnPlay.TabIndex = 7;
            this.btnPlay.TabStop = false;
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Visible = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // tabCrews
            // 
            this.tabCrews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCrews.Location = new System.Drawing.Point(5, 5);
            this.tabCrews.Name = "tabCrews";
            this.tabCrews.Padding = new System.Drawing.Point(6, 8);
            this.tabCrews.Size = new System.Drawing.Size(358, 400);
            this.tabCrews.TabIndex = 7;
            this.tabCrews.UseSelectable = true;
            this.tabCrews.SelectedIndexChanged += new System.EventHandler(this.tabCrews_SelectedIndexChanged);
            // 
            // floCrew
            // 
            this.floCrew.AutoScroll = true;
            this.floCrew.BackColor = System.Drawing.Color.Black;
            this.floCrew.Location = new System.Drawing.Point(0, 183);
            this.floCrew.Name = "floCrew";
            this.floCrew.Size = new System.Drawing.Size(344, 230);
            this.floCrew.TabIndex = 6;
            // 
            // floArt
            // 
            this.floArt.AutoScroll = true;
            this.floArt.BackColor = System.Drawing.Color.Black;
            this.floArt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.floArt.Location = new System.Drawing.Point(5, 5);
            this.floArt.Name = "floArt";
            this.floArt.Size = new System.Drawing.Size(358, 400);
            this.floArt.TabIndex = 8;
            // 
            // floVideos
            // 
            this.floVideos.AutoScroll = true;
            this.floVideos.BackColor = System.Drawing.Color.Black;
            this.floVideos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.floVideos.Location = new System.Drawing.Point(5, 5);
            this.floVideos.Name = "floVideos";
            this.floVideos.Size = new System.Drawing.Size(358, 400);
            this.floVideos.TabIndex = 7;
            // 
            // btnRelated
            // 
            this.btnRelated.BackColor = System.Drawing.Color.Transparent;
            this.btnRelated.BackgroundImage = global::MovieControls.Properties.Resources.btnScan_ResourceImage;
            this.btnRelated.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRelated.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRelated.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRelated.Location = new System.Drawing.Point(614, 0);
            this.btnRelated.Name = "btnRelated";
            this.btnRelated.Padding = new System.Windows.Forms.Padding(5);
            this.btnRelated.Size = new System.Drawing.Size(32, 29);
            this.btnRelated.TabIndex = 8;
            this.btnRelated.TabStop = false;
            this.btnRelated.UseVisualStyleBackColor = false;
            this.btnRelated.Click += new System.EventHandler(this.btnRelated_Click);
            // 
            // btnDelUser
            // 
            this.btnDelUser.BackColor = System.Drawing.Color.Transparent;
            this.btnDelUser.BackgroundImage = global::MovieControls.Properties.Resources._1472645271_user_delete;
            this.btnDelUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDelUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelUser.Location = new System.Drawing.Point(32, 0);
            this.btnDelUser.Name = "btnDelUser";
            this.btnDelUser.Padding = new System.Windows.Forms.Padding(5);
            this.btnDelUser.Size = new System.Drawing.Size(32, 29);
            this.btnDelUser.TabIndex = 9;
            this.btnDelUser.TabStop = false;
            this.btnDelUser.UseVisualStyleBackColor = false;
            this.btnDelUser.Click += new System.EventHandler(this.btnDelUser_Click);
            // 
            // btnDelFile
            // 
            this.btnDelFile.BackColor = System.Drawing.Color.Transparent;
            this.btnDelFile.BackgroundImage = global::MovieControls.Properties.Resources._1472645344_delete_file;
            this.btnDelFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelFile.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDelFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelFile.Location = new System.Drawing.Point(64, 0);
            this.btnDelFile.Name = "btnDelFile";
            this.btnDelFile.Padding = new System.Windows.Forms.Padding(5);
            this.btnDelFile.Size = new System.Drawing.Size(32, 29);
            this.btnDelFile.TabIndex = 10;
            this.btnDelFile.TabStop = false;
            this.btnDelFile.UseVisualStyleBackColor = false;
            this.btnDelFile.Click += new System.EventHandler(this.btnDelFile_Click);
            // 
            // grdFiles
            // 
            this.grdFiles.Buttons = null;
            this.grdFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdFiles.Location = new System.Drawing.Point(0, 0);
            this.grdFiles.Name = "grdFiles";
            this.grdFiles.Size = new System.Drawing.Size(437, 413);
            this.grdFiles.TabIndex = 0;
            this.grdFiles.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.grdFiles.UseSelectable = true;
            // 
            // btnAddUser
            // 
            this.btnAddUser.BackColor = System.Drawing.Color.Transparent;
            this.btnAddUser.BackgroundImage = global::MovieControls.Properties.Resources._1483911157_user_add;
            this.btnAddUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddUser.Location = new System.Drawing.Point(0, 0);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Padding = new System.Windows.Forms.Padding(5);
            this.btnAddUser.Size = new System.Drawing.Size(32, 29);
            this.btnAddUser.TabIndex = 11;
            this.btnAddUser.TabStop = false;
            this.btnAddUser.UseVisualStyleBackColor = false;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // btnMovFile
            // 
            this.btnMovFile.BackColor = System.Drawing.Color.Transparent;
            this.btnMovFile.BackgroundImage = global::MovieControls.Properties.Resources._1484540019_import;
            this.btnMovFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMovFile.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMovFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMovFile.Location = new System.Drawing.Point(96, 0);
            this.btnMovFile.Name = "btnMovFile";
            this.btnMovFile.Padding = new System.Windows.Forms.Padding(5);
            this.btnMovFile.Size = new System.Drawing.Size(32, 29);
            this.btnMovFile.TabIndex = 12;
            this.btnMovFile.TabStop = false;
            this.btnMovFile.UseVisualStyleBackColor = false;
            // 
            // MovieControl
            // 
            this.Buttons.Controls.Add(this.btnMovFile);
            this.Buttons.Controls.Add(this.btnDelFile);
            this.Buttons.Controls.Add(this.btnDelUser);
            this.Buttons.Controls.Add(this.btnAddUser);
            this.Buttons.Controls.Add(this.btnRelated);
            this.Buttons.Controls.Add(this.btnPlay);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Name = "MovieControl";
            this.Load += new System.EventHandler(this.MovieControl_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel floVideos;
        private System.Windows.Forms.FlowLayoutPanel floArt;
        private System.Windows.Forms.Button btnPlay;
        private MetroFramework.Controls.MetroTabControl tabCrews;
        private System.Windows.Forms.FlowLayoutPanel floCrew;
        private System.Windows.Forms.Button btnRelated;
        private MovieWall moviesWall;
        private MovieWall setWall;
        private System.Windows.Forms.Button btnDelUser;
        private System.Windows.Forms.Button btnDelFile;
        private FilesControl grdFiles;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.Button btnMovFile;
    }
}
