﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MovieModel.Entities;
using System.Threading;
using System.Windows.Media.Imaging;
using MetroFramework;

namespace MovieControls.Usercontrols
{
    partial class MovieThumb : PictureThumb
    {
        private MovieThumb():base()
        {

        }

        public MovieThumb(Movie aMovie, int aWidth, bool aRelateds)
        : base(aWidth)
        {
            AllowRemove = aRelateds;
            SetMovie(aMovie, aRelateds);
        }

        public Movie Movie { get; private set; }

        private void SetMovie(Movie aMovie, bool aRelateds)
        {
            Movie = aMovie;
            FileName = Movie.Extender.Poster?.Extender.GetFileName();
            SetLabels(aMovie.Year?.ToString(), aMovie.Title, aMovie.PropertyGet(aMovie.GetParent()?.Sorting?.FirstOrDefault()?.SortProperty)?.ToString(), aMovie.Rating, aMovie.UserRating, aMovie.PredictedRating, aMovie.ViewedTimes ?? 0, aMovie.FilesNumber);
            if (aMovie.FilesNumber > 0)
            {
                this.BackColor = Color.Black;
            }
            else
            {
                this.Theme = MetroFramework.MetroThemeStyle.Light;
            }
            if (aRelateds)
            {
                this.RemoveItem += new EventHandler(MovieThumb_RemoveItem);
            }
        }

        private void MovieThumb_RemoveItem(object sender, EventArgs e)
        {
            var lDad = Movie?.GetParent<Movie, Movie>()?.GetParent();
            if (lDad != null)
            {
                var lDel = lDad.Relateds().Items.Where(r => r.IdMovie?.Value == Movie.IdMovie?.Value).FirstOrDefault();
                if (lDel == null)
                    lDel = lDad.Relateds().Items.Where(r => r.IdMovieRelated?.Value == Movie.IdMovie?.Value).FirstOrDefault();
                if (lDel != null)
                {
                    var lRes = MetroMessageBox.Show(this, "Do you want to remove the movie from relateds?", lDel.IdMovieRelated.Reference.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (lRes == DialogResult.Yes)
                    {
                        lDel.Delete("User removed related");
                        lDel.SaveChanges();
                    }
                }
            }
            //this.OnParentChanged()
        }

        protected override void OnClick(EventArgs e)
        {
            MovieControl lCtl = new MovieControl(Movie);
            this.Navigator()?.Navigate(lCtl);

        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MovieThumb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "MovieThumb";
            this.ResumeLayout(false);

        }

    }
}
