﻿using KnkInterfaces.Interfaces;
using KnkScrapers.MoviesScraper;
using MovieModel.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MovieControls.Usercontrols
{
    public partial class MovieSearch : NavigationItem
    {
        readonly KnkConnectionItf _Connection;
        readonly MovieEnricher _Enricher;
        readonly File _File;
        private MovieSearch()
        {
            InitializeComponent();
        }

        public MovieSearch(File aFile) : this()
        {
            _Connection = aFile.Connection();
            _File = aFile;
            _Enricher = new MovieEnricher(_Connection);
            ScanFile();
        }

        private void ScanFile()
        {
            //var lLst = _Enricher.FindMovies(_File, true);
            //var lSrc = (from mov in lLst select new { mov.Id, mov.Title, mov.ReleaseDate, mov.OriginalTitle }).ToList();
            //grdSearch.DataSource = lSrc;
        }

        private void btnScan_Click(object sender, System.EventArgs e)
        {
            EnrichCurrentRow();
        }

        private void EnrichCurrentRow()
        {
            var lRow = grdSearch.CurrentRow;
            var lMov = lRow.DataBoundItem;
            int lIdMovie = (int)lMov.GetType().GetProperty("Id").GetValue(lMov, null);

            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                btnScan.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.FindEnrichMovie(lIdMovie, _File, bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            grdResults.DataSource = _Enricher.Results.Messages.ToList().Reverse<KnkChangeDescriptorItf>();
        }

        void ProcessChanged()
        {
            grdResults.DataSource = typeof(List<>);
            grdResults.DataSource = _Enricher.Results.Messages.ToList().Reverse<KnkChangeDescriptorItf>();
        }

        void ProcessFinished()
        {
            btnScan.Image = null;
        }

    }
}
