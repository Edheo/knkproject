﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MovieModel.Entities;
using KnkScrapers.Classes;
using System.Threading;
using Vlc.DotNet.Forms;
using MovieControls.Enumerations;
using MovieControls.Classes;
using System.ComponentModel;
using MetroFramework;
using MetroFramework.Controls;
using KnkCore;
using KnkCore.Utilities;
using MovieModel.Lists;
using MovieModel.Criterias;
using KnkScrapers.MoviesScraper;
using KnkInterfaces.Interfaces;

namespace MovieControls.Usercontrols
{
    public partial class MovieControl : ElementControl
    {
        private Movie _Movie;

        public MovieControl(Movie aMovie)
        {
            InitializeComponent();
            InitializeControl(aMovie);
            SetMovie(aMovie);
        }

        private void InitializeControl(Movie aMovie)
        {
            var lCri = new MoviesCriteria(aMovie) { IsRelated = true };
            this.moviesWall = new MovieWall(lCri);
            this.moviesWall.Dock = DockStyle.Fill;

            this.AddTabControl("Info", tblProperties);
            this.AddTabControl("Crew", tabCrews);
            if (aMovie.Files().Count() > 0) this.AddTabControl("Files", grdFiles);
            if(aMovie.IdSet?.Value!=null)
            {
                this.setWall = new MovieWall(new MoviesCriteria() { Saga = aMovie.IdSet.Reference.Name });
                this.setWall.Dock = DockStyle.Fill;
                this.AddTabControl("Saga", setWall);
            }
            this.AddTabControl("Related Movies", moviesWall);
            this.AddTabControl("Pictures", floArt);
            this.AddTabControl("Videos", floVideos);
        }

        private Movies GetRelatedMovies(Movie aMovie)
        {
            Movies lMov = new Movies(aMovie);
            KnkCriteria<Movie, Movie> lCri = new KnkCriteria<Movie, Movie>(lMov, new KnkTableEntity("vieMovies", "Movies"));
            KnkCoreUtils.CreateInParameter(new MovieRelateds(aMovie.Connection(), aMovie.IdMovie.Value ?? 0), lCri, "IdMovie");
            lMov.Criteria = lCri;
            lMov.SortAttribute = new KnkSortAttribute("ReleaseDate");
            return lMov;
        }

        private void SetMovie(Movie aMovie)
        {
            _Movie = aMovie;
            tblProperties.Controls.Clear();
            tblProperties.ClearRows();
            SetPoster(_Movie.Extender.Poster?.Extender.GetFileName());
            tblProperties.AddTagInfo("Title", $"{_Movie.Title}");
            tblProperties.AddTagInfo("Original Title", $"{_Movie.OriginalTitle}");
            tblProperties.AddTagInfo("Duration", _Movie.Duration?.ToString(@"hh\:mm\:ss"));
            tblProperties.AddTagInfo("Saga", $"{_Movie.MovieSet}", LinkTypeEnu.MovieWall, _Movie.Connection(), "Saga");
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("Genres", $"{_Movie.Extender.Genres}", LinkTypeEnu.MovieWall, _Movie.Connection(), "Genre");
            tblProperties.AddTagInfo("Country", $"{_Movie.Extender.Countries}", LinkTypeEnu.MovieWall, _Movie.Connection(), "Country");
            tblProperties.AddTagInfo("Company", $"{_Movie.Extender.Companies}", LinkTypeEnu.MovieWall, _Movie.Connection(), "Company");
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("Budget", $"{_Movie.Budget:n}");
            tblProperties.AddTagInfo("Revenue", $"{_Movie.Revenue:n}");
            tblProperties.AddTagInfo("Audio Traks", $"{_Movie.AudioTracks}");
            tblProperties.AddTagInfo("Subtitles", $"{_Movie.Subtitles}");
            tblProperties.AddTagInfo("Year", $"{_Movie.Year}", LinkTypeEnu.MovieWall, _Movie.Connection(), "Year");
            tblProperties.AddTagInfo("Release Date", $"{_Movie.ReleaseDate:dd/MM/yyyy}");
            tblProperties.AddTagInfo("Date Added", $"{_Movie.CreationDate:dd/MM/yyyy}");
            tblProperties.AddTagInfo("Date Modified", $"{_Movie.ModifiedDate:dd/MM/yyyy}");
            tblProperties.AddTagInfo("Date Scraped", $"{_Movie.ScrapedDate:dd/MM/yyyy}");
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("Popularity", $"{_Movie.Popularity:0.0}");
            tblProperties.AddTagInfo("Votes", $"{_Movie.Votes}");
            tblProperties.AddRating("Rating", _Movie.Rating);
            tblProperties.AddRating("Casting Rating", _Movie.CastingRating);
            tblProperties.AddRating("Related Movies Rating", _Movie.RelatedMoviesRating);
            tblProperties.AddRating("Predicted Rating", _Movie.PredictedRating);
            tblProperties.AddRating("User Rating", _Movie.UserRating ?? 0, _Movie, "UserRating");
            tblProperties.AddTagInfo("MPA Rating", $"{_Movie.MPARating}");
            if(_Movie.AdultContent) tblProperties.AddTagInfo("Adult Content", $"Yes");
            tblProperties.AddTagInfo();
            if (_Movie.ViewedTimes > 0)
            {
                tblProperties.AddTagInfo();
                tblProperties.AddTagInfo("Last Played", $"{_Movie.LastViewed:dd/MM/yyyy}");
                tblProperties.AddTagInfo("Plays", $"{_Movie.ViewedTimes}");
            }
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("HomePage", $"{_Movie.HomePage}", LinkTypeEnu.Url, _Movie.Connection(), "Url");
            tblProperties.AddTagInfo("Imdb", $"{_Movie.Extender.ImdbUrl()}", LinkTypeEnu.Url, _Movie.Connection(), "Url");
            tblProperties.AddTagInfo("Hispashare", $"{_Movie.Extender.HispashareUrl()}", LinkTypeEnu.Url, _Movie.Connection(), "Url");
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("Tag Line", $"{_Movie.TagLine}");
            tblProperties.AddTagInfo();
            tblProperties.AddTextInfo("Summary", _Movie.Extender.Summary);
            var lFil = (from f in _Movie.Files().Items select f.IdFile.Reference).ToList();
            grdFiles.SetFiles(lFil);
            //
            btnPlay.Visible = _Movie.Files().Items.Count > 0;
            btnDelFile.Visible = _Movie.Files().Items.Count > 0 && _Movie.Users().Items.Any(u => u.IdUser.Value == _Movie.Connection().CurrentUser.PrimaryKeyValue().Value);
            btnDelUser.Visible = _Movie.Users().Items.Any(u => u.IdUser.Value == _Movie.Connection().CurrentUser.PrimaryKeyValue().Value);
            btnAddUser.Visible = !_Movie.Users().Items.Any(u => u.IdUser.Value == _Movie.Connection().CurrentUser.PrimaryKeyValue().Value);
        }

        public Movie Movie
        {
            get { return _Movie; }
        }


        private void AddCastingThread()
        {
            var lThr = new Thread(() => AddCasting());
            lThr.Start();
        }

        private void AddVideosThread()
        {
            var lThr = new Thread(() => AddVideos());
            lThr.Start();
        }

        private void AddArtThread()
        {
            var lThr = new Thread(() => AddArt());
            lThr.Start();
        }

        private void AddCasting()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new delNoParams(AddCastingTabs));
            }
            else
                AddCastingTabs();

            if (tabCrews.Controls.Count > 0)
                LoadCrewDepartment(tabCrews.Controls[0] as MetroTabPage);
        }

        private void AddCastingTabs()
        {
            tabCrews.SuspendLayout();
            var lTypes = (from cst in _Movie.Casting().Items
                          group cst by cst.IdCastingType.Reference.Department into g
                          select new { Department = g.Key, Items = g.ToList() }
                          );
                _Movie.Casting().Items.GroupBy(typ => typ.IdCastingType.Reference.Department).Select(grp => grp.First()).OrderBy(grp => grp.IdCastingType.Reference.Department);

            MetroTabPage lSel = null;
            foreach (var dep in lTypes)
            {
                MetroTabPage lPage = new MetroTabPage();
                lPage.Style = MetroColorStyle.Blue;
                lPage.Theme = MetroThemeStyle.Dark;
                lPage.Text = dep.Department;
                lPage.Padding = new Padding(5);
                if(dep.Department.ToLower().Equals("casting")) lSel = lPage;
                tabCrews.Controls.Add(lPage);
            }
            if(lSel!=null) tabCrews.SelectedTab = (TabPage)lSel;
            tabCrews.ResumeLayout();
            tabCrews.PerformLayout();
        }

        private void LoadCrewDepartment(MetroTabPage aPage)
        {
            if (aPage != null)
            {
                aPage.Controls.Clear();
                floCrew.Controls.Clear();
                string lDepartment = aPage.Text;
                var lCast = (from itm in _Movie.Casting().Items
                             where itm.IdCastingType.Reference.Department == lDepartment
                             orderby itm.IdCastingType, itm.Ordinal
                             select itm);
                foreach (var cst in lCast)
                {
                    PersonThumb lTmb = new PersonThumb(cst, 60);
                    if (floCrew.InvokeRequired)
                    {
                        this.Invoke(new delAddLayoutControl(AddControlToFlow), floCrew, lTmb);
                    }
                    else
                        AddControlToFlow(floCrew, lTmb);
                }
                floCrew.Dock = DockStyle.Fill;
                aPage.Controls.Add(floCrew);
                aPage.Refresh();
            }
        }

        private void AddControlToFlow(FlowLayoutPanel aPanel, Control aControl)
        {
            aPanel.Controls.Add(aControl);
        }

        private void AddFlowBreak(FlowLayoutPanel aPanel, Control aControl)
        {
            aPanel.SetFlowBreak(aControl, true);
        }

        private void AddVideos()
        {
            var lMedias = (from vid in _Movie.Pictures().Items where vid.IdType==MovieModel.Enums.LinkTypeEnum.Videos select vid);
            foreach (var med in lMedias)
            {
                VideoThumb lTmb = new VideoThumb(med, 200);
                if (floVideos.InvokeRequired)
                {
                    this.Invoke(new delAddLayoutControl(AddControlToFlow), floVideos, lTmb);
                }
                else
                    AddControlToFlow(floVideos, lTmb);
            }
        }

        private void AddArt()
        {
            var lArt = (from art in _Movie.Pictures().Items where art.IdType!=MovieModel.Enums.LinkTypeEnum.Videos select art).ToList();
            foreach (var med in lArt)
            {
                MediaThumb lTmb = new MediaThumb(med, 60);
                if (floArt.InvokeRequired)
                {
                    this.Invoke(new delAddLayoutControl(AddControlToFlow), floArt, lTmb);
                }
                else
                    AddControlToFlow(floArt, lTmb);
            }
        }

        private void MovieControl_Load(object sender, EventArgs e)
        {
            AddCasting();
            this.moviesWall.Invalidate();
            //moviesWall.LoadMovies(GetRelatedMovies(_Movie), true, false);
            AddVideosThread();
            AddArtThread();
            LoadCrewDepartment(tabCrews.SelectedTab as MetroTabPage);
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            OnPlay();
        }

        private void OnPlay()
        {
            PlayForm lFrm = new PlayForm();

            var lScr = Screen.FromPoint(Cursor.Position);
            lFrm.StartPosition = FormStartPosition.Manual;
            lFrm.Left = lScr.Bounds.Left + lScr.Bounds.Width / 2 - lFrm.Width / 2;
            lFrm.Top = lScr.Bounds.Top + lScr.Bounds.Height / 2 - lFrm.Height / 2;
            lFrm.Play(_Movie.Files().Items.FirstOrDefault().IdFile.Reference);
        }

        private void tabCrews_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCrewDepartment(tabCrews.SelectedTab as MetroTabPage);
        }

        private void OnSearchRelated()
        {
            var lRes = DialogResult.Yes;
            if (!_Movie.IsScrappable())
            {
                lRes = MetroMessageBox.Show(this, "Do you want to re-scrap the movie?", _Movie.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (lRes == DialogResult.Yes) _Movie.ScrapedDate = null;
            }
            if (lRes == DialogResult.Yes)
            {

                MovieEnricher _Enricher = new MovieEnricher(_Movie.Connection());
                if (!_Enricher.IsBusy)
                {
                    var bw = new BackgroundWorker();
                    bw.WorkerReportsProgress = true;
                    btnRelated.Image = global::MovieControls.Properties.Resources.Ani200_2;
                    bw.DoWork += (sender, args) =>
                    {
                        var lfil = _Movie.Files().Items.FirstOrDefault();
                        var lRet = _Enricher.EnrichMovie(_Movie, lfil?.IdFile);
                        lRet.SaveChanges();
                    };
                    bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(); };
                    bw.RunWorkerAsync(); // starts the background worker
                }
            }
        }

        void ProcessFinished()
        {
            btnRelated.Image = null;
            SetMovie(_Movie);
            moviesWall.OnPerformSearch();
        }

        private void btnRelated_Click(object sender, EventArgs e)
        {
            OnSearchRelated();
        }

        private void btnDelUser_Click(object sender, EventArgs e)
        {
            var lRes = MetroMessageBox.Show(this, "Do you want to remove the movie from your library?", _Movie.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lRes == DialogResult.Yes)
            {
                User lCur = _Movie.Connection().CurrentUser as User;
                var lUsr = _Movie.Users().Items.Where(us => us.IdUser.Value.Equals(lCur.IdUser.Value)).FirstOrDefault();
                lUsr.Delete("Removed from user Library");
                lUsr.SaveChanges();
                SetMovie(_Movie);
            }
        }

        private void btnDelFile_Click(object sender, EventArgs e)
        {
            var lRes = MetroMessageBox.Show(this, "Do you want to remove the files from the movie?", _Movie.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lRes == DialogResult.Yes)
            {
                foreach(var lFil in _Movie.Files().Items)
                {
                    System.IO.File.Delete(lFil.IdFile.Reference.ToString());
                    lFil.IdFile.Reference.Delete("Removed File from system");
                    lFil.IdFile.Reference.SaveChanges();
                }
                _Movie.Files().DeleteAll("Removed Files from Movie");
                _Movie.Files().SaveChanges();
                SetMovie(_Movie);
            }
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            User lCur = _Movie.Connection().CurrentUser as User;
            var lRes = MetroMessageBox.Show(this, $"Do you want to add the movie to {lCur.Username} library?", _Movie.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lRes == DialogResult.Yes)
            {
                var lUsr = _Movie.Users().Create();
                lUsr.IdUser = lCur;
                lUsr.IdMovie = _Movie;
                lUsr.Update("Added to user Library");
                lUsr.SaveChanges();
                SetMovie(_Movie);
            }
        }
    }
}
