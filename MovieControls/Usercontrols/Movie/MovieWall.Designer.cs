﻿using MovieControls.Usercontrols.Other;

namespace MovieControls.Usercontrols
{
    partial class MovieWall
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new MetroFramework.Controls.MetroPanel();
            this.lblCount = new System.Windows.Forms.Label();
            this.chkMylib = new MetroFramework.Controls.MetroToggle();
            this.chkFiles = new MetroFramework.Controls.MetroToggle();
            this.chkViewed = new MetroFramework.Controls.MetroToggle();
            this.chkRated = new MetroFramework.Controls.MetroToggle();
            this.chkDesc = new MetroFramework.Controls.MetroToggle();
            this.cmbSort = new MetroFramework.Controls.MetroComboBox();
            this.btnFilter = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.moviesWall = new MovieControls.Usercontrols.MovieWallLayout();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.tblCriterias = new System.Windows.Forms.TableLayoutPanel();
            this.lblSearch = new System.Windows.Forms.Label();
            this.txtSearch = new MetroFramework.Controls.MetroTextBox();
            this.lblArtist = new System.Windows.Forms.Label();
            this.txtArtist = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbGenres = new MetroFramework.Controls.MetroComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSaga = new MetroFramework.Controls.MetroComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.trkYearFrom = new MovieControls.Usercontrols.Other.RangeSelector();
            this.trkYearTo = new MovieControls.Usercontrols.Other.RangeSelector();
            this.label5 = new System.Windows.Forms.Label();
            this.trkRatingFrom = new MovieControls.Usercontrols.Other.RangeSelector();
            this.trkRatingTo = new MovieControls.Usercontrols.Other.RangeSelector();
            this.lblUserRating = new System.Windows.Forms.Label();
            this.trkUserRatingFrom = new MovieControls.Usercontrols.Other.RangeSelector();
            this.trkUserRatingTo = new MovieControls.Usercontrols.Other.RangeSelector();
            this.label4 = new System.Windows.Forms.Label();
            this.trkPredictedFrom = new MovieControls.Usercontrols.Other.RangeSelector();
            this.trkPredictedTo = new MovieControls.Usercontrols.Other.RangeSelector();
            this.pnlButtons.SuspendLayout();
            this.pnlSearch.SuspendLayout();
            this.tblCriterias.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.Color.Black;
            this.pnlButtons.Controls.Add(this.lblCount);
            this.pnlButtons.Controls.Add(this.chkMylib);
            this.pnlButtons.Controls.Add(this.chkFiles);
            this.pnlButtons.Controls.Add(this.chkViewed);
            this.pnlButtons.Controls.Add(this.chkRated);
            this.pnlButtons.Controls.Add(this.chkDesc);
            this.pnlButtons.Controls.Add(this.cmbSort);
            this.pnlButtons.Controls.Add(this.btnFilter);
            this.pnlButtons.Controls.Add(this.btnClear);
            this.pnlButtons.Controls.Add(this.btnSearch);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.HorizontalScrollbarBarColor = true;
            this.pnlButtons.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlButtons.HorizontalScrollbarSize = 10;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(1241, 41);
            this.pnlButtons.TabIndex = 6;
            this.pnlButtons.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.pnlButtons.VerticalScrollbarBarColor = true;
            this.pnlButtons.VerticalScrollbarHighlightOnWheel = false;
            this.pnlButtons.VerticalScrollbarSize = 10;
            // 
            // lblCount
            // 
            this.lblCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCount.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblCount.Location = new System.Drawing.Point(0, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(447, 41);
            this.lblCount.TabIndex = 9;
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkMylib
            // 
            this.chkMylib.AutoSize = true;
            this.chkMylib.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkMylib.Checked = true;
            this.chkMylib.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMylib.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkMylib.ForeColor = System.Drawing.Color.White;
            this.chkMylib.Location = new System.Drawing.Point(447, 0);
            this.chkMylib.Margin = new System.Windows.Forms.Padding(6);
            this.chkMylib.MaximumSize = new System.Drawing.Size(0, 21);
            this.chkMylib.Name = "chkMylib";
            this.chkMylib.Size = new System.Drawing.Size(110, 21);
            this.chkMylib.TabIndex = 22;
            this.chkMylib.Text = "In My Lib";
            this.chkMylib.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkMylib.TextStatus = "In My Lib|Not in My Lib|All";
            this.chkMylib.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkMylib.UseSelectable = true;
            this.chkMylib.UseVisualStyleBackColor = true;
            // 
            // chkFiles
            // 
            this.chkFiles.AutoSize = true;
            this.chkFiles.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFiles.Checked = true;
            this.chkFiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFiles.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkFiles.ForeColor = System.Drawing.Color.White;
            this.chkFiles.Location = new System.Drawing.Point(557, 0);
            this.chkFiles.Margin = new System.Windows.Forms.Padding(6);
            this.chkFiles.MaximumSize = new System.Drawing.Size(0, 21);
            this.chkFiles.Name = "chkFiles";
            this.chkFiles.Size = new System.Drawing.Size(97, 21);
            this.chkFiles.TabIndex = 21;
            this.chkFiles.Text = "Has File";
            this.chkFiles.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFiles.TextStatus = "Has File|Hasn\'t File|All";
            this.chkFiles.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkFiles.ThreeState = true;
            this.chkFiles.UseSelectable = true;
            this.chkFiles.UseVisualStyleBackColor = true;
            // 
            // chkViewed
            // 
            this.chkViewed.AutoSize = true;
            this.chkViewed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkViewed.Checked = true;
            this.chkViewed.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.chkViewed.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkViewed.ForeColor = System.Drawing.Color.White;
            this.chkViewed.Location = new System.Drawing.Point(654, 0);
            this.chkViewed.Margin = new System.Windows.Forms.Padding(6);
            this.chkViewed.MaximumSize = new System.Drawing.Size(0, 21);
            this.chkViewed.Name = "chkViewed";
            this.chkViewed.Size = new System.Drawing.Size(102, 21);
            this.chkViewed.TabIndex = 20;
            this.chkViewed.Text = "All";
            this.chkViewed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkViewed.TextStatus = "Viewed|Not Viewed|All";
            this.chkViewed.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkViewed.ThreeState = true;
            this.chkViewed.UseSelectable = true;
            this.chkViewed.UseVisualStyleBackColor = true;
            // 
            // chkRated
            // 
            this.chkRated.AutoSize = true;
            this.chkRated.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRated.Checked = true;
            this.chkRated.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.chkRated.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkRated.ForeColor = System.Drawing.Color.White;
            this.chkRated.Location = new System.Drawing.Point(756, 0);
            this.chkRated.Margin = new System.Windows.Forms.Padding(6);
            this.chkRated.MaximumSize = new System.Drawing.Size(0, 21);
            this.chkRated.Name = "chkRated";
            this.chkRated.Size = new System.Drawing.Size(95, 21);
            this.chkRated.TabIndex = 19;
            this.chkRated.Text = "All";
            this.chkRated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRated.TextStatus = "Rated|Not Rated|All";
            this.chkRated.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkRated.ThreeState = true;
            this.chkRated.UseSelectable = true;
            this.chkRated.UseVisualStyleBackColor = true;
            // 
            // chkDesc
            // 
            this.chkDesc.AutoSize = true;
            this.chkDesc.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDesc.Checked = true;
            this.chkDesc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDesc.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkDesc.ForeColor = System.Drawing.Color.White;
            this.chkDesc.Location = new System.Drawing.Point(851, 0);
            this.chkDesc.Margin = new System.Windows.Forms.Padding(6);
            this.chkDesc.MaximumSize = new System.Drawing.Size(0, 21);
            this.chkDesc.Name = "chkDesc";
            this.chkDesc.Size = new System.Drawing.Size(104, 21);
            this.chkDesc.TabIndex = 18;
            this.chkDesc.Text = "Descending";
            this.chkDesc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDesc.TextStatus = "Descending|Ascending|All";
            this.chkDesc.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkDesc.UseSelectable = true;
            this.chkDesc.UseVisualStyleBackColor = true;
            // 
            // cmbSort
            // 
            this.cmbSort.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmbSort.FormattingEnabled = true;
            this.cmbSort.ItemHeight = 23;
            this.cmbSort.Location = new System.Drawing.Point(955, 0);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(163, 29);
            this.cmbSort.TabIndex = 23;
            this.cmbSort.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cmbSort.UseSelectable = true;
            // 
            // btnFilter
            // 
            this.btnFilter.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnFilter.BackColor = System.Drawing.Color.Transparent;
            this.btnFilter.BackgroundImage = global::MovieControls.Properties.Resources._1467080815_filter;
            this.btnFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilter.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilter.ForeColor = System.Drawing.Color.White;
            this.btnFilter.Location = new System.Drawing.Point(1118, 0);
            this.btnFilter.Margin = new System.Windows.Forms.Padding(6);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(41, 41);
            this.btnFilter.TabIndex = 17;
            this.btnFilter.TabStop = false;
            this.btnFilter.UseVisualStyleBackColor = false;
            this.btnFilter.CheckedChanged += new System.EventHandler(this.btnFilter_CheckedChanged);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.BackgroundImage = global::MovieControls.Properties.Resources._1466916624_package_purge;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Location = new System.Drawing.Point(1159, 0);
            this.btnClear.Name = "btnClear";
            this.btnClear.Padding = new System.Windows.Forms.Padding(5);
            this.btnClear.Size = new System.Drawing.Size(41, 41);
            this.btnClear.TabIndex = 6;
            this.btnClear.TabStop = false;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Transparent;
            this.btnSearch.BackgroundImage = global::MovieControls.Properties.Resources._1466916496_search;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Location = new System.Drawing.Point(1200, 0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(5);
            this.btnSearch.Size = new System.Drawing.Size(41, 41);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.TabStop = false;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // moviesWall
            // 
            this.moviesWall.Buttons = null;
            this.moviesWall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moviesWall.Location = new System.Drawing.Point(0, 131);
            this.moviesWall.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.moviesWall.Name = "moviesWall";
            this.moviesWall.Size = new System.Drawing.Size(1241, 406);
            this.moviesWall.TabIndex = 1;
            this.moviesWall.UseSelectable = true;
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.Black;
            this.pnlSearch.Controls.Add(this.tblCriterias);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSearch.Location = new System.Drawing.Point(0, 41);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(1241, 90);
            this.pnlSearch.TabIndex = 0;
            this.pnlSearch.Visible = false;
            // 
            // tblCriterias
            // 
            this.tblCriterias.ColumnCount = 8;
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tblCriterias.Controls.Add(this.lblSearch, 0, 0);
            this.tblCriterias.Controls.Add(this.txtSearch, 1, 0);
            this.tblCriterias.Controls.Add(this.lblArtist, 2, 0);
            this.tblCriterias.Controls.Add(this.txtArtist, 3, 0);
            this.tblCriterias.Controls.Add(this.label1, 4, 0);
            this.tblCriterias.Controls.Add(this.cmbGenres, 5, 0);
            this.tblCriterias.Controls.Add(this.label2, 6, 0);
            this.tblCriterias.Controls.Add(this.cmbSaga, 7, 0);
            this.tblCriterias.Controls.Add(this.label3, 0, 1);
            this.tblCriterias.Controls.Add(this.trkYearFrom, 1, 1);
            this.tblCriterias.Controls.Add(this.trkYearTo, 1, 2);
            this.tblCriterias.Controls.Add(this.label5, 2, 1);
            this.tblCriterias.Controls.Add(this.trkRatingFrom, 3, 1);
            this.tblCriterias.Controls.Add(this.trkRatingTo, 3, 2);
            this.tblCriterias.Controls.Add(this.lblUserRating, 6, 1);
            this.tblCriterias.Controls.Add(this.trkUserRatingFrom, 7, 1);
            this.tblCriterias.Controls.Add(this.trkUserRatingTo, 7, 2);
            this.tblCriterias.Controls.Add(this.label4, 4, 1);
            this.tblCriterias.Controls.Add(this.trkPredictedFrom, 5, 1);
            this.tblCriterias.Controls.Add(this.trkPredictedTo, 5, 2);
            this.tblCriterias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblCriterias.ForeColor = System.Drawing.Color.White;
            this.tblCriterias.Location = new System.Drawing.Point(0, 0);
            this.tblCriterias.Name = "tblCriterias";
            this.tblCriterias.RowCount = 3;
            this.tblCriterias.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblCriterias.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblCriterias.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblCriterias.Size = new System.Drawing.Size(1241, 90);
            this.tblCriterias.TabIndex = 2;
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(6, 6);
            this.lblSearch.Margin = new System.Windows.Forms.Padding(6);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(52, 13);
            this.lblSearch.TabIndex = 0;
            this.lblSearch.Text = "Search";
            // 
            // txtSearch
            // 
            // 
            // 
            // 
            this.txtSearch.CustomButton.Image = null;
            this.txtSearch.CustomButton.Location = new System.Drawing.Point(127, 2);
            this.txtSearch.CustomButton.Name = "";
            this.txtSearch.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txtSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSearch.CustomButton.TabIndex = 1;
            this.txtSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtSearch.CustomButton.UseSelectable = true;
            this.txtSearch.CustomButton.Visible = false;
            this.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSearch.Lines = new string[0];
            this.txtSearch.Location = new System.Drawing.Point(158, 3);
            this.txtSearch.MaxLength = 32767;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.PasswordChar = '\0';
            this.txtSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSearch.SelectedText = "";
            this.txtSearch.SelectionLength = 0;
            this.txtSearch.SelectionStart = 0;
            this.txtSearch.ShortcutsEnabled = true;
            this.txtSearch.Size = new System.Drawing.Size(149, 24);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtSearch.UseSelectable = true;
            this.txtSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblArtist
            // 
            this.lblArtist.AutoSize = true;
            this.lblArtist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblArtist.Location = new System.Drawing.Point(316, 6);
            this.lblArtist.Margin = new System.Windows.Forms.Padding(6);
            this.lblArtist.Name = "lblArtist";
            this.lblArtist.Size = new System.Drawing.Size(143, 18);
            this.lblArtist.TabIndex = 2;
            this.lblArtist.Text = "Artist";
            // 
            // txtArtist
            // 
            // 
            // 
            // 
            this.txtArtist.CustomButton.Image = null;
            this.txtArtist.CustomButton.Location = new System.Drawing.Point(127, 2);
            this.txtArtist.CustomButton.Name = "";
            this.txtArtist.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txtArtist.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtArtist.CustomButton.TabIndex = 1;
            this.txtArtist.CustomButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtArtist.CustomButton.UseSelectable = true;
            this.txtArtist.CustomButton.Visible = false;
            this.txtArtist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtArtist.Lines = new string[0];
            this.txtArtist.Location = new System.Drawing.Point(468, 3);
            this.txtArtist.MaxLength = 32767;
            this.txtArtist.Name = "cmbArtist";
            this.txtArtist.PasswordChar = '\0';
            this.txtArtist.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtArtist.SelectedText = "";
            this.txtArtist.SelectionLength = 0;
            this.txtArtist.SelectionStart = 0;
            this.txtArtist.ShortcutsEnabled = true;
            this.txtArtist.Size = new System.Drawing.Size(149, 24);
            this.txtArtist.TabIndex = 1;
            this.txtArtist.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtArtist.UseSelectable = true;
            this.txtArtist.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtArtist.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(626, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "Genre";
            // 
            // cmbGenres
            // 
            this.cmbGenres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbGenres.FormattingEnabled = true;
            this.cmbGenres.ItemHeight = 23;
            this.cmbGenres.Location = new System.Drawing.Point(778, 3);
            this.cmbGenres.Name = "cmbGenres";
            this.cmbGenres.Size = new System.Drawing.Size(149, 29);
            this.cmbGenres.TabIndex = 2;
            this.cmbGenres.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cmbGenres.UseSelectable = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(936, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Saga";
            // 
            // cmbSaga
            // 
            this.cmbSaga.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSaga.FormattingEnabled = true;
            this.cmbSaga.ItemHeight = 23;
            this.cmbSaga.Location = new System.Drawing.Point(1088, 3);
            this.cmbSaga.Name = "cmbSaga";
            this.cmbSaga.Size = new System.Drawing.Size(150, 29);
            this.cmbSaga.TabIndex = 3;
            this.cmbSaga.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cmbSaga.UseSelectable = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(6, 36);
            this.label3.Margin = new System.Windows.Forms.Padding(6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Year";
            // 
            // trkYearFrom
            // 
            this.trkYearFrom.BackColor = System.Drawing.Color.Transparent;
            this.trkYearFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkYearFrom.Location = new System.Drawing.Point(158, 33);
            this.trkYearFrom.Maximum = 2020;
            this.trkYearFrom.Minimum = 1800;
            this.trkYearFrom.Name = "trkYearFrom";
            this.trkYearFrom.Size = new System.Drawing.Size(149, 24);
            this.trkYearFrom.SmallChange = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.trkYearFrom.TabIndex = 4;
            this.trkYearFrom.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkYearFrom.UseSelectable = true;
            this.trkYearFrom.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            // 
            // trkYearTo
            // 
            this.trkYearTo.BackColor = System.Drawing.Color.Transparent;
            this.trkYearTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkYearTo.Location = new System.Drawing.Point(158, 63);
            this.trkYearTo.Maximum = 2020;
            this.trkYearTo.Minimum = 1800;
            this.trkYearTo.Name = "trkYearTo";
            this.trkYearTo.Size = new System.Drawing.Size(149, 24);
            this.trkYearTo.SmallChange = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.trkYearTo.TabIndex = 5;
            this.trkYearTo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkYearTo.UseSelectable = true;
            this.trkYearTo.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(316, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "Rating";
            // 
            // trkRatingFrom
            // 
            this.trkRatingFrom.BackColor = System.Drawing.Color.Transparent;
            this.trkRatingFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkRatingFrom.Location = new System.Drawing.Point(468, 33);
            this.trkRatingFrom.Maximum = 10;
            this.trkRatingFrom.Minimum = 0;
            this.trkRatingFrom.Name = "trkRatingFrom";
            this.trkRatingFrom.Size = new System.Drawing.Size(149, 24);
            this.trkRatingFrom.SmallChange = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trkRatingFrom.TabIndex = 6;
            this.trkRatingFrom.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkRatingFrom.UseSelectable = true;
            this.trkRatingFrom.Value = new decimal(new int[] {
            0,
            0,
            0,
            65536});
            // 
            // trkRatingTo
            // 
            this.trkRatingTo.BackColor = System.Drawing.Color.Transparent;
            this.trkRatingTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkRatingTo.Location = new System.Drawing.Point(468, 63);
            this.trkRatingTo.Maximum = 10;
            this.trkRatingTo.Minimum = 0;
            this.trkRatingTo.Name = "trkRatingTo";
            this.trkRatingTo.Size = new System.Drawing.Size(149, 24);
            this.trkRatingTo.SmallChange = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trkRatingTo.TabIndex = 7;
            this.trkRatingTo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkRatingTo.UseSelectable = true;
            this.trkRatingTo.Value = new decimal(new int[] {
            80,
            0,
            0,
            65536});
            // 
            // lblUserRating
            // 
            this.lblUserRating.AutoSize = true;
            this.lblUserRating.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUserRating.Location = new System.Drawing.Point(936, 36);
            this.lblUserRating.Margin = new System.Windows.Forms.Padding(6);
            this.lblUserRating.Name = "lblUserRating";
            this.lblUserRating.Size = new System.Drawing.Size(143, 18);
            this.lblUserRating.TabIndex = 14;
            this.lblUserRating.Text = "User Rating";
            // 
            // trkUserRatingFrom
            // 
            this.trkUserRatingFrom.BackColor = System.Drawing.Color.Transparent;
            this.trkUserRatingFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkUserRatingFrom.Location = new System.Drawing.Point(1088, 33);
            this.trkUserRatingFrom.Maximum = 10;
            this.trkUserRatingFrom.Minimum = 0;
            this.trkUserRatingFrom.Name = "trkUserRatingFrom";
            this.trkUserRatingFrom.Size = new System.Drawing.Size(150, 24);
            this.trkUserRatingFrom.SmallChange = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trkUserRatingFrom.TabIndex = 10;
            this.trkUserRatingFrom.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkUserRatingFrom.UseSelectable = true;
            this.trkUserRatingFrom.Value = new decimal(new int[] {
            0,
            0,
            0,
            65536});
            // 
            // trkUserRatingTo
            // 
            this.trkUserRatingTo.BackColor = System.Drawing.Color.Transparent;
            this.trkUserRatingTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkUserRatingTo.Location = new System.Drawing.Point(1088, 63);
            this.trkUserRatingTo.Maximum = 10;
            this.trkUserRatingTo.Minimum = 0;
            this.trkUserRatingTo.Name = "trkUserRatingTo";
            this.trkUserRatingTo.Size = new System.Drawing.Size(150, 24);
            this.trkUserRatingTo.SmallChange = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trkUserRatingTo.TabIndex = 11;
            this.trkUserRatingTo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkUserRatingTo.UseSelectable = true;
            this.trkUserRatingTo.Value = new decimal(new int[] {
            80,
            0,
            0,
            65536});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(626, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Predicted";
            // 
            // trkPredictedFrom
            // 
            this.trkPredictedFrom.BackColor = System.Drawing.Color.Transparent;
            this.trkPredictedFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkPredictedFrom.Location = new System.Drawing.Point(778, 33);
            this.trkPredictedFrom.Maximum = 10;
            this.trkPredictedFrom.Minimum = 0;
            this.trkPredictedFrom.Name = "trkPredictedFrom";
            this.trkPredictedFrom.Size = new System.Drawing.Size(149, 24);
            this.trkPredictedFrom.SmallChange = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trkPredictedFrom.TabIndex = 8;
            this.trkPredictedFrom.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkPredictedFrom.UseSelectable = true;
            this.trkPredictedFrom.Value = new decimal(new int[] {
            0,
            0,
            0,
            65536});
            // 
            // trkPredictedTo
            // 
            this.trkPredictedTo.BackColor = System.Drawing.Color.Transparent;
            this.trkPredictedTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkPredictedTo.Location = new System.Drawing.Point(778, 63);
            this.trkPredictedTo.Maximum = 10;
            this.trkPredictedTo.Minimum = 0;
            this.trkPredictedTo.Name = "trkPredictedTo";
            this.trkPredictedTo.Size = new System.Drawing.Size(149, 24);
            this.trkPredictedTo.SmallChange = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trkPredictedTo.TabIndex = 9;
            this.trkPredictedTo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkPredictedTo.UseSelectable = true;
            this.trkPredictedTo.Value = new decimal(new int[] {
            80,
            0,
            0,
            65536});
            // 
            // MovieWall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Buttons = this.pnlButtons;
            this.Controls.Add(this.moviesWall);
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlButtons);
            this.Name = "MovieWall";
            this.Size = new System.Drawing.Size(1241, 537);
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            this.pnlSearch.ResumeLayout(false);
            this.tblCriterias.ResumeLayout(false);
            this.tblCriterias.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private MovieWallLayout moviesWall;
        private MetroFramework.Controls.MetroTextBox txtSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Label lblArtist;
        private MetroFramework.Controls.MetroTextBox txtArtist;
        private System.Windows.Forms.TableLayoutPanel tblCriterias;
        private MetroFramework.Controls.MetroComboBox cmbGenres;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox cmbSaga;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroPanel pnlButtons;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.CheckBox btnFilter;
        private MetroFramework.Controls.MetroToggle chkFiles;
        private MetroFramework.Controls.MetroToggle chkViewed;
        private MetroFramework.Controls.MetroToggle chkRated;
        private MetroFramework.Controls.MetroToggle chkDesc;
        private MetroFramework.Controls.MetroToggle chkMylib;
        private MetroFramework.Controls.MetroComboBox cmbSort;
        private System.Windows.Forms.Label label3;
        private RangeSelector trkYearFrom;
        private RangeSelector trkYearTo;
        private RangeSelector trkRatingFrom;
        private RangeSelector trkRatingTo;
        private RangeSelector trkUserRatingFrom;
        private RangeSelector trkUserRatingTo;
        private System.Windows.Forms.Label lblUserRating;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private RangeSelector trkPredictedFrom;
        private RangeSelector trkPredictedTo;
    }
}
