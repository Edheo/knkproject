﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MovieModel.Lists;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Enumerations;
using System.Threading;
using System.Text.RegularExpressions;
using MovieControls.Classes;
using MovieModel.Criterias;
using MovieModel.Utilities;
using MovieModel.Enums;
using MovieControls.Utilities;
using MovieControls.Usercontrols.Other;

namespace MovieControls.Usercontrols
{
    public partial class MovieWall : NavigationItem
    {
        delegate void delNoPar();

        public event CancelEventHandler PerformSearch;
        private readonly MoviesCriteria _Parameters = new MoviesCriteria();

        bool _Initialized = false;

        bool _resend = false;

        public MovieWall()
        {
            InitializeComponent();
            trkYearFrom.Maximum = DateTime.Now.Year;
            trkYearTo.Maximum = DateTime.Now.Year;
            trkYearTo.Value = DateTime.Now.Year;
            bool designMode = (LicenseManager.UsageMode == LicenseUsageMode.Designtime);
            if (!designMode)
            {
                InitializeControl();
            }

            this.Resize += (s, e) => {
                if (!_resend)
                {
                    var lFrm = this.ParentForm;
                    if (lFrm != null)
                    {
                        lFrm.ResizeEnd += (s2, e2) => { this.OnPerformSearch(); };
                        _resend = true;
                    }
                }
            };

        }

        private void InitializeControl()
        {
            if (Connection() != null)
            {
                InitCombos();
                OnClearParams(false);
                moviesWall.LoadedItems += (s, e) => { OnFinish(); };
                _Initialized = true;
            }
        }

        public MovieWall(MoviesCriteria aParameters):this()
        {
            OnClearParams(true);
            _Parameters = aParameters;
            _Parameters.IsRelated = true;
            var Aux = cmbSaga.Items;
        }

        private void OnFinish()
        {
            if (this.InvokeRequired)
                this.Invoke(new delNoPar(SetCount));
            else
                SetCount();
        }

        private void SetCount()
        {
            lblCount.Text = $"{moviesWall.LoadedMovies()}/{_Parameters.CurrentList().Count()}";
        }

        private void  InitCombos()
        {
            if (cmbGenres.Items.Count.Equals(0)) MoviesControlsUtils.LoadCombo<Genre>(cmbGenres, "GenreName", new Genres(Connection()).Datasource());
            if (cmbSaga.Items.Count.Equals(0)) MoviesControlsUtils.LoadCombo<MovieSet>(cmbSaga, "Name", new MovieSets(Connection()).Datasource());

            var lBin = new BindingSource(KnkSolutionMoviesUtils.MoviePropertiesCombo(Connection().CurrentUser.PrimaryKeyValue().Value), null); // Key => null
            cmbSort.DataSource = lBin;
            cmbSort.DisplayMember = "Key";
            cmbSort.ValueMember = "Value";
        }

        private void LoadMovies()
        {
            GetCriteria();
            Thread lThr = new Thread(new ThreadStart(LoadMoviesThreaded));
            lThr.Start();
        }

        public void LoadMoviesThreaded()
        {
            if (Connection() != null)
            {
                moviesWall.LoadMovies(_Parameters);
            }
        }

        public void OnPerformSearch()
        {
            bool lCancel = false;
            var lCon = Connection();
            if (lCon != null && _Initialized)
            {
                if ((LicenseManager.UsageMode == LicenseUsageMode.Designtime)) return;
                if (PerformSearch != null)
                {
                    CancelEventArgs lArgs = new CancelEventArgs();
                    PerformSearch(this, lArgs);
                    lCancel = lArgs.Cancel;
                }
                if (!lCancel)
                {
                    this.LoadMovies();
                }
            }
        }

        private void OnClearParams(bool aShowAll)
        {
            txtSearch.Text = string.Empty;
            txtArtist.Text = string.Empty;
            //cmbArtist.SelectedIndex = -1;
            cmbGenres.Text = string.Empty;
            cmbGenres.SelectedIndex = -1;
            cmbSaga.Text = string.Empty;
            cmbSaga.SelectedIndex = -1;
            if (!aShowAll)
            {
                chkFiles.CheckState = CheckState.Checked;
                chkViewed.CheckState = CheckState.Unchecked;
                chkMylib.CheckState = CheckState.Checked;
                cmbSort.SelectedValue = "FileDate";
            }
            else
            {
                chkFiles.CheckState = CheckState.Indeterminate;
                chkViewed.CheckState = CheckState.Indeterminate;
                chkMylib.CheckState = CheckState.Indeterminate;
                cmbSort.SelectedValue = "ReleaseDate";
            }
            chkRated.CheckState = CheckState.Indeterminate;
            trkPredictedFrom.Value = trkPredictedFrom.Minimum;
            trkPredictedTo.Value = trkPredictedTo.Maximum;
            trkRatingFrom.Value = trkRatingFrom.Minimum;
            trkRatingTo.Value = trkRatingTo.Maximum;
            trkUserRatingFrom.Value = trkUserRatingFrom.Minimum;
            trkUserRatingTo.Value = trkUserRatingTo.Maximum;
            trkYearFrom.Value = trkYearFrom.Minimum;
            trkYearTo.Value = trkYearTo.Maximum;
        }

        private MoviesCriteria GetCriteria()
        {
            var lPar = _Parameters;
            if (!lPar.IsRelated)
            {
                lPar.User = this.Navigator().SelectedUser()?.IdUser.Value;
                lPar.InMyLib = !chkMylib.CheckState.Equals(CheckState.Indeterminate) ? chkMylib.Checked : (bool?)null;
                lPar.UserRated = !chkRated.CheckState.Equals(CheckState.Indeterminate)?chkRated.Checked : (bool?)null;
                lPar.Viewed = !chkViewed.CheckState.Equals(CheckState.Indeterminate) ? chkViewed.Checked : (bool?)null;
                lPar.HasFiles = !chkFiles.CheckState.Equals(CheckState.Indeterminate) ? chkFiles.Checked : (bool?)null;
                lPar.TextSearch = txtSearch.Text;
                lPar.PersonName = txtArtist.Text;
                lPar.Genre = cmbGenres.Text;
                lPar.Saga = cmbSaga.Text;

                lPar.YearFrom = (int?)IsMinValue(trkYearFrom);
                lPar.YearTo = (int?)IsMaxValue(trkYearTo);

                lPar.RatingFrom = IsMinValue(trkRatingFrom);
                lPar.RatingTo = IsMaxValue(trkRatingTo);

                lPar.UserRatingFrom = IsMinValue(trkUserRatingFrom);
                lPar.UserRatingTo = IsMaxValue(trkUserRatingTo);

                lPar.PredictedRatingFrom = IsMinValue(trkPredictedFrom);
                lPar.PredictedRatingTo = IsMaxValue(trkPredictedTo);


                if (cmbSort.SelectedValue != null)
                {
                    lPar.SortProperty = cmbSort.SelectedValue.ToString();
                    lPar.SortDirectionAsc = !chkDesc.Checked;
                }
            }
            else
            {
                lPar.SortProperty = cmbSort.SelectedValue.ToString();
                lPar.SortDirectionAsc = !chkDesc.Checked;
            }
            return lPar;
        }

        decimal? IsMaxValue(RangeSelector aTrk)
        {
            return aTrk.Value < aTrk.Maximum ? (decimal?)aTrk.Value : null;
        }

        decimal? IsMinValue(RangeSelector aTrk)
        {
            return aTrk.Value > aTrk.Minimum ? (decimal?)aTrk.Value : null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            OnPerformSearch();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                OnPerformSearch();
                return true; 
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            OnClearParams(false);
        }

        private void btnFilter_CheckedChanged(object sender, EventArgs e)
        {
            pnlSearch.Visible = btnFilter.Checked;
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {

        }
    }
}
