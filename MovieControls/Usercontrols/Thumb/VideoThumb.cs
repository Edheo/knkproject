﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MovieModel.Entities;
using MovieControls.Utilities;
using Vlc.DotNet.Forms;

namespace MovieControls.Usercontrols
{
    public partial class VideoThumb : PictureThumb
    {
        private Button btnPlay;
        private MediaLink _MediaLnk;

        public MediaLink MediaLink()
        {
            return _MediaLnk;
        }

        private VideoThumb()
        : base()
        {
        }

        public VideoThumb(MediaLink aMedia, int aWidth)
        : base(aWidth)
        {
            InitializeComponent();
            SetMedia(aMedia);
        }

        private void SetMedia(MediaLink aMedia)
        {
            _MediaLnk = aMedia;
            FileName = _MediaLnk.Extender.GetFileName();
            Horizontal = false;
            SetLabels(String.Empty, aMedia.ToString(), string.Empty);
            CenterButton();
        }

        public override Size NormalSize()
        {
            return MoviesControlsUtils.NormalVideoSize();
        }

        private void InitializeComponent()
        {
            this.btnPlay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Black;
            this.btnPlay.BackgroundImage = global::MovieControls.Properties.Resources.YouTube_2_128;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Location = new System.Drawing.Point(116, 189);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Padding = new System.Windows.Forms.Padding(5);
            this.btnPlay.Size = new System.Drawing.Size(41, 41);
            this.btnPlay.TabIndex = 6;
            this.btnPlay.TabStop = false;
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // VideoThumb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.btnPlay);
            this.Name = "VideoThumb";
            this.Size = new System.Drawing.Size(266, 239);
            this.Controls.SetChildIndex(this.btnPlay, 0);
            this.ResumeLayout(false);

        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            PlayForm lFrm = new PlayForm();
            lFrm.Play(_MediaLnk);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            CenterButton();
        }

        private void CenterButton()
        {
            if (btnPlay != null)
            {
                //var lRec = this.LabelRectangle();
                //btnPlay.Location = new Point((lRec.Width - btnPlay.Width) / 2, (lRec.Height - btnPlay.Height) / 2);
            }
        }

    }
}
