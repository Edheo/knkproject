﻿using System.Drawing;
using System.Windows.Forms;
using MovieControls.Classes;
using MovieControls.Enumerations;
using MetroFramework.Controls;
using KnkInterfaces.Interfaces;

namespace MovieControls.Usercontrols
{
    public partial class ElementControl : NavigationItem
    {
        public delegate void delAddLayoutControl(FlowLayoutPanel aPanel, Control aControl);
        public delegate void delNoParams();

        public ElementControl() : base()
        {
            InitializeComponent();
            picPoster.Factor(new Size(200, 310));
        }

        public void SetPoster(string aFilename)
        {
            picPoster.Filename = aFilename;
        }

        private void AddControlToFlow(FlowLayoutPanel aPanel, Control aControl)
        {
            aPanel.Controls.Add(aControl);
        }


        public void AddTabControl(string aTitle, Control aControl)
        {
            MetroTabPage lTab = new MetroTabPage();
            lTab.Enabled = true;
            lTab.HorizontalScrollbarBarColor = true;
            lTab.HorizontalScrollbarHighlightOnWheel = false;
            lTab.HorizontalScrollbarSize = 10;
            lTab.Name = aTitle;
            lTab.Padding = new Padding(5);
            lTab.Text = aTitle;
            lTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            lTab.VerticalScrollbarBarColor = true;
            lTab.VerticalScrollbarHighlightOnWheel = false;
            lTab.VerticalScrollbarSize = 10;
            if (aControl != null) lTab.Controls.Add(aControl);
            this.tabElements.TabPages.Add(lTab);
        }

        public void SetTabControl(string aName, Control aControl)
        {
            if (this.tabElements.TabPages.ContainsKey(aName))
            {
                var lTab = this.tabElements.TabPages[aName];
                lTab.Controls.Clear();
                lTab.Controls.Add(aControl);
            }
        }

    }
}
