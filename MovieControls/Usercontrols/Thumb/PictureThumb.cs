﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MovieModel.Entities;
using System.Threading;
using System.Windows.Media.Imaging;
using MovieControls.Utilities;

namespace MovieControls.Usercontrols
{
    public partial class PictureThumb : NavigationItem
    {
        public ScaleEnu ScalePicture = ScaleEnu.Normal;
        public bool AllowRemove { get; set; }
        public event EventHandler RemoveItem;

        internal PictureThumb()
        {
            InitializeComponent();
        }

        internal PictureThumb(int aWidth):this()
        {
            picPoster.Click += (sender, e) => { this.OnClick(e); };
            picPoster.MouseHover += (sender, e) => { OnRemarkMovie(sender, e); };
            SetSize(aWidth);
        }

        private void OnRemarkMovie(object sender, EventArgs e)
        {
            if (!IsMarked())
            {
                this.Parent.SuspendLayout();
                picPoster.ReMarkMovie(true);
                btnRemove.Visible = AllowRemove;
                //Control lContainer = this.Container as Control;
                var controls = from lCtn in Parent.Controls.OfType<PictureThumb>() where lCtn != this && lCtn.IsMarked() select lCtn;
                foreach (var lCtl in controls)
                {
                    lCtl.UnRemarkMovie();
                }
                this.Parent.ResumeLayout();
            }
        }

        public bool Horizontal
        {
            get
            {
                //return picVals.Horizontal;
                return false;
            }
            set
            {
                //picVals.Horizontal = value;
            }
        }
        public void UnRemarkMovie()
        {
            picPoster.ReMarkMovie(false);
            btnRemove.Visible = false;
        }

        public bool IsMarked()
        {
            return picPoster.HasBorder();
        }

        public virtual Size NormalSize()
        {
            return MoviesControlsUtils.NormalPosterSize();
        }

        public override Size MinimumSize { get { return MoviesControlsUtils.GetMinimumSize(NormalSize(), ScalePicture); } set { base.MinimumSize = MoviesControlsUtils.GetMinimumSize(NormalSize(), ScalePicture); } }

        public override Size MaximumSize { get { return MoviesControlsUtils.GetMaximumSize(NormalSize(), ScalePicture); } set { base.MaximumSize = MoviesControlsUtils.GetMaximumSize(NormalSize(), ScalePicture); } }

        public void SetSize(int aWidth)
        {
            Size = new Size(aWidth, MoviesControlsUtils.GetHeightFromWidth(MoviesControlsUtils.Aspect(NormalSize()), aWidth));
        }

        public string FileName
        {
            get
            {
                return picPoster.Filename;
            }
            set
            {
                picPoster.Filename = value;
            }
        }

        public void SetLabels(string aCaption, string aText, string aInfo)
        {
            SetLabels(aCaption, aText, aInfo, null, null, null, 0, 0);
        }

        public void SetLabels(string aCaption, string aText, string aInfo, decimal? aRating, decimal? aUserRating, decimal? aPredictedRating, int aPlayCount, int aFiles)
        {
            List<string> lMsg = new List<string>();
            string lTxt = string.Empty;
            if (aRating != null || aUserRating != null || aPredictedRating != null || aPlayCount > 0 || aFiles > 0)
            {
                if (aRating != null) lMsg.Add($"R:{aRating:0.0}");
                if (aUserRating != null) lMsg.Add($"U:{aUserRating:0.0}");
                if (aPredictedRating != null) lMsg.Add($"G:{aPredictedRating:0.0}");
                if (aPlayCount > 0) lMsg.Add($"P:{aPlayCount}");
                if (aFiles > 0) lMsg.Add($"F:{aFiles}");
                lTxt = lMsg.Aggregate((i, j) => i + " " + j);
            }
            label1.Text = aCaption;
            label2.Text = aInfo;
            label3.Text = aText;
            label4.Text = lTxt;
            label1.Visible = !string.IsNullOrEmpty(label1.Text);
            label2.Visible = !string.IsNullOrEmpty(label2.Text);
            label3.Visible = !string.IsNullOrEmpty(label3.Text);
            label4.Visible = !string.IsNullOrEmpty(lTxt);
        }

        public void SetLabels(string aCaption, string aText, string aInfo, int? aMovies, decimal? aRating, int? aRatedMovies, decimal? aUserRating)
        {
            List<string> lMsg = new List<string>();
            string lTxt = string.Empty;
            if (aRating != null) lMsg.Add($"R:{aRating:0.0}");
            if (aMovies != null || aRatedMovies != null || aUserRating != null)
            {
                if (aMovies != null) lMsg.Add($"M:{aMovies}");
                if (aRatedMovies != null) lMsg.Add($"V:{aRatedMovies}");
                if (aUserRating != null) lMsg.Add($"U:{aUserRating:0.0}");
                lTxt = lMsg.Aggregate((i, j) => i + " " + j);
            }
            label1.Text = aCaption;
            label2.Text = aInfo;
            label3.Text = aText;
            label4.Text = lTxt;
            label1.Visible = !string.IsNullOrEmpty(label1.Text);
            label2.Visible = !string.IsNullOrEmpty(label2.Text);
            label3.Visible = !string.IsNullOrEmpty(label3.Text);
            label4.Visible = !string.IsNullOrEmpty(lTxt);
        }

        public Rectangle PictureRectangle()
        {
            return picPoster.ClientRectangle;
        }

        private void picPoster_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.FileName) && System.IO.File.Exists(this.FileName))
            {
                var lPho = MoviesControlsUtils.PictureHolder(this);
                if (lPho != null && !(this is PersonThumb) && !(this is MovieThumb))
                    lPho.SetPoster(this.FileName);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            RemoveItem?.Invoke(this, new EventArgs());
        }
    }
}
