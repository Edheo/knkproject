﻿using MovieControls.Classes;

namespace MovieControls.Usercontrols
{
    partial class ElementControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new MetroFramework.Controls.MetroPanel();
            this.tabElements = new MetroFramework.Controls.MetroTabControl();
            this.picPoster = new MovieControls.Usercontrols.MoviePicture();
            this.tblProperties = new MovieControls.Usercontrols.ListProperties();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.HorizontalScrollbarBarColor = true;
            this.pnlButtons.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlButtons.HorizontalScrollbarSize = 10;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(678, 29);
            this.pnlButtons.TabIndex = 0;
            this.pnlButtons.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.pnlButtons.VerticalScrollbarBarColor = true;
            this.pnlButtons.VerticalScrollbarHighlightOnWheel = false;
            this.pnlButtons.VerticalScrollbarSize = 10;
            // 
            // tabElements
            // 
            this.tabElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabElements.Location = new System.Drawing.Point(302, 29);
            this.tabElements.Name = "metroTabControl1";
            this.tabElements.Padding = new System.Drawing.Point(6, 8);
            this.tabElements.Size = new System.Drawing.Size(376, 452);
            this.tabElements.TabIndex = 5;
            this.tabElements.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tabElements.UseSelectable = true;
            this.tabElements.UseStyleColors = true;
            // 
            // picPoster
            // 
            this.picPoster.BackColor = System.Drawing.Color.Black;
            this.picPoster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picPoster.Dock = System.Windows.Forms.DockStyle.Left;
            this.picPoster.FactorSize = 0F;
            this.picPoster.Filename = null;
            this.picPoster.IsButton = false;
            this.picPoster.Location = new System.Drawing.Point(0, 29);
            this.picPoster.Name = "picPoster";
            this.picPoster.RemarkColor = System.Drawing.Color.Red;
            this.picPoster.ResourceImage = null;
            this.picPoster.Size = new System.Drawing.Size(302, 452);
            this.picPoster.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
            this.picPoster.TabIndex = 3;
            this.picPoster.TabStop = false;
            // 
            // tblProperties
            // 
            this.tblProperties.AutoScroll = true;
            this.tblProperties.BackColor = System.Drawing.Color.Black;
            this.tblProperties.ColumnCount = 2;
            this.tblProperties.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblProperties.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tblProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblProperties.Location = new System.Drawing.Point(5, 5);
            this.tblProperties.Name = "tblProperties";
            this.tblProperties.RowCount = 1;
            this.tblProperties.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tblProperties.Size = new System.Drawing.Size(200, 100);
            this.tblProperties.TabIndex = 0;
            // 
            // ElementControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Buttons = this.pnlButtons;
            this.Controls.Add(this.tabElements);
            this.Controls.Add(this.picPoster);
            this.Controls.Add(this.pnlButtons);
            this.Name = "ElementControl";
            this.Size = new System.Drawing.Size(678, 481);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTabControl tabElements;
        public ListProperties tblProperties;
        private MetroFramework.Controls.MetroPanel pnlButtons;
        private MoviePicture picPoster;
    }
}
