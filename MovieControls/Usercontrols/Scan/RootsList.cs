﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MovieModel.Lists;

namespace MovieControls.Usercontrols.Scan
{
    public partial class RootsList : MetroFramework.Controls.MetroUserControl
    {
        Roots _roots;

        public RootsList()
        {
            InitializeComponent();
        }

        public Roots Roots
        {
            get { return _roots; }
            set
            {
                _roots = value;
                if (_roots != null)
                {
                    grdRoots.AutoGenerateColumns = true;
                    grdRoots.DataSource = _roots.Items.Select(o => new { DateAdded = o.CreationDate, Path = o.Path, Files = o.Files }).ToList();

                    grdRoots.Width = GridRootsWidth();
                }
            }
        }

        private int GridRootsWidth()
        {
            int width = 0;
            foreach (DataGridViewColumn col in grdRoots.Columns)
            {
                width += col.Width;
            }
            width += grdRoots.RowHeadersWidth * 3;
            return width;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                string folder = fbd.SelectedPath;
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folder))
                {
                    var uri = new Uri(folder);
                    if (uri.IsUnc || MetroMessageBox.Show(this, "This is not an unc folder, do you want to continue?", folder, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var lRot = _roots.Create();
                        lRot.ContentType = _roots.MediaType.ToString();
                        lRot.Path = folder;
                        lRot.Update($"Added Root to {_roots.MediaType.ToString()}");
                        _roots.SaveChanges();
                    }
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var lSel = Roots.Items.Where(f => f.Path == grdRoots.Columns["Path"].ToString()).FirstOrDefault();
            if(lSel!=null && MetroMessageBox.Show(this, "Do you want to remove this Root folder?", lSel.Path, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                lSel.Delete("Deleted by user");
                Roots.SaveChanges();
            }
        }
    }
}
