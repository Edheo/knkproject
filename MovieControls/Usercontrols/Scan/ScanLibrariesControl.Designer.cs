﻿using MetroFramework.Controls;

namespace MovieControls.Usercontrols
{
    partial class ScanLibrariesControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlButtons = new MetroFramework.Controls.MetroPanel();
            this.btnFromvds = new System.Windows.Forms.Button();
            this.btnOrganizer = new System.Windows.Forms.Button();
            this.btnFavorites = new System.Windows.Forms.Button();
            this.btnBestMovies = new System.Windows.Forms.Button();
            this.btnScrapFiles = new System.Windows.Forms.Button();
            this.btnScanFolders = new System.Windows.Forms.Button();
            this.grdResults = new MetroFramework.Controls.MetroGrid();
            this.ctlroots = new MovieControls.Usercontrols.Scan.RootsList();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdResults)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.Color.Black;
            this.pnlButtons.Controls.Add(this.btnFromvds);
            this.pnlButtons.Controls.Add(this.btnOrganizer);
            this.pnlButtons.Controls.Add(this.btnFavorites);
            this.pnlButtons.Controls.Add(this.btnBestMovies);
            this.pnlButtons.Controls.Add(this.btnScrapFiles);
            this.pnlButtons.Controls.Add(this.btnScanFolders);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.HorizontalScrollbarBarColor = true;
            this.pnlButtons.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlButtons.HorizontalScrollbarSize = 10;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(753, 41);
            this.pnlButtons.TabIndex = 7;
            this.pnlButtons.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.pnlButtons.VerticalScrollbarBarColor = true;
            this.pnlButtons.VerticalScrollbarHighlightOnWheel = false;
            this.pnlButtons.VerticalScrollbarSize = 10;
            // 
            // btnFromvds
            // 
            this.btnFromvds.BackColor = System.Drawing.Color.Transparent;
            this.btnFromvds.BackgroundImage = global::MovieControls.Properties.Resources._1478665032_left_top;
            this.btnFromvds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromvds.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFromvds.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFromvds.Image = global::MovieControls.Properties.Resources.descarga;
            this.btnFromvds.Location = new System.Drawing.Point(507, 0);
            this.btnFromvds.Name = "btnFromvds";
            this.btnFromvds.Padding = new System.Windows.Forms.Padding(5);
            this.btnFromvds.Size = new System.Drawing.Size(41, 41);
            this.btnFromvds.TabIndex = 13;
            this.btnFromvds.TabStop = false;
            this.btnFromvds.UseVisualStyleBackColor = false;
            this.btnFromvds.Click += new System.EventHandler(this.btnFromvds_Click);
            // 
            // btnOrganizer
            // 
            this.btnOrganizer.BackColor = System.Drawing.Color.Transparent;
            this.btnOrganizer.BackgroundImage = global::MovieControls.Properties.Resources._1478665032_left_top;
            this.btnOrganizer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOrganizer.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOrganizer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrganizer.Location = new System.Drawing.Point(548, 0);
            this.btnOrganizer.Name = "btnOrganizer";
            this.btnOrganizer.Padding = new System.Windows.Forms.Padding(5);
            this.btnOrganizer.Size = new System.Drawing.Size(41, 41);
            this.btnOrganizer.TabIndex = 12;
            this.btnOrganizer.TabStop = false;
            this.btnOrganizer.UseVisualStyleBackColor = false;
            this.btnOrganizer.Click += new System.EventHandler(this.btnOrganizer_Click);
            // 
            // btnFavorites
            // 
            this.btnFavorites.BackColor = System.Drawing.Color.Transparent;
            this.btnFavorites.BackgroundImage = global::MovieControls.Properties.Resources._1473118102_favorite;
            this.btnFavorites.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFavorites.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFavorites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFavorites.Location = new System.Drawing.Point(589, 0);
            this.btnFavorites.Name = "btnFavorites";
            this.btnFavorites.Padding = new System.Windows.Forms.Padding(5);
            this.btnFavorites.Size = new System.Drawing.Size(41, 41);
            this.btnFavorites.TabIndex = 11;
            this.btnFavorites.TabStop = false;
            this.btnFavorites.UseVisualStyleBackColor = false;
            this.btnFavorites.Click += new System.EventHandler(this.btnFavorites_Click);
            // 
            // btnBestMovies
            // 
            this.btnBestMovies.BackColor = System.Drawing.Color.Transparent;
            this.btnBestMovies.BackgroundImage = global::MovieControls.Properties.Resources._1472759323_Movies___Oscar;
            this.btnBestMovies.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBestMovies.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBestMovies.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBestMovies.Location = new System.Drawing.Point(630, 0);
            this.btnBestMovies.Name = "btnBestMovies";
            this.btnBestMovies.Padding = new System.Windows.Forms.Padding(5);
            this.btnBestMovies.Size = new System.Drawing.Size(41, 41);
            this.btnBestMovies.TabIndex = 10;
            this.btnBestMovies.TabStop = false;
            this.btnBestMovies.UseVisualStyleBackColor = false;
            this.btnBestMovies.Click += new System.EventHandler(this.btnBestMovies_Click);
            // 
            // btnScrapFiles
            // 
            this.btnScrapFiles.BackColor = System.Drawing.Color.Transparent;
            this.btnScrapFiles.BackgroundImage = global::MovieControls.Properties.Resources._1472759083_interact;
            this.btnScrapFiles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnScrapFiles.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnScrapFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScrapFiles.Location = new System.Drawing.Point(671, 0);
            this.btnScrapFiles.Name = "btnScrapFiles";
            this.btnScrapFiles.Padding = new System.Windows.Forms.Padding(5);
            this.btnScrapFiles.Size = new System.Drawing.Size(41, 41);
            this.btnScrapFiles.TabIndex = 9;
            this.btnScrapFiles.TabStop = false;
            this.btnScrapFiles.UseVisualStyleBackColor = false;
            this.btnScrapFiles.Click += new System.EventHandler(this.btnScrapFiles_Click);
            // 
            // btnScanFolders
            // 
            this.btnScanFolders.BackColor = System.Drawing.Color.Transparent;
            this.btnScanFolders.BackgroundImage = global::MovieControls.Properties.Resources._1472759171_icontexto_aurora_folders_movies;
            this.btnScanFolders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnScanFolders.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnScanFolders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScanFolders.Location = new System.Drawing.Point(712, 0);
            this.btnScanFolders.Name = "btnScanFolders";
            this.btnScanFolders.Padding = new System.Windows.Forms.Padding(5);
            this.btnScanFolders.Size = new System.Drawing.Size(41, 41);
            this.btnScanFolders.TabIndex = 5;
            this.btnScanFolders.TabStop = false;
            this.btnScanFolders.UseVisualStyleBackColor = false;
            this.btnScanFolders.Click += new System.EventHandler(this.butScan_Click);
            // 
            // grdResults
            // 
            this.grdResults.AllowUserToAddRows = false;
            this.grdResults.AllowUserToDeleteRows = false;
            this.grdResults.AllowUserToResizeRows = false;
            this.grdResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grdResults.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.grdResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdResults.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdResults.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdResults.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdResults.EnableHeadersVisualStyles = false;
            this.grdResults.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grdResults.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.grdResults.Location = new System.Drawing.Point(429, 41);
            this.grdResults.Name = "grdResults";
            this.grdResults.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdResults.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdResults.Size = new System.Drawing.Size(324, 489);
            this.grdResults.Style = MetroFramework.MetroColorStyle.Purple;
            this.grdResults.TabIndex = 10;
            this.grdResults.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // ctlroots
            // 
            this.ctlroots.Dock = System.Windows.Forms.DockStyle.Left;
            this.ctlroots.Location = new System.Drawing.Point(0, 41);
            this.ctlroots.Name = "ctlroots";
            this.ctlroots.Roots = null;
            this.ctlroots.Size = new System.Drawing.Size(429, 489);
            this.ctlroots.TabIndex = 11;
            this.ctlroots.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ctlroots.UseSelectable = true;
            // 
            // ScanLibrariesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Buttons = this.pnlButtons;
            this.Controls.Add(this.grdResults);
            this.Controls.Add(this.ctlroots);
            this.Controls.Add(this.pnlButtons);
            this.Name = "ScanLibrariesControl";
            this.Size = new System.Drawing.Size(753, 530);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.pnlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdResults)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel pnlButtons;
        private MetroFramework.Controls.MetroGrid grdResults;
        private System.Windows.Forms.Button btnScanFolders;
        private System.Windows.Forms.Button btnScrapFiles;
        private System.Windows.Forms.Button btnBestMovies;
        private System.Windows.Forms.Button btnFavorites;
        private Scan.RootsList ctlroots;
        private System.Windows.Forms.Button btnOrganizer;
        private System.Windows.Forms.Button btnFromvds;
    }
}