﻿using KnkInterfaces.Interfaces;
using KnkScrapers.BooksScraper;
using KnkScrapers.Classes;
using KnkScrapers.MoviesScraper;
using MetroFramework.Forms;
using MovieModel.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace MovieControls.Usercontrols
{
    public partial class ScanLibrariesControl : NavigationItem
    {
        KnkConnectionItf _Connnection;
        EnrichCollections _Enricher;
        MediaTypeEnum _MediaType;
        //BindingSource _Bing = new BindingSource();

        private ScanLibrariesControl()
        {
            InitializeComponent();
        }

        public ScanLibrariesControl(MediaTypeEnum aMediaType, KnkConnectionItf aCon):this()
        {
            _Connnection = aCon;
            _MediaType = aMediaType;
            _Enricher = GetEnricher(aMediaType);
            ctlroots.Roots = _Enricher.Roots;
            grdResults.AutoGenerateColumns = true;
            InitializeOptions();
            ProcessChanged();
        }

        private EnrichCollections GetEnricher(MediaTypeEnum aMediaType)
        {
            switch(aMediaType)
            {
                case MediaTypeEnum.Movies:
                    return new MovieEnricher(_Connnection);
                case MediaTypeEnum.Books:
                    return new BookEnricher(_Connnection);
                default:
                    return new MovieEnricher(_Connnection);
            }
        }

        private void InitializeOptions()
        {
            btnBestMovies.Visible = _MediaType == MediaTypeEnum.Movies;
            btnFavorites.Visible = _MediaType == MediaTypeEnum.Movies;
        }

        private void OnSyncFiles(Button aButton)
        {
            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                aButton.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.ScanForNewFiles(bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(aButton); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            ProcessChanged();
        }

        private void OnScrapFiles(Button aButton)
        {
            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                aButton.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.ScrapFiles(bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(aButton); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            ProcessChanged();
        }

        private void OnBestMovies(Button aButton)
        {
            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                aButton.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.BestOnes(bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(aButton); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            ProcessChanged();
        }

        private void OnRatedMovies(Button aButton)
        {
            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                aButton.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.RatedOnes(bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(aButton); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            ProcessChanged();
        }

        private void OnArrangeFiles(Button aButton)
        {
            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                aButton.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.ArrangeFiles(bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(aButton); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            ProcessChanged();
        }

        private void OnImportVideoStation(Button aButton)
        {
            if (!_Enricher.IsBusy)
            {
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                aButton.Image = global::MovieControls.Properties.Resources.Ani200_2;
                bw.DoWork += (sender, args) =>
                {
                    _Enricher.ImportVideoStation(bw);
                };
                bw.ProgressChanged += (sender, args) => { ProcessChanged(); };
                bw.RunWorkerCompleted += (sender, args) => { ProcessFinished(aButton); };
                bw.RunWorkerAsync(); // starts the background worker
            }
            ProcessChanged();
        }

        void ProcessChanged()
        {
            if(grdResults.DataSource!=null) grdResults.DataSource = typeof(List<>);
            var res = _Enricher.Results.Messages;
            grdResults.DataSource = res.Skip(Math.Max(0, res.Count() - 50)).ToList().Reverse<KnkChangeDescriptorItf>().ToList();
        }

        void ProcessFinished(Button aButton)
        {
            aButton.Image = null;
        }

        private void butScan_Click(object sender, EventArgs e)
        {
            OnSyncFiles(btnScanFolders);
        }

        private void btnScrapFiles_Click(object sender, EventArgs e)
        {
            OnScrapFiles(btnScrapFiles);
        }

        private void btnBestMovies_Click(object sender, EventArgs e)
        {
            OnBestMovies(btnBestMovies);
        }

        private void btnFavorites_Click(object sender, EventArgs e)
        {
            OnRatedMovies(btnFavorites);
        }

        private void btnOrganizer_Click(object sender, EventArgs e)
        {
            OnArrangeFiles(btnOrganizer);
        }

        private void btnFromvds_Click(object sender, EventArgs e)
        {
            OnImportVideoStation(btnFromvds);
        }
    }
}
