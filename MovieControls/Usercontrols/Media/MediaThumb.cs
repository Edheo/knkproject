﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MovieModel.Entities;

namespace MovieControls.Usercontrols
{
    public partial class MediaThumb : PictureThumb
    {
        private MediaLink _Media;

        public MediaLink Media()
        {
            return _Media;
        }

        private MediaThumb()
        : base()
        {
        }

        public MediaThumb(MediaLink aMedia, int aWidth)
        : base(aWidth)
        {
            SetMedia(aMedia);
        }

        private void SetMedia(MediaLink aMedia)
        {
            _Media = aMedia;
            FileName = _Media.Extender.GetFileName();
            Horizontal = false;
            SetLabels(String.Empty, aMedia.Site, string.Empty);
        }
    }
}
