using System;
using System.IO;

namespace MovieControls.Usercontrols
{
    public sealed class VlcLibDirectoryNeededEventArgs : EventArgs
    {
        public DirectoryInfo VlcLibDirectory { get; set; }

        public VlcLibDirectoryNeededEventArgs()
        {
            
        }
    }
}