﻿namespace MovieControls.Usercontrols
{
    partial class NavigatorControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.pnlButtons = new MetroFramework.Controls.MetroPanel();
            this.btnScan = new System.Windows.Forms.Button();
            this.cmbLibrary = new MetroFramework.Controls.MetroComboBox();
            this.cmbUser = new MetroFramework.Controls.MetroComboBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCfg = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.pnlNav = new MetroFramework.Controls.MetroPanel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.pnlButtons);
            this.metroPanel1.Controls.Add(this.btnScan);
            this.metroPanel1.Controls.Add(this.cmbLibrary);
            this.metroPanel1.Controls.Add(this.cmbUser);
            this.metroPanel1.Controls.Add(this.btnBack);
            this.metroPanel1.Controls.Add(this.btnCfg);
            this.metroPanel1.Controls.Add(this.btnHome);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(547, 39);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.HorizontalScrollbarBarColor = true;
            this.pnlButtons.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlButtons.HorizontalScrollbarSize = 10;
            this.pnlButtons.Location = new System.Drawing.Point(406, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(141, 39);
            this.pnlButtons.TabIndex = 7;
            this.pnlButtons.VerticalScrollbarBarColor = true;
            this.pnlButtons.VerticalScrollbarHighlightOnWheel = false;
            this.pnlButtons.VerticalScrollbarSize = 10;
            // 
            // btnScan
            // 
            this.btnScan.BackColor = System.Drawing.Color.Transparent;
            this.btnScan.BackgroundImage = global::MovieControls.Properties.Resources._1466916706_gtk_refresh;
            this.btnScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnScan.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScan.Location = new System.Drawing.Point(365, 0);
            this.btnScan.Name = "btnScan";
            this.btnScan.Padding = new System.Windows.Forms.Padding(5);
            this.btnScan.Size = new System.Drawing.Size(41, 39);
            this.btnScan.TabIndex = 27;
            this.btnScan.TabStop = false;
            this.btnScan.UseVisualStyleBackColor = false;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // cmbLibrary
            // 
            this.cmbLibrary.Dock = System.Windows.Forms.DockStyle.Left;
            this.cmbLibrary.FormattingEnabled = true;
            this.cmbLibrary.ItemHeight = 23;
            this.cmbLibrary.Location = new System.Drawing.Point(244, 0);
            this.cmbLibrary.Name = "cmbLibrary";
            this.cmbLibrary.Size = new System.Drawing.Size(121, 29);
            this.cmbLibrary.TabIndex = 28;
            this.cmbLibrary.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cmbLibrary.UseSelectable = true;
            // 
            // cmbUser
            // 
            this.cmbUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.ItemHeight = 23;
            this.cmbUser.Location = new System.Drawing.Point(123, 0);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(121, 29);
            this.cmbUser.TabIndex = 26;
            this.cmbUser.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cmbUser.UseSelectable = true;
            this.cmbUser.SelectedIndexChanged += new System.EventHandler(this.cmbUser_SelectedIndexChanged);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.BackgroundImage = global::MovieControls.Properties.Resources._1466884390_pre;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(82, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Padding = new System.Windows.Forms.Padding(5);
            this.btnBack.Size = new System.Drawing.Size(41, 39);
            this.btnBack.TabIndex = 8;
            this.btnBack.TabStop = false;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnCfg
            // 
            this.btnCfg.BackColor = System.Drawing.Color.Transparent;
            this.btnCfg.BackgroundImage = global::MovieControls.Properties.Resources._1467079072_gnome_system_config;
            this.btnCfg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCfg.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCfg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCfg.Location = new System.Drawing.Point(41, 0);
            this.btnCfg.Name = "btnCfg";
            this.btnCfg.Padding = new System.Windows.Forms.Padding(5);
            this.btnCfg.Size = new System.Drawing.Size(41, 39);
            this.btnCfg.TabIndex = 9;
            this.btnCfg.TabStop = false;
            this.btnCfg.UseVisualStyleBackColor = false;
            this.btnCfg.Click += new System.EventHandler(this.btnCfg_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Transparent;
            this.btnHome.BackgroundImage = global::MovieControls.Properties.Resources.Home;
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Location = new System.Drawing.Point(0, 0);
            this.btnHome.Name = "btnHome";
            this.btnHome.Padding = new System.Windows.Forms.Padding(5);
            this.btnHome.Size = new System.Drawing.Size(41, 39);
            this.btnHome.TabIndex = 6;
            this.btnHome.TabStop = false;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // pnlNav
            // 
            this.pnlNav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNav.HorizontalScrollbarBarColor = true;
            this.pnlNav.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlNav.HorizontalScrollbarSize = 10;
            this.pnlNav.Location = new System.Drawing.Point(0, 39);
            this.pnlNav.Name = "pnlNav";
            this.pnlNav.Size = new System.Drawing.Size(547, 286);
            this.pnlNav.Style = MetroFramework.MetroColorStyle.Blue;
            this.pnlNav.TabIndex = 1;
            this.pnlNav.VerticalScrollbarBarColor = true;
            this.pnlNav.VerticalScrollbarHighlightOnWheel = false;
            this.pnlNav.VerticalScrollbarSize = 10;
            // 
            // NavigatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlNav);
            this.Controls.Add(this.metroPanel1);
            this.Name = "NavigatorControl";
            this.Size = new System.Drawing.Size(547, 325);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Button btnHome;
        private MetroFramework.Controls.MetroPanel pnlNav;
        private MetroFramework.Controls.MetroPanel pnlButtons;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnCfg;
        private MetroFramework.Controls.MetroComboBox cmbUser;
        private System.Windows.Forms.Button btnScan;
        private MetroFramework.Controls.MetroComboBox cmbLibrary;
    }
}
