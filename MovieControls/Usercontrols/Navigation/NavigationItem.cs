﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MovieControls.Utilities;
using KnkInterfaces.Interfaces;

namespace MovieControls.Usercontrols
{
    public partial class NavigationItem : MetroUserControl
    {
        MetroPanel _Buttons;

        public NavigationItem()
        {
            InitializeComponent();
        }

        public MetroPanel Buttons
        {
            get
            {
                return _Buttons;
            }

            set
            {
                _Buttons = value;
            }
        }

        public NavigatorControl Navigator()
        {
            return MoviesControlsUtils.Navigator;
        }

        public KnkConnectionItf Connection()
        {
            return Navigator().Connection();
        }

        public ElementControl PictureHolder()
        {
            return MoviesControlsUtils.PictureHolder(this);
        }


    }
}
