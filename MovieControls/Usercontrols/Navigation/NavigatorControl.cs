﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MovieControls.Utilities;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using KnkCore;
using MovieModel.Lists;
using MovieModel.Enums;

namespace MovieControls.Usercontrols
{
    public partial class NavigatorControl : MetroFramework.Controls.MetroUserControl
    {
        Stack<Control> _Previous = new Stack<Control>();
        private KnkConnectionItf _Connection;

        Control _Current;

        public NavigatorControl()
        {
            MoviesControlsUtils.Navigator = this;
            InitializeComponent();
            _Connection = new KnkConnection();
            var lUsr = new Users(Connection()).Datasource();
            MoviesControlsUtils.LoadCombo(cmbUser, "Username", lUsr, (Connection().CurrentUser as User).Username);
            MoviesControlsUtils.LoadCombo(cmbLibrary, Enum.GetValues(typeof(MediaTypeEnum)).Cast<MediaTypeEnum>().ToList());
        }

        public void Navigate(Control aControl)
        {
            if (_Current != null) _Previous.Push(_Current);
            SetCurrentControl(aControl);
        }

        private void SetCurrentControl(Control aControl)
        {
            pnlNav.Controls.Clear();
            pnlButtons.Controls.Clear();
            _Current = aControl;
            aControl.Dock = DockStyle.Fill;
            pnlNav.Controls.Add(_Current);
            NavigationItem ctlItm = aControl as NavigationItem;
            if (ctlItm != null)
            {
                if (ctlItm.Buttons != null)
                {
                    this.pnlButtons.Controls.Add(ctlItm.Buttons);
                }
            }
            this.metroPanel1.SuspendLayout();
            btnHome.Visible = _Previous.Count > 0;
            btnBack.Visible = _Previous.Count > 0;
            btnCfg.Visible = _Previous.Count == 0;
            btnScan.Visible = _Previous.Count == 0;
            cmbUser.Visible = _Previous.Count == 0;
            cmbLibrary.Visible = _Previous.Count == 0;
            this.metroPanel1.ResumeLayout(true);
            metroPanel1.Refresh();
        }

        public void Home()
        {
            if (_Previous.Count > 0)
            {
                var lCtl = _Previous.Peek();
                if (lCtl != null)
                {
                    _Previous = new Stack<Control>();
                    _Current = null;
                    Navigate(lCtl);
                }
            }
        }

        public void Previous()
        {
            if(_Previous.Count>0)
            {
                var lCtl = _Previous.Pop();
                SetCurrentControl(lCtl);
            }
        }

        public KnkConnectionItf Connection()
        {
            return _Connection;
        }

        public MediaTypeEnum MediaType()
        {
            return (MediaTypeEnum)cmbLibrary.SelectedItem;
        }

        public Control CurrentControl
        {
            get
            {
                return _Current;
            }
            set
            {
                Navigate(value);
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            Previous();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Home();
        }

        private void btnCfg_Click(object sender, EventArgs e)
        {
            MoviesControlsUtils.CheckConfiguration(true);
        }

        private void cmbUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUser.SelectedIndex >= 0)
                _Connection.CurrentUser = cmbUser.Items[cmbUser.SelectedIndex] as User;
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            ScanLibrariesControl lFrm = new ScanLibrariesControl(this.MediaType(), Connection());
            Navigate(lFrm);
        }

        public User SelectedUser()
        {
            return cmbUser.SelectedItem as User;
        }
    }
}
