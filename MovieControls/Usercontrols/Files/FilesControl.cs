﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using KnkCore;
using MovieModel.Entities;
using MetroFramework;
using Vlc.DotNet.Forms;

namespace MovieControls.Usercontrols
{
    public partial class FilesControl : NavigationItem
    {
        public FilesControl()
        {
            InitializeComponent();
        }

        public void SetFiles(List<File> aList)
        {
            cboFiles.DisplayMember = "Filename";
            cboFiles.DataSource = aList;
        }

        private void OnPlay()
        {
            PlayForm lFrm = new PlayForm();
            var lFile = prpProperties.SelectedObject as File;
            var lScr = Screen.FromPoint(Cursor.Position);
            lFrm.StartPosition = FormStartPosition.Manual;
            lFrm.Left = lScr.Bounds.Left + lScr.Bounds.Width / 2 - lFrm.Width / 2;
            lFrm.Top = lScr.Bounds.Top + lScr.Bounds.Height / 2 - lFrm.Height / 2;
            lFrm.Play(lFile);
        }

        private void OnRescan()
        {
            Navigator()?.Navigate(new MovieSearch(CurrentFile));
        }

        private void cboFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            prpProperties.SelectedObject = cboFiles.SelectedItem;
        }

        private File CurrentFile
        {
            get
            {
                return prpProperties.SelectedObject as File;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var _File = CurrentFile;
            var lRes = MetroMessageBox.Show(this, "Do you want to remove this file from the sytem?", _File.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lRes == DialogResult.Yes)
            {
                System.IO.File.Delete(_File.ToString());
                _File.Delete("Removed File from system");
                _File.SaveChanges();
                _File.MovieFiles().DeleteAll("Removed Movies from File");
                _File.MovieFiles().SaveChanges();
                Navigator().Previous();
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            OnPlay();
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            OnRescan();
        }
    }
}
