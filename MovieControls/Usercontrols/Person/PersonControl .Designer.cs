﻿namespace MovieControls.Usercontrols
{
    partial class PersonControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.floArt = new System.Windows.Forms.FlowLayoutPanel();
            this.floVideos = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // floArt
            // 
            this.floArt.AutoScroll = true;
            this.floArt.BackColor = System.Drawing.Color.Black;
            this.floArt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.floArt.Location = new System.Drawing.Point(5, 5);
            this.floArt.Name = "floArt";
            this.floArt.Size = new System.Drawing.Size(460, 400);
            this.floArt.TabIndex = 8;
            // 
            // floVideos
            // 
            this.floVideos.AutoScroll = true;
            this.floVideos.BackColor = System.Drawing.Color.Black;
            this.floVideos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.floVideos.Location = new System.Drawing.Point(5, 5);
            this.floVideos.Name = "floVideos";
            this.floVideos.Size = new System.Drawing.Size(460, 400);
            this.floVideos.TabIndex = 7;
            // 
            // CastingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Name = "CastingControl";
            this.Load += new System.EventHandler(this.CastingControl_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel floVideos;
        private System.Windows.Forms.FlowLayoutPanel floArt;
        private MovieWall walMovies;
    }
}
