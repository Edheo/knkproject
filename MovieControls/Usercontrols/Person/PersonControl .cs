﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MovieModel.Entities;
using KnkScrapers.Classes;
using System.Threading;
using Vlc.DotNet.Forms;
using MovieControls.Utilities;
using MovieControls.Classes;
using MovieControls.Enumerations;
using MovieModel.Criterias;

namespace MovieControls.Usercontrols
{
    public partial class PersonControl : ElementControl
    {
        private Person _Person;

        public PersonControl(Person aPerson) : base()
        {
            InitializeComponent();
            InitializeControl(aPerson);
            SetCasting(aPerson);
        }

        private void InitializeControl(Person aPerson)
        {
            MoviesCriteria lPar = new MoviesCriteria(personid: aPerson.IdPerson.Value.Value);
            walMovies = new MovieWall(lPar);
            walMovies.Dock = DockStyle.Fill;
            this.AddTabControl("Info", tblProperties);
            this.AddTabControl("Movies", walMovies);
            this.AddTabControl("Art", floArt);
            this.AddTabControl("Videos", floVideos);
        }

        private void SetCasting(Person aPerson)
        {
            _Person = aPerson;
            tblProperties.ClearRows();
            SetPoster(_Person.Extender.Poster?.Extender.GetFileName());

            tblProperties.AddTagInfo("Name", $"{_Person.Name}");
            tblProperties.AddTagInfo("Birthday", $"{_Person.BirthDay}");
            tblProperties.AddTagInfo("Birthplace", $"{_Person.BirthPlace}");
            tblProperties.AddTagInfo("Deathdaty", $"{_Person.DeathDay}");
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("Movies", $"{_Person.Movies}");
            tblProperties.AddTagInfo("Popularity", $"{_Person.Popularity}");
            tblProperties.AddRating("Rating", _Person.Rating);
            tblProperties.AddTagInfo("Viewed", $"{_Person.RatedMovies}");
            tblProperties.AddRating("User Rated", _Person.UserRating);
            tblProperties.AddTagInfo();
            tblProperties.AddTagInfo("Homepage", $"{_Person.HomePage}", LinkTypeEnu.Url, _Person.Connection(), "Url");
            tblProperties.AddTagInfo();
            tblProperties.AddTextInfo("Biography", $"{_Person.Extender.Biography}");
            tblProperties.AddTagInfo();
        }

        public Person Person
        {
            get { return _Person; }
        }

        private void AddVideosThread()
        {
            var lThr = new Thread(() => AddVideos());
            lThr.Start();
        }

        private void AddArtThread()
        {
            var lThr = new Thread(() => AddArt());
            lThr.Start();
        }

        private void AddControlToFlow(FlowLayoutPanel aPanel, Control aControl)
        {
            aPanel.Controls.Add(aControl);
        }

        private void AddFlowBreak(FlowLayoutPanel aPanel, Control aControl)
        {
            aPanel.SetFlowBreak(aControl, true);
        }

        private void AddVideos()
        {
            var lMedias = (from vid in _Person.Pictures().Items where vid.IdType==MovieModel.Enums.LinkTypeEnum.Videos select vid);
            foreach (var med in lMedias)
            {
                VideoThumb lTmb = new VideoThumb(med, 200);
                if (floVideos.InvokeRequired)
                {
                    this.Invoke(new delAddLayoutControl(AddControlToFlow), floVideos, lTmb);
                }
                else
                    AddControlToFlow(floVideos, lTmb);
            }
        }

        private void AddArt()
        {
            var lArt = (from art in _Person.Pictures().Items where art.IdType!=MovieModel.Enums.LinkTypeEnum.Videos select art);
            foreach (var med in lArt)
            {
                MediaThumb lTmb = new MediaThumb(med, 60);
                if (floArt.InvokeRequired)
                {
                    this.Invoke(new delAddLayoutControl(AddControlToFlow), floArt, lTmb);
                }
                else
                    AddControlToFlow(floArt, lTmb);
            }
        }

        private void CastingControl_Load(object sender, EventArgs e)
        {
            AddVideosThread();
            AddArtThread();
        }

    }
}
