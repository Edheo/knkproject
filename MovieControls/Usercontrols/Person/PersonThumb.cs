﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MovieModel.Entities;

namespace MovieControls.Usercontrols
{
    public partial class PersonThumb : PictureThumb
    {
        private MovieCasting _Casting;

        public MovieCasting Casting()
        {
            return _Casting;
        }

        private PersonThumb()
        : base()
        {
        }

        public PersonThumb(MovieCasting aCasting, int aWidth)
        : base(aWidth)
        {
            SetCasting(aCasting);
        }

        private void SetCasting(MovieCasting aCasting)
        {
            _Casting = aCasting;
            FileName = _Casting.IdPerson.Reference.Extender.Poster?.Extender.GetFileName();
            Horizontal = true;
            SetLabels(string.Empty, aCasting.IdPerson.Reference.Name, aCasting.Role, _Casting.IdPerson.Reference.Movies, _Casting.IdPerson.Reference.Rating, _Casting.IdPerson.Reference.RatedMovies, _Casting.IdPerson.Reference.UserRating);
        }

        protected override void OnClick(EventArgs e)
        {
            PersonControl lCtl = new PersonControl(_Casting.IdPerson.Reference);
            this.Navigator()?.Navigate(lCtl);
        }
    }
}
