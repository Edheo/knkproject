﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MovieControls.Usercontrols
{
    public class RatingControl : UserControl
    {
        public event EventHandler Change;

        private decimal _Value;
        private Label lblRating;
        private bool _Readonly;

        public RatingControl()
        {
            InitializeComponent();
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.ResizeRedraw, true);
            m_starAreas = new Rectangle[StarCount];
        }

        #region Properties

        public int LeftMargin
        {
            get
            {
                return m_leftMargin;
            }
            set
            {
                if (m_leftMargin != value)
                {
                    m_leftMargin = value;
                    Invalidate();
                }
            }
        }

        public int RightMargin
        {
            get
            {
                return m_rightMargin;
            }
            set
            {
                if (m_rightMargin != value)
                {
                    m_rightMargin = value;
                    Invalidate();
                }
            }
        }

        public int TopMargin
        {
            get
            {
                return m_topMargin;
            }
            set
            {
                if (m_topMargin != value)
                {
                    m_topMargin = value;
                    Invalidate();
                }
            }
        }

        public int BottomMargin
        {
            get
            {
                return m_bottomMargin;
            }
            set
            {
                if (m_bottomMargin != value)
                {
                    m_bottomMargin = value;
                    Invalidate();
                }
            }
        }

        public int StarSpacing
        {
            get
            {
                return m_starSpacing;
            }
            set
            {
                if (m_starSpacing != value)
                {
                    m_starSpacing = value;
                    Invalidate();
                }
            }
        }

        public int StarCount
        {
            get
            {
                return m_starCount;
            }
            set
            {
                if (m_starCount != value)
                {
                    m_starCount = value;
                    m_starAreas = new Rectangle[m_starCount];
                    MinimumSize = new Size(18 * m_starCount, 18);
                    MaximumSize = new Size(18 * m_starCount, 18);
                    Size = new Size(18 * m_starCount, 18);
                    Invalidate();
                }
            }
        }

        public bool IsHovering
        {
            get
            {
                return m_hovering;
            }
        }

        public decimal StarValue {
            get
            {
                return Value / Factor();
            }
            set
            {
                Value = value * Factor();
            }
        }

        private decimal Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
                Invalidate();
                Change?.Invoke(this, new EventArgs());
            }
        }

        private decimal Factor()
        {
            return m_starCount /10;
        }

        public Color OutlineColor
        {
            get
            {
                return m_outlineColor;
            }
            set
            {
                if (m_outlineColor != value)
                {
                    m_outlineColor = value;
                    Invalidate();
                }
            }
        }

        public Color HoverColor
        {
            get
            {
                return m_hoverColor;
            }
            set
            {
                if (m_hoverColor != value)
                {
                    m_hoverColor = value;
                    Invalidate();
                }
            }
        }

        public Color SelectedColor
        {
            get
            {
                return m_selectedColor;
            }
            set
            {
                if (m_selectedColor != value)
                {
                    m_selectedColor = value;
                    Invalidate();
                }
            }
        }

        public int OutlineThickness
        {
            get
            {
                return m_outlineThickness;
            }
            set
            {
                if (m_outlineThickness != value)
                {
                    m_outlineThickness = value;
                    Invalidate();
                }
            }
        }

        public decimal HoverStar
        {
            get
            {
                return m_hoverStar;
            }
        }

        public bool Readonly
        {
            get
            {
                return _Readonly;
            }

            set
            {
                _Readonly = value;
            }
        }

        #endregion

        protected override void OnPaint(PaintEventArgs pe)
        {
            pe.Graphics.Clear(BackColor);
            if (lblRating != null)
            {
                lblRating.ForeColor = m_outlineColor;
                lblRating.Text = $"{StarValue:0.0}";
                int starWidth = (Width - (lblRating.Width + LeftMargin + RightMargin + (StarSpacing * (StarCount - 1)))) / StarCount;
                int starHeight = (Height - (TopMargin + BottomMargin));

                Rectangle drawArea = new Rectangle(LeftMargin + lblRating.Width, 0, starWidth, starHeight);

                for (int i = 0; i < StarCount; ++i)
                {
                    m_starAreas[i].X = drawArea.X - StarSpacing / 2;
                    m_starAreas[i].Y = drawArea.Y;
                    m_starAreas[i].Width = drawArea.Width + StarSpacing / 2;
                    m_starAreas[i].Height = drawArea.Height;

                    DrawStar(pe.Graphics, drawArea, i);

                    drawArea.X += drawArea.Width + StarSpacing;
                }
            }
            base.OnPaint(pe);
        }

        protected void DrawStar(Graphics g, Rectangle rect, int starAreaIndex)
        {
            Brush fillBrush;
            Pen outlinePen = new Pen(OutlineColor, OutlineThickness);
            decimal lStar = starAreaIndex;

            var lValue = Value;
            var lColor = SelectedColor;

            if(m_hovering)
            {
                lValue = HoverStar;
                lColor = HoverColor;
            }

            if (lValue<=lStar || lValue > lStar + (decimal)0.5)
            {
                if (lValue > lStar)
                {
                    fillBrush = new LinearGradientBrush(rect, lColor, BackColor, LinearGradientMode.ForwardDiagonal);
                }
                else
                {
                    fillBrush = new SolidBrush(BackColor);
                }

                var p = new PointF[10];
                p[0].X = rect.X + (rect.Width / 2);
                p[0].Y = rect.Y;
                p[1].X = rect.X + (42 * rect.Width / 64);
                p[1].Y = rect.Y + (19 * rect.Height / 64);
                p[2].X = rect.X + rect.Width;
                p[2].Y = rect.Y + (22 * rect.Height / 64);
                p[3].X = rect.X + (48 * rect.Width / 64);
                p[3].Y = rect.Y + (38 * rect.Height / 64);
                p[4].X = rect.X + (52 * rect.Width / 64);
                p[4].Y = rect.Y + rect.Height;
                p[5].X = rect.X + (rect.Width / 2);
                p[5].Y = rect.Y + (52 * rect.Height / 64);
                p[6].X = rect.X + (12 * rect.Width / 64);
                p[6].Y = rect.Y + rect.Height;
                p[7].X = rect.X + rect.Width / 4;
                p[7].Y = rect.Y + (38 * rect.Height / 64);
                p[8].X = rect.X;
                p[8].Y = rect.Y + (22 * rect.Height / 64);
                p[9].X = rect.X + (22 * rect.Width / 64);
                p[9].Y = rect.Y + (19 * rect.Height / 64);

                g.FillPolygon(fillBrush, p);
                g.DrawPolygon(outlinePen, p);
            }
            else
            {
                fillBrush = new LinearGradientBrush(rect, lColor, BackColor, LinearGradientMode.ForwardDiagonal);

                var p = new PointF[6];
                p[0].X = rect.X + (rect.Width / 2) - 1;
                p[0].Y = rect.Y + (52 * rect.Height / 64);
                p[1].X = rect.X + (12 * rect.Width / 64);
                p[1].Y = rect.Y + rect.Height;
                p[2].X = rect.X + rect.Width / 4;
                p[2].Y = rect.Y + (38 * rect.Height / 64);
                p[3].X = rect.X;
                p[3].Y = rect.Y + (22 * rect.Height / 64);
                p[4].X = rect.X + (22 * rect.Width / 64);
                p[4].Y = rect.Y + (19 * rect.Height / 64);
                p[5].X = rect.X + (rect.Width / 2) - 1;
                p[5].Y = rect.Y;

                g.FillPolygon(fillBrush, p);
                g.DrawPolygon(outlinePen, p);

                fillBrush = new SolidBrush(BackColor);

                p = new PointF[6];
                p[0].X = rect.X + (rect.Width / 2) - 1;
                p[0].Y = rect.Y;
                p[1].X = rect.X + (42 * rect.Width / 64);
                p[1].Y = rect.Y + (19 * rect.Height / 64);
                p[2].X = rect.X + rect.Width;
                p[2].Y = rect.Y + (22 * rect.Height / 64);
                p[3].X = rect.X + (48 * rect.Width / 64);
                p[3].Y = rect.Y + (38 * rect.Height / 64);
                p[4].X = rect.X + (52 * rect.Width / 64);
                p[4].Y = rect.Y + rect.Height;
                p[5].X = rect.X + (rect.Width / 2) - 1;
                p[5].Y = rect.Y + (52 * rect.Height / 64);
                g.FillPolygon(fillBrush, p);
                g.DrawPolygon(outlinePen, p);
            }
        }

        protected override void OnMouseEnter(System.EventArgs ea)
        {
            if (!Readonly)
            {
                m_hovering = true;
                Invalidate();
            }
            base.OnMouseEnter(ea);
        }

        protected override void OnMouseLeave(System.EventArgs ea)
        {
            m_hovering = false;
            Invalidate();
            base.OnMouseLeave(ea);
        }

        protected override void OnMouseMove(MouseEventArgs args)
        {
            if (!Readonly)
            {
                for (int i = 0; i < StarCount; ++i)
                {
                    if (m_starAreas[i].Contains(args.X, args.Y))
                    {
                        var lAux = (decimal)(args.X- m_starAreas[i].Left);
                        var lMid = (decimal)(m_starAreas[i].Width / 2);
                        if (lMid < lAux)
                            m_hoverStar = i + 1;
                        else
                            m_hoverStar = i + (decimal)0.5;
                        Invalidate();
                        break;
                    }
                }
            }
            base.OnMouseMove(args);
        }

        protected override void OnClick(System.EventArgs args)
        {
            if (!Readonly)
            {
                Point p = PointToClient(MousePosition);

                for (int i = 0; i < StarCount; ++i)
                {
                    if (m_starAreas[i].Contains(p))
                    {
                        var lAux = (decimal)(p.X - m_starAreas[i].Left);
                        var lMid = (decimal)(m_starAreas[i].Width / 2);
                        if (lMid < lAux)
                            m_hoverStar = i + 1;
                        else
                            m_hoverStar = i + (decimal)0.5;
                        Value = m_hoverStar;
                        Invalidate();
                        break;
                    }
                }
            }
            base.OnClick(args);
        }

        #region Protected Data

        protected int m_leftMargin = 2;
        protected int m_rightMargin = 2;
        protected int m_topMargin = 2;
        protected int m_bottomMargin = 2;
        protected int m_starSpacing = 8;
        protected int m_starCount = 5;
        protected Rectangle[] m_starAreas;
        protected bool m_hovering = false;

        protected decimal m_hoverStar = 0;

        protected Color m_outlineColor = Color.DarkGray;
        protected Color m_hoverColor = Color.LightBlue;
        protected Color m_selectedColor = Color.Yellow;

        protected int m_outlineThickness = 1;

        #endregion

        private void InitializeComponent()
        {
            this.lblRating = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRating
            // 
            this.lblRating.AutoSize = true;
            this.lblRating.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRating.Location = new System.Drawing.Point(0, 0);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(22, 13);
            this.lblRating.TabIndex = 0;
            this.lblRating.Text = "0.0";
            this.lblRating.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RatingControl
            // 
            this.Controls.Add(this.lblRating);
            this.MaximumSize = new System.Drawing.Size(180, 18);
            this.MinimumSize = new System.Drawing.Size(180, 18);
            this.Name = "RatingControl";
            this.Size = new System.Drawing.Size(180, 18);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}