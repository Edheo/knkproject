﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KnkInterfaces.Interfaces;

namespace MovieControls.Usercontrols
{
    public partial class Results : UserControl
    {
        private List<KnkChangeDescriptorItf> _datasource;

        public Results()
        {
            InitializeComponent();
        }

        public Results(List<KnkChangeDescriptorItf> aDataSource)
        {
            grdResults.AutoGenerateColumns = true;
            _datasource = aDataSource;
            grdResults.DataSource = aDataSource;
        }

        public List<KnkChangeDescriptorItf> Datasource
        {
            get
            {
                return _datasource;
            }

            set
            {
                _datasource = value;
            }
        }

        public void RefreshData()
        {
            grdResults.DataSource = typeof(List<>);
            grdResults.DataSource = Datasource;
        }
    }
}
