﻿namespace MovieControls.Usercontrols.Other
{
    partial class RangeSelector
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.trkValue = new MetroFramework.Controls.MetroTrackBar();
            this.lblValue = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // trkValue
            // 
            this.trkValue.BackColor = System.Drawing.Color.Transparent;
            this.trkValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkValue.Location = new System.Drawing.Point(23, 0);
            this.trkValue.Name = "trkValue";
            this.trkValue.Size = new System.Drawing.Size(127, 24);
            this.trkValue.TabIndex = 0;
            this.trkValue.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkValue.ValueChanged += new System.EventHandler(this.trkValue_ValueChanged);
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblValue.Location = new System.Drawing.Point(0, 0);
            this.lblValue.Name = "metroLabel1";
            this.lblValue.Size = new System.Drawing.Size(23, 19);
            this.lblValue.TabIndex = 0;
            this.lblValue.Text = "00";
            this.lblValue.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // RangeSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.trkValue);
            this.Controls.Add(this.lblValue);
            this.Name = "RangeSelector";
            this.Size = new System.Drawing.Size(150, 24);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTrackBar trkValue;
        private MetroFramework.Controls.MetroLabel lblValue;
    }
}
