﻿using MovieControls.Classes;
using MovieModel.Criterias;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieControls.Usercontrols
{
    public class LinkControl: System.Windows.Forms.LinkLabel
    {
        private LinkBuiilder _LinkBuilder;

        public LinkControl(string aText, LinkBuiilder aBuilder) : base()
        {
            Text = aText;
            LinkColor = System.Drawing.Color.LightBlue;
            _LinkBuilder = aBuilder;
            foreach (var lLnk in aBuilder.Links())
                Links.Add(lLnk);
        }

        protected override void OnLinkClicked(LinkLabelLinkClickedEventArgs e)
        {
            switch(_LinkBuilder.Type)
            {
                case Enumerations.LinkTypeEnu.MovieWall:
                    var lPar = new MoviesCriteria();
                    Type type = lPar.GetType();
                    PropertyInfo pi = type.GetProperty(_LinkBuilder.Property);
                    pi.SetValue(lPar, e.Link.LinkData, null);
                    Navigator()?.Navigate(new MovieWall(lPar));
                    break;
                case Enumerations.LinkTypeEnu.MovieFileDelete:
                    var lMof = _LinkBuilder.Item as MediaFile;
                    if(lMof!=null)
                    {
                        lMof.Delete("Removed from Movie");
                        lMof.SaveChanges();
                    }
                    break;
                default:
                    System.Diagnostics.Process.Start((string)e.Link.LinkData);
                    break;
            }
        }

        public NavigatorControl Navigator()
        {
            return Navigator(this);
        }

        private NavigatorControl Navigator(Control aControl)
        {
            NavigatorControl lRet = null;
            var lDad = aControl.Parent;
            if (lDad != null)
            {
                lRet = lDad as NavigatorControl;
                if (lRet == null)
                    lRet = Navigator(lDad);
            }
            return lRet;
        }
    }
}
