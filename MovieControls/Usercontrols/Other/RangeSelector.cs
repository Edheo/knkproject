﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieControls.Usercontrols.Other
{
    public partial class RangeSelector : MetroFramework.Controls.MetroUserControl
    {
        private int _minimum = 0;
        private int _maximum = 10;
        private decimal _smallc = 1;

        public decimal SmallChange
        {
            get { return _smallc; }
            set { _smallc = value;
                Minimum = _minimum;
                Maximum = _maximum;
            }
        }

        public RangeSelector()
        {
            InitializeComponent();
        }

        public int Minimum
        {
            get
            {
                return _minimum;
            }
            set
            {
                _minimum = value;
                trkValue.Minimum = (int)(_minimum / _smallc);
            }
        }
        public int Maximum
        {
            get
            {
                return _maximum;
            }
            set
            {
                _maximum = value;
                trkValue.Maximum = (int)(_maximum / _smallc);
                Value = trkValue.Value * _smallc;
            }
        }
        public decimal Value
        {
            get
            {
                return trkValue.Value * _smallc;
            }
            set
            {
                var val = (int)(value / _smallc);
                if (val != trkValue.Value)
                {
                    trkValue.Value = val;
                }
                lblValue.Text = value.ToString().PadLeft(trkValue.Maximum.ToString().Length, '0');
            }
        }

        private void trkValue_ValueChanged(object sender, EventArgs e)
        {
            Value = trkValue.Value * _smallc;
        }
    }
}
