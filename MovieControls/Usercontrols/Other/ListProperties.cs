﻿using KnkInterfaces.Interfaces;
using KnkScrapers.MoviesScraper;
using MetroFramework.Controls;
using MovieControls.Classes;
using MovieControls.Enumerations;
using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace MovieControls.Usercontrols
{
    public class ListProperties: TableLayoutPanel
    {
        public ListProperties()
        {
            AutoScroll = true;
            BackColor = Color.Black;
            ColumnCount = 2;
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 70F));
            Location = new Point(5, 5);
            Name = "tblProperties";
            RowCount = 1;
            RowStyles.Add(new RowStyle(SizeType.Absolute, 22F));
            Size = new Size(200, 100);
            TabIndex = 0;
        }

        public void AddTagInfo()
        {
            AddTagInfo(string.Empty, " ");
        }

        public void AddTagInfo(string aLabel, string aContent)
        {
            AddTagInfo(FontStyle.Regular, aLabel, aContent, null);
        }

        public void AddRating(string aLabel, decimal? aValue)
        {
            AddRating(FontStyle.Regular, aLabel, aValue, null, string.Empty);
        }

        public void AddRating(string aLabel, decimal? aValue, KnkItemItf aItem, string aProperty)
        {
            AddRating(FontStyle.Regular, aLabel, aValue, aItem, aProperty);
        }

        public void AddTagInfo(string aLabel, string aContent, LinkTypeEnu aType, KnkConnectionItf aConnection, string aProperty)
        {
            AddTagInfo(FontStyle.Regular, aLabel, aContent, new LinkBuiilder(aConnection, aType, aProperty, aContent));
        }

        public void AddTagInfo(string aLabel, string aContent, LinkTypeEnu aType, KnkItemItf aItem, string aProperty)
        {
            AddTagInfo(FontStyle.Regular, aLabel, aContent, new LinkBuiilder(aItem, aType, aProperty, aContent));
        }

        public void AddTextInfo(string aLabel, string aContent)
        {
            AddTextInfo(FontStyle.Regular, aLabel, aContent, true);
        }

        public void AddTextBox(string aLabel, string aContent)
        {
            AddTextInfo(FontStyle.Regular, aLabel, aContent, false);
        }

        private void AddTagInfo(FontStyle aFontStyle, string aLabel, string aContent, LinkBuiilder aLink)
        {
            if (string.IsNullOrEmpty(aContent)) return;
            RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            Label lblLabel1 = CreateLabelTag(!string.IsNullOrWhiteSpace(aContent) ? aLabel : string.Empty, aFontStyle);
            Controls.Add(lblLabel1);
            Label lblContent1;
            if (aLink == null)
            {
                lblContent1 = CreateLabelContent(aContent, aFontStyle);
                Controls.Add(lblContent1);
            }
            else
            {
                lblContent1 = CreateLabelLinkContent(aContent, aFontStyle, aLink);
                Controls.Add(lblContent1);
            }

        }

        private void AddRating(FontStyle aFontStyle, string aLabel, decimal? aValue, KnkItemItf aItem, string aProperty)
        {
            if (aValue != null)
            {
                RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
                Label lblLabel1 = CreateLabelTag(aLabel, aFontStyle);
                RatingControl lRat = CreateRatingContent(aValue ?? 0, aItem, aProperty);
                Controls.Add(lblLabel1);
                Controls.Add(lRat);
            }
        }

        private void AddTextInfo(FontStyle aFontStyle, string aLabel, string aContent, bool aMultiline)
        {
            if (string.IsNullOrEmpty(aContent)) return;
            RowStyles.Add(new RowStyle(SizeType.AutoSize));
            Label lblLabel1 = CreateLabelTag(!string.IsNullOrWhiteSpace(aContent) ? aLabel : string.Empty, aFontStyle);
            Controls.Add(lblLabel1);
            if (aMultiline)
            {
                Label lbllContent = CreateTextContent(aContent, aFontStyle);
                Controls.Add(lbllContent);
            }
            else
            {
                MetroTextBox lbllContent = CreateTextBox(aContent, aFontStyle);
                Controls.Add(lbllContent);

            }
        }

        private Label CreateLabelTag(string aLabel, FontStyle aFontStyle)
        {
            Label lLblLabel = new Label() { Text = aLabel };
            FillLabelProperties(lLblLabel, aFontStyle, ContentAlignment.TopRight);
            return lLblLabel;
        }

        private LinkControl CreateLabelLinkContent(string aLabel, FontStyle aFontStyle, LinkBuiilder aLink)
        {
            LinkControl lLblLabel = new LinkControl(aLabel, aLink);
            FillLabelProperties(lLblLabel, aFontStyle, ContentAlignment.TopLeft);
            return lLblLabel;
        }

        private Label CreateLabelContent(string aContent, FontStyle aFontStyle)
        {
            Label lLblLabel = new Label() { Text = aContent };
            FillLabelProperties(lLblLabel, aFontStyle, ContentAlignment.TopLeft);
            return lLblLabel;
        }

        private Label CreateTextContent(string aContent, FontStyle aFontStyle)
        {
            Label lLblLabel = new Label() { Text = aContent };
            lLblLabel.AutoSize = true;
            lLblLabel.Dock = DockStyle.Fill;
            lLblLabel.Font = new Font("Verdana", 8.25F, aFontStyle, GraphicsUnit.Point, ((byte)(0)));
            lLblLabel.ForeColor = Color.White;
            return lLblLabel;
        }

        private RatingControl CreateRatingContent(decimal aValue, KnkItemItf aItem, string aProperty)
        {
            RatingControl lRat = new RatingControl();
            lRat.StarSpacing = 3;
            lRat.StarCount = 10;
            lRat.StarValue = aValue;
            lRat.Readonly = aItem == null;
            if (aItem != null)
            {
                lRat.Change += (sender, e) =>
                {
                    aItem.PropertySet(aProperty, lRat.StarValue);
                    MovieModel.Entities.Movie lMov = aItem as MovieModel.Entities.Movie;

                    if(lMov.LastViewed==null || (DateTime.Now - lMov.LastViewed.Value).TotalDays > 15)
                    {
                        var lVie = lMov.Views().Create();
                        lVie.IdMovie = lMov;
                        lVie.IdFile = lMov.Files().Items.FirstOrDefault()?.IdFile;
                        lVie.DateStart = DateTime.Now;
                        lMov.Views().Add(lVie, $"Rated movie { lMov.Title}");
                    }

                    MovieEnricher lEnr = new MovieEnricher(lMov.Connection());
                    if (lMov != null) lEnr.UploadRating(lMov.TmdbId ?? 0, lMov.UserRating);
                };
            }
            return lRat;
        }

        private MetroTextBox CreateTextBox(string aContent, FontStyle aFontStyle)
        {
            MetroTextBox lLblLabel = new MetroTextBox() { Text = aContent };
            lLblLabel.Theme = MetroFramework.MetroThemeStyle.Dark;
            lLblLabel.AutoSize = true;
            lLblLabel.Dock = DockStyle.Fill;
            lLblLabel.Font = new Font("Verdana", 8.25F, aFontStyle, GraphicsUnit.Point, ((byte)(0)));
            lLblLabel.Style = MetroFramework.MetroColorStyle.Blue;
            return lLblLabel;
        }

        private void FillLabelProperties(Label aLabel, FontStyle aFontStyle, ContentAlignment aTextAlign)
        {
            aLabel.AutoSize = true;
            aLabel.Dock = DockStyle.Fill;
            aLabel.Font = new Font("Verdana", 8.25F, aFontStyle, GraphicsUnit.Point, ((byte)(0)));
            aLabel.ForeColor = Color.White;
            aLabel.TextAlign = aTextAlign;
        }

        public void ClearRows()
        {
            RowStyles.Clear();
        }

    }
}
