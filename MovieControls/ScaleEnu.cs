﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieControls
{
    public enum ScaleEnu
    {
        UltraSmall=1,
        Small=2,
        Normal=3,
        Large=4,
        ExtraLarge=5
    }
}
