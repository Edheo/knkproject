﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieControls.Usercontrols;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieControls.Utilities
{
    public static class MoviesControlsUtils
    {
        delegate void delLoadComboList<T>(ComboBox aCombo, List<T> aList, string aDefault);

        public static NavigatorControl Navigator;

        public static bool IsAnimating(PictureBox box)
        {
            var fi = box.GetType().GetField("currentlyAnimating",
                BindingFlags.NonPublic | BindingFlags.Instance);
            return (bool)fi.GetValue(box);
        }

        public static void Animate(PictureBox box, bool enable)
        {
            var anim = box.GetType().GetMethod("Animate",
                BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(bool) }, null);
            anim.Invoke(box, new object[] { enable });
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            if (image != null && width>0 && height>0)
            {
                var destRect = new Rectangle(0, 0, width, height);
                var destImage = new Bitmap(width, height);

                destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                using (var graphics = Graphics.FromImage(destImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    try
                    {
                        using (var wrapMode = new ImageAttributes())
                        {
                            wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                            graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                        }
                    }
                    finally
                    {

                    }
                }

                return destImage;
            }
            else
                return null;
        }

        public static Size NormalPosterSize()
        {
            return new Size(200, 310 + 60);
        }

        public static Size NormalVideoSize()
        {
            return new Size(160, 90 + 60);
        }

        public static int GetHeightFromWidth(float aAspect, int aWidth)
        {
            return (int)(aWidth * aAspect);
        }

        private static float ScaleMin(ScaleEnu aScale)
        {
            return ((float)aScale - 1) / (float)aScale;
        }

        private static float ScaleMax(ScaleEnu aScale)
        {
            return ((float)aScale + 1) / (float)aScale;
        }

        public static float Aspect(Size aNormalSize)
        {
            Size lSiz = aNormalSize;
            return (float)lSiz.Height / (float)lSiz.Width;
        }


        public static Size GetMinimumSize(Size aNormalSize, ScaleEnu aScale)
        {
            return GetSize(aNormalSize, ScaleMin(aScale));
        }

        public static Size GetMaximumSize(Size aNormalSize, ScaleEnu aScale)
        {
            return GetSize(aNormalSize, ScaleMax(aScale));
        }

        public static Size GetSize(Size aNormalSize, float aFactor)
        {
            Size lSiz = aNormalSize;
            lSiz.Width = (int)(lSiz.Width * aFactor);
            lSiz.Height = MoviesControlsUtils.GetHeightFromWidth(Aspect(aNormalSize), lSiz.Width);
            return lSiz;
        }

        public static void ShowWindowsControl(Control aControl)
        {
            ShowWindowsControl(aControl, false);
        }

        public static void ShowWindowsControl(Control aControl, bool aFullScreen)
        {
            Form lForm = new MetroFramework.Forms.MetroForm();
            if(aFullScreen)
            {
                lForm.FormBorderStyle = FormBorderStyle.None;
                lForm.WindowState = FormWindowState.Maximized;
            }
            else
            {
                lForm.WindowState = FormWindowState.Normal;
            }
            lForm.ShowInTaskbar = false;
            aControl.Dock = DockStyle.Fill;
            lForm.Controls.Add(aControl);
            lForm.Show();
        }

        //public static NavigatorControl Navigator(Control aControl)
        //{
        //    NavigatorControl lRet = null;
        //    var lDad = aControl.Parent;
        //    if (lDad != null)
        //    {
        //        lRet = lDad as NavigatorControl;
        //        if (lRet == null)
        //            lRet = Navigator(lDad);
        //    }
        //    return lRet;
        //}

        public static ElementControl PictureHolder(Control aControl)
        {
            ElementControl lRet = null;
            var lDad = aControl.Parent;
            if (lDad != null)
            {
                lRet = lDad as ElementControl;
                if (lRet == null)
                    lRet = PictureHolder(lDad);
            }
            return lRet;
        }

        public static bool CheckConfiguration()
        {
            return CheckConfiguration(false);
        }

        public static bool CheckConfiguration(bool aForce)
        {
            KnkConnection lCon = new KnkConnection();
            lCon.CheckConfiguration();
            while (!lCon.CheckConfiguration() || aForce)
            {
                var lFrm = new Forms.ConfigureConnections(lCon);
                lFrm.ShowDialog();
                aForce = false;
            }
            return lCon.CheckConfiguration();
        }

        public static void LoadCombo<T>(ComboBox aCombo, string aDisplayMember, List<T> aList) where T : KnkItemItf
        {
            LoadCombo(aCombo, aDisplayMember, aList, null);
        }

        public static void LoadCombo<T>(ComboBox aCombo, List<T> aList) where T :  struct, IConvertible
        {
            foreach (T t in aList)
            {
                aCombo.Items.Add(t);
            }
            var lSel = aList.FirstOrDefault();
            aCombo.SelectedValue = lSel;
            aCombo.SelectedIndex = aCombo.FindStringExact(lSel.ToString());
        }

        public static void LoadCombo<T>(ComboBox aCombo, string aDisplayMember, List<T> aList, string aDefault) where T : KnkItemItf
        {
            if (aCombo.Items.Count == 0)
            {
                aCombo.DisplayMember = aDisplayMember;
                if (aCombo.InvokeRequired)
                    aCombo.Invoke(new delLoadComboList<T>(LoadComboList<T>), aCombo, aList, aDefault);
                else
                    LoadComboList<T>(aCombo, aList, aDefault);
            }
        }

        private static void LoadComboList<T>(ComboBox aCombo, List<T> aList, string aDefault) where T : KnkItemItf
        {
            foreach (T t in aList)
            {
                aCombo.Items.Add(t);
                if (!string.IsNullOrEmpty(aDefault) && t.PropertyGet(aCombo.DisplayMember).ToString() == aDefault)
                {
                    aCombo.SelectedValue = t;
                    aCombo.SelectedIndex = aCombo.FindStringExact(aDefault);
                }
            }

        }
    }
}
