﻿using KnkInterfaces.Interfaces;
using MovieControls.Enumerations;
using MovieControls.Usercontrols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.LinkLabel;

namespace MovieControls.Classes
{
    public class LinkBuiilder
    {
        private readonly KnkConnectionItf _Connection;
        private LinkTypeEnu _Type;
        private string _Property;
        private string _Values;
        private KnkItemItf _Item;

        public LinkBuiilder(KnkConnectionItf aConnection, LinkTypeEnu aType, string aProperty, string aValues)
        {
            _Connection = aConnection;
            _Type = aType;
            _Property = aProperty;
            _Values = aValues;
        }

        public LinkBuiilder(KnkItemItf aItem, LinkTypeEnu aType, string aProperty, string aValues)
        {
            _Connection = aItem.Connection();
            _Item = aItem;
            _Type = aType;
            _Property = aProperty;
            _Values = aValues;
        }

        public string Property { get { return _Property; } }
        public LinkTypeEnu Type { get { return _Type; } }
        public KnkConnectionItf Connection { get { return _Connection; } }
        public KnkItemItf Item { get { return _Item; } }

        public List<Link> Links()
        {
            List<Link> lLst = new List<Link>();
            var lVls = _Values.Split(',');
            int lPos = 0;

            foreach(var lItm in lVls)
            {
                var lTxt = lItm;
                int lIni = lPos;
                while (lTxt.StartsWith(" "))
                {
                    lTxt = lTxt.Substring(1, lTxt.Length - 1);
                    lIni++;
                }
                while (lTxt.EndsWith(" "))
                {
                    lTxt = lTxt.Substring(0, lTxt.Length - 1);
                }
                lLst.Add(new Link(lIni, lTxt.Length, lTxt));
                lPos = lPos + lItm.Length + 1;
            }
            return lLst;
        }

    }
}
