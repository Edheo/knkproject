﻿
using System.Windows.Forms;

namespace Vlc.DotNet.Forms
{
    partial class PlayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panButtons = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.prgVolume = new MetroFramework.Controls.MetroTrackBar();
            this.panel1 = new MetroFramework.Controls.MetroPanel();
            this.lblEta = new System.Windows.Forms.Label();
            this.lblLenght = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.btnFull = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.prgTrack = new MetroFramework.Controls.MetroProgressBar();
            this.panCombos = new MetroFramework.Controls.MetroPanel();
            this.cboSubs = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboAudio = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboAspect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.vlcControl1 = new MovieControls.Usercontrols.VlcControl();
            this.tmrTools = new System.Windows.Forms.Timer(this.components);
            this.panButtons.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panCombos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // panButtons
            // 
            this.panButtons.Controls.Add(this.lblTitle);
            this.panButtons.Controls.Add(this.prgVolume);
            this.panButtons.Controls.Add(this.panel1);
            this.panButtons.Controls.Add(this.btnFull);
            this.panButtons.Controls.Add(this.btnPlay);
            this.panButtons.Controls.Add(this.prgTrack);
            this.panButtons.Controls.Add(this.panCombos);
            this.panButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panButtons.HorizontalScrollbarBarColor = true;
            this.panButtons.HorizontalScrollbarHighlightOnWheel = false;
            this.panButtons.HorizontalScrollbarSize = 10;
            this.panButtons.Location = new System.Drawing.Point(0, 388);
            this.panButtons.Name = "panButtons";
            this.panButtons.Size = new System.Drawing.Size(577, 73);
            this.panButtons.TabIndex = 1;
            this.panButtons.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.panButtons.VerticalScrollbarBarColor = true;
            this.panButtons.VerticalScrollbarHighlightOnWheel = false;
            this.panButtons.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.LightBlue;
            this.lblTitle.Location = new System.Drawing.Point(41, 29);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(360, 44);
            this.lblTitle.TabIndex = 15;
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // prgVolume
            // 
            this.prgVolume.BackColor = System.Drawing.Color.Transparent;
            this.prgVolume.Dock = System.Windows.Forms.DockStyle.Right;
            this.prgVolume.Location = new System.Drawing.Point(401, 29);
            this.prgVolume.Maximum = 200;
            this.prgVolume.Name = "prgVolume";
            this.prgVolume.Size = new System.Drawing.Size(73, 44);
            this.prgVolume.Style = MetroFramework.MetroColorStyle.Blue;
            this.prgVolume.TabIndex = 14;
            this.prgVolume.Text = "Volume";
            this.prgVolume.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.prgVolume.Value = 100;
            this.prgVolume.Scroll += new System.Windows.Forms.ScrollEventHandler(this.prgVolume_Scroll);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblEta);
            this.panel1.Controls.Add(this.lblLenght);
            this.panel1.Controls.Add(this.lblStart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.HorizontalScrollbarBarColor = true;
            this.panel1.HorizontalScrollbarHighlightOnWheel = false;
            this.panel1.HorizontalScrollbarSize = 10;
            this.panel1.Location = new System.Drawing.Point(474, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(62, 44);
            this.panel1.TabIndex = 13;
            this.panel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.panel1.VerticalScrollbarBarColor = true;
            this.panel1.VerticalScrollbarHighlightOnWheel = false;
            this.panel1.VerticalScrollbarSize = 10;
            // 
            // lblEta
            // 
            this.lblEta.BackColor = System.Drawing.Color.Transparent;
            this.lblEta.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblEta.ForeColor = System.Drawing.Color.Aquamarine;
            this.lblEta.Location = new System.Drawing.Point(0, 30);
            this.lblEta.Margin = new System.Windows.Forms.Padding(0);
            this.lblEta.Name = "lblEta";
            this.lblEta.Size = new System.Drawing.Size(62, 15);
            this.lblEta.TabIndex = 14;
            this.lblEta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLenght
            // 
            this.lblLenght.BackColor = System.Drawing.Color.Transparent;
            this.lblLenght.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLenght.ForeColor = System.Drawing.Color.Aquamarine;
            this.lblLenght.Location = new System.Drawing.Point(0, 15);
            this.lblLenght.Margin = new System.Windows.Forms.Padding(0);
            this.lblLenght.Name = "lblLenght";
            this.lblLenght.Size = new System.Drawing.Size(62, 15);
            this.lblLenght.TabIndex = 13;
            this.lblLenght.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStart
            // 
            this.lblStart.BackColor = System.Drawing.Color.Transparent;
            this.lblStart.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStart.ForeColor = System.Drawing.Color.Aquamarine;
            this.lblStart.Location = new System.Drawing.Point(0, 0);
            this.lblStart.Margin = new System.Windows.Forms.Padding(0);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(62, 15);
            this.lblStart.TabIndex = 12;
            this.lblStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFull
            // 
            this.btnFull.BackColor = System.Drawing.Color.Transparent;
            this.btnFull.BackgroundImage = global::MovieControls.Properties.Resources._1489216371_window_fullscreen;
            this.btnFull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFull.Location = new System.Drawing.Point(536, 29);
            this.btnFull.Name = "btnFull";
            this.btnFull.Padding = new System.Windows.Forms.Padding(5);
            this.btnFull.Size = new System.Drawing.Size(41, 44);
            this.btnFull.TabIndex = 10;
            this.btnFull.TabStop = false;
            this.btnFull.UseVisualStyleBackColor = false;
            this.btnFull.Click += new System.EventHandler(this.btnFull_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Transparent;
            this.btnPlay.BackgroundImage = global::MovieControls.Properties.Resources._1466267994_player_in_blue;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlay.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Location = new System.Drawing.Point(0, 29);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Padding = new System.Windows.Forms.Padding(5);
            this.btnPlay.Size = new System.Drawing.Size(41, 44);
            this.btnPlay.TabIndex = 6;
            this.btnPlay.TabStop = false;
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // prgTrack
            // 
            this.prgTrack.Dock = System.Windows.Forms.DockStyle.Top;
            this.prgTrack.Location = new System.Drawing.Point(0, 22);
            this.prgTrack.Name = "prgTrack";
            this.prgTrack.Size = new System.Drawing.Size(577, 7);
            this.prgTrack.Style = MetroFramework.MetroColorStyle.Blue;
            this.prgTrack.TabIndex = 9;
            this.prgTrack.MouseMove += new System.Windows.Forms.MouseEventHandler(this.prgTrack_MouseMove);
            this.prgTrack.MouseUp += new System.Windows.Forms.MouseEventHandler(this.prgTrack_MouseUp);
            // 
            // panCombos
            // 
            this.panCombos.Controls.Add(this.cboSubs);
            this.panCombos.Controls.Add(this.label3);
            this.panCombos.Controls.Add(this.cboAudio);
            this.panCombos.Controls.Add(this.label2);
            this.panCombos.Controls.Add(this.cboAspect);
            this.panCombos.Controls.Add(this.label1);
            this.panCombos.Dock = System.Windows.Forms.DockStyle.Top;
            this.panCombos.HorizontalScrollbarBarColor = true;
            this.panCombos.HorizontalScrollbarHighlightOnWheel = false;
            this.panCombos.HorizontalScrollbarSize = 10;
            this.panCombos.Location = new System.Drawing.Point(0, 0);
            this.panCombos.Name = "panCombos";
            this.panCombos.Size = new System.Drawing.Size(577, 22);
            this.panCombos.TabIndex = 2;
            this.panCombos.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.panCombos.VerticalScrollbarBarColor = true;
            this.panCombos.VerticalScrollbarHighlightOnWheel = false;
            this.panCombos.VerticalScrollbarSize = 10;
            // 
            // cboSubs
            // 
            this.cboSubs.Dock = System.Windows.Forms.DockStyle.Left;
            this.cboSubs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSubs.FormattingEnabled = true;
            this.cboSubs.Items.AddRange(new object[] {
            "16:9",
            "16:10",
            "4:3"});
            this.cboSubs.Location = new System.Drawing.Point(369, 0);
            this.cboSubs.Name = "cboSubs";
            this.cboSubs.Size = new System.Drawing.Size(121, 21);
            this.cboSubs.TabIndex = 21;
            this.cboSubs.SelectedIndexChanged += new System.EventHandler(this.cboSubs_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(322, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 22);
            this.label3.TabIndex = 20;
            this.label3.Text = "Subitles";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboAudio
            // 
            this.cboAudio.Dock = System.Windows.Forms.DockStyle.Left;
            this.cboAudio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAudio.FormattingEnabled = true;
            this.cboAudio.Items.AddRange(new object[] {
            "16:9",
            "16:10",
            "4:3"});
            this.cboAudio.Location = new System.Drawing.Point(201, 0);
            this.cboAudio.Name = "cboAudio";
            this.cboAudio.Size = new System.Drawing.Size(121, 21);
            this.cboAudio.TabIndex = 19;
            this.cboAudio.SelectedIndexChanged += new System.EventHandler(this.cboAudio_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label2.Location = new System.Drawing.Point(161, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 22);
            this.label2.TabIndex = 18;
            this.label2.Text = "Audio";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboAspect
            // 
            this.cboAspect.Dock = System.Windows.Forms.DockStyle.Left;
            this.cboAspect.FormattingEnabled = true;
            this.cboAspect.Items.AddRange(new object[] {
            "16:9",
            "16:10",
            "4:3"});
            this.cboAspect.Location = new System.Drawing.Point(40, 0);
            this.cboAspect.Name = "cboAspect";
            this.cboAspect.Size = new System.Drawing.Size(121, 21);
            this.cboAspect.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Aspect";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vlcControl1
            // 
            this.vlcControl1.BackColor = System.Drawing.Color.Black;
            this.vlcControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vlcControl1.Location = new System.Drawing.Point(0, 0);
            this.vlcControl1.Name = "vlcControl1";
            this.vlcControl1.Size = new System.Drawing.Size(577, 388);
            this.vlcControl1.Spu = -1;
            this.vlcControl1.TabIndex = 0;
            this.vlcControl1.Text = "vlcControl1";
            this.vlcControl1.VlcLibDirectory = null;
            this.vlcControl1.VlcMediaplayerOptions = null;
            this.vlcControl1.EndReached += new System.EventHandler<Vlc.DotNet.Core.VlcMediaPlayerEndReachedEventArgs>(this.vlcControl1_EndReached);
            this.vlcControl1.LengthChanged += new System.EventHandler<Vlc.DotNet.Core.VlcMediaPlayerLengthChangedEventArgs>(this.OnVlcMediaLengthChanged);
            this.vlcControl1.PositionChanged += new System.EventHandler<Vlc.DotNet.Core.VlcMediaPlayerPositionChangedEventArgs>(this.OnVlcPositionChanged);
            this.vlcControl1.Click += new System.EventHandler(this.vlcControl1_Click);
            // 
            // tmrTools
            // 
            this.tmrTools.Interval = 1000;
            this.tmrTools.Tick += new System.EventHandler(this.tmrTools_Tick);
            // 
            // PlayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 461);
            this.Controls.Add(this.vlcControl1);
            this.Controls.Add(this.panButtons);
            this.MinimizeBox = false;
            this.Name = "PlayForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayForm_FormClosing);
            this.panButtons.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panCombos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MovieControls.Usercontrols.VlcControl vlcControl1;
        private MetroFramework.Controls.MetroPanel panButtons;
        private System.Windows.Forms.Button btnPlay;
        private MetroFramework.Controls.MetroProgressBar prgTrack;
        private System.Windows.Forms.Button btnFull;
        private System.Windows.Forms.ToolTip toolTip1;
        private MetroFramework.Controls.MetroPanel panel1;
        private System.Windows.Forms.Label lblEta;
        private System.Windows.Forms.Label lblLenght;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Timer tmrTools;
        private MetroFramework.Controls.MetroTrackBar prgVolume;
        private System.Windows.Forms.Label lblTitle;
        private MetroFramework.Controls.MetroPanel panCombos;
        private System.Windows.Forms.ComboBox cboAspect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboAudio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboSubs;
        private System.Windows.Forms.Label label3;
    }
}