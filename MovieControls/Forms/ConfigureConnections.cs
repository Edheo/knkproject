﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using KnkCore;
using KnkInterfaces.Interfaces;

namespace MovieControls.Forms
{
    public partial class ConfigureConnections : MetroForm
    {
        KnkConnection _Connection;
        List<KnkConfigurer> _List;

        public ConfigureConnections(KnkConnection aConnection)
        {
            InitializeComponent();
            _Connection = aConnection;
            _List = aConnection.LoadConfigurers();
            cboConnection.DisplayMember = "Name";
            cboConnection.DataSource = _List.Cast<KnkDataModelerItf>().ToList();
        }

        private void cboConnection_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.prpProperties.SelectedObject = cboConnection.SelectedItem;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            foreach (var con in _List)
                con.WriteConfig();
            this.Close();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            //_Config.CheckConfiguration();
        }
    }
}
