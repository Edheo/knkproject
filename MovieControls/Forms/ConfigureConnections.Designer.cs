﻿namespace MovieControls.Forms
{
    partial class ConfigureConnections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.prpProperties = new System.Windows.Forms.PropertyGrid();
            this.cboConnection = new MetroFramework.Controls.MetroComboBox();
            this.pnlPannel = new MetroFramework.Controls.MetroPanel();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.btnTest = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnlPannel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.prpProperties, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.cboConnection, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnlPannel, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(694, 409);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(144, 40);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Connection";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // prpProperties
            // 
            this.prpProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prpProperties.Location = new System.Drawing.Point(153, 43);
            this.prpProperties.Name = "prpProperties";
            this.prpProperties.Size = new System.Drawing.Size(538, 363);
            this.prpProperties.TabIndex = 0;
            // 
            // cboConnection
            // 
            this.cboConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboConnection.FormattingEnabled = true;
            this.cboConnection.ItemHeight = 23;
            this.cboConnection.Location = new System.Drawing.Point(153, 3);
            this.cboConnection.Name = "cboConnection";
            this.cboConnection.Size = new System.Drawing.Size(538, 29);
            this.cboConnection.TabIndex = 1;
            this.cboConnection.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cboConnection.UseSelectable = true;
            this.cboConnection.SelectedIndexChanged += new System.EventHandler(this.cboConnection_SelectedIndexChanged);
            // 
            // pnlPannel
            // 
            this.pnlPannel.Controls.Add(this.btnSave);
            this.pnlPannel.Controls.Add(this.btnTest);
            this.pnlPannel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPannel.HorizontalScrollbarBarColor = true;
            this.pnlPannel.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlPannel.HorizontalScrollbarSize = 10;
            this.pnlPannel.Location = new System.Drawing.Point(3, 43);
            this.pnlPannel.Name = "pnlPannel";
            this.pnlPannel.Size = new System.Drawing.Size(144, 363);
            this.pnlPannel.TabIndex = 2;
            this.pnlPannel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.pnlPannel.VerticalScrollbarBarColor = true;
            this.pnlPannel.VerticalScrollbarHighlightOnWheel = false;
            this.pnlPannel.VerticalScrollbarSize = 10;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSave.Location = new System.Drawing.Point(0, 23);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(144, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnTest
            // 
            this.btnTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTest.Location = new System.Drawing.Point(0, 0);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(144, 23);
            this.btnTest.TabIndex = 0;
            this.btnTest.Text = "Test";
            this.btnTest.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnTest.UseSelectable = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // ConfigureConnections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 489);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ConfigureConnections";
            this.Text = "Configure Connections";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.pnlPannel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PropertyGrid prpProperties;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cboConnection;
        private MetroFramework.Controls.MetroButton btnTest;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroPanel pnlPannel;
    }
}