﻿using MovieModel.Entities;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Vlc.DotNet.Core;

namespace Vlc.DotNet.Forms
{
    public partial class PlayForm : Form
    {
        delegate void delLenght(int aValue);
        delegate void delBool(bool aValue);
        delegate void delNoPars();

        int _Steps = 0;
        bool _add = true;

        private Movie _movie;
        private File _mediafile;

        private MediaLink _medialink;

        public PlayForm()
        {
            InitializeComponent();
        }

        public void Play(File aFile)
        {
            _movie = aFile.MovieFiles().Items.FirstOrDefault().IdMovie.Reference;
            this.Text = _movie.ToString();
            lblTitle.Text = this.Text;
            _mediafile = aFile;
            var lUri = new Uri(_mediafile.ToString());
            vlcControl1.Play(lUri);
            cboAspect.Text = "16:9";

            InnerPannel();
            if (!Visible) Show();
        }

        private void AddView(File aFile)
        {
            if (_movie != null && _add)
            {
                var lVie = CurrentView;
                if (lVie == null || lVie.DateEnd != null)
                {
                    lVie = _movie.Views().Create();
                    lVie.IdMovie = _movie;
                    lVie.IdFile = aFile;
                    lVie.DateStart = DateTime.Now;
                    _movie.Views().Add(lVie, $"Playing movie { _movie.Title}");
                    lVie.SaveChanges();
                }
                else if (lVie?.DateStart < DateTime.Now.AddMonths(-1))
                {
                    FinishView(lVie.CurrentTime != null ? lVie.CurrentTime.Value : TimeSpan.FromSeconds(vlcControl1.GetCurrentMedia().Duration.TotalSeconds));
                    AddView(aFile);
                }
                else if (lVie?.CurrentTime!=null)
                    SetPosition(lVie.CurrentTime.Value);
                _add = false;
            }
        }

        private void SetPosition(TimeSpan aTime)
        {
            var lAux = (float)aTime.TotalSeconds / (float)vlcControl1.GetCurrentMedia().Duration.TotalSeconds;
            vlcControl1.Position = lAux;
        }

        private void UpdateView(File aFile, TimeSpan aTime)
        {
            if (_movie != null)
            {
                var lVie = CurrentView;
                if (lVie != null && lVie.DateEnd == null)
                {
                    var lDif = aTime.TotalSeconds;
                    if (lVie.CurrentTime != null) lDif = (aTime - lVie.CurrentTime.Value).TotalSeconds;
                    if (Math.Abs(lDif) >= 60)
                    {
                        lVie.CurrentTime = aTime;
                        lVie.Update($"Playing movie { _movie.Title}");
                        lVie.SaveChanges();
                    }
                }
                else if (lVie == null)
                    AddView(aFile);
            }
        }

        private void FinishView(TimeSpan aTime)
        {
            if (_movie != null)
            {
                var lVie = CurrentView;
                if (lVie != null && lVie.DateEnd == null)
                {
                    lVie.CurrentTime = aTime;
                    lVie.DateEnd = DateTime.Now;
                    lVie.Update($"Played movie { _movie.Title}");
                    lVie.SaveChanges();
                }
            }
        }

        private MovieView CurrentView
        {
            get
            {
                return (from vie in _movie.Views().Items orderby vie.DateStart descending select vie).FirstOrDefault();
            }
        }

        public void Play(MediaLink aMedia)
        {
            _medialink = aMedia;
            this.Text = aMedia.ToString();
            //string lTxt = "https://www.youtube.com/embed/wO1fBR72t6I";
            var lUri = new Uri(_medialink.Extender.SiteUrl());
            //lUri = new Uri(lTxt);
            vlcControl1.Play(lUri);
            InnerPannel();
            if (!Visible) Show();
        }

        private void InnerPannel()
        {
            var lPannel = new Panel();
            lPannel.BackColor = System.Drawing.Color.Transparent;
            lPannel.Dock = DockStyle.Fill;
            lPannel.Location = new System.Drawing.Point(0, 0);
            lPannel.Size = new System.Drawing.Size(577, 408);
            lPannel.TabIndex = 2;
            lPannel.Click += InnerPannel_Click;
            lPannel.MouseWheel += InnerPannel_MouseWheel;
            vlcControl1.Audio.Volume = prgVolume.Value;
            vlcControl1.Controls.Add(lPannel);
        }

        private void InnerPannel_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Location.X > ((Panel)sender).Width / 3)
            {
                int lNew= prgVolume.Value + (e.Delta > 0 ? 1 : -1);
                if (lNew >= prgVolume.Minimum && lNew <= prgVolume.Maximum)
                {
                    prgVolume.Value = lNew;
                    vlcControl1.Audio.Volume = lNew;
                }
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            vlcControl1.Pause();
        }

        private void OnVlcMediaLengthChanged(object sender, Vlc.DotNet.Core.VlcMediaPlayerLengthChangedEventArgs e)
        {
            var lLng = (new TimeSpan((long)e.NewLength)).TotalSeconds;
            if (prgTrack.InvokeRequired)
            {
                this.Invoke(new delLenght(SetMaxLenght), (int)lLng);
                this.Invoke(new delNoPars(LoadTraksCombo));
            }
            else
            {
                SetMaxLenght((int)lLng);
                LoadTraksCombo();
            }

        }

        private void LoadTraksCombo()
        {
            var lTrks = vlcControl1.Audio.Tracks.All.Where(a => a.Name.ToLower() != "disable");
            cboAudio.Items.Clear();
            cboAudio.DisplayMember = "Name";
            cboAudio.DataSource = lTrks.ToList();
            if (lTrks.Count() > 0) cboAudio.SelectedIndex = 0;
            var lSubs = vlcControl1.SubTitles.All;
            
            cboSubs.Items.Clear();
            cboSubs.DisplayMember = "Name";
            cboSubs.DataSource = lSubs.ToList();
            if (lSubs.Count() > 0) cboSubs.SelectedIndex = 0;
        }

        private void SetMaxLenght(int aLenght)
        {
            prgTrack.Maximum = aLenght;
            lblLenght.Text = TimeSpanToString(aLenght);
        }

        private void SetCurrentLenght(int aLenght)
        {
            prgTrack.Value = aLenght;
            var lTime = TimeSpan.FromSeconds(aLenght);
            lblStart.Text = TimeSpanToString(aLenght);
            lblEta.Text = TimeSpanToString(GetEndTimeSpan(TimeSpan.FromSeconds(prgTrack.Maximum - aLenght)));
            toolTip1.SetToolTip(this.prgTrack, aLenght.ToString());
            if(aLenght<10)
                AddView(_mediafile);
            else
                UpdateView(_mediafile, lTime);
        }

        private void OnVlcPositionChanged(object sender, Core.VlcMediaPlayerPositionChangedEventArgs e)
        {
            var lLng = vlcControl1.GetCurrentMedia().Duration.TotalSeconds * e.NewPosition;
            if (prgTrack.InvokeRequired)
                this.Invoke(new delLenght(SetCurrentLenght), (int)lLng);
            else
                SetCurrentLenght((int)lLng);
        }

        private void btnFull_Click(object sender, EventArgs e)
        {
            ToggleFullScreen();
        }

        private void ToggleFullScreen()
        {
            ToggleFullScreen(false);
        }

        private void ToggleFullScreen(bool aForceNormal)
        {
            if (aForceNormal || WindowState == FormWindowState.Maximized)
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;
            }
            ShowButtons();
        }

        private void PlayForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.vlcControl1.IsPlaying)
            {
                vlcControl1.Pause();
                vlcControl1.Stop();
            }
            vlcControl1.Controls.Clear();
        }

        private void prgTrack_MouseMove(object sender, MouseEventArgs e)
        {
            _Steps = 0;
            string lVal = TimeSpanToString(TimeSpanFromProgress(e.Location.X));
            if(!lVal.Equals(toolTip1.GetToolTip(prgTrack)))
                toolTip1.SetToolTip(prgTrack, lVal);
        }

        private TimeSpan TimeSpanFromProgress(int aPos)
        {
            return TimeSpan.FromSeconds(aPos * prgTrack.Maximum / prgTrack.ClientRectangle.Width);
        }

        TimeSpan GetEndTimeSpan(TimeSpan aTim)
        {
            return aTim.Add(DateTime.Now.TimeOfDay);
        }
        string TimeSpanToString(int aSeconds)
        {
            return TimeSpanToString(TimeSpan.FromSeconds(aSeconds));
        }

        string TimeSpanToString(TimeSpan aTim)
        {
            return aTim.ToString(@"hh\:mm\:ss");
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            vlcControl1.Pause();
        }

        private void prgTrack_MouseUp(object sender, MouseEventArgs e)
        {
            var lVal = TimeSpanFromProgress(e.Location.X).TotalSeconds / prgTrack.Maximum;
            vlcControl1.Position = (float)lVal;
        }

        private void tmrTools_Tick(object sender, EventArgs e)
        {
            _Steps++;
            if(_Steps>5)
            {
                HideButtons();
            }
        }

        private void vlcControl1_Click(object sender, EventArgs e)
        {
            ShowButtons();                
        }

        void ShowButtons()
        {
            _Steps = 0;
            if (WindowState == FormWindowState.Maximized)
            {
                tmrTools.Start();
            }
            else
            {
                tmrTools.Stop();
            }
            panButtons.Visible = true;
        }

        void HideButtons()
        {
            _Steps = 0;
            panButtons.Visible = false;
            tmrTools.Stop();
        }

        private void InnerPannel_Click(object sender, EventArgs e)
        {
            ShowButtons();
        }

        private void vlcControl1_EndReached(object sender, Core.VlcMediaPlayerEndReachedEventArgs e)
        {
            FinishView(TimeSpan.FromSeconds(vlcControl1.GetCurrentMedia().Duration.TotalSeconds));
            if (this.InvokeRequired)
                this.Invoke(new delBool(ToggleFullScreen), true);
            else
                ToggleFullScreen(true);
        }

        private void prgVolume_Scroll(object sender, ScrollEventArgs e)
        {
            vlcControl1.Audio.Volume = e.NewValue;
        }

        private void cboAspect_TextChanged(object sender, EventArgs e)
        {
            vlcControl1.Video.AspectRatio = vlcControl1.Text;
            //ResizeVlcControl();
        }

        private void cboAudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            vlcControl1.Audio.Tracks.Current = cboAudio.SelectedItem as Vlc.DotNet.Core.TrackDescription;
        }

        private void cboSubs_SelectedIndexChanged(object sender, EventArgs e)
        {
            vlcControl1.SubTitles.Current = cboSubs.SelectedItem as Vlc.DotNet.Core.TrackDescription;
        }
    }
}
