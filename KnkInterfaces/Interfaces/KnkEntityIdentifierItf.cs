﻿using System;

namespace KnkInterfaces.Interfaces
{
    public interface KnkEntityIdentifierItf : IConvertible, IComparable
    {
        int? Value { get; set; }
        KnkItemItf Reference { get; set; }
}
}
