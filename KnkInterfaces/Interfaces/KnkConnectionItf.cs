﻿using System;
using System.Collections.Generic;

namespace KnkInterfaces.Interfaces
{
    public interface KnkConnectionItf
    {
        List<Tlst> GetList<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new();

        T GetItem<T>(int? aEntityId) where T : KnkItemItf, new();
        KnkItemItf GetItem(Type atype, int? aEntityId);
        T ReadItem<T>(T aItm) where T : KnkItemItf, new();

        IEnumerable<dynamic> GetData(string aQuery);
        //List<KnkEntityIdentifierItf> GetListIds<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
        //            where Tdad : KnkItemItf, new()
        //            where Tlst : KnkItemItf, new();

        object GetCommandListIds<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, string aDistinctField)
                    where Tdad : KnkItemItf, new()
                    where Tlst : KnkItemItf, new();

        void SaveData<T>(List<T> aItems) where T : KnkItemItf, new();

        KnkItemItf CurrentUser { get; set; }

        KnkItemItf Login(KnkItemItf aUser);
    }
}
