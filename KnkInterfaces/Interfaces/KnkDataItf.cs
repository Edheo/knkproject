﻿using System.Collections.Generic;
using System.Data;

namespace KnkInterfaces.Interfaces
{
    public interface KnkDataItf
    {
        bool Connect();
        DataTable GetData<T>()
            where T : KnkItemItf, new();

        DataTable GetData<Tdad,Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria) 
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new();

        IEnumerable<dynamic> GetData(string aCriteria);

        //DataTable GetListIds<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria)
        //    where Tdad : KnkItemItf, new()
        //    where Tlst : KnkItemItf, new();

        object GetCommandListIds<Tdad, Tlst>(KnkCriteriaItf<Tdad, Tlst> aCriteria, string aDistinctField)
            where Tdad : KnkItemItf, new()
            where Tlst : KnkItemItf, new();

        string SaveData<T>(T aItem) where T : KnkItemItf, new();
    }
}
