﻿using KnkInterfaces.Classes;
using KnkInterfaces.Enumerations;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;

namespace KnkInterfaces.Interfaces
{
    public interface KnkListItf
    {
        KnkConnectionItf Connection { get; set; }
        int Count();
        void DeleteAll(string aMessage);
        bool SaveChanges();
        bool SaveChanges(KnkItemItf aItem);
        void Refresh();

        List<KnkSortAttribute> Sorting { get; set; }
        KnkSortAttribute SortAttribute { get; set; }
        string InfoValue { get; set; }

        KnkChangesItf Changes { get; set; }
        List<KnkItemItf> Items { get; }
    }

    public interface KnkListItf<Tdad, Tlst> : KnkListItf
        where Tdad : KnkItemItf, new()
        where Tlst : KnkItemItf, new()
    {
        new ConcurrentList<Tlst> Items { get; set; }

        List<Tlst> DeletedItems { get; }

        List<Tlst> ItemsChanged();
        List<Tlst> ItemsChanged(ConcurrentList<Tlst> aList);

        List<Tlst> Datasource(int? page = null, int? elements = null);
        KnkCriteriaItf<Tdad, Tlst> Criteria { get; set; }
        Tdad GetParent();
        //List<KnkEntityIdentifierItf> GetListIds();
        //List<KnkEntityIdentifierItf> GetListIds(List<Tlst> aItems);
        Tlst Create(bool aAddToList=true);
        void Add(Tlst aItem, string aMessage);
        bool SaveChanges(List<Tlst> aList);
        object GetCommandListIds(string aDistinctField);
    }
}
