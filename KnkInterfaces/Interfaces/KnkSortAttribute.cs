﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkInterfaces.Interfaces
{
    public class KnkSortAttribute
    {
        public KnkSortAttribute(string property, bool ascendend=false)
        {
            SortProperty = property;
            Ascendent = ascendend;
        }

        public string SortProperty { get; set; }
        public bool Ascendent { get; set; }
    }
}
