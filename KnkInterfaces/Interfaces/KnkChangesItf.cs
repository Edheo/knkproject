﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnkInterfaces.Interfaces
{
    public interface KnkChangesItf
    {
        List<KnkChangeDescriptorItf> Messages { get; }
        KnkChangeDescriptorItf AddMessage(KnkChangeDescriptorItf aMessage);
        KnkChangeDescriptorItf AddMessage(KnkItemItf aItem);
        KnkChangeDescriptorItf AddMessage(string aAction, string aText);
        KnkChangeDescriptorItf AddMessage(string aAction, string aText, string aMessage);
        void AddMessages(List<KnkChangeDescriptorItf> aMessages);
        KnkChangeDescriptorItf UpdateMessage(KnkItemItf aItem, string aMessage);
        void Clear();
    }
}
