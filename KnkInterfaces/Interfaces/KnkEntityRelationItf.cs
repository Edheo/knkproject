﻿namespace KnkInterfaces.Interfaces
{
    public interface KnkEntityRelationItf<Tdad, Titm> : KnkListItf<Tdad, Titm>
        where Tdad : KnkItemItf, new()
        where Titm : KnkItemItf, new()
    {
    }
}
