﻿using System;
using System.ComponentModel;

namespace KnkInterfaces.Interfaces
{
    public interface KnkChangeDescriptorItf
    {
        [Browsable(false)]
        KnkEntityIdentifierItf IdValue { get; }
        [Browsable(false)]
        KnkItemItf Item { get; }
        string Object { get; }

        DateTime? CreationDate { get; }
        DateTime? ModifiedDate { get; }
        [Browsable(false)]
        DateTime? DeletedDate { get; }

        string Action { get; }
        string Text { get; }
        string Message { get; }

        void UpdateMessage(string aAction, string aMessage);
        void UpdateMessage(string aMessage);
    }
}
