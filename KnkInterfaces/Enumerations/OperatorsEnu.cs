﻿using System.ComponentModel;

namespace KnkInterfaces.Enumerations
{
    public enum OperatorsEnu
    {
        [Description("@Field = @Value |@Field is @Value ")] Equal,
        [Description("@Field <> @Value ")]                  Distinct,
        [Description("@Field > @Value ")]                   GreatThan,
        [Description("@Field >= @Value ")]                  GreatEqualThan,
        [Description("@Field < @Value ")]                   LowerThan,
        [Description("@Field <= @Value ")]                   LowerEqualThan,
        [Description("@Field Like @Value ")]                Like,
        [Description("@Field In ( @List[Field] )")]         In,
        [Description("@Field Not In ( @List[Field] )")]     NotIn,
        [Description("@Field Is Null")]                     IsNull,
        [Description("DATALENGTH( @Field ) = 0 ")]          IsNullOrEmpty,
        [Description("@Field Is Not Null")]                 IsNotNull
    }
}
