﻿using System;
using System.Linq;
using System.Windows.Forms;
using MovieModel.Lists;
using MovieControls.Utilities;

namespace Movies
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MoviesControlsUtils.CheckConfiguration();
            Users lUsr = new Users();
            lUsr.Connection.Login(lUsr.Datasource().FirstOrDefault(u=>u.IdUser==1));
            Application.Run(new Movies());
        }
    }
}
