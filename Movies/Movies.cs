﻿using MetroFramework.Forms;

namespace Movies
{
    public partial class Movies : MetroForm
    {
        public Movies()
        {
            InitializeComponent();
            wallMovies.OnPerformSearch();
        }
    }
}
