﻿namespace Movies
{
    partial class Movies
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.navigatorControl1 = new MovieControls.Usercontrols.NavigatorControl();
            this.wallMovies = new MovieControls.Usercontrols.MovieWall();
            this.SuspendLayout();
            // 
            // navigatorControl1
            // 
            this.navigatorControl1.CurrentControl = this.wallMovies;
            this.navigatorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigatorControl1.Location = new System.Drawing.Point(20, 30);
            this.navigatorControl1.Name = "navigatorControl1";
            this.navigatorControl1.Size = new System.Drawing.Size(958, 541);
            this.navigatorControl1.TabIndex = 1;
            this.navigatorControl1.UseSelectable = true;
            // 
            // wallMovies
            // 
            this.wallMovies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wallMovies.Location = new System.Drawing.Point(0, 0);
            this.wallMovies.Name = "wallMovies";
            this.wallMovies.Size = new System.Drawing.Size(958, 502);
            this.wallMovies.TabIndex = 0;
            this.wallMovies.UseSelectable = true;
            // 
            // Movies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 591);
            this.Controls.Add(this.navigatorControl1);
            this.DisplayHeader = false;
            this.Name = "Movies";
            this.Text = "Movies";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }


        private MovieControls.Usercontrols.MovieWall wallMovies;
        #endregion
        private MovieControls.Usercontrols.NavigatorControl navigatorControl1;
    }
}

