﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using MovieModel.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Criterias
{
    [Serializable]
    public class BooksCriteria
    {
        private Books _books;
        private Book _book;

        public int Columns = 6;
        public int Rows = 6;
        public int Elements => Columns * Rows;

        public int Page { get; set; } = 1;
        public string Filter { get; set; }
        public bool AllowNavigate { get; set; }
        public bool IsRelated { get; set; }

        public int? User { get; set; }
        public string Artist { get; set; }
        public int? PersonId { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public string Genre { get; set; }
        public string Saga { get; set; }
        public string Year { get; set; }
        public int? Book { get; set; }
        public string TextSearch { get; set; }

        public bool? InMyLib { get; set; }
        public bool? UserRated { get; set; }
        public bool? Readed { get; set; }
        public bool? HasFiles { get; set; }
        public bool? HasLink { get; set; }

        public int? AudioTracks { get; set; }
        public int? Subtitles { get; set; }

        public string SortProperty { get; set; }
        public bool SortDirectionAsc { get; set; }

        public BooksCriteria()
        {
            CleanValues();
        }

        public BooksCriteria(int personid)
        {
            CleanValues();
            PersonId = personid;
            SortProperty = "ReleaseDate";
        }

        public BooksCriteria(string artist)
        {
            CleanValues();
            Artist = artist;
            SortProperty = "ReleaseDate";
        }

        public BooksCriteria(string saga, string country, string company, string genre, string year, int? movie)
        {
            CleanValues();
            IsRelated = true;
            Saga = saga;
            Country = country;
            Company = company;
            Genre = genre;
            Year = year;
            Book = movie;
            SortProperty = "ReleaseDate";
        }

        public BooksCriteria(Book aBook)
        {
            CleanValues();
            _book = aBook;
            Book = aBook.IdBook;
            SortProperty = "ReleaseDate";
        }

        private Books GetCriteria()
        {
            if (_book != null)
                _books = new Books(_book);
            else
                _books = new Books();

            var lCon = _books.Connection;
            var lCri = new KnkCriteria<Book, Book>(_books, new KnkTableEntity("vieBooks", "Books"));
            if (lCon != null)
            {
                if (User != null)
                {
                    var lUsr = lCon.GetItem<User>(User.Value);
                    var lBus = new BookUsers(lCon, lUsr);
                    var linmy = InMyLib ?? (UserRated != null ? true : UserRated);
                    if (linmy != null)
                    {
                        if (linmy.Value)
                        {
                            if (UserRated != null)
                            {
                                if (UserRated.Value)
                                    lBus.Criteria.AddParameter(typeof(int), "UserRating", OperatorsEnu.IsNotNull);
                                else
                                    lBus.Criteria.AddParameter(typeof(int), "UserRating", OperatorsEnu.IsNull);
                            }
                            KnkCoreUtils.CreateInParameter(lBus, lCri, "IdBook", "IdBook");
                        }
                        else
                            KnkCoreUtils.CreateNotInParameter(lBus, lCri, "IdBook", "IdBook");
                    }
                }
                if (Readed != null)
                {
                    if (Readed.Value)
                    {
                        lCri.AddParameter(typeof(int), "Readed", OperatorsEnu.GreatThan, $"0");
                    }
                    else
                    {
                        lCri.AddParameter(typeof(int), "Readed", OperatorsEnu.LowerThan, $"1");
                    }
                }

                if (HasFiles != null)
                {
                    if (HasFiles.Value)
                    {
                        lCri.AddParameter(typeof(int), "FilesNumber", OperatorsEnu.GreatThan, $"0");
                    }
                    else
                    {
                        lCri.AddParameter(typeof(int), "FilesNumber", OperatorsEnu.LowerThan, $"1");
                    }
                }

                if (HasLink != null)
                {
                    if (HasLink.Value)
                    {
                        lCri.AddParameter(typeof(int), "HasLink", OperatorsEnu.GreatThan, $"0");
                    }
                    else
                    {
                        lCri.AddParameter(typeof(int), "HasLink", OperatorsEnu.LowerThan, $"1");
                    }
                }

                if (!string.IsNullOrEmpty(TextSearch))
                {
                    string[] lSearch = TextSearch.Split(' ');
                    foreach (string lStr in lSearch)
                    {
                        KnkParameterItf lPar = lCri.AddParameter(typeof(string), "TextSearch", OperatorsEnu.Like, $"%{lStr}%");
                    }
                }

                if (Book != null)
                {
                    var lBok = lCon.GetItem<Book>(Book);
                    KnkCoreUtils.CreateInParameter(new MovieRelateds(lCon, Book ?? 0), lCri, "IdBook");
                    //if (lBok?.IdSet?.Value != null)
                    //{
                    //    var lSts = new Movies(lCon);
                    //    var lCrs = new KnkCriteria<Book, Book>(lSts, new KnkTableEntity("vieMovies", "Movies"));
                    //    lCrs.AddParameter(typeof(int), "IdSet", OperatorsEnu.Equal, lMov.IdSet.Value);
                    //    lSts.Criteria = lCrs;
                    //    if (lSts.Items.Count() > 0)
                    //        KnkCoreUtils.CreateNotInParameter(lSts, lCri, "IdMovie");
                    //}
                }

                if (!string.IsNullOrEmpty(Artist))
                {
                    KnkCoreUtils.CreateInParameter(new MovieCastings(lCon, $"%{Artist}%"), lCri, "IdMovie");
                }

                if (PersonId != null)
                {
                    KnkCoreUtils.CreateInParameter(new MovieCastings(lCon, PersonId.Value), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Genre))
                {
                    KnkCoreUtils.CreateInParameter(new MovieGenres(lCon, $"%{Genre}%"), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Saga))
                {
                    KnkCoreUtils.CreateInParameter(new MovieMovieSets(lCon, $"%{Saga}%"), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Year))
                {
                    KnkParameterItf lPar = lCri.AddParameter(typeof(string), "Year", OperatorsEnu.Equal, $"{Year}");
                }

                if (!string.IsNullOrEmpty(Country))
                {
                    KnkCoreUtils.CreateInParameter(new MovieCountries(lCon, $"{Country}"), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Company))
                {
                    KnkCoreUtils.CreateInParameter(new MovieCompanies(lCon, $"{Company}"), lCri, "IdMovie");
                }

                if (!lCri.HasParameters())
                    lCri = null;
            }

            if (!string.IsNullOrEmpty(SortProperty)) _books.SortAttribute = new KnkSortAttribute(SortProperty, SortDirectionAsc);
            _books.Criteria = lCri;
            return _books;
        }

        public Books RefreshList()
        {
            return GetCriteria();
        }

        public Books CurrentList()
        {
            if (_books == null)
                return GetCriteria();
            else
                return _books;
        }

        public override string ToString()
        {
            return Filter.ToString();
        }

        public void DefaultFilter(int? user)
        {
            CleanValues();
            Filter = "Default";
            if (user != null)
            {
                HasLink = true;
                SortProperty = "LastViewed";
            }
            else
            {
                HasFiles = true;
                SortProperty = "CreationDate";
                SortDirectionAsc = true;
            }
            User = user;
        }

        public void BestMovies()
        {
            CleanValues();
            Filter = "Best Movies";
            SortProperty = "Rating";
        }

        public void MyMovies(int? user)
        {
            CleanValues();
            Filter = "My Movies";
            SortProperty = "DateAdded";
            User = user;
            InMyLib = true;
        }

        public void FavouriteMovies(int? user)
        {
            MyMovies(user);
            if (user != null)
            {
                HasFiles = true;
                SortProperty = "UserRating";
            }
        }

        public void WhisedMovies(int? user)
        {
            CleanValues();
            Filter = "Whised Movies";
            SortProperty = "PredictedRating";
            User = user;
            HasFiles = false;
            InMyLib = true;
            Readed = false;
            UserRated = false;
        }

        public void PendingMovies(int? user)
        {
            CleanValues();
            Filter = "Pending Movies";
            SortProperty = "PredictedRating";
            User = user;
            HasFiles = true;
            InMyLib = true;
            UserRated = false;
        }

        public void ArtistRecommended(int? user)
        {
            CleanValues();
            Filter = "Artist Recommended";
            SortProperty = "BestCastingRating";
            User = user;
            HasFiles = true;
            InMyLib = true;
            UserRated = false;
        }

        public void Recomendations(int? user)
        {
            CleanValues();
            Filter = "Recomendations";
            SortProperty = "PredictedRating";
            User = user;
            HasFiles = true;
            InMyLib = false;
        }

        public void NewMovies()
        {
            CleanValues();
            Filter = "New Movies";
            SortProperty = "FileDate";
            HasFiles = true;
        }

        private void CleanValues()
        {
            Filter = "Default";
            _books = null;
            _book = null;
            Page = 1;
            User = null;
            SortDirectionAsc = false;
            AllowNavigate = false;
            IsRelated = false;

            TextSearch = null;
            InMyLib = null;

            Artist = null;
            PersonId = null;
            Country = null;
            Company = null;
            Genre = null;
            Saga = null;
            Year = null;
            Book = null;
            UserRated = null;
            Readed = null;
            HasFiles = null;
            AudioTracks = null;
            Subtitles = null;
            HasLink = null;
        }
    }
}
