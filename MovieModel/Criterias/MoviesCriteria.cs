﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using MovieModel.Lists;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Criterias
{
    [Serializable]
    public class MoviesCriteria
    {
        private Movies _movies;
        private Movie _movie;

        public int Columns = 6;
        public int Rows = 6;
        public int Elements => Columns * Rows;
        public string CriteriaTitle { get; set; }

        public int Page { get; set; } = 1;
        public bool AllowNavigate { get; set; }
        public bool IsRelated { get; set; }

        public int? User { get; set; }
        public string PersonName { get; set; }
        public List<int> Persons { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public string Genre { get; set; }
        public string Saga { get; set; }
        public int? Movie { get; set; }
        public string TextSearch { get; set; }
        public string Keyword { get; set; }

        public int? YearFrom { get; set; }
        public int? YearTo { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? RatingFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? RatingTo { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? UserRatingFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? UserRatingTo { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? PredictedRatingFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? PredictedRatingTo { get; set; }

        public bool? InMyLib { get; set; }
        public bool? UserRated { get; set; }
        public bool? Viewed { get; set; }
        public bool? HasFiles { get; set; }
        public bool? HasLink { get; set; }

        public bool? ReleasedToday { get; set; }

        public int? AudioTracks { get; set; }
        public int? Subtitles { get; set; }

        public string SortProperty { get; set; }
        public bool SortDirectionAsc { get; set; }

        public string InfoValue { get; set; }
        public int? TmdbId { get; set; }

        public MoviesCriteria()
        {
            CleanValues();
        }

        public MoviesCriteria(int? personid = null, string personname = null, string saga = null) : this()
        {
            IsRelated = true;
            AllowNavigate = false;
            if (personid.HasValue) Persons.Add(personid.Value);
            PersonName = personname;
            Saga = saga;
            if (personid != null || personname != null)
                SortProperty = "Rating";
            else
                SortProperty = "ReleaseDate";
        }

        public MoviesCriteria(int personid1, int personid2) : this()
        {
            AllowNavigate = false;
            Persons.Add(personid1);
            Persons.Add(personid2);
            SortProperty = "Rating";
        }

        public MoviesCriteria(int? movie = null, string country = null, string company = null, string genre = null, int? year = null, string keyword=null) :
            this(movie, country, company, genre, keyword, year, year)
        {

        }

        private MoviesCriteria(int? movie, string country, string company, string genre, string keyword,
            int? yearfrom, int? yearto)
        {
            CleanValues();
            Movie = movie;
            Country = country;
            Company = company;
            Genre = genre;
            YearFrom = yearfrom;
            YearTo = yearto;
            Keyword = keyword;
            SortProperty = "ReleaseDate";
        }

        public MoviesCriteria(Movie aMovie)
        {
            CleanValues();
            _movie = aMovie;
            Movie = aMovie.IdMovie;
            SortProperty = "ReleaseDate";
        }

        private Movies GetCriteria()
        {
            if (_movie != null)
                _movies = new Movies(_movie);
            else
                _movies = new Movies();

            var lCon = _movies.Connection;
            var lCri = new KnkCriteria<Movie, Movie>(_movies, new KnkTableEntity("vieMovies", "Movies"));
            if (lCon != null)
            {
                if (User != null)
                {
                    var lUsr = lCon.GetItem<User>(User.Value);
                    var lMus = new MovieUsers(lCon, lUsr);
                    var linmy = InMyLib??true || UserRated.HasValue || UserRatingFrom.HasValue || UserRatingTo.HasValue;
                    if (linmy)
                    {
                        if(UserRatingFrom.HasValue || UserRatingTo.HasValue)
                        {
                            if (UserRatingFrom != null) lMus.Criteria.AddParameter(typeof(decimal), "UserRating", OperatorsEnu.GreatEqualThan, UserRatingFrom);
                            if (UserRatingTo != null) lMus.Criteria.AddParameter(typeof(decimal), "UserRating", OperatorsEnu.LowerEqualThan, UserRatingTo);
                        }
                        else if (UserRated.HasValue)
                        {
                            if (UserRated.Value)
                                lMus.Criteria.AddParameter(typeof(int), "UserRating", OperatorsEnu.IsNotNull);
                            else
                                lMus.Criteria.AddParameter(typeof(int), "UserRating", OperatorsEnu.IsNull);
                        }
                        KnkCoreUtils.CreateInParameter(lMus, lCri, "IdMovie");
                    }
                    else
                        KnkCoreUtils.CreateNotInParameter(lMus, lCri, "IdMovie");
                }
                if (Viewed != null)
                {
                    if (Viewed.Value)
                    {
                        lCri.AddParameter(typeof(int), "ViewedTimes", OperatorsEnu.GreatThan, $"0");
                    }
                    else
                    {
                        lCri.AddParameter(typeof(int), "ViewedTimes", OperatorsEnu.LowerThan, $"1");
                    }
                }

                if (HasFiles != null)
                {
                    if (HasFiles.Value)
                    {
                        lCri.AddParameter(typeof(int), "FilesNumber", OperatorsEnu.GreatThan, $"0");
                    }
                    else
                    {
                        lCri.AddParameter(typeof(int), "FilesNumber", OperatorsEnu.LowerThan, $"1");
                    }
                }

                if (RatingFrom != null) lCri.AddParameter(typeof(decimal), "Rating", OperatorsEnu.GreatEqualThan, RatingFrom);
                if (RatingTo != null) lCri.AddParameter(typeof(decimal), "Rating", OperatorsEnu.LowerEqualThan, RatingTo);

                if (PredictedRatingFrom != null) lCri.AddParameter(typeof(decimal), "PredictedRating", OperatorsEnu.GreatEqualThan, PredictedRatingFrom);
                if (PredictedRatingTo != null) lCri.AddParameter(typeof(decimal), "PredictedRating", OperatorsEnu.LowerEqualThan, PredictedRatingTo);

                if (HasLink != null)
                {
                    if (HasLink.Value)
                    {
                        lCri.AddParameter(typeof(int), "HasLink", OperatorsEnu.GreatThan, $"0");
                    }
                    else
                    {
                        lCri.AddParameter(typeof(int), "HasLink", OperatorsEnu.LowerThan, $"1");
                    }
                }

                if (TmdbId.HasValue)
                {
                    if(TmdbId.Value==0)
                        lCri.AddParameter(typeof(int), "TmdbId", OperatorsEnu.GreatThan, $"0");
                    else
                        lCri.AddParameter(typeof(int), "TmdbId", OperatorsEnu.Equal, TmdbId.Value);
                }

                if (!string.IsNullOrEmpty(TextSearch))
                {
                    string[] lSearch = TextSearch.Split(' ');
                    foreach (string lStr in lSearch)
                    {
                        KnkParameterItf lPar = lCri.AddParameter(typeof(string), "TextSearch", OperatorsEnu.Like, $"%{lStr}%");
                    }
                }

                if (Movie != null)
                {
                    var lMov = lCon.GetItem<Movie>(Movie);
                    KnkCoreUtils.CreateInParameter(new MovieRelateds(lCon, Movie ?? 0), lCri, "IdMovie");
                    if (lMov?.IdSet?.Value != null)
                    {
                        var lSts = new Movies(lCon);
                        var lCrs = new KnkCriteria<Movie, Movie>(lSts, new KnkTableEntity("vieMovies", "Movies"));
                        lCrs.AddParameter(typeof(int), "IdSet", OperatorsEnu.Equal, lMov.IdSet.Value);
                        lSts.Criteria = lCrs;
                        if (lSts.Items.Count() > 0)
                            KnkCoreUtils.CreateNotInParameter(lSts, lCri, "IdMovie");
                    }
                }

                if (!string.IsNullOrEmpty(PersonName))
                {
                    KnkCoreUtils.CreateInParameter(new MovieCastings(lCon, $"%{PersonName}%"), lCri, "IdMovie");
                }

                foreach(var id in Persons)
                {
                    KnkCoreUtils.CreateInParameter(new MovieCastings(lCon, id), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Genre))
                {
                    KnkCoreUtils.CreateInParameter(new MovieGenres(lCon, $"%{Genre}%"), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Saga))
                {
                    KnkCoreUtils.CreateInParameter(new MovieMovieSets(lCon, $"%{Saga}%"), lCri, "IdMovie");
                }

                //if(ReleasedToday!=null)
                //{
                //    //var lmm=new MoviesOldfashioned
                //    KnkCoreUtils.CreateInParameter(new MovieMovieSets(lCon, $"%{Saga}%"), lCri, "IdMovie");
                //}

                if (YearFrom!=null)
                {
                    KnkParameterItf lPar = lCri.AddParameter(typeof(int), "Year", OperatorsEnu.GreatEqualThan, $"{YearFrom}");
                }

                if (YearTo != null)
                {
                    KnkParameterItf lPar = lCri.AddParameter(typeof(int), "Year", OperatorsEnu.LowerEqualThan, $"{YearTo}");
                }

                if (!string.IsNullOrEmpty(Country))
                {
                    KnkCoreUtils.CreateInParameter(new MovieCountries(lCon, $"{Country}"), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Company))
                {
                    KnkCoreUtils.CreateInParameter(new MovieCompanies(lCon, $"{Company}"), lCri, "IdMovie");
                }

                if (!string.IsNullOrEmpty(Keyword))
                {
                    KnkCoreUtils.CreateInParameter(new MovieKeywords(lCon, $"{Keyword}"), lCri, "IdMovie");
                }

                if (!lCri.HasParameters())
                    lCri = null;
            }

            if (!string.IsNullOrEmpty(SortProperty))
            {
                _movies.Sorting = null;
                _movies.SortAttribute = new KnkSortAttribute(SortProperty, SortDirectionAsc);
                _movies.InfoValue = InfoValue;
            }
            _movies.Criteria = lCri;
            return _movies;
        }

        public Movies RefreshList()
        {
            return GetCriteria();
        }

        public Movies CurrentList()
        {
            if (_movies == null)
                return GetCriteria();
            else
                return _movies;
        }

        public override string ToString()
        {
            return CriteriaTitle.ToString();
        }

        public void DefaultFilter(int? user)
        {
            NewFiles(user);
        }

        public void BestMovies()
        {
            CleanValues();
            CriteriaTitle = "Best Movies";
            SortProperty = "Rating";
        }

        public void MyMovies(int? user)
        {
            CleanValues();
            CriteriaTitle = "My Movies";
            SortProperty = "DateAdded";
            User = user;
            InMyLib = true;
        }

        public void FavouriteMovies(int? user)
        {
            MyMovies(user);
            if (user != null)
            {
                HasFiles = true;
                SortProperty = "UserRating";
            }
        }

        public void WhisedMovies(int? user)
        {
            CleanValues();
            CriteriaTitle = "Whised Movies";
            SortProperty = "PredictedRating";
            User = user;
            HasFiles = false;
            InMyLib = true;
            Viewed = false;
            UserRated = false;
        }

        public void PendingMovies(int? user)
        {
            CleanValues();
            CriteriaTitle = "Pending Movies";
            SortProperty = "PredictedRating";
            User = user;
            HasFiles = true;
            InMyLib = true;
            UserRated = false;
        }

        public void ArtistRecommended(int? user)
        {
            CleanValues();
            CriteriaTitle = "Artist Recommended";
            SortProperty = "BestCastingRating";
            User = user;
            HasFiles = true;
            InMyLib = true;
            UserRated = false;
        }

        public void Recomendations(int? user)
        {
            CleanValues();
            CriteriaTitle = "Recomendations";
            SortProperty = "PredictedRating";
            User = user;
            HasFiles = true;
            InMyLib = false;
        }

        public void LastViewed(int? user)
        {
            if (user != null)
            {
                CleanValues();
                Viewed = true;
                SortProperty = "LastViewed";
            }
            else
            {
                DefaultFilter(user);
            }
            CriteriaTitle = "Last Viewed";
            User = user;
        }

        public void MostViewed(int? user)
        {
            if (user != null)
            {
                CleanValues();
                Viewed = true;
                SortProperty = "ViewedTimes";
            }
            else
            {
                DefaultFilter(user);
            }
            CriteriaTitle = "Most Viewed";
            User = user;
        }

        public void NewMovies(int? user)
        {
            CleanValues();
            CriteriaTitle = "New Movies";
            SortProperty = "ReleaseDate";
            if (user != null)
            {
                HasLink = true;
            }
            else
            {
                HasFiles = true;
            }
            User = user;
        }

        public void LastUpdates(int? user)
        {
            CleanValues();
            CriteriaTitle = "Last Updates";
            if (user != null)
            {
                SortProperty = "CreationDate";
            }
            else
            {
                HasFiles = true;
                SortProperty = "CreationDate";
                SortDirectionAsc = true;
            }
            User = user;
        }

        public void NewFiles(int? user)
        {
            CleanValues();
            CriteriaTitle = "New Files";
            if (user != null)
            {
                HasFiles = true;
                SortProperty = "FileDate";
            }
            else
            {
                HasFiles = true;
                SortProperty = "CreationDate";
                SortDirectionAsc = true;
            }
            User = user;
        }

        private void CleanValues()
        {
            _movies = null;
            _movie = null;
            CriteriaTitle = string.Empty;
            Page = 1;
            User = null;
            SortDirectionAsc = false;
            AllowNavigate = true;
            IsRelated = false;

            TextSearch = null;
            InMyLib = null;

            PersonName = null;
            Persons = new List<int>();
            Country = null;
            Company = null;
            Genre = null;
            Saga = null;
            YearFrom = null;
            YearTo = null;
            Movie = null;
            UserRated = null;
            Viewed = null;
            HasFiles = null;
            AudioTracks = null;
            Subtitles = null;
            HasLink = null;

            RatingFrom = null;
            RatingTo = null;
            UserRatingFrom = null;
            UserRatingTo = null;
            PredictedRatingFrom = null;
            PredictedRatingTo = null;
            TmdbId = null;
        }
    }
}
