﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Enums
{
    public enum LinkTypeEnum
    {
        Poster = 1,
        Fanart = 2,
        Videos = 3
    }
}
