﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Enums
{
    public enum MediaTypeEnum
    {
        [Description("IdMovie")]            Movies,
        [Description("IdShow,IdEpisode")]   TvShows,
        [Description("IdSong,IdAlbum")]     Music,
        [Description("IdBook")]             Books
    }
}
