﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieCompanies : KnkList<Movie, MovieCompany>
    {
        public MovieCompanies(KnkConnectionItf aConnection, string aCompany)
        : base(aConnection)
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildLikeCriteria(this, "CompanyName", aCompany, "vieMovieCompanies", "IdMovie");
        }
    }
}
