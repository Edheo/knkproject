﻿using System.Collections.Generic;
using System.Linq;
using KnkCore;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;

namespace MovieModel.Lists
{
    public class Genres : KnkList<Genre>
    {
        public Genres(KnkConnectionItf aConnection) 
        : base(aConnection)
        {

        }

        public List<Genre> Datasource()
        {
            return (from c in Items orderby c.GenreName select c).ToList();
        }
    }
}
