﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieCountries : KnkList<Movie, MovieCountry>
    {
        public MovieCountries(KnkConnectionItf aConnection, string aCountry)
        : base(aConnection)
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildLikeCriteria(this, "CountryName", aCountry, "vieMovieCountries", "IdMovie");
        }
    }
}
