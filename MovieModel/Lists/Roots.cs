﻿using System.Linq;
using System.Collections.Generic;
using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using MovieModel.Enums;

namespace MovieModel.Lists
{
    public class Roots : KnkList<Folder>
    {
        public readonly MediaTypeEnum? MediaType;
        private Roots():base(new KnkConnection())
        {
        }

        public Roots(MediaTypeEnum aType):this()
        {
            MediaType = aType;
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildEqualCriteria(this, typeof(string), "ContentType", aType.ToString());
        }

    }
}
