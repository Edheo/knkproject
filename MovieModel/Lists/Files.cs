﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using MovieModel.Enums;

namespace MovieModel.Lists
{
    public class Files : KnkList<File>
    {
        public Files(KnkConnectionItf aConnection)
        : base(aConnection)
        {
        }

        public File Create(Folder aFolder, string aFilename, int aSynoId)
        {
            var ret = Create(aFolder, aFilename);
            ret.SynologyId = aSynoId;
            return ret;
        }

        public File Create(Folder aFolder, string aFilename)
        {
            System.IO.FileInfo lInfo = new System.IO.FileInfo(System.IO.Path.Combine(aFolder.Path, aFilename));
            var lFil = Create(true);
            lFil.Filedate = lInfo.LastWriteTime;
            lFil.Filesize = (int)(lInfo.Length / 1024);
            lFil.IdPath = aFolder;
            lFil.Filename = lInfo.Name;
            return lFil;
        }
    }

    public class FilesMissingMedia : Files
    {
        public FilesMissingMedia(KnkConnectionItf aConnection, MediaTypeEnum aType)
        : base(aConnection)
        {
            var lMef = new MediaFiles(aConnection);
            var lRot = new Roots(aType);
            switch (aType)
            {
                case MediaTypeEnum.Movies:
                    lMef.Criteria.AddParameter(typeof(int), "IdMovie", OperatorsEnu.IsNotNull);
                    break;
                case MediaTypeEnum.Books:
                    lMef.Criteria.AddParameter(typeof(int), "IdBook", OperatorsEnu.IsNotNull);
                    break;
            }
            //var mefids = lMef.GetListIds();
            //var rotids = lRot.GetListIds();

            KnkCoreUtils.CreateNotInParameter(lMef, Criteria, "IdFile", "IdFile");
            KnkCoreUtils.CreateInParameter(lRot, Criteria, "IdRoot");
        }
    }
}
