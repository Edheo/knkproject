﻿using System.Linq;
using System.Collections.Generic;
using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;

namespace MovieModel.Lists
{
    public class Users : KnkList<User>
    {
        public Users():base(new KnkConnection())
        {
        }

        public Users(KnkConnectionItf aConnection) : base(aConnection)
        {
        }

        public List<User> Datasource()
        {
            this.SortAttribute = new KnkSortAttribute("UserName");
            return base.Datasource();
        }
    }
}
