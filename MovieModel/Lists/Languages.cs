﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class Languages : KnkList<Language>
    {
        public Languages(KnkConnectionItf aConnection) 
        : base(aConnection)
        {

        }

        public List<Language> Datasource()
        {
            return (from c in Items orderby c.ToString() select c).ToList();
        }
    }
}
