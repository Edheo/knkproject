﻿using System.Collections.Generic;
using System.Linq;
using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using KnkInterfaces.Enumerations;

namespace MovieModel.Lists
{
    public class Persons : KnkList<Person>
    {
        public Persons()
        : this(new KnkConnection())
        {
        }

        public Persons(KnkConnectionItf aConnection) 
        : base(aConnection)
        {
            
        }

        public Persons(int aPersonid)
        : base(new KnkConnection())
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildEqualCriteria(this, typeof(int), "IdPersonWith", aPersonid, "viePersonWorkedTogether", "IdPerson");
        }

        public List<Person> Datasource()
        {
            return (from c in Items orderby c.Name select c).ToList();
        }
    }
}
