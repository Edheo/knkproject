﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;

namespace MovieModel.Lists
{
    public class Comments : KnkList<Comment>
    {
        public Comments(KnkConnectionItf aConnection)
        : base(aConnection)
        {
        }
    }
}
