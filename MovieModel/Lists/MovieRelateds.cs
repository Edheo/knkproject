﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieRelateds : KnkList<Movie, MovieRelated>
    {
        public MovieRelateds(KnkConnectionItf aConnection, int aIdMovie)
        : base(aConnection)
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildLikeCriteria<Movie, MovieRelated>(this, "IdMovieRelated", aIdMovie.ToString(), "vieMovieRelatedsWall", "IdMovie");
        }

    }
}
