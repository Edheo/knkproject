﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Interfaces;
using KnkInterfaces.Utilities;
using MovieModel.Entities;
using MovieModel.Enums;

namespace MovieModel.Lists
{
    public class BookUsers : KnkList<Book, MediaUser>
    {
        public BookUsers(KnkConnectionItf aConnection, User aUser)
        : base(aConnection)
        {
            KnkCoreUtils.BuildIsNotNullCriteria(Criteria, "IdBook");
            KnkCoreUtils.BuildEqualCriteria(this, typeof(int), "IdUser", aUser.IdUser.Value);
        }

        private BookUsers(KnkConnectionItf aConnection)
        : this(aConnection, (User)aConnection.CurrentUser)
        {
        }

        public BookUsers(KnkConnectionItf aConnection, MediaTypeEnum aType, bool aRated)
        : this(aConnection,(User)aConnection.CurrentUser)
        {
            if (aRated)
                KnkCoreUtils.BuildIsNotNullCriteria(Criteria, "UserRating");
            else
                KnkCoreUtils.BuildIsNullCriteria(Criteria, "UserRating");
        }

    }
}
