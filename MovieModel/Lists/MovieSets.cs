﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieSets : KnkList<MovieSet>
    {
        public MovieSets(KnkConnectionItf aConnection)
        : base(aConnection)
        {

        }

        public MovieSets(KnkConnectionItf aConnection, int aIdSet)
        : base(aConnection)
        {
            Criteria = KnkCoreUtils.BuildEqualCriteria(this, this.GetParent().GetType(), "IdSet", aIdSet);
        }

        public List<MovieSet> Datasource()
        {
            return (from c in Items orderby c.ToString() select c).ToList();
        }
    }
}
