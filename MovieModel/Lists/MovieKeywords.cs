﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieKeywords : KnkList<Movie, MediaTag>
    {
        public MovieKeywords(KnkConnectionItf aConnection, string aKeyword)
        : base(aConnection)
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildEqualCriteria(this, typeof(String), "Tag", aKeyword, "vieMediaTags", "IdMovie");
        }
    }
}
