﻿using KnkCore;
using KnkInterfaces.Enumerations;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieComments : KnkList<Movie, Comment>
    {
        public MovieComments(KnkConnectionItf aConnection, Movie aMovie)
        : base(aConnection)
        {
            var lCri = KnkCore.Utilities.KnkCoreUtils.BuildEqualCriteria(this, aMovie.GetType(), "IdValue", aMovie.IdMovie);
            lCri.AddParameter(typeof(int), "Object", OperatorsEnu.Equal, aMovie.GetType().ToString());
            Criteria = lCri;
        }
    }
}
