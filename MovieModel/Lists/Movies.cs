﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System.Linq;
using System.Collections.Generic;
using MovieModel.Criterias;

namespace MovieModel.Lists
{
    public class Movies:KnkList<Movie>
    {
        public Movies()
        : this(new KnkConnection())
        {
        }

        public Movies(KnkConnectionItf aConnection)
        : base(aConnection)
        {
        }

        public Movies(Movie aMovie)
        : base(aMovie)
        {
        }

        public Movies(KnkConnectionItf aConnection, KnkCriteriaItf<Movie,Movie> aCriteria) 
        : base(aConnection, aCriteria)
        {
        }

        public Movie FindMovie(int aTmdbId, bool aCreateIfNotExists = true)
        {
            MoviesCriteria criteria = new MoviesCriteria() { TmdbId = aTmdbId };
            var result = criteria.RefreshList();
            result.Changes = this.Changes;
            var lMovieDst = result.Items.FirstOrDefault();
            if (lMovieDst == null && aTmdbId>0 && aCreateIfNotExists)
            {
                lMovieDst = Create();
                lMovieDst.TmdbId = aTmdbId;
            }
            return lMovieDst;
        }

        public override bool SaveChanges()
        {
            var lLst = ItemsChanged().OrderBy(m=>m.Relateds().Items.Count).ToList();
            return SaveChanges(lLst);
        }

    }
}
