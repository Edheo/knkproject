﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MoviesOldfashioned : KnkList<MovieOldfashion>
    {
        public MoviesOldfashioned(KnkConnectionItf aConnection)
        : base(aConnection)
        {
        }
    }
}
