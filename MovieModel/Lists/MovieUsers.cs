﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Interfaces;
using KnkInterfaces.Utilities;
using MovieModel.Entities;
using MovieModel.Enums;

namespace MovieModel.Lists
{
    public class MovieUsers : KnkList<Movie, MediaUser>
    {
        public MovieUsers(KnkConnectionItf aConnection, KnkCriteria<Movie, MediaUser> aCriteria)
        : base(aConnection, aCriteria)
        {
        }

        public MovieUsers(KnkConnectionItf aConnection, User aUser)
        : base(aConnection)
        {
            KnkCoreUtils.BuildIsNotNullCriteria(Criteria, "IdMovie");
            Criteria = KnkCoreUtils.BuildEqualCriteria(this, typeof(int), "IdUser", aUser.IdUser.Value);
        }

        private MovieUsers(KnkConnectionItf aConnection)
        : this(aConnection, (User)aConnection.CurrentUser)
        {
        }

        public MovieUsers(KnkConnectionItf aConnection, MediaTypeEnum aType, bool aRated)
        : this(aConnection,(User)aConnection.CurrentUser)
        {
            if (aRated)
                KnkCoreUtils.BuildIsNotNullCriteria(Criteria, "UserRating");
            else
                KnkCoreUtils.BuildIsNullCriteria(Criteria, "UserRating");
        }

    }
}
