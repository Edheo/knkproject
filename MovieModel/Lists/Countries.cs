﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class Countries : KnkList<Country>
    {
        public Countries(KnkConnectionItf aConnection)
        : base(aConnection)
        {

        }

        public List<Country> Datasource()
        {
            return (from c in Items orderby c.CountryName select c).ToList();
        }
    }
}
