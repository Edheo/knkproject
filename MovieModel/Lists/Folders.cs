﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;

namespace MovieModel.Lists
{
    public class Folders : KnkList<Folder>
    {
        public Folders(KnkConnectionItf aConnection) 
        : base(aConnection)
        {
        }

        public Folder Create(Folder aParent, string aPath)
        {
            var lFol = Create(true);
            lFol.Protocol = aParent.Protocol;
            lFol.Path = aPath;
            lFol.ContentType = null;
            lFol.Scraper = null;
            lFol.Hash = null;
            lFol.ScanRecursive = null;
            lFol.UseFolderNames = null;
            lFol.NoUpdate = null;
            lFol.Exclude = null;
            lFol.IdParentPath = aParent;
            lFol.IdRoot = aParent.IdRoot.Reference;
            lFol.CreateIfNotExists();
            return lFol;
        }
    }

    public class SubFolders : KnkList<Folder>
    {
        public SubFolders(Folder aFolder)
        : base(aFolder.Connection())
        {
            Criteria = new KnkCriteria<Folder, Folder>(this, new KnkTableEntityRelation<Folder>("vieFolders", "IdParentPath"));
        }
    }
}
