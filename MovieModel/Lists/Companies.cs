﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class Companies : KnkList<Company>
    {
        public Companies(KnkConnectionItf aConnection) 
        : base(aConnection)
        {

        }

        public List<Company> Datasource()
        {
            return (from c in Items orderby c.ToString() select c).ToList();
        }
    }
}
