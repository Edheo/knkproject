﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System.Linq;
using System.Collections.Generic;
using System;

namespace MovieModel.Lists
{
    public class Books : KnkList<Book>
    {
        public Books()
        : this(new KnkConnection())
        {
        }

        public Books(KnkConnectionItf aConnection)
        : base(aConnection)
        {
        }

        public Books(KnkConnectionItf aConnection, KnkCriteriaItf<Book, Book> aCriteria)
        : base(aConnection, aCriteria)
        {
        }

        public Books(Book aBook)
        : base(aBook)
        {
        }

        public Book FindBook(string aTextSearch, bool aCreateIfNotExists = true)
        {
            var lParts = aTextSearch.Split(new string[] { " - " }, StringSplitOptions.None);
            Book lBook = null;
            if(lParts.Count()==2)
            {
                lBook = FindBook(lParts[0], lParts[1], aCreateIfNotExists);
            }
            return lBook;
        }

        public Book FindBook(string aTitle, string aAuthor, bool aCreateIfNotExists = true)
        {
            var lBookDst = (from bok in Items where (bok.Title.ToLower() == aTitle.ToLower() || bok.OriginalTitle.ToLower() == aTitle.ToLower()) select bok).FirstOrDefault();
            if (lBookDst == null && aCreateIfNotExists)
            {
                lBookDst = Create();
                lBookDst.OriginalTitle = aTitle;
                lBookDst.Title = aTitle;
            }
            return lBookDst;
        }

        public Book FindBook(string aGoodReadId, string aTitle, string aOriginalTitle, string aAuthor, bool aCreateIfNotExists = true)
        {
            var lBookDst = (from bok in Items where bok.GoodReadId == aGoodReadId select bok).FirstOrDefault();
            if (lBookDst == null)
            {
                lBookDst = (from bok in Items where (bok.Title.ToLower() == aTitle.ToLower() || bok.OriginalTitle.ToLower() == aOriginalTitle.ToLower()) select bok).FirstOrDefault();
                if (lBookDst == null && aCreateIfNotExists)
                {
                    lBookDst = Create();
                    lBookDst.GoodReadId = aGoodReadId;
                    lBookDst.OriginalTitle = aOriginalTitle;
                    lBookDst.Title = aTitle;
                }
            }
            return lBookDst;
        }

    }
}
