﻿using KnkCore;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieCastings : KnkList<Movie, MovieCasting>
    {
        public MovieCastings(KnkConnectionItf aConnection, string aArtist)
        : base(aConnection)
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildLikeCriteria<Movie, MovieCasting>(this, "Name", aArtist, "vieMovieCasting", "IdPerson");
        }

        public MovieCastings(KnkConnectionItf aConnection, int aPersonId)
        : base(aConnection)
        {
            Criteria = KnkCore.Utilities.KnkCoreUtils.BuildEqualCriteria(this, typeof(int), "IdPerson", aPersonId);
        }
    }
}
