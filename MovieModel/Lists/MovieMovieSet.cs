﻿using KnkCore;
using KnkCore.Utilities;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Lists
{
    public class MovieMovieSets : KnkList<Movie, MovieSet>
    {
        public MovieMovieSets(KnkConnectionItf aConnection, string aName)
        : base(aConnection)
        {
            Criteria = KnkCoreUtils.BuildLikeCriteria(this, "Name", aName, "vieMovieMovieSets", "IdSet");
        }
    }
}
