﻿using KnkCore;
using KnkInterfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Interfaces
{
    public interface IHasComment<T,C>
        where T : KnkItemItf, new()
        where C : KnkItemItf, new()
    {
        KnkEntityRelationItf<T, C> Comments();
    }
}
