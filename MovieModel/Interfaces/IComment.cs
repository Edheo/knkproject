﻿using KnkCore;
using KnkInterfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Interfaces
{
    public interface IComment
    {
        KnkItem ItemOwner { get; }
    }
}
