﻿using KnkInterfaces.Utilities;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace MovieModel.Utilities
{
    public static class KnkSolutionMoviesUtils
    {
        public static string GetLastPart(string aString, char aSpliter)
        {
            var lSplit = aString.Split(aSpliter);
            return lSplit[lSplit.Length -1];
        }

        public static Stream GetUrlStream(string aUrl)
        {
            Stream lStream = null;
            try
            {
                WebRequest lRequest = WebRequest.Create(aUrl);
                WebResponse lResponse = lRequest.GetResponse();
                lStream = lResponse.GetResponseStream();
            }
            catch (Exception)
            {
                //throw new Exception($"Problem downloading resource {aUrl}");
            }
            return lStream;
        }

        private static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[16 * 1024];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }

        public static void WriteStreamToFile(Stream aStream, string aFilename)
        {
            if (aStream != null)
            {
                using (var lFile = new FileStream(aFilename, FileMode.Create, FileAccess.Write))
                {
                     aStream.CopyTo(lFile);
                }
            }

        }

        public static Stream GetFileStream(string aFilename)
        {
            return new FileStream(aFilename, FileMode.Open, FileAccess.Read);
        }

        public static Entities.File EnrichTitleYear(Entities.File aFile)
        {
            if (string.IsNullOrEmpty(aFile.TitleSearch))
            {
                aFile.TitleSearch = TitleWithoutYear(aFile.Extender.TitleFromFilename());
                aFile.Update("Extracted Title");
            }
            var lTitleNoYear = aFile.TitleSearch;
            if (string.IsNullOrEmpty(aFile.YearSearch))
            {
                aFile.YearSearch = YearsFromFilename(aFile.Extender.TitleFromFilename(), lTitleNoYear);
                if (aFile.Status == KnkInterfaces.Enumerations.UpdateStatusEnu.NoChanges) aFile.Update("Extracted Years");
            }
            return aFile;
        }

        public static Entities.File EnrichTitleAuthor(Entities.File aFile)
        {
            if (string.IsNullOrEmpty(aFile.TitleSearch))
            {
                aFile.TitleSearch = TitleWithoutYear(aFile.Extender.TitleFromFilename());
                aFile.Update("Extracted Title");
            }
            var lTitleNoYear = aFile.TitleSearch;
            if (string.IsNullOrEmpty(aFile.YearSearch))
            {
                aFile.YearSearch = YearsFromFilename(aFile.Extender.TitleFromFilename(), lTitleNoYear);
                if (aFile.Status == KnkInterfaces.Enumerations.UpdateStatusEnu.NoChanges) aFile.Update("Extracted Years");
            }
            return aFile;
        }

        public static string TitleWithoutYear(string aTitle)
        {
            string[] lSplit = aTitle.Split(' ');
            List<string> lJoin = new List<string>();
            foreach (var lStr in lSplit)
            {
                int? lYear = IsAYear(lStr);
                if (lYear == null)
                {
                    lJoin.Add(lStr);
                }
            }
            if (lJoin.Count.Equals(0))
                lJoin.Add(lSplit.Max(e => e));
            return KnkInterfacesUtils.ConcatStrings(lJoin, true, " ");
        }

        public static string YearsFromFilename(string aTitle, string aSearchTitle)
        {
            string[] lSplit = aTitle.Split(' ');
            List<string> lJoin = new List<string>();
            foreach (var lStr in lSplit)
            {
                int? lYear = IsAYear(lStr);
                if (lYear != null)
                {
                    lJoin.Add(lStr);
                }
            }
            return KnkInterfacesUtils.ConcatStrings(lJoin, false, ";");
        }

        static int? IsAYear(string aText)
        {
            var lReturn = ToNullableInt32(aText);
            if (!(lReturn != null && aText.Length.Equals(4) && lReturn > 1890 && lReturn <= DateTime.Now.Year + 1))
                lReturn = null;
            return lReturn;
        }

        public static int? ToNullableInt32(this string s)
        {
            int i;
            if (Int32.TryParse(s, out i)) return i;
            return null;
        }

        private static string SplitOnCapitals(string text)
        {
            Regex regex = new Regex(@"\p{Lu}\p{Ll}*");
            var lLst = regex.Matches(text).Cast<Match>().Select(m => m.Value);
            return lLst.Aggregate((i, j) => $"{i} {j}");
        }

        public static SortedDictionary<string, string> MoviePropertiesCombo(int? aIdUser)
        {
            SortedDictionary<string, string> lDic = new SortedDictionary<string, string>();
            Movie lMov = new Movie();
            foreach (var lPrp in KnkInterfacesUtils.GetProperties(lMov, false))
            {
                string lName = lPrp.Name.ToLower();
                string lDescrip = SplitOnCapitals(lPrp.Name);
                switch (lName)
                {
                    case "idmovie":
                    case "tagline":
                    case "imdbid":
                    case "tmdbid":
                    case "seconds":
                    case "mparating":
                    case "originaltitle":
                    case "movieset":
                    case "trailerurl":
                    case "lastplayed":
                    case "idset":
                    case "adultcontent":
                    case "homepage":
                    case "deleted":
                    case "deleteddate":
                    case "usercreationid":
                    case "usermodifiedid":
                    case "userdeletedid":
                    case "creationtext":
                    case "modifiedtext":
                    case "deletedtext":
                    case "studio":
                    case "insertoutput":
                    case "status":

                        break;
                    default:
                        if (aIdUser == null)
                        {
                            switch (lName)
                            {
                                case "title":
                                case "votes":
                                case "rating":
                                case "year":
                                case "releasedate":
                                case "budget":
                                case "revenue":
                                case "popularity":
                                case "duration":
                                    lDic.Add($"{lDescrip}", $"{lPrp.Name}");
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            lDic.Add($"{lDescrip}", $"{lPrp.Name}");
                        break;
                }
            }

            return lDic;
        }
    }
}
