﻿using KnkCore;
using KnkInterfaces.Interfaces;
using System;
using System.Reflection;

namespace MovieModel.Utilities
{
    [Serializable]
    public class KnkDataModeler : KnkConfigurer, KnkDataModelerItf
    {
        private Assembly _Assembly = Assembly.GetAssembly(typeof(KnkDataModeler));

        public Assembly Assembly => _Assembly;
        public override string Name => new Uri(_Assembly.GetName().CodeBase).AbsolutePath;
        public Version Version => _Assembly.GetName().Version;
    }
}
