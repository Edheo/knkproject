﻿using KnkInterfaces.Utilities;
using MovieModel.Criterias;
using MovieModel.Entities;
using MovieModel.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace MovieModel.Extenders
{
    public class MovieExtender
    {
        private readonly Movie _Movie;
        private MediaLink _MediaThumb;

        public MovieExtender(Movie aMovie)
        {
            _Movie = aMovie;
        }

        #region Relationships

        public string Genres
        {
            get
            {
                return KnkInterfacesUtils.ConcatStrings(_Movie.Genres().Items.Select(g => g.ToString()).ToList());
            }
        }

        public string Summary
        {
            get
            {
                var lLst = (from sum in _Movie.Summary().Items orderby sum.IdSummary?.Value??0 select sum).ToList();
                return KnkInterfacesUtils.ConcatStrings(lLst.Select(g => g.ToString()).ToList(), true, Environment.NewLine);
            }
        }

        public MediaLink Poster
        {
            get
            {
                if (_MediaThumb == null)
                    _MediaThumb = (from p in _Movie.Pictures().Items orderby p.IdLink.Value where p.IdType == Enums.LinkTypeEnum.Poster select p).FirstOrDefault();
                if (_MediaThumb == null)
                    _MediaThumb = (from p in _Movie.Pictures().Items orderby p.IdLink.Value select p).FirstOrDefault();
                return _MediaThumb;
            }
        }

        public string Countries
        {
            get
            {
                return KnkInterfacesUtils.ConcatStrings(_Movie.Countries().Items.Select(g => g.ToString()).ToList());
            }
        }

        public string Companies
        {
            get
            {
                return KnkInterfacesUtils.ConcatStrings(_Movie.Companies().Items.Select(g => g.ToString()).ToList());
            }
        }

        private MediaUser MovieUser()
        {
            return _Movie.Users().Items.FirstOrDefault(u => u.User.IdUser.Value == _Movie.Connection().CurrentUser.PrimaryKeyValue().Value);
        }

        public DateTime? DateAdded
        {
            get
            {
                var lRet = MovieUser()?.CreationDate;
                return lRet;
            }
        }

        public decimal? UserRating
        {
            get
            {
                var lRet = MovieUser()?.UserRating;
                if (lRet != null) lRet = Math.Round(lRet ?? 0, 3);
                return lRet;
            }

            set
            {
                var lMus = MovieUser();
                lMus.UserRating = value;
                lMus.Update("User Rating");
                lMus.SaveChanges();
            }

        }

        public decimal? UserCriteria => _Movie.PredictedRating + UserRating;

        public decimal? AveragedRate
        {
            get
            {
                decimal? lRet = null;
                if (UserRating != null)
                {
                    lRet = _Movie.Rating;
                    lRet = (lRet + (2 * UserRating)) / 3;
                    if (lRet != null) lRet = Math.Round(lRet ?? 0, 3);
                }
                return lRet;
            }
        }

        #endregion

        #region Methods

        public string Director()
        {
            return KnkInterfacesUtils.ConcatStrings((from c in _Movie.Casting().Items where c.IdCastingType.Reference.Type.Equals("Director") orderby c.IdCastingType.Reference.Type descending, c.Ordinal select c.IdPerson.Reference.Name).ToList());
        }

        public string Writer()
        {
            return KnkInterfacesUtils.ConcatStrings((from c in _Movie.Casting().Items where c.IdCastingType.Reference.Type.Equals("Writer") orderby c.IdCastingType.Reference.Type descending, c.Ordinal select c.IdPerson.Reference.Name).ToList());
        }

        public List<MovieCasting> ArtistCasting()
        {
            return (from c in _Movie.Casting().Items where c.IdCastingType.Reference.Type.Equals("Actor") orderby c.IdCastingType.Reference.Type descending, c.Ordinal select c).ToList();
        }

        public string ImdbUrl()
        {
            return _Movie.ImdbId != null ? $"http://www.imdb.com/title/{_Movie.ImdbId}/" : null;
        }

        public string TmdbUrl()
        {
            return _Movie.TmdbId != null ? $"https://www.themoviedb.org/movie/{_Movie.TmdbId}/" : null;
        }

        public string HispashareUrl()
        {
            var lSearh = WebUtility.UrlEncode(_Movie.Title.Contains("??") ? _Movie.OriginalTitle : _Movie.Title);
            return $"http://www.hispashare.com/?view=search&q={lSearh}&year={_Movie.Year}&advsrch=1";
        }

        public DateTime? FileDate()
        {
            var lFile = (from fil in _Movie.Files().Items orderby fil.CreationDate descending select fil).FirstOrDefault();
            return lFile?.IdFile?.Reference?.Filedate;
        }

        public bool ShouldRenameFile()
        {
            return ShouldRenameFile((from fil in _Movie.Files().Items orderby fil.CreationDate descending select fil.IdFile.Reference).FirstOrDefault());
        }

        public bool ShouldRenameFile(File aFile)
        {
            return !aFile.Filename.Equals(FilenameProposal(aFile));
        }

        public string FilenameProposal(File aFile)
        {
            return FilenameProposal(aFile, 1);
        }

        public string FilenameProposal(File aFile, int aOrdinal)
        {
            if (!string.IsNullOrEmpty(_Movie.SuggestedFileName))
            {
                if (aOrdinal > 1)
                    return $"{_Movie.SuggestedFileName}.{aOrdinal}.{aFile.Extender.FileExtension()}";
                return $"{_Movie.SuggestedFileName}.{aFile.Extender.FileExtension()}";
            }
            else
            {
                return aFile.Filename;
            }
        }

        public bool ShouldBeMoved()
        {
            return ShouldBeMoved((from fil in _Movie.Files().Items orderby fil.CreationDate descending select fil.IdFile.Reference).FirstOrDefault());
        }

        public bool ShouldBeMoved(File aFile)
        {
            var lPath = aFile.IdPath.ToString();
            return !string.IsNullOrEmpty(_Movie.SuggestedPath) && !lPath.Equals(_Movie.SuggestedPath);
        }

        public bool AlreadyExistsRelated(Movie aMovie, Movie aMovieRelated)
        {
            var lLst = _Movie.Relateds().DeletedItems;
            var lAny = lLst.Any(d => d.IdMovie?.Value == aMovie.IdMovie?.Value && d.IdMovieRelated?.Value == (aMovieRelated.IdMovie?.Value ?? 0));
            return lAny;
        }

        public int MissingValue()
        {
            if (_Movie.LastViewed.HasValue && UserRating.HasValue)
                return (int)((decimal)(DateTime.Now - _Movie.LastViewed.Value).TotalDays * UserRating.Value);
            else
                return 0;
        }

        public string Tags()
        {
            var lLst = (from tag in _Movie.Tags().Items orderby tag.Tag select tag).ToList();
            return KnkInterfacesUtils.ConcatStrings(lLst.Select(g => g.ToString()).ToList(), true, " ");
        }
        #endregion

    }
}
