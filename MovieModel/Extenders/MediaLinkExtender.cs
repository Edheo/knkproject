﻿using KnkCore;
using KnkCore.Utilities;
using MovieModel.Entities;
using MovieModel.Utilities;
using System.IO;
using System.Xml;

namespace MovieModel.Extenders
{
    public class MediaLinkExtender
    {
        private readonly MediaLink _MediaLink;

        public object Connection { get; private set; }

        public MediaLinkExtender(MediaLink aMediaThumb)
        {
            _MediaLink = aMediaThumb;
        }

        private string ParsedId(int? aId, int aSize)
        {
            if(aId!=null) return aId.ToString().PadLeft(aSize, '0');
            return string.Empty;
        }


        public string GetFileName()
        {
            string lFrom = _MediaLink.ToString();

            KnkConnection lConf = _MediaLink.Connection() as KnkConnection;
            string lFolder = lConf.GetMediaFolder(typeof(Movie));

            string lPartName1 = string.Empty;
            if (_MediaLink.IdPerson != null)
                lPartName1 = ParsedId(_MediaLink.IdPerson, 12);
            else
                lPartName1 = ParsedId(_MediaLink.IdMovie, 8);
            //string lPartName2 = KnkSolutionMoviesUtils.GetLastPart(lFrom, '/');
            string lPartName2 = KnkSolutionMoviesUtils.GetLastPart(lFrom, '/');
            lPartName2 = KnkSolutionMoviesUtils.GetLastPart(lPartName2, '=');

            if (!string.IsNullOrEmpty(lPartName1) && !string.IsNullOrEmpty(lPartName2))
            {
                string lFileName = Path.Combine(lFolder, KnkCoreUtils.CleanFileName($"{lPartName1}_{lPartName2}"));
                string lExt = Path.GetExtension(lFileName);
                if(lExt.Equals(string.Empty))
                {
                    lFileName = Path.Combine(lFolder, KnkCoreUtils.CleanFileName($"{lPartName1}_{lPartName2}.jpg"));
                }
                if (!System.IO.File.Exists(lFileName))
                {
                    KnkSolutionMoviesUtils.WriteStreamToFile(KnkSolutionMoviesUtils.GetUrlStream(lFrom), lFileName);
                }
                return lFileName;
            }
            return string.Empty;
        }

        public string SiteUrl()
        {
            return string.Format(_MediaLink.Site, _MediaLink.Value);
        }
    }
}
