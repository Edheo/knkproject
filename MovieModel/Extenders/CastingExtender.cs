﻿using KnkInterfaces.Utilities;
using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Extenders
{
    public class CastingExtender
    {
        private readonly Person _Casting;
        private MediaLink _MediaThumb;

        public CastingExtender(Person aCasting)
        {
            _Casting = aCasting;
        }

        public MediaLink Poster
        {
            get
            {
                if (_MediaThumb == null)
                {

                    var rand = new Random();
                    var elems = _Casting.Pictures().Items.Where(p => p.IdType == Enums.LinkTypeEnum.Poster).Count();
                    if (elems > 0)
                        _MediaThumb = _Casting.Pictures().Items[rand.Next(elems)];
                    if (_MediaThumb == null)
                        _MediaThumb = _Casting.Pictures().Items.OrderByDescending(p => p.IdLink.Value).FirstOrDefault();
                }
                return _MediaThumb;
            }
        }

        public string Biography
        {
            get
            {
                var lLst = (from sum in _Casting.Biography().Items orderby sum.IdPersonBiography.Value select sum).ToList();
                return KnkInterfacesUtils.ConcatStrings(lLst.Select(g => g.ToString()).ToList(), true, Environment.NewLine);
            }
        }


    }
}
