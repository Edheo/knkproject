﻿using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MovieModel.Extenders
{
    public class FileExtender
    {
        private readonly File _File;

        public FileExtender(File aFile)
        {
            _File = aFile;
        }

        public string TitleFromFilename()
        {
            string lReturn = System.IO.Path.GetFileNameWithoutExtension(_File.ToString());
            lReturn = new string((from c in lReturn
                                  where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                  select c).ToArray());
            return lReturn;
        }

        public string SuggestedFileName()
        {
            var lMof = _File.MovieFiles().Items.FirstOrDefault();
            var lStr = _File.ToString();
            if (lMof != null)
            {
                var lGen = lMof.IdMovie?.Reference?.Genres().Items.FirstOrDefault();
                if (lGen != null)
                {
                    lStr = _File.IdPath.Reference.IdRoot?.Reference?.Path;
                }
            }
            return lStr;
        }

        public string FileExtension() => new System.IO.FileInfo(_File.ToString()).Extension.Substring(1);

        public bool Exists()
        {
            return System.IO.File.Exists(_File.ToString());
        }

        public bool ExistsMagnetFile()
        {
            var file = MagnetFile();
            return System.IO.File.Exists(file) && new System.IO.FileInfo(file).Length > 0;
        }

        private string MagnetFile()
        {
            return $"D:\\Temp\\Magnet\\{_File.IdFile}.magnet";
        }

        public string eD2kLink()
        {
            if (Exists() && !_File.IsNew())
            {
                BuildMagnetFile();
            }
            return BuildeD2kLink();
        }

        public string MagnetLink()
        {
            if (Exists() && !_File.IsNew())
            {
                BuildMagnetFile();
            }
            return BuildMagnetLink();
        }

        private string BuildMagnetLink()
        {
            if (ExistsMagnetFile())
            {
                var lenc = System.IO.File.ReadAllText(MagnetFile());
                var laich = new Regex(@"aich:\s*([^\n\r]*)").Match(lenc).Groups[1].Value;
                var lurin = Uri.EscapeUriString(_File.Filename);

                if (!string.IsNullOrEmpty(laich) && !string.IsNullOrEmpty(lurin))
                    return $"magnet:?xt=urn:sha1:{laich}&dn={lurin}";
            }
            return string.Empty;
        }

        private string BuildeD2kLink()
        {
            if (ExistsMagnetFile())
            {
                var lenc = System.IO.File.ReadAllText(MagnetFile());
                var led2k = new Regex(@":ed2k:\s*([^\n\r]*)&xt=").Match(lenc).Groups[1].Value;
                var laich = new Regex(@"aich:\s*([^\n\r]*)").Match(lenc).Groups[1].Value;
                var lurin = Uri.EscapeUriString(_File.Filename);
                var lflen = new System.IO.FileInfo(_File.ToString()).Length;

                if (!string.IsNullOrEmpty(led2k) && !string.IsNullOrEmpty(laich))
                    return $"ed2k://|file|{lurin}|{lflen}|{led2k}|h={laich}|/";
            }
            return string.Empty;
        }

        private void BuildMagnetFile()
        {
            if (!ExistsMagnetFile() & !_File.IsNew())
            {

                //var script = $"Read-Host -Prompt \"Press Enter to continue\";C:\\ProgramData\\chocolatey\\bin\\rhash.exe --magnet \"{_File.Filename}\" > \"{filedes}\";Read-Host -Prompt \"Press Enter to continue\";";
                var script = $"rhash.exe --magnet '{_File.Filename.Replace("'", "''")}' > '{MagnetFile()}';";
                ProcessStartInfo cmdsi = new ProcessStartInfo();
                cmdsi.FileName = "powershell.exe";
                cmdsi.Arguments = script;
                cmdsi.WorkingDirectory = _File.IdPath.Reference?.Path;
                cmdsi.WindowStyle = ProcessWindowStyle.Hidden;
                Process cmd = Process.Start(cmdsi);
                cmd.WaitForExit();
            }

        }

        private static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
    }
}
