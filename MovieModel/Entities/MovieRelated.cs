﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Entities
{
    public class MovieRelated : KnkItem
    {
        #region Interface/Implementation
        public MovieRelated() : base(new KnkTableEntity("vieMovieRelateds", "MovieRelateds"))
        {
            InsertOutput = false;
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdRelated { get; set; }
        public KnkEntityReference<Movie> IdMovie { get; set; }
        public KnkEntityReference<MovieSet> IdSet { get; set; }
        public KnkEntityReference<Movie> IdMovieRelated { get; set; }
        public KnkEntityReference<MovieSet> IdSetRelated { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            if (IdSetRelated?.Reference != null)
                return $"{IdSetRelated?.Reference.ToString()})";
            else
                return $"{IdMovieRelated?.Reference.ToString()})";
        }

    }
}
