﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Entities
{
    public class PersonBiography : KnkItem
    {
        #region Interface/Implementation
        public PersonBiography():base(new KnkTableEntity("viePersonBiographies", "PersonBiographies"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdPersonBiography { get; set; }
        public KnkEntityReference<Person> IdPerson { get; set; }
        public int Ordinal { get; set; }
        public string Text { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            return $"{Text}{Environment.NewLine}";
        }
    }
}
