﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;

namespace MovieModel.Entities
{
    public class MediaUser : KnkItem
    {
        #region Interface/Implementation
        public MediaUser():base(new KnkTableEntity("vieMediaUsers", "MediaUsers"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdMediaUser { get; set; }
        public KnkEntityReference<Movie> IdMovie { get; set; }
        public KnkEntityReference<User> IdUser { get; set; }
        public decimal? UserRating { get; set; }
        #endregion Class Properties

        public Movie Movie { get { return IdMovie?.Reference; } }
        public User User { get { return IdUser?.Reference; } }

        public override string ToString()
        {
            return UserRating.ToString();
        }
    }
}
