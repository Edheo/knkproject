﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;

namespace MovieModel.Entities
{
    public class Folder : KnkItem
    {
        #region Interface/Implementation
        public Folder():base(new KnkTableEntity("vieFolders", "Paths"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties


        [AtributePrimaryKey]
        public KnkEntityIdentifier IdPath { get; set; }
        public string Protocol { get; set; }
        public string Path { get; set; }
        public string ContentType { get; set; }
        public string Scraper { get; set; }
        public string Hash { get; set; }
        public int? ScanRecursive { get; set; }
        public decimal? UseFolderNames { get; set; }
        public decimal? NoUpdate { get; set; }
        public decimal? Exclude { get; set; }
        public KnkEntityReference<Folder> IdParentPath { get; set; }
        public KnkEntityReference<Folder> IdRoot { get; set; }
        public int? Files { get; set; }
        public bool IsTemp { get; set; }
        public bool IsPermanent { get; set; }

        #endregion Class Properties

        public void CreateIfNotExists()
        {
            if (!System.IO.Directory.Exists(Path)) System.IO.Directory.CreateDirectory(Path);
        }

        public void DeleteIfHasNoFiles()
        {
            if (System.IO.Directory.Exists(Path) && System.IO.Directory.GetFiles(Path).Length==0) System.IO.Directory.Delete(Path);
        }

        public override string ToString()
        {
            return Path;
        }

    }
}
