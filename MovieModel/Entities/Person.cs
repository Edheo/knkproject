﻿using KnkCore;
using KnkInterfaces.Interfaces;
using KnkInterfaces.PropertyAtributes;
using MovieModel.Extenders;
using System;

namespace MovieModel.Entities
{
    public class Person : KnkItem
    {
        public readonly CastingExtender Extender;

        KnkEntityRelation<Person, MediaLink> _Pictures;
        KnkEntityRelation<Person, PersonBiography> _Biography;
        KnkEntityRelation<Person, PersonName> _Names;

        #region Interface/Implementation
        public Person():base(new KnkTableEntity("viePerson", "Person"))
        {
            Extender = new CastingExtender(this);
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdPerson { get; set; }
        public string Name { get; set; }
        public string BirthDay { get; set; }
        public string DeathDay { get; set; }
        public string HomePage { get; set; }
        public string BirthPlace { get; set; }
        public string ImdbId { get; set; }
        public string TmdbId { get; set; }
        public string TvdbId { get; set; }
        public DateTime? ScrapedDate { get; set; }
        public decimal? Rating { get; set; }
        public int? Movies { get; set; }
        public int? RatedMovies { get; set; }
        public decimal? UserRating { get; set; }
        public decimal? TotalRating { get; set; }
        public decimal? Relevance { get; set; }
        public decimal? Popularity { get; set; }
        public int? WorkedTogether { get; set; }

        #endregion Class Properties

        public KnkEntityRelationItf<Person, MediaLink> Pictures()
        {
            if (_Pictures == null) _Pictures = new KnkEntityRelation<Person, MediaLink>(this, "viePersonLinks");
            return _Pictures;
        }

        public KnkEntityRelationItf<Person, PersonBiography> Biography()
        {
            if (_Biography == null) _Biography = new KnkEntityRelation<Person, PersonBiography>(this, "viePersonBiographies");
            return _Biography;
        }

        public KnkEntityRelationItf<Person, PersonName> Names()
        {
            if (_Names == null) _Names = new KnkEntityRelation<Person, PersonName>(this, "viePersonNames");
            return _Names;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
