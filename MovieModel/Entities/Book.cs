﻿using KnkCore;
using KnkInterfaces.Interfaces;
using KnkInterfaces.PropertyAtributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Entities
{
    public class Book : KnkItem
    {
        KnkEntityRelation<Book, MediaFile> _Files;

        #region Interface/Implementation
        public Book() : base(new KnkTableEntity("vieBooks", "Books"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdBook { get; set; }
        public string Title { get; set; }
        public string OriginalTitle { get; set; }
        public string TagLine { get; set; }
        public int Votes { get; set; }
        public decimal Rating { get; set; }
        public int Pages { get; set; }
        public string HomePage { get; set; }
        public string GoodReadId { get; set; }
        public string Isbn { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? ScrapedDate { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            return $"{Title}";
        }

        public KnkEntityRelationItf<Book, MediaFile> Files()
        {
            if (_Files == null) _Files = new KnkEntityRelation<Book, MediaFile>(this, "vieBookFiles");
            return _Files;
        }

    }
}
