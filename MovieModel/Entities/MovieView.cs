﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Entities
{
    public class MovieView : KnkItem
    {
        #region Interface/Implementation
        public MovieView() : base(new KnkTableEntity("vieMovieViews", "MovieViews"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdView { get; set; }
        public KnkEntityReference<Movie> IdMovie { get; set; }
        public KnkEntityReference<File> IdFile { get; set; }
        public DateTime DateStart { get; set; }
        public TimeSpan? CurrentTime { get; set; }
        public DateTime? DateEnd { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            return $"{(DateEnd ?? DateStart):dd/MM/yyyy - hh:mm}";
        }
    }
}