﻿using KnkCore;
using KnkInterfaces.Interfaces;
using KnkInterfaces.PropertyAtributes;
using MovieModel.Extenders;
using System;
using System.Linq;

namespace MovieModel.Entities
{
    public class Movie : KnkItem
    {
        public readonly MovieExtender Extender;
        KnkEntityRelation<Movie, MovieGenre> _Genres;
        KnkEntityRelation<Movie, MediaFile> _Files;
        KnkEntityRelation<Movie, File> _Links;
        KnkEntityRelation<Movie, MovieCasting> _Casting;
        KnkEntityRelation<Movie, MovieCountry> _Countries;
        KnkEntityRelation<Movie, MediaLink> _Pictures;
        KnkEntityRelation<Movie, MediaSummary> _Summary;
        KnkEntityRelation<Movie, MediaTag> _Tags;
        KnkEntityRelation<Movie, MovieCompany> _Companies;
        KnkEntityRelation<Movie, MovieLanguage> _Languages;
        KnkEntityRelation<Movie, MediaUser> _Users;
        KnkEntityRelation<Movie, MovieView> _Views;
        KnkEntityRelation<Movie, MovieRelated> _Relateds;
        KnkEntityRelation<Movie, Comment> _Comments;

        decimal _Rating;

        #region Interface/Implementation
        public Movie() : this(new KnkTableEntity("vieMovies", "Movies"))
        {
        }

        internal Movie(KnkTableEntity aEntity) : base(aEntity)
        {
            Extender = new MovieExtender(this);
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdMovie { get; set; }
        public string Title { get; set; }
        public string TagLine { get; set; }
        public int Votes { get; set; }
        public decimal? PredictedRating { get; set; }
        public decimal? PredictedBestRating { get; set; }
        public decimal? CastingRating { get; set; }
        public decimal? BestCastingRating { get; set; }
        public decimal? RelatedMoviesRating { get; set; }
        public decimal? CastingPopularity { get; set; }
        public decimal Rating
        {
            get
            {
                if (Votes < 50 && (UserRating??0) == 0) return _Rating / 2;
                else return _Rating;
            }

            set
            {
                _Rating = value;
            }
        }
        public int? Year { get; set; }
        public string ImdbId { get; set; }
        public int? TmdbId { get; set; }
        public string MPARating { get; set; }
        public string OriginalTitle { get; set; }
        public string Studio { get; set; }
        public KnkEntityReference<MovieSet> IdSet { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public bool AdultContent { get; set; }
        public decimal Budget { get; set; }
        public decimal Revenue { get; set; }
        public string HomePage { get; set; }
        public decimal Popularity { get; set; }
        public DateTime? ScrapedDate { get; set; }
        public int? AudioTracks => Files()?.Items.Max(f => f.IdFile.Reference.AudioTracks);
        public int? Subtitles => Files()?.Items.Max(f => f.IdFile.Reference.Subtitles);
        public string SuggestedPath { get; set; }
        public string SuggestedFileName { get; set; }
        #endregion

        public int FilesNumber { get; set; }
        public DateTime? FirstViewed { get; set; }
        public DateTime? LastViewed { get; set; }
        public int? ViewedTimes { get; set; }

        public TimeSpan? Duration { get; set; }
        public decimal? UserRating
        {
            get
            {
                return Extender.UserRating;
            }
            set
            {
                Extender.UserRating = value;
            }
        }

        public decimal? UserCriteria => Extender.UserCriteria;

        public DateTime? DateAdded
        {
            get
            {
                return Extender.DateAdded;
            }
            set
            {
                value = value;
            }
        }

        public decimal? AveragedRate
        {
            get
            {
                return Extender.AveragedRate;
            }
            set
            {
                value = Extender.AveragedRate;
            }
        }

        public DateTime? FileDate
        {
            get
            {
                return Extender.FileDate();
            }

            set
            {
                value = Extender.FileDate();
            }
        }

        public int MissingValue
        {
            get
            {
                return Extender.MissingValue();
            }

            set
            {
                value = Extender.MissingValue();
            }
        }


        public MovieSet MovieSet { get { return IdSet?.Reference; } }

        public KnkEntityRelationItf<Movie, MediaUser> Users()
        {
            if (_Users == null) _Users = new KnkEntityRelation<Movie, MediaUser>(this, "vieMediaUsers");
            return _Users;
        }

        public KnkEntityRelationItf<Movie, MovieGenre> Genres()
        {
            if (_Genres == null) _Genres = new KnkEntityRelation<Movie, MovieGenre>(this, "vieMovieGenres");
            return _Genres;
        }

        public KnkEntityRelationItf<Movie, MediaFile> Files()
        {
            if (_Files == null) _Files = new KnkEntityRelation<Movie, MediaFile>(this, "vieMovieFiles");
            return _Files;
        }

        public KnkEntityRelationItf<Movie, File> Links()
        {
            if (_Links == null) _Links = new KnkEntityRelation<Movie, File>(this, "vieMovieElinks");
            return _Links;
        }

        public KnkEntityRelationItf<Movie, MovieCasting> Casting()
        {
            if (_Casting == null) _Casting = new KnkEntityRelation<Movie, MovieCasting>(this);
            return _Casting;
        }

        public KnkEntityRelationItf<Movie, MovieCountry> Countries()
        {
            if (_Countries == null) _Countries = new KnkEntityRelation<Movie, MovieCountry>(this, "vieMovieCountries");
            return _Countries;
        }

        public KnkEntityRelationItf<Movie, MediaLink> Pictures()
        {
            if (_Pictures == null) _Pictures = new KnkEntityRelation<Movie, MediaLink>(this, "vieMovieLinks");
            return _Pictures;
        }

        public KnkEntityRelationItf<Movie, MediaSummary> Summary()
        {
            if (_Summary == null) _Summary = new KnkEntityRelation<Movie, MediaSummary>(this, "vieMediaSummaries");
            return _Summary;
        }

        public KnkEntityRelationItf<Movie, MovieCompany> Companies()
        {
            if (_Companies == null) _Companies = new KnkEntityRelation<Movie, MovieCompany>(this, "vieMovieCompanies");
            return _Companies;
        }

        public KnkEntityRelationItf<Movie, MovieLanguage> Languages()
        {
            if (_Languages == null) _Languages = new KnkEntityRelation<Movie, MovieLanguage>(this, "vieMovieLanguages");
            return _Languages;
        }

        public KnkEntityRelationItf<Movie, MovieView> Views()
        {
            if (_Views == null) _Views = new KnkEntityRelation<Movie, MovieView>(this, "vieMovieViews");
            return _Views;
        }

        public KnkEntityRelationItf<Movie, MovieRelated> Relateds()
        {
            if (_Relateds == null) _Relateds = new KnkEntityRelation<Movie, MovieRelated>(this, "vieMovieRelatedsWall");
            return _Relateds;
        }

        public KnkEntityRelationItf<Movie, Comment> Comments()
        {
            if (_Comments == null) _Comments = new KnkEntityRelation<Movie, Comment>(this, "vieComments", "IdMovieOwner");
            return _Comments;
        }

        public KnkEntityRelationItf<Movie, MediaTag> Tags()
        {
            if (_Tags == null) _Tags = new KnkEntityRelation<Movie, MediaTag>(this, "vieMediaTags", "IdMovie");
            return _Tags;
        }

        public override string ToString()
        {
            return $"{Title} ({Year})";
        }

        public bool IsScrappable(int aCalllevel = 0)
        {
            if (this.ModifiedDate.HasValue && this.ModifiedDate.Value > DateTime.Now.AddMinutes(-5))
                return false;
            var lret = aCalllevel == 0 && (this.ScrapedDate == null || (this.TmdbId ?? 0) == 0 || this.ScrapedDate < DateTime.Now.AddYears(-1) || Tags().Items.Count < 3);
            return lret || (this.ScrapedDate == null || (this.TmdbId ?? 0) == 0 || this.ScrapedDate < DateTime.Now.AddYears(-1) || (aCalllevel <= 1 && Tags().Items.Count < 3 && FilesNumber > 0));
        }

    }

    public class MovieOldfashion : Movie
    {
        public MovieOldfashion() : base(new KnkTableEntity("vieMovieOldfashion", "Movies"))
        {
        }
    }
}
