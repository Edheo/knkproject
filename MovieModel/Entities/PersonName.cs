﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieModel.Entities
{
    public class PersonName : KnkItem
    {
        #region Interface/Implementation
        public PersonName():base(new KnkTableEntity("viePersonNames", "PersonNames"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdPersonName { get; set; }
        public KnkEntityReference<Person> IdPerson { get; set; }
        public string Name { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            return Name;
        }

    }
}
