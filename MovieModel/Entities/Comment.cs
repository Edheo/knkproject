﻿using KnkCore;
using KnkInterfaces.Interfaces;
using KnkInterfaces.PropertyAtributes;
using MovieModel.Extenders;
using MovieModel.Interfaces;
using System;

namespace MovieModel.Entities
{
    public class Comment : KnkItem, IComment
    {
        KnkEntityRelation<Comment, Comment> _Comments;

        #region Interface/Implementation
        public Comment():base(new KnkTableEntity("vieComments", "Comments"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdComment { get; set; }

        public KnkEntityReference<Movie> IdMovieOwner { get; set; }
        public KnkEntityReference<Comment> IdCommentOwner { get; set; }
        public int CommentLevel { get; set; }

        public string CommentText { get; set; }

        public KnkItem ItemOwner
        {
            get
            {
                if (IdMovieOwner!= null)
                    return IdMovieOwner.Reference;
                else
                    return IdCommentOwner.Reference;
            }
        }
        #endregion Class Properties

        public override string ToString()
        {
            return CommentText;
        }

        public KnkEntityRelationItf<Comment,Comment> Comments()
        {
            if (_Comments == null) _Comments = new KnkEntityRelation<Comment, Comment>(this, "vieComments", "IdCommentOwner");
            return _Comments;
        }
    }
}
