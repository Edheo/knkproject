﻿using KnkCore;
using KnkInterfaces.Interfaces;
using KnkInterfaces.PropertyAtributes;
using MovieModel.Extenders;
using System;

namespace MovieModel.Entities
{
    public class File : KnkItem
    {
        KnkEntityRelation<File, MediaFile> _MovieFiles;

        public readonly FileExtender Extender;

        #region Interface/Implementation
        public File():this(new KnkTableEntity("vieFiles","Files"))
        {
        }

        internal File(KnkTableEntity aEntity) : base(aEntity)
        {
            Extender = new FileExtender(this);
        }

        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdFile { get; set; }
        public KnkEntityReference<Folder> IdPath { get; set; }
        public string Filename { get; set; }
        public DateTime Filedate { get; set; }
        public int? Filesize { get; set; }
        public int Scraped { get; set; }
        public TimeSpan? Duration { get; set; }
        public string TitleSearch { get; set; }
        public string YearSearch { get; set; }
        public int? AudioTracks { get; set; }
        public int? Subtitles { get; set; }
        public string Elink { get; set; }
        public string TorrentLink { get; set; }
        public string VideoCodec { get; set; }
        public string AudioCodec { get; set; }
        public int? SynologyId { get; set; }
        public KnkEntityReference<Folder> IdRoot { get; set; }
        #endregion Class Properties

        public void LinksBuild()
        {
            if (string.IsNullOrEmpty(Elink) || string.IsNullOrEmpty(TorrentLink))
            {
                Elink = Extender.eD2kLink();
                TorrentLink = Extender.MagnetLink();
            }
        }

        public KnkEntityRelationItf<File, MediaFile> MovieFiles()
        {
            if (_MovieFiles == null) _MovieFiles = new KnkEntityRelation<File, MediaFile>(this, "vieMovieFiles");
            return _MovieFiles;
        }

        public override string ToString()
        {
            return IdPath.Reference?.Path + Filename;
        }
    }

}
