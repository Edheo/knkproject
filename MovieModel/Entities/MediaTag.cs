﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;

namespace MovieModel.Entities
{
    public class MediaTag : KnkItem
    {
        #region Interface/Implementation
        public MediaTag() : base(new KnkTableEntity("vieMediaTags", "MediaTags"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdTag { get; set; }
        public KnkEntityReference<Movie> IdMovie { get; set; }
        public string Tag { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            return $"#{Tag}";
        }
    }
}
