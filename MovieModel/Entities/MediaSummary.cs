﻿using KnkCore;
using KnkInterfaces.PropertyAtributes;
using System;

namespace MovieModel.Entities
{
    public class MediaSummary : KnkItem
    {
        #region Interface/Implementation
        public MediaSummary():base(new KnkTableEntity("vieMediaSummaries", "MediaSummaries"))
        {
        }
        #endregion Interface/Implementation

        #region Class Properties
        [AtributePrimaryKey]
        public KnkEntityIdentifier IdSummary { get; set; }
        public KnkEntityReference<Movie> IdMovie { get; set; }
        public int Ordinal { get; set; }
        public string SummaryItem { get; set; }
        #endregion Class Properties

        public override string ToString()
        {
            return $"{SummaryItem}{Environment.NewLine}";
        }
    }
}
