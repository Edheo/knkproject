﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MovieModel.Criterias;
using System.Net;
using System.Net.Sockets;
using MovieModel.Entities;
using System.Text;
using KnkCore;
using Catharsis.Web.Widgets;

namespace Movies.Web
{
    public static class Utils
    {
        public static double RoundDecimal(decimal? aValue)
        {
            return RoundDecimal((decimal)(aValue ?? 0));
        }

        public static double RoundDecimal(decimal aValue)
        {
            int lReturn = (int)(aValue * 2);
            var lVal = Math.Round((double)lReturn, MidpointRounding.AwayFromZero);
            return lVal / 2;
        }

        public static bool IsLoggedUser()
        {
            return !string.IsNullOrEmpty(HttpContext.Current?.User?.Identity?.Name);
        }

        public static int? CurrentUserId()
        {
            var lRet = (int?)null;
            if (HttpContext.Current?.User?.Identity?.IsAuthenticated ?? false)
            {
                var lUsr = HttpContext.Current?.User?.Identity?.Name;
                if (!string.IsNullOrEmpty(lUsr))
                {
                    var lUsers = new MovieModel.Lists.Users();
                    lRet = lUsers.Items.FirstOrDefault(u => u.Email == lUsr)?.IdUser;
                }
            }
            return lRet;
        }

        public static User CurrentUser()
        {
            User lRet = null;
            if (HttpContext.Current?.User?.Identity?.IsAuthenticated ?? false)
            {
                var lUsr = HttpContext.Current?.User?.Identity?.Name;
                if (!string.IsNullOrEmpty(lUsr))
                {
                    var lUsers = new MovieModel.Lists.Users();
                    lRet = lUsers.Items.FirstOrDefault(u => u.Email == lUsr);
                }
            }
            return lRet;
        }

        public static KnkConnection Connection()
        {
            return new KnkConnection();
        }

        public static void ConnectDatabase()
        {
            MovieModel.Lists.Users lUsr = new MovieModel.Lists.Users();
            if (Utils.CurrentUserId() != null)
            {
                lUsr.Connection.Login(lUsr.Connection.GetItem<User>(Utils.CurrentUserId()));
            }
            else
            {
                lUsr.Connection.Login(lUsr.Datasource().OrderBy(u=>u.IdUser).FirstOrDefault());
            }
        }

        public static string Serialize(MoviesCriteria aCriteria)
        {
            return KnkCore.Utilities.KnkCoreUtils.XmlSerializeObject(aCriteria);
        }

        public static string GetIPAddress()
        {
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            var ipaddress = string.Empty;
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ipaddress = Convert.ToString(IP);
                    break;
                }
            }
            return ipaddress;
        }

        public static string VideoCodec(string aVideoCodec, string aAudioCodec)
        {
            string audio = $"; codecs=\"{aAudioCodec}";
            switch(aVideoCodec)
            {
                //case "h264":
                //    return VideoContentTypes.MP4 + audio;
                //case "dvvideo":
                default:
                    return VideoContentTypes.WebM;
            }
        }

        public static bool IsIntranet(string movie)
        {
            var ipremote = GetIPAddress();
            var ipserver = IpServer();
            if (ipremote == "::1") ipremote = ipserver;
            if (ipserver.Count(x => x == '.') != 3 || ipremote.Count(x => x == '.') != 3)
            {
                WriteLog($"{ipserver}-{ipremote}-Denied-{movie}");
                return false;
            }
            else
            {
                var intra = CurrentUserId() != null && ipserver.Substring(0, ipserver.LastIndexOf(".")) == ipremote.Substring(0, ipremote.LastIndexOf("."));
                WriteLog($"{ipserver}-{ipremote}-Acess-{movie}");
                return intra;
            }
        }

        public static void ELinkEnabled(string ipremote, User user, Movie movie)
        {
            WriteLog($"{IpServer()}\t{ipremote}\tLinkEnabled\t{user.Email}\t{movie.IdMovie}\t{movie}");
        }

        public static string IpServer()
        {
            return Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault().ToString();
        }

        public static string UriFile(string aPath)
        {
            return $"file:///{aPath.ToLower()}";
        }

        public static void WriteLog(string aMessage)
        {
            var message = $"{DateTime.Now}\t{aMessage}\r\n";
            string logfile = HttpContext.Current.Server.MapPath("~/mylog.txt");
            System.IO.File.AppendAllText(logfile, message);
        }

        public static IEnumerable<MovieCasting> CastingList(Movie movie, string department)
        {
            return (from a in movie.Casting().Items
                    where a.IdCastingType.Reference.Department.Equals(department)
                    orderby a.Ordinal
                    select a).Take(5);
        }

        public static string MovieColorClass(Movie movie)
        {
            var ret = "knkmovie-color-none";
            if (CurrentUserId() != null)
            {
                if (movie.UserRating != null)
                {
                    ret = "knkmovie-color-userrated";
                }
                else if (movie.ViewedTimes > 0)
                {
                    ret = "knkmovie-color-viewed";
                }
            }
            return ret;
        }

        public static string MovieBackClass(Movie movie)
        {
            var ret = "knkmovie-back-none";
            if (CurrentUserId() != null)
            {
                if (movie.Files().Items.Any())
                {
                    ret = "knkmovie-back-file";
                }
                else if (movie.Links().Items.Any())
                {
                    ret = "knkmovie-back-link";
                }
            }
            return ret;
        }
    }
}