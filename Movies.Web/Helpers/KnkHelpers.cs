﻿using KnkCore;
using MovieModel.Entities;
using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Movies.Web
{
    public static class KnkHelpers
    {
        public static MvcHtmlString MweStarRating(this HtmlHelper helper, decimal? aRating, bool aReadOnly = true)
        {
            return MweStarRating(helper, aRating ?? 0, aReadOnly);
        }

        public static MvcHtmlString MweStarRating(this HtmlHelper helper, decimal aRating, bool aReadOnly)
        {
            if (aReadOnly)
                return MvcHtmlString.Create(aRating.ToString("0.0") + " " + helper.BootstrapRating().ReadOnly.Fractions(2).Stop(10).Value(Utils.RoundDecimal(aRating)).ToHtmlString());
            else
                return MvcHtmlString.Create(aRating.ToString("0.0") + " " + helper.BootstrapRating().ReadOnly.Fractions(2).Stop(10).Value(Utils.RoundDecimal(aRating)).ToHtmlString());
        }

        private static UrlHelper Url()
        {
            return new UrlHelper(HttpContext.Current.Request.RequestContext);
        }

    }
}