﻿using System.Web.Mvc;
using KnkInterfaces.Interfaces;
using MovieModel.Entities;
using MovieModel.Lists;

namespace Movies.Web.Controllers
{
    public class PersonController : Controller
    {
        // GET: Casting
        public ActionResult Index()
        {
            Utils.ConnectDatabase();
            return View("bestcasting");
        }

        // GET: Casting
        public ActionResult Popular()
        {
            Utils.ConnectDatabase();
            return View("popularcasting");
        }

        // GET: Casting
        public ActionResult Rated()
        {
            Utils.ConnectDatabase();
            return View("ratedcasting");
        }

        // GET: Casting/Details/5
        public ActionResult Details(int id)
        {
            Utils.ConnectDatabase();
            var lPerson = Utils.Connection().GetItem<Person>(id);
            return View("person", model: lPerson);
        }

        public ActionResult Thumbnail(Person person)
        {
            return PartialView("personthumb", model: person);
        }

        public ActionResult Casting(MovieCasting casting)
        {
            return PartialView("castingthumb", model: casting);
        }

        public ActionResult WorkedWith(int personid)
        {
            var persons = new Persons(personid);
            persons.Criteria.TopElements = 12;
            persons.SortAttribute = new KnkSortAttribute("WorkedTogether");
            persons.Sorting.Add(new KnkSortAttribute("Popularity"));
            return PartialView("persons", model: persons);
        }

        // GET: Casting/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Casting/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Casting/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Casting/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Casting/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Casting/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
