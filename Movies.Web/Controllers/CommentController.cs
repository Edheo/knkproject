﻿using MovieModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movies.Web.Controllers
{
    public class CommentController : Controller
    {
        // GET: Comment
        public ActionResult MovieComments(Movie aMovie)
        {
            return View(aMovie);
        }

        [HttpPost]
        public ActionResult Addcomment(string commenttext, int? idmovie, int? idcomment)
        {
            //var type = Type.GetType($"{comment.Object},MovieModel");
            dynamic owner = null;
            if(idmovie != null)
                owner = Utils.Connection().GetItem<Movie>(idmovie);
            else
                owner = Utils.Connection().GetItem<Comment>(idcomment);

            Comment comment = owner.Comments().Create();
            comment.IdMovieOwner = idmovie;
            comment.IdCommentOwner = idcomment;
            comment.CommentText = commenttext;
            comment.CreationDate = DateTime.Now;
            comment.UserCreationId = Utils.CurrentUser().IdUser;
            comment.Update("Created Comment");
            comment.SaveChanges();

            var lObj = owner;

            while (lObj is Comment)
            {
                if (lObj.ItemOwner == null)
                    break;
                else
                    lObj = lObj.ItemOwner;
            }
            return RedirectToAction("MovieComments", "Movie", new { id = lObj.PrimaryKeyValue().Value });
        }

    }
}