﻿using System.Linq;
using System.Web.Mvc;
using MovieModel.Entities;
using MovieModel.Criterias;
using System;
using Movies.Web.Classes;

namespace Movies.Web.Controllers
{
    public class MovieController : Controller
    {
        public ActionResult Index()
        {
            return View("movies");
        }

        public ActionResult MoviesInitial()
        {
            SessionVars.SetCriteria(SessionVars.DefaultCriteria());
            return PartialView("movies");
        }

        public ActionResult Movies()
        {
            return PartialView("movies");
        }

        public ActionResult Movies(MoviesCriteria moviesCriteria)
        {
            return PartialView("movies", moviesCriteria);
        }

        public ActionResult MoviesView(MoviesCriteria moviesCriteria)
        {
            return View("movies", moviesCriteria);
        }

        public ActionResult BestMovies()
        {
            var criteria=new MoviesCriteria();
            criteria.BestMovies();
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult MyMovies()
        {
            var criteria = new MoviesCriteria();
            criteria.MyMovies(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult FavouriteMovies()
        {
            var criteria = new MoviesCriteria();
            criteria.FavouriteMovies(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult WhisedMovies()
        {
            var criteria = new MoviesCriteria();
            criteria.WhisedMovies(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult RecomendedMovies()
        {
            var criteria = new MoviesCriteria();
            criteria.Recomendations(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult PendingMovies()
        {
            var criteria = new MoviesCriteria();
            criteria.PendingMovies(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult ArtistRecommended()
        {
            var criteria = new MoviesCriteria();
            criteria.ArtistRecommended(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult NewMovies()
        {
            var criteria = new MoviesCriteria();
            criteria.NewMovies(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult LastUpdated()
        {
            var criteria = new MoviesCriteria();
            criteria.LastUpdates(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult LastViewed()
        {
            var criteria = new MoviesCriteria();
            criteria.LastViewed(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult MostViewed()
        {
            var criteria = new MoviesCriteria();
            criteria.MostViewed(Utils.CurrentUserId());
            SessionVars.SetCriteria(criteria);
            return Index();
        }

        public ActionResult Artist(string Artist)
        {
            SessionVars.SetCriteria(new MoviesCriteria(personname: Artist) { CriteriaTitle = $"{Artist} Movies" });
            return Index();
        }

        public ActionResult PersonId(int PersonId)
        {
            return Movies(new MoviesCriteria(personid: PersonId) { InfoValue = $"MovieCasting.Role:{PersonId}" });
        }

        public ActionResult Saga(string MovieSet)
        {
            return Movies(new MoviesCriteria(saga: MovieSet) { CriteriaTitle = $"{MovieSet} Movies", SortDirectionAsc = true });
        }

        public ActionResult MovieSet(string MovieSet)
        {
            return MoviesView(new MoviesCriteria(saga: MovieSet) { CriteriaTitle = $"{MovieSet} Movies" });
        }

        public ActionResult MoviesRelated(int movie)
        {
            return Movies(new MoviesCriteria(movie: movie) { SortProperty = "PredictedRating", Rows = 2, Viewed=false, AllowNavigate=false });
        }

        public ActionResult Country(string Country)
        {
            SessionVars.SetCriteria(new MoviesCriteria(country: Country) { CriteriaTitle = $"{Country} Movies" });
            return Index();
        }

        public ActionResult Company(string Company)
        {
            SessionVars.SetCriteria(new MoviesCriteria(company: Company) { CriteriaTitle = $"{Company} Movies" });
            return Index();
        }

        public ActionResult Keyword(string Keyword)
        {
            SessionVars.SetCriteria(new MoviesCriteria(keyword: Keyword) { CriteriaTitle = $"Keyword {Keyword} Movies" });
            return Index();
        }

        public ActionResult Genre(string Genre)
        {
            SessionVars.SetCriteria(new MoviesCriteria(genre: Genre) { CriteriaTitle = $"{Genre} Movies" });
            return Index();
        }

        public ActionResult Year(int Year)
        {
            SessionVars.SetCriteria(new MoviesCriteria(year: Year) { CriteriaTitle = $"{Year} Movies" });
            return Index();
        }

        public ActionResult RelatedMovies(int movieid, string title)
        {
            SessionVars.SetCriteria(new MoviesCriteria(movie: movieid) { CriteriaTitle = $"Related {title} Movies" });
            return Index();
        }

        public ActionResult WorkedTogheter(int PersonId1, int PersonId2)
        {
            var person1 = Utils.Connection().GetItem<Person>(PersonId1);
            var person2 = Utils.Connection().GetItem<Person>(PersonId2);
            SessionVars.SetCriteria(new MoviesCriteria(personid1: PersonId1, personid2: PersonId2) { CriteriaTitle = $"{person1.Name} Worked with {person2.Name}" });
            return Index();
        }

        public ActionResult MoviesPaged(int Page)
        {
            MoviesCriteria criteria = SessionVars.GetCriteria();
            criteria.Page = Page;
            SessionVars.SetCriteria(criteria);
            return Index();
        }
       
        // GET: Movie/Details/5
        public ActionResult Details(int id)
        {
            Utils.ConnectDatabase();
            var lMovie = Utils.Connection().GetItem<Movie>(id);
            return View("movie", model: lMovie);
        }

        // GET: Movie/Details/5
        public ActionResult Thumbnail(Movie movie)
        {
            return PartialView("moviethumb", model: movie);
        }

        public ActionResult DetailsLink(int id, bool enablelink)
        {
            var lMovies = new MovieModel.Lists.Movies();
            var lMovie = lMovies.Connection.GetItem<Movie>(id);
            var lUsu = Utils.CurrentUserId();
            if (lUsu != null && enablelink)
            {
                Utils.ELinkEnabled(HttpContext.Request.UserHostAddress, lMovies.Connection.GetItem<User>(lUsu), lMovie);
                if(!lMovie.Users().Items.Any(u=>u.IdUser==lUsu))
                {
                    var lUsm = lMovie.Users().Create();
                    lUsm.IdMovie = id;
                    lUsm.IdUser = lUsu;
                    lUsm.Update("Imported " + HttpContext.Request.UserHostAddress);
                    lUsm.SaveChanges();
                }
                ViewBag.EnableLink = enablelink;
            }
            else
                ViewBag.EnableLink = false;
            return View("movie", model: lMovie);
        }

        public ActionResult AddViewed(int movie_id, int? file_id)
        {
            var lMovie = Utils.Connection().GetItem<Movie>(movie_id);
            var lUsu = Utils.CurrentUserId();
            if (lUsu != null)
            {
                if (!lMovie.Views().Items.Any(v => v.DateEnd.HasValue && v.DateEnd.Value.Date == DateTime.Now.Date))
                {
                    File lFile = null;
                    if (file_id != null) lMovie.Connection().GetItem<File>(file_id);
                    var duration = lFile?.Duration ?? (lMovie?.Duration ?? new TimeSpan(2, 0, 0));
                    var lView = lMovie.Views().Create();
                    lView.IdMovie = lMovie;
                    lView.IdFile = file_id;
                    lView.DateEnd = DateTime.Now;
                    lView.DateStart = lView.DateEnd.Value;
                    lView.DateStart = lView.DateStart.Subtract(duration);
                    lView.CurrentTime = duration;
                    lView.Update("Registered Manually View");
                    lView.SaveChanges();
                }
            }
            return RedirectToAction("Details", new { id = movie_id });
        }

        public ActionResult DeleteViewed(int movieid, int viewid)
        {
            var movie = Utils.Connection().GetItem<Movie>(movieid);
            var view = movie.Views().Items.FirstOrDefault(v => v.IdView == viewid);
            if (view != null)
            {
                view.Delete("Deleted");
                view.SaveChanges();
            }
            return RedirectToAction("Details", new { id = movieid });
        }

        public ActionResult MovieCasting(int id)
        {
            var lMovies = new MovieModel.Lists.Movies();
            var lMovie = lMovies.Connection.GetItem<Movie>(id);
            return View("moviecasting", lMovie);
        }

        // GET: Movie/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Movie/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Movie/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Movie/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Movie/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Movie/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult MovieComments(int id)
        {
            var lMovie = Utils.Connection().GetItem<Movie>(id);
            return View("moviecomments", model: lMovie);
        }

    }
}
