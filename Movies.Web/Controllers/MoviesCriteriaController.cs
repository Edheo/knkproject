﻿using MovieModel.Criterias;
using Movies.Web.Classes;
using System.Web.Mvc;

namespace Movies.Web.Controllers
{
    public class MoviesCriteriaController : Controller
    {
        // GET: MoviesCriteria
       
        public ActionResult Filter(MoviesCriteria model)
        {
            return PartialView("moviescriteria", model);
        }

        public ActionResult Pager(MoviesCriteria model)
        {
            return PartialView("moviespages", model);
        }
        [HttpPost]
        public ActionResult ApplyFilter(MoviesCriteria model)
        {
            try
            {
                SessionVars.SetCriteria(model);
                model.Page = 1;
                return RedirectToAction("Index", "Movie");
            }
            catch
            {
                return View();
            }
        }

    }
}