﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Movies.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_EndRequest()
        {
            Models.MoviesWebEntities lEnt = new Models.MoviesWebEntities();
            //RouteData route = RouteTable.Routes.GetRouteData();
            var lReq = lEnt.AspNetRequests.Create();
            lReq.id = Guid.NewGuid();
            lReq.request = this.Context.Request.Url.AbsoluteUri;
            lReq.hostip = this.Context.Request.UserHostAddress;
            lReq.type = this.Context.Request.RequestType;
            lReq.logeduser = this.Context.User?.Identity?.Name;
            lReq.datetime = DateTime.Now;
            lEnt.AspNetRequests.Add(lReq);
            lEnt.SaveChanges();
        }
    }
}
