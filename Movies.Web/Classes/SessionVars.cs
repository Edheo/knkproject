﻿using MovieModel.Criterias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Movies.Web.Classes
{
    public class SessionVars
    {
        public static MoviesCriteria DefaultCriteria()
        {
            var lCrit = new MoviesCriteria();
            lCrit.DefaultFilter(Utils.CurrentUserId());
            return SetCriteria(lCrit);
        }
        public static MoviesCriteria GetCriteria()
        {
            if (HttpContext.Current.Session["Criteria"] == null)
            {
                return DefaultCriteria();
            }
            return KnkCore.Utilities.KnkCoreUtils.XmlDeserializeFromString<MoviesCriteria>(HttpContext.Current.Session["Criteria"].ToString());
        }

        public static MoviesCriteria SetCriteria(MoviesCriteria value)
        {
            if (!value.IsRelated)
            {
                value.User = Utils.CurrentUserId();
                HttpContext.Current.Session["Criteria"] = KnkCore.Utilities.KnkCoreUtils.XmlSerializeObject<MoviesCriteria>(value);
            }
            return value;

        }

    }
}