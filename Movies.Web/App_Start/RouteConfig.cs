﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Movies.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Movies/movies",
                url: "Movies",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "CastingCasting",
                url: "Casting/casting",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "Moviesalt",
                url: "Movies/movies",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "AddMovieComment",
                url: "Movie/Addcomment",
                defaults: new { controller = "Movie", action = "Addcomment" }
            );
        }
    }
}
